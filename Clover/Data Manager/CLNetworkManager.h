//
//  CLNetworkManager.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AFNetworking/AFNetworking.h>

@interface CLNetworkManager : AFHTTPRequestOperationManager

typedef void (^GETResponseWithSuccess)(NSDictionary *responseDictionary);
typedef void (^GETResponseFailed)(NSString *errorMessage);

typedef void (^POSTResponseWithSuccess)(NSDictionary *responseDictionary);
typedef void (^POSTResponseFailed)(NSString *errorMessage);

typedef void(^SuccessHandler) (NSArray *result,NSString *resultString,BOOL apiStatus);
typedef void(^ErrorHandler) (NSError *error,NSString *errorMessage,BOOL apiStatus);

+ (CLNetworkManager *)sharedManager;

- (void)startGETRequestWithAPI:(NSString *)getAPI andParameters:(id)parameters withSuccess:(GETResponseWithSuccess)success andFailure:(GETResponseFailed)failure;
- (void)startPOSTRequestWithAPI:(NSString *)postAPI andParameters:(id)parameters withSuccess:(POSTResponseWithSuccess)success andFailure:(POSTResponseFailed)failure;
-(void)uploadImage:(UIImage *)lookImage withUserID:(NSString *)userID WithSuccessHandler:(SuccessHandler)resultHandler WithErrorHandler:(ErrorHandler)errorHandler;
- (BOOL)isNetworkReachable;

@property (nonatomic, strong) NSDictionary *filterDictionary;

@end
