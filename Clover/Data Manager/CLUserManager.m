//
//  CLUserManager.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLUserManager.h"
#import "CLConstants.h"

@implementation CLUserManager

+ (CLUserManager *)sharedManager
{
    __strong static id _manager = nil;
    
    static dispatch_once_t onceToken = 0;
    
    dispatch_once(&onceToken, ^{
        
        _manager = [[self alloc] init];
        
    });
    
    
    return _manager;
}

-(void)setUser:(CLUser *)user
{
    if(_user != user && user != nil)
    {
        //Assign user
        _user = user;
        
        // set YES to isLoggedIn
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:IS_USER_LOGGED_IN];
        
        // Archive this new user to "loggedInUser"
        NSData *archivedData = [NSKeyedArchiver archivedDataWithRootObject:self.user];
        [[NSUserDefaults standardUserDefaults] setObject:archivedData forKey:@"loggedInUser"];
        
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
    
    
    
}

- (CLUser *)unArchivedUser
{
    if (_unArchivedUser == nil)
    {
        NSData *unArchiveData = [[NSUserDefaults standardUserDefaults] objectForKey:@"loggedInUser"];
        _unArchivedUser = (CLUser *) [NSKeyedUnarchiver unarchiveObjectWithData:unArchiveData];
    }
    
    return _unArchivedUser;
}
- (BOOL)hasPreviousLoggedInUser
{
    BOOL has = NO;
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"loggedInUser"])
    {
        if (self.user == nil)
        {
            self.user = self.unArchivedUser;
        }
        
        has = YES;
    }
    else
    {
        has = NO;
    }
    
    return has;
    
}

- (void)logOutUser
{
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"loggedInUser"];
    self.user = nil;
    self.unArchivedUser = nil;
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
