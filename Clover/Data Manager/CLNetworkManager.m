//
//  CLNetworkManager.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLNetworkManager.h"
#import "CLConstants.h"
#import "CLUserManager.h"
#import "NSString+DataValidator.h"

@implementation CLNetworkManager

+ (CLNetworkManager *)sharedManager
{
    __strong static id _manager = nil;
    
    static dispatch_once_t onceToken = 0;
    
    dispatch_once(&onceToken, ^{
        
        _manager = [[self alloc] initWithBaseURL:[NSURL URLWithString:Base_URL]];
        
    });
    
    return _manager;
}


#pragma mark - Netwokring Methods

- (void)startGETRequestWithAPI:(NSString *)getAPI andParameters:(id)parameters withSuccess:(GETResponseWithSuccess)success andFailure:(GETResponseFailed)failure
{
    
    NSMutableDictionary *parameterWithSource;
    parameterWithSource =[NSMutableDictionary dictionaryWithDictionary:@{@"source" : ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) ? @"ipad" : @"iphone"}];
    if (parameters != nil) {
        
        
        [parameterWithSource addEntriesFromDictionary:(NSMutableDictionary *)parameters];
    }
    [self GET:getAPI parameters:parameterWithSource success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *error = nil;
        
        // Check for response object type
        if ([responseObject isKindOfClass:[NSDictionary class]])
        {
            success (responseObject);
        }
        else if ([responseObject isKindOfClass:[NSData class]])
        {
            NSDictionary *objDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
            if (error != nil)
            {
                failure (error.localizedDescription);
            }
            else
            {
                success (objDict);
            }
        }
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (error.localizedDescription);
        
    }];
}


- (void)startPOSTRequestWithAPI:(NSString *)postAPI andParameters:(id)parameters withSuccess:(POSTResponseWithSuccess)success andFailure:(POSTResponseFailed)failure
{
    
    NSMutableDictionary *parameterWithSource;
    parameterWithSource =[NSMutableDictionary dictionaryWithDictionary:@{@"source" : ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPad) ? @"ipad" : @"iphone"}];
    if (parameters != nil) {
        
        
        [parameterWithSource addEntriesFromDictionary:(NSMutableDictionary *)parameters];
    }
    
    [self POST:postAPI parameters:parameterWithSource success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSError *error = nil;
        
        // Check for response object type
        if ([responseObject isKindOfClass:[NSDictionary class]])
        {
            success (responseObject);
        }
        else if ([responseObject isKindOfClass:[NSData class]])
        {
            NSDictionary *objDict = [NSJSONSerialization JSONObjectWithData:responseObject options:NSJSONReadingAllowFragments error:&error];
            if (error != nil)
            {
                failure (error.localizedDescription);
            }
            else
            {
                success (objDict);
            }
        }
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        
        failure (error.localizedDescription);
    }];
}

-(void)uploadImage:(UIImage *)lookImage withUserID:(NSString *)userID WithSuccessHandler:(SuccessHandler)resultHandler WithErrorHandler:(ErrorHandler)errorHandler
{
    int userId = [userID intValue];
    NSString *apiURL = [NSString stringWithFormat:@"%@cloverupload/",Base_URL];
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSData *imageData = UIImageJPEGRepresentation(lookImage, 1);
    AFHTTPRequestOperation *operation = [manager POST:apiURL parameters:@{@"userid":[NSNumber numberWithInt:userId]} constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        
        [formData appendPartWithFileData:imageData name:@"files" fileName:@"photo.jpg" mimeType:@"image/jpeg"];
        
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *responseDict = (NSDictionary *)responseObject;
           NSString *imageLink = [responseDict valueForKey:@"url"];
        
        resultHandler(nil,imageLink,YES);
        
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        errorHandler(error,@"Error in network connection. Try again",NO);
    }];
    [operation start];
}

- (BOOL)isNetworkReachable
{
    return [AFNetworkReachabilityManager sharedManager].reachable;
}


@end
