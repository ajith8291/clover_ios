//
//  CLSharedManager.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLSharedManager.h"

@implementation CLSharedManager


+ (CLSharedManager *)sharedManager
{
    __strong static id _manager = nil;
    
    static dispatch_once_t onceToken = 0;
    
    dispatch_once(&onceToken, ^{
        
        _manager = [[self alloc] init];
        
    });
    
    return _manager;
}


- (void)shareViaFBWithContent:(NSString *)content inViewController:(UIViewController *)viewController WithVideoType:(NSString *)type
{
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeFacebook])
    {
        SLComposeViewController *fbSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeFacebook];
        [fbSheet setInitialText:[NSString stringWithFormat:@"Check out this %@ from Cook with Clover",type]];
        [fbSheet addURL:[NSURL URLWithString:content]];
         
        [viewController presentViewController:fbSheet animated:YES completion:nil];
        
        // Configure the completion handler
        fbSheet.completionHandler = ^(SLComposeViewControllerResult result)
        {
            switch (result) {
                case SLComposeViewControllerResultDone:
                {
                    NSString *alertStr = [NSString stringWithFormat:@"%@ shared successfully",type];
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:alertStr delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                    [alertView show];
                }
                    break;
                case SLComposeViewControllerResultCancelled:
                    break;
                default:
                    break;
            }
            
            [viewController dismissViewControllerAnimated:YES completion:nil];
            
        };
    }
    else
    {
        UIAlertView *noAccounts = [[UIAlertView alloc] initWithTitle:nil message:@"Cannot share No Facebook Account Found" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [noAccounts show];
    }
}
- (void)shareViaTwitterWithContent:(NSString *)content inViewController:(UIViewController *)viewController WithVideoType:(NSString *)type
{
    // Check if twitter is available in settings
    if ([SLComposeViewController isAvailableForServiceType:SLServiceTypeTwitter])
    {
        SLComposeViewController *tweetSheet = [SLComposeViewController composeViewControllerForServiceType:SLServiceTypeTwitter];
        [tweetSheet setInitialText:content];
        
        [viewController presentViewController:tweetSheet animated:YES completion:nil];
        // Configure the completion handler
        tweetSheet.completionHandler = ^(SLComposeViewControllerResult result)
        {
            switch (result) {
                case SLComposeViewControllerResultDone:
                {
                    NSString *alertStr = [NSString stringWithFormat:@"%@ shared successfully",type];
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:alertStr delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
                    [alertView show];
                }

                    break;
                case SLComposeViewControllerResultCancelled:
                    break;
                default:
                    break;
            }
            
            [viewController dismissViewControllerAnimated:YES completion:nil];
            
        };
        
    }
    else
        
    {
        UIAlertView *noAccounts = [[UIAlertView alloc] initWithTitle:nil message:@"Cannot share No Twitter Account Found" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [noAccounts show];

    }
}

@end
