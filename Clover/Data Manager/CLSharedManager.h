//
//  CLSharedManager.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Social/Social.h>
#import <Accounts/Accounts.h>

@interface CLSharedManager : NSObject

+ (CLSharedManager *)sharedManager;

- (void)shareViaFBWithContent:(NSString *)content inViewController:(UIViewController *)viewController WithVideoType:(NSString *)type;
- (void)shareViaTwitterWithContent:(NSString *)content inViewController:(UIViewController *)viewController WithVideoType:(NSString *)type;
@end
