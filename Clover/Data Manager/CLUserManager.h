//
//  CLUserManager.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CLUser.h"

@interface CLUserManager : NSObject

@property (nonatomic, strong) CLUser *user;
@property (nonatomic, strong) CLUser *unArchivedUser;

+ (CLUserManager *)sharedManager;

- (BOOL)hasPreviousLoggedInUser;
- (void)logOutUser;

@end
