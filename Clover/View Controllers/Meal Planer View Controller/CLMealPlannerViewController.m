//
//  CLMealPlannerViewController.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 05/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLMealPlannerViewController.h"
#import "CLConstants.h"
#import "CLMealPlannerTableViewCell.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "CLAddToMealPlannerViewController.h"
#import "CLCalenderView.h"
#import "CLMealPlan.h"
#import "CLMealPlannerWeeklyTableViewCell.h"
#import "CLRecipeDetailsViewController.h"
#import "CLMealPlanerGridView.h"

#define BACKGROUND_COLOR [UIColor colorWithRed:208.0/255.0 green:209.0/255.0 blue:210.0/255.0 alpha:1.0]
#define BACKGROUND_COLOR_SELECTED [UIColor colorWithRed:225.0/255.0 green:225.0/255.0 blue:225.0/255.0 alpha:1.0]

@interface CLMealPlannerViewController ()<CLCustomNavigationViewDelegate,UITableViewDataSource,UITableViewDelegate,UIScrollViewDelegate,CLMealPlannerTableViewCellDelegate,CLAddToMealPlannerViewControllerDelegate,CLCalenderViewDelegate,CLMealPlannerWeeklyTableViewCellDelegate,CLRecipeDetailsViewControllerDelegate>

//[[[responseDictionary objectForKey:@"mealplan"] objectForKey:@"breakfast"] objectForKey:@"data"]

@property (nonatomic, assign) BOOL isDailyViewSelected;
@property (nonatomic, assign) BOOL isBreakFastSelected;
@property (nonatomic, assign) BOOL islunchSelected;
@property (nonatomic, assign) BOOL issnacksSelected;
@property (nonatomic, assign) BOOL isDinnerSelected;
@property (nonatomic, strong) NSMutableArray *dateArray;

@property (nonatomic, strong) CLCalenderView *calenderView;
@property (nonatomic, strong) UIView *calenderBlackView;

@property (nonatomic, assign) NSInteger page;

@property (nonatomic, strong) NSMutableArray *mealPlanArray;
@property (nonatomic, strong) NSDictionary *mealPlanDictionary;
@property (nonatomic, strong) NSMutableDictionary *mealPlanWeeklyDict;
@property (nonatomic, strong) NSString *selectedDate;
@property (nonatomic, strong) NSDate *currentDate;

@property (nonatomic, assign) BOOL isDataLoaded;

@end

@implementation CLMealPlannerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationView.navTitleTextLabel.text = [@"Meal Planner" capitalizedString];
    self.navigationView.navTitleTextLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:18.0];
    self.navigationView.navLogoImageView.hidden = YES;
    self.navigationView.navSearchButton.hidden = YES;
    self.mealPlanWeeklyDict = [NSMutableDictionary new];
    if (self.isFromSettings)
    {
        self.navigationView.navBackButton.hidden = NO;
        self.navigationView.navMenuButton.hidden= YES;
    }
    else
    {
        self.navigationView.navBackButton.hidden = YES;
        self.navigationView.navMenuButton.hidden= NO;
    }
    self.navigationView.navEditButton.hidden = YES;
    self.navigationView.navCartButton.hidden = YES;
    self.navigationView.navCalenderButton.hidden = NO;
    self.navigationView.delegate = self;
    // Do any additional setup after loading the view.
    self.isDailyViewSelected = YES;
    self.isBreakFastSelected = YES;
    self.popUpView.hidden = YES;
    self.popUpView.layer.cornerRadius = 3.0;
    self.popUpView.clipsToBounds = YES;
    self.weeklyScrollView.hidden = YES;
    self.weeklyScrollView.delegate = self;
    self.currentDate = [NSDate date];
    [self getFirstDayOfTheWeekFromDate:self.currentDate];
    [self addtapGestureAndModifyHeaderView];
    self.page = 0;
    
    self.mealPlannerTableView.showsVerticalScrollIndicator = NO;
}


-(void)addtapGestureAndModifyHeaderView
{
    UITapGestureRecognizer *swapViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(swapViewTapped)];
    [self.swapView addGestureRecognizer:swapViewTap];
    self.swapViewLabel.text = @"Day View";
    
}

-(NSMutableArray *)generateDaysArray
{
    NSMutableArray *daysArray = [NSMutableArray new];
    for (int i = 1; i<=56; i++)
    {
        NSString *dayStr;
        switch (i%7) {
            case 0:
                dayStr = @"SUN";
                break;
            case 1:
                dayStr = @"MON";
                break;
            case 2:
                dayStr = @"TUE";
                break;
            case 3:
                dayStr = @"WED";
                break;
            case 4:
                dayStr = @"THU";
                break;
            case 5:
                dayStr = @"FRI";
                break;
            case 6:
                dayStr = @"SAT";
                break;
                
            default:
                break;
        }
        [daysArray setObject:dayStr atIndexedSubscript:i-1];
    }
    return daysArray;
}

#pragma mark UIButton/Tap Action Methods

-(void)swapViewTapped
{
    self.isDataLoaded = NO;

    if (self.isDailyViewSelected)
    {
        self.swapViewLabel.text = @"Week View";
        self.weeklyScrollView.hidden = NO;
        self.dateScrollView.hidden = YES;
        [UIView transitionWithView:self.mealPlannerTableView duration:0.8 options:UIViewAnimationOptionTransitionFlipFromLeft animations:nil completion:nil];

    }
    else
    {
        self.swapViewLabel.text = @"Day View";
        self.dateScrollView.hidden = NO;
        self.weeklyScrollView.hidden = YES;
        [UIView transitionWithView:self.mealPlannerTableView duration:0.8 options:UIViewAnimationOptionTransitionFlipFromRight  animations:nil completion:nil];
    }
    self.isDailyViewSelected=!self.isDailyViewSelected;
    [self getFirstDayOfTheWeekFromDate:self.currentDate];
    [self.mealPlannerTableView reloadData];
}

- (void )getFirstDayOfTheWeekFromDate:(NSDate *)givenDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    // Edge case where beginning of week starts in the prior month
    NSDateComponents *edgeCase = [[NSDateComponents alloc] init];
    [edgeCase setMonth:2];
    [edgeCase setDay:1];
    NSDateComponents *yearComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];

    [edgeCase setYear:[yearComponents year]];
    NSDate *edgeCaseDate = [calendar dateFromComponents:edgeCase];
    
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:edgeCaseDate];
    [components setWeekday:2]; // 1 == Sunday, 7 == Saturday
    [components weekOfMonth];
    
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *now = [NSDate date];
    NSDate *startOfTheWeek;
    NSDate *endOfWeek;
    NSTimeInterval interval;
    [cal rangeOfUnit:NSWeekCalendarUnit
           startDate:&startOfTheWeek
            interval:&interval
             forDate:now];
    //startOfWeek holds now the first day of the week, according to locale (monday vs. sunday)
    
    endOfWeek = [startOfTheWeek dateByAddingTimeInterval:interval];
    NSDate *newDate1 = [startOfTheWeek dateByAddingTimeInterval:60*60*24*1];
    [self makeDateArray:newDate1];
}

-(void)makeDateArray :(NSDate *)givenDate
{
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *comps = [gregorian components:NSWeekdayCalendarUnit fromDate:[NSDate date]];
    int weekday = [comps weekday];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSDateComponents *dateComponents = [[NSDateComponents alloc] init];
    [dateComponents setDay:56-(weekday-1)];
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *newDate = [calendar dateByAddingComponents:dateComponents toDate:[NSDate date] options:0];
    
    NSDate *startDate = givenDate;//[df dateFromString:@"2012-07-20"]; // your start date
    NSDateComponents *dayDifference = [[NSDateComponents alloc] init];
    
    NSMutableArray *dates = [[NSMutableArray alloc] init];
    NSUInteger dayOffset = 1;
    NSDate *nextDate = startDate;
    do {
        [dates addObject:nextDate];
        
        [dayDifference setDay:dayOffset++];
        NSDate *d = [[NSCalendar currentCalendar] dateByAddingComponents:dayDifference toDate:startDate options:0];
        nextDate = d;
    } while([nextDate compare:newDate] == NSOrderedAscending);
    
    [df setDateStyle:NSDateFormatterFullStyle];
    self.dateArray = dates;
    [self configureDateViewScrollViewWithDateArray:dates];
    [self configureWeekLyScrollViewWith:dates];
    if (self.isDailyViewSelected)
    {
        [self loadDayViewWithDetails];
    }
    else
    {
        [self loadWeeklyViewWithDetails];
    }
    
}

-(void)loadDayViewWithDetails
{
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"];

    NSString *currentDate = [dateformate stringFromDate:self.currentDate];
    self.selectedDate = currentDate;
    NSDictionary *params = @{@"view":@"day",
                             @"user":self.userManager.user.userId,
                             @"mealdate":currentDate};
    
    [self loadDailyMealPlanDetailsWithParams:params];

}

-(void)loadWeeklyViewWithDetails
{
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"yyyy-MM-dd";
    
    NSString *startDate = [format stringFromDate:[self.dateArray objectAtIndex:self.page*7]];
    NSString *endDate = [format stringFromDate:[self.dateArray objectAtIndex:((self.page+1)*7)-1]];
    
    NSDictionary *params = @{@"user":self.userManager.user.userId,
                             @"view":@"week",
                             @"startday":startDate,
                             @"endday":endDate};
    
    [self loadWeeklyMealPlanDetailsWithParams:params];

}


-(void)configureDateViewScrollViewWithDateArray:(NSArray *)dateArray
{
   CGFloat xAxis = 0;
    int tag = 1;
    
    CGFloat width = (self.view.frame.size.width-105)/7;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"];
    int i = 0;
    for (NSDate *date in dateArray) {
        NSMutableArray *dayArray = [self generateDaysArray];
        
        [df setDateFormat:@"dd"];
        NSString *date_String=[dateformate stringFromDate:date];
        NSString *currentDate = [df stringFromDate:date];
        int date = [currentDate intValue];
        UIView *dateView = [[UIView alloc] init];
        dateView.tag = tag;
        NSString *dateStr = [NSString stringWithFormat:@"%d", date];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(2, 25, 25, 25)];
        label.font = [UIFont fontWithName:ROBOTO_REGULAR size:14.0];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = dateStr;
        label.textColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0];
        label.backgroundColor = [UIColor clearColor];

        label.layer.cornerRadius = label.frame.size.width/2;
        label.layer.masksToBounds = YES;
        
        UILabel *daylabel = [[UILabel alloc] initWithFrame:CGRectMake(2, 7, 25, 16)];
        daylabel.textAlignment = NSTextAlignmentCenter;
        daylabel.text = [dayArray objectAtIndex:i];
        daylabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:10.0];
        daylabel.textColor = [UIColor colorWithRed:180.0/255.0 green:180.0/255.0 blue:180.0/255.0 alpha:1.0];

        dateView.frame = CGRectMake(xAxis, 0, width, 58);
        xAxis = xAxis +width;
        
        NSString *date_StringToCompare=[dateformate stringFromDate:self.currentDate];
        if ([date_String isEqualToString:date_StringToCompare])
        {
            label.textColor = [UIColor blackColor];
            label.backgroundColor = [UIColor whiteColor];
        }
        
        [dateView addSubview:daylabel];
        [dateView addSubview:label];
        UITapGestureRecognizer *closeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dateViewSelectedWithDate:)];
        [dateView addGestureRecognizer:closeTap];
        tag++;
        i++;
        [self.dateScrollView addSubview:dateView];
    }

    self.dateScrollView.contentSize = CGSizeMake(xAxis, 40);
    self.dateScrollView.pagingEnabled = YES;
    
}

-(void)configureWeekLyScrollViewWith:(NSMutableArray *)dateArray
{
    CGFloat xAxis = 0;
    int tag = 1;
    
    CGFloat width = (self.view.frame.size.width-105)/7;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd"];
    
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"];
    
    int i = 0;
    for (NSDate *date in dateArray) {
        NSMutableArray *dayArray = [self generateDaysArray];
        
        [df setDateFormat:@"dd"];
        NSString *currentDate = [df stringFromDate:date];
        int date = [currentDate intValue];
        UIView *dateView = [[UIView alloc] init];
        dateView.tag = tag;
        NSString *dateStr = [NSString stringWithFormat:@"%d", date];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(2, 25, 25, 25)];
        label.font = [UIFont fontWithName:ROBOTO_REGULAR size:14.0];
        label.textAlignment = NSTextAlignmentCenter;
        label.text = dateStr;
        label.textColor = [UIColor colorWithRed:238.0/255.0 green:238.0/255.0 blue:238.0/255.0 alpha:1.0];
        label.backgroundColor = [UIColor clearColor];
        label.layer.cornerRadius = label.frame.size.width/2;
        label.layer.masksToBounds = YES;
        
        UILabel *daylabel = [[UILabel alloc] initWithFrame:CGRectMake(2, 7, 25, 16)];
        daylabel.textAlignment = NSTextAlignmentCenter;
        daylabel.text = [dayArray objectAtIndex:i];
        daylabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:10.0];
        daylabel.textColor = [UIColor colorWithRed:180.0/255.0 green:180.0/255.0 blue:180.0/255.0 alpha:1.0];
        
        dateView.frame = CGRectMake(xAxis, 0, width, 58);
        xAxis = xAxis +width;
        
        [dateView addSubview:daylabel];
        [dateView addSubview:label];
        tag++;
        i++;
        [self.weeklyScrollView addSubview:dateView];
    }
    
    self.weeklyScrollView.contentSize = CGSizeMake(xAxis, 40);
    self.weeklyScrollView.pagingEnabled = YES;
    
}




-(void)dateViewSelectedWithDate :(UITapGestureRecognizer *)tapView
{
    self.isDataLoaded = NO;
    [self.mealPlannerTableView reloadData];
    UIView *tagView = (UIView *)[self.dateScrollView viewWithTag:tapView.view.tag];
    
    NSDateFormatter *format = [[NSDateFormatter alloc] init];
    format.dateFormat = @"yyyy-MM-dd";
    self.currentDate = [self.dateArray objectAtIndex:tapView.view.tag-1];
   NSString *selectedDate = [format stringFromDate:[self.dateArray objectAtIndex:tapView.view.tag-1]];
    NSDictionary *params = @{@"view":@"day",
                             @"user":self.userManager.user.userId,
                             @"mealdate":selectedDate};
    
    [self loadDailyMealPlanDetailsWithParams:params];
    self.selectedDate = selectedDate;

    for (UIView *view in self.dateScrollView.subviews)
    {
        if (view == tagView)
        {
            for (UILabel *label in view.subviews)
            {
                if (label.text.length!=3)
                {
                    label.textColor = [UIColor blackColor];
                    label.backgroundColor = [UIColor whiteColor];
                }
                else
                {
                    label.backgroundColor= [UIColor clearColor];
                    label.textColor = [UIColor colorWithRed:180.0/255.0 green:180.0/255.0 blue:180.0/255.0 alpha:1.0];
                }
            }
        }
        else
        {
            for (UILabel *label in view.subviews)
            {
                if (label.text.length!=3)
                {
                    label.backgroundColor= [UIColor clearColor];
                    label.textColor = [UIColor colorWithRed:180.0/255.0 green:180.0/255.0 blue:180.0/255.0 alpha:1.0];
                }
                else
                {
                    
                }
            }
        }
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark CLCustomNavigationViewDelegateMethods

-(void)calenderButtonTapped
{
    
    [self addCalenderBlackView];
    self.calenderView = [[[NSBundle mainBundle] loadNibNamed:@"CLCalenderView" owner:self options:nil] objectAtIndex:0];
    self.calenderView.center = self.view.center;
    self.calenderView.delegate = self;
    [self.calenderView configureSelectedDate:self.currentDate];
    [self ViewZoomInEffect:self.calenderView];
    [self.view addSubview:self.calenderView];

}

-(void)addCalenderBlackView
{
    if (!self.calenderBlackView)
    {
        self.calenderBlackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.calenderBlackView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        UITapGestureRecognizer *blackViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(calenderBlackViewTapped)];
        [self.calenderBlackView addGestureRecognizer:blackViewTap];
        [self.view addSubview:self.calenderBlackView];
    }
    else
    {
        self.calenderBlackView.hidden = NO;
    }
}

-(void)removeCalenderBlackView
{
    [self.calenderBlackView removeFromSuperview];
    self.calenderBlackView = nil;
}

#pragma mark UITableViewDelegate/UITableViewDatasourse Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 130;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isDataLoaded) {
        if (self.isDailyViewSelected) {
            return 4;
        }
        return 7;
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isDailyViewSelected)
    {
        CLMealPlannerTableViewCell *cell = (CLMealPlannerTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CLMealPlannerTableViewCell"];
        cell.frame = CGRectMake(0, 0, self.view.frame.size.width, 120);
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        cell.delegate = self;

        cell.isDailyViewSelected = YES;
        NSArray *mealTypeArray;
        switch (indexPath.row)
        {
            case 0:
                [cell.mealPlannerCategoryImageView setImage:[UIImage imageNamed:@"Breakfast"]];
                mealTypeArray = [[self.mealPlanDictionary objectForKey:@"breakfast"] objectForKey:@"data"];
                break;
            case 1:
                [cell.mealPlannerCategoryImageView setImage:[UIImage imageNamed:@"Snacks"]];
                mealTypeArray = [[self.mealPlanDictionary objectForKey:@"snacks"] objectForKey:@"data"];
                break;
            case 2:
                [cell.mealPlannerCategoryImageView setImage:[UIImage imageNamed:@"LunchNew"]];
                mealTypeArray = [[self.mealPlanDictionary objectForKey:@"lunch"] objectForKey:@"data"];
                break;
            case 3:
                [cell.mealPlannerCategoryImageView setImage:[UIImage imageNamed:@"DinnerNew"]];
                mealTypeArray = [[self.mealPlanDictionary objectForKey:@"dinner"] objectForKey:@"data"];

                break;
                
            default:
                break;
        }
        
        cell.mealPlannerGridScrollView.hidden = NO;
        [cell configureMealPlannerBreakFastWithdataWithRecipeArray:mealTypeArray andIndex:indexPath.row withWidth:self.view.frame.size.width];
        return cell;
    }
    else
    {
        CLMealPlannerWeeklyTableViewCell *cell = (CLMealPlannerWeeklyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CLMealPlannerWeeklyTableViewCell"];
        cell.frame = CGRectMake(0, 0, self.view.frame.size.width, 120);
        cell.selectionStyle = UITableViewCellSeparatorStyleNone;
        cell.delegate = self;

        cell.isDailyViewSelected = NO;
//        NSArray *mealTypeArray;
        if (self.isBreakFastSelected)
        {
            cell.mealType = @"Breakfast";
            [cell configureMealPlannerWeeklyWithdataWithDateArray:self.dateArray andPage:self.page andIndexPath:indexPath.row andRecipeArray:[self.mealPlanWeeklyDict objectForKey:@"breakfast"] withWidth:self.view.frame.size.width];
        }
        else if (self.islunchSelected)
        {
            cell.mealType = @"Lunch";
            [cell configureMealPlannerWeeklyWithdataWithDateArray:self.dateArray andPage:self.page andIndexPath:indexPath.row andRecipeArray:[self.mealPlanWeeklyDict objectForKey:@"lunch"] withWidth:self.view.frame.size.width];
        }
        else if (self.issnacksSelected)
        {
            cell.mealType = @"Snacks";
            [cell configureMealPlannerWeeklyWithdataWithDateArray:self.dateArray andPage:self.page andIndexPath:indexPath.row andRecipeArray:[self.mealPlanWeeklyDict objectForKey:@"snacks"]withWidth:self.view.frame.size.width];
        }
        else if (self.isDinnerSelected)
        {
            cell.mealType = @"Dinner";
            [cell configureMealPlannerWeeklyWithdataWithDateArray:self.dateArray andPage:self.page andIndexPath:indexPath.row andRecipeArray:[self.mealPlanWeeklyDict objectForKey:@"dinner"] withWidth:self.view.frame.size.width];
        }
        return cell;
    }
    return nil;
}


-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.isDailyViewSelected)
    {
        return 1;
    }
    return 35;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.isDailyViewSelected)
    {
        return nil;
    }
    CGFloat width = self.view.frame.size.width/4;
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 35)];
    UIView *breakfastView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 35)];
    UIView *breakfastInnerView;
    if (self.isBreakFastSelected)
    {
        breakfastInnerView = [self innerViewWithName:@"Breakfast" andImage:[UIImage imageNamed:@"BreakfastSelected"] AndSelected:YES];
        breakfastView.backgroundColor = BACKGROUND_COLOR_SELECTED;
    }
    else
    {
        breakfastInnerView = [self innerViewWithName:@"Breakfast" andImage:[UIImage imageNamed:@"Breakfast"] AndSelected:NO];
        breakfastView.backgroundColor = BACKGROUND_COLOR;
    }
    [breakfastView addSubview:breakfastInnerView];
    
    UIView *lunchView = [[UIView alloc] initWithFrame:CGRectMake(width, 0, width, 35)];
    UIView *lunchInnerView;
    if (self.islunchSelected)
    {
        lunchInnerView = [self innerViewWithName:@"Lunch" andImage:[UIImage imageNamed:@"LunchSelected"] AndSelected:YES];
        lunchView.backgroundColor = BACKGROUND_COLOR_SELECTED;
    }
    else
    {
        lunchInnerView = [self innerViewWithName:@"Lunch" andImage:[UIImage imageNamed:@"LunchNew"] AndSelected:NO];
        lunchView.backgroundColor = BACKGROUND_COLOR;
    }
    [lunchView addSubview:lunchInnerView];

    
    UIView *snacksView = [[UIView alloc] initWithFrame:CGRectMake(width*2, 0, width, 35)];
    UIView *snacksInnerView;
    if (self.issnacksSelected)
    {
        snacksInnerView = [self innerViewWithName:@"Snacks" andImage:[UIImage imageNamed:@"SnacksSelected"] AndSelected:YES];
        snacksView.backgroundColor = BACKGROUND_COLOR_SELECTED;
    }
    else
    {
        snacksInnerView = [self innerViewWithName:@"Snacks" andImage:[UIImage imageNamed:@"Snacks"] AndSelected:NO];
        snacksView.backgroundColor = BACKGROUND_COLOR;
    }
    [snacksView addSubview:snacksInnerView];


    UIView *dinnerView = [[UIView alloc] initWithFrame:CGRectMake(width*3, 0, width, 35)];
    UIView *dinnerInnerView;
    if (self.isDinnerSelected)
    {
        dinnerInnerView = [self innerViewWithName:@"Dinner" andImage:[UIImage imageNamed:@"DinnerSelected"] AndSelected:YES];
        dinnerView.backgroundColor = BACKGROUND_COLOR_SELECTED;
    }
    else
    {
        dinnerInnerView = [self innerViewWithName:@"Dinner" andImage:[UIImage imageNamed:@"DinnerNew"] AndSelected:NO];
        dinnerView.backgroundColor = BACKGROUND_COLOR;
    }
    [dinnerView addSubview:dinnerInnerView];

    [breakfastView addSubview:[self returnSeperator]];
    [lunchView addSubview:[self returnSeperator]];
    [snacksView addSubview:[self returnSeperator]];
    [dinnerView addSubview:[self returnSeperator]];

    UITapGestureRecognizer *breakfastTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(breakfastViewTapped)];
    [breakfastView addGestureRecognizer:breakfastTap];
    
    UITapGestureRecognizer *lunchTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(lunchViewTapped)];
    [lunchView addGestureRecognizer:lunchTap];

    UITapGestureRecognizer *snacksTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(snacksViewTapped)];
    [snacksView addGestureRecognizer:snacksTap];

    UITapGestureRecognizer *dinnerTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dinnerViewTapped)];
    [dinnerView addGestureRecognizer:dinnerTap];


    [headerView addSubview:breakfastView];
    [headerView addSubview:lunchView];
    [headerView addSubview:snacksView];
    [headerView addSubview:dinnerView];
    return headerView;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if (self.isDailyViewSelected)
//    {
//        CLMealPlannerTableViewCell *cell = (CLMealPlannerTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CLMealPlannerTableViewCell" forIndexPath:indexPath];
//        for (UIView *view in cell.mealPlannerGridScrollView.subviews) {
//            if ([view isKindOfClass:[CLMealPlanerGridView class]])
//            {
//                [view.layer removeAnimationForKey:@"iconShake"];
//            }
//        }
//    }
//    else
//    {
//        CLMealPlannerWeeklyTableViewCell *cell = (CLMealPlannerWeeklyTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CLMealPlannerWeeklyTableViewCell"forIndexPath:indexPath];
//        for (UIView *view in cell.mealPlannerGridScrollView.subviews) {
//            if ([view isKindOfClass:[CLMealPlanerGridView class]])
//            {
//                [view.layer removeAnimationForKey:@"iconShake"];
//            }
//        }
//
//    }
    [self.mealPlannerTableView reloadData];

}

-(UIView *)innerViewWithName:(NSString *)name andImage:(UIImage *)image AndSelected:(BOOL)flag
{
    CGFloat width = self.view.frame.size.width/4;
    UIView *testView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, 35)];
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:13]};
    NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:name attributes:attributes];
    CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(280, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    CGSize size = rect.size;
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, size.width, 35)];
    label.font = [UIFont fontWithName:ROBOTO_REGULAR size:13.0];
    label.text = name;
    if (flag == YES) {
        label.textColor = CL_BLUE_COLOR;
    }
    else
    {
        label.textColor = [UIColor colorWithRed:143.0/255.0 green:143.0/255.0 blue:143.0/255.0 alpha:1.0];
    }
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 11, 12, 12)];
    [imageView setImage:image];
    
    UIView *innerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, size.width+15, 35)];
    [innerView addSubview:label];
    [innerView addSubview:imageView];
    
    innerView.center = testView.center;
    [testView addSubview:innerView];
    return testView;
}

-(UIView *)returnSeperator
{
    CGFloat width = self.view.frame.size.width/4;
    UIView *seperator = [[UIView alloc] initWithFrame:CGRectMake(width-1, 0, 1, 35)];
    seperator.backgroundColor = [UIColor whiteColor];
    seperator.alpha = 0.3;
    return seperator;
}

#pragma mark UIButton/UITap Action Methods

-(void)breakfastViewTapped
{
    self.isBreakFastSelected = YES;
    self.islunchSelected = NO;
    self.issnacksSelected = NO;
    self.isDinnerSelected = NO;
    [self.mealPlannerTableView reloadData];
}

-(void)lunchViewTapped
{
    self.islunchSelected = YES;
    self.isBreakFastSelected = NO;
    self.issnacksSelected = NO;
    self.isDinnerSelected = NO;

    [self.mealPlannerTableView reloadData];
}

-(void)snacksViewTapped
{
    self.issnacksSelected = YES;
    self.isBreakFastSelected = NO;
    self.islunchSelected = NO;
    self.isDinnerSelected = NO;
    [self.mealPlannerTableView reloadData];
}

-(void)dinnerViewTapped
{
    self.isDinnerSelected = YES;
    self.isBreakFastSelected = NO;
    self.islunchSelected = NO;
    self.issnacksSelected = NO;
    [self.mealPlannerTableView reloadData];
}


-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    int page;

    if (scrollView == self.weeklyScrollView )
    {
        self.isDataLoaded = NO;
        [self.mealPlannerTableView reloadData];
        page = self.weeklyScrollView.contentOffset.x/(self.view.frame.size.width-105);
        NSDateFormatter *format = [[NSDateFormatter alloc] init];
        format.dateFormat = @"yyyy-MM-dd";
        
        NSString *startDate = [format stringFromDate:[self.dateArray objectAtIndex:page*7]];
        NSString *endDate = [format stringFromDate:[self.dateArray objectAtIndex:((page+1)*7)-1]];
        
        NSDictionary *params = @{@"user":self.userManager.user.userId,
                                 @"view":@"week",
                                 @"startday":startDate,
                                 @"endday":endDate};
        
        [self loadWeeklyMealPlanDetailsWithParams:params];
        //http://cloverapi.jumpcatch.com/api/getplans/?user=99&view=day&mealdate=2015-01-30
        
        self.page = page;
    }
    
}

-(void)loadDailyMealPlanDetailsWithParams:(NSDictionary *)params
{
    AFHTTPRequestOperationManager *manager =[[AFHTTPRequestOperationManager alloc] init];
    [manager.operationQueue cancelAllOperations];
    
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        
        [self.networkManager startGETRequestWithAPI:@"getplans/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             
             if ([errorCode isEqualToString:@"200"])
             {
                 self.mealPlanDictionary = [responseDictionary objectForKey:@"mealplan"];
             }
             else
             {
                 self.mealPlanDictionary = nil;
//                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                 [errorAlert show];
             }
             self.isDataLoaded = YES;
             [self.mealPlannerTableView reloadData];
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }
}

-(void)loadWeeklyMealPlanDetailsWithParams:(NSDictionary *)params
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        AFHTTPRequestOperationManager *manager =[[AFHTTPRequestOperationManager alloc] initWithBaseURL:[NSURL URLWithString:Base_URL]];
        [manager.operationQueue cancelAllOperations];
//
//        [manager GET:@"getplans/" parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
//            [operation cancel];
//            [hud hide:YES];
//        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
//            [operation cancel];
//            [hud hide:YES];
//        }];
        
        
        [self.networkManager startGETRequestWithAPI:@"getplans/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             
             if ([errorCode isEqualToString:@"200"])
             {
                 NSArray *array = [responseDictionary objectForKey:@"week_mealplan"];
                 [self makeWeeklyViewDictionaryWithProperTypeWith:array];
             }
             else
             {
//                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                 [errorAlert show];
             }
             
             self.isDataLoaded = YES;
             [self.mealPlannerTableView reloadData];

         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }

}

-(void)makeWeeklyViewDictionaryWithProperTypeWith:(NSArray *)array
{
    NSArray *breakfastArray = [NSArray new];
    NSArray *lunchArray = [NSArray new];
    NSArray *snacksArray = [NSArray new];
    NSArray *dinnerArray = [NSArray new];
    for (int i = 0; i<array.count; i++)
    {
        NSDictionary *dict = [array objectAtIndex:i];
        if ([[dict objectForKey:@"mealtype"] isEqualToString:@"breakfast"])
        {
            breakfastArray = [dict objectForKey:@"plan"];
        }
        else if ([[dict objectForKey:@"mealtype"] isEqualToString:@"lunch"])
        {
            lunchArray = [dict objectForKey:@"plan"];
        }
        else if ([[dict objectForKey:@"mealtype"] isEqualToString:@"snacks"])
        {
            snacksArray = [dict objectForKey:@"plan"];
        }
        else if ([[dict objectForKey:@"mealtype"] isEqualToString:@"dinner"])
        {
            dinnerArray = [dict objectForKey:@"plan"];
        }
    }
    
    [self.mealPlanWeeklyDict setObject:breakfastArray forKey:@"breakfast"];
    [self.mealPlanWeeklyDict setObject:lunchArray forKey:@"lunch"];
    [self.mealPlanWeeklyDict setObject:snacksArray forKey:@"snacks"];
    [self.mealPlanWeeklyDict setObject:dinnerArray forKey:@"dinner"];
    

}

-(void)addButtonTappedWithMealType:(int)index
{
    NSString *mealType;
        switch (index) {
            case 0:
                mealType = @"Breakfast";
                break;
            case 1:
                mealType = @"Snacks";

                break;
            case 2:
                mealType = @"Lunch";

                break;
            case 3:
                mealType = @"Dinner";
                break;
                
            default:
                break;
    }
    CLAddToMealPlannerViewController *addToMealVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLAddToMealPlannerViewController"];
    addToMealVC.mealType = mealType;
    addToMealVC.selectedDate = self.selectedDate;
    addToMealVC.delegate = self;
    [self.navigationController pushViewController:addToMealVC animated:YES];
    [self.mealPlannerTableView reloadData];

}

-(void)addButtonTappedWeeklyViewWithDate:(NSString *)dateStr andMealType:(NSString *)mealType
{
    CLAddToMealPlannerViewController *addToMealVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLAddToMealPlannerViewController"];
    addToMealVC.mealType = mealType;
    addToMealVC.selectedDate = dateStr;
    addToMealVC.delegate = self;
    [self.navigationController pushViewController:addToMealVC animated:YES];
    [self.mealPlannerTableView reloadData];
}

-(void)scrollViewSelcetedStopAnimating
{
    [self.mealPlannerTableView reloadData];
}

-(void)removeRecipeFromMealPlannerWith:(NSString *)mealPlanId
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        NSDictionary *params = @{@"plan":mealPlanId};
        
        [self.networkManager startGETRequestWithAPI:@"deleteplan/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             NSString *errorStr = [responseDictionary objectForKey:@"msg"];
             
             if ([errorCode isEqualToString:@"200"])
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
                 if (self.isDailyViewSelected)
                 {
                     [self loadDayViewWithDetails];
                 }
                 else
                 {
                     [self loadWeeklyViewWithDetails];
                 }
                 
             }
             else
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
             }
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }

}

-(void)loadMealPlannerWithUpdate
{
    if (self.isDailyViewSelected)
    {
        NSDictionary *params = @{@"view":@"day",
                                 @"user":self.userManager.user.userId,
                                 @"mealdate":self.selectedDate};
        
        [self loadDailyMealPlanDetailsWithParams:params];

    }
    else
    {
        [self loadWeeklyViewWithDetails];
    }
}

-(void)doneButtonTappedWithSelectedDate:(NSString *)dateStr andDate:(NSDate *)date
{
    NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
    [dateformate setDateFormat:@"yyyy-MM-dd"];
    
    self.currentDate = date;
    NSString *currentDate = [dateformate stringFromDate:date];
    if (![self.selectedDate isEqualToString:currentDate])
    {
        [self dateViewSelectedWithSelectedDate:currentDate andDate:date];
    }
    
    [self ViewZoomOutEffect:self.calenderView];
    [self removeCalenderBlackView];
}


-(void)dateViewSelectedWithSelectedDate:(NSString *)dateStr andDate:(NSDate *)date
{
    
    UIView *tagView;
    self.selectedDate = dateStr;
    for (int i = 0; i<self.dateArray.count; i++)
    {
        NSDate *arraydate = [self.dateArray objectAtIndex:i];
        NSDateFormatter *dateformate=[[NSDateFormatter alloc]init];
        [dateformate setDateFormat:@"yyyy-MM-dd"];
        NSString *currentDate = [dateformate stringFromDate:arraydate];
        
        if ([currentDate isEqualToString:dateStr])
        {
            NSDictionary *params = @{@"view":@"day",
                                     @"user":self.userManager.user.userId,
                                     @"mealdate":dateStr};
            
            [self loadDailyMealPlanDetailsWithParams:params];
            tagView =  (UIView *)[self.dateScrollView viewWithTag:i+1];
            int x = (i/7)*self.dateScrollView.frame.size.width;
            self.dateScrollView.contentOffset = CGPointMake(x,0);
        }
        
    }
    for (UIView *view in self.dateScrollView.subviews)
    {
        if (view == tagView)
        {
            for (UILabel *label in view.subviews)
            {
                if (label.text.length!=3)
                {
                    label.textColor = [UIColor blackColor];
                    label.backgroundColor = [UIColor whiteColor];
                }
                else
                {
                    label.backgroundColor= [UIColor clearColor];
                    label.textColor = [UIColor colorWithRed:180.0/255.0 green:180.0/255.0 blue:180.0/255.0 alpha:1.0];
                }
            }
        }
        else
        {
            for (UILabel *label in view.subviews)
            {
                if (label.text.length!=3)
                {
                    label.backgroundColor= [UIColor clearColor];
                    label.textColor = [UIColor colorWithRed:180.0/255.0 green:180.0/255.0 blue:180.0/255.0 alpha:1.0];
                }
                else
                {
                    
                }
            }
        }
    }
    
    self.isDailyViewSelected = YES;
    self.swapViewLabel.text = @"Day View";
    self.dateScrollView.hidden = NO;
    self.weeklyScrollView.hidden = YES;
    [self.mealPlannerTableView reloadData];

}

-(void)calenderBlackViewTapped
{
    [self.calenderBlackView removeFromSuperview];
    self.calenderBlackView = nil;
    [self ViewZoomOutEffect:self.calenderView];
}

#pragma mark CLMealPlanGridViewDelegate Methods

-(void)gridViewTappedWithRecipe:(NSString *)recipeId
{
    CLRecipeDetailsViewController *recipedetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeDetailsViewController"];
    recipedetailVC.recipeID = recipeId;
    recipedetailVC.isfromFavourites = YES;
    recipedetailVC.delegate = self;
    [self.navigationController pushViewController:recipedetailVC animated:YES];

}

#pragma mark CLRecipeDetailsViewControllerDelegate Methods

-(void)loadMealPlannerView
{
    if (self.isDailyViewSelected)
    {
        [self loadDayViewWithDetails];
    }
    else
    {
        [self loadWeeklyViewWithDetails];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
