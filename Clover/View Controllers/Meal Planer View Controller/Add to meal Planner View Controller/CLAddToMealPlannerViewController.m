//
//  CLAddToMealPlannerViewController.m
//  Clover
//
//  Created by Ajith Kumar on 28/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLAddToMealPlannerViewController.h"
#import "CLRecipeCollectionViewLGridCell.h"
#import "CLRecipeDetailsCollectionViewCell.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "NSString+DataValidator.h"
#import "CLConstants.h"
#import "CLRecipeDetailsViewController.h"

@interface CLAddToMealPlannerViewController ()<CLRecipeCollectionViewLGridCellDelegate,UITextFieldDelegate,CLRecipeDetailsViewControllerDelegate>

@property (nonatomic, assign) BOOL isSearchSelected;
@property (nonatomic, strong) NSArray *recipeFilteredArray;
@property (nonatomic, strong) NSMutableArray *recipeArray;

@property (nonatomic, strong) NSMutableArray *addedRecipeArray;

@end

@implementation CLAddToMealPlannerViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationView.navLogoImageView.hidden = YES;
    self.navigationView.navMenuButton.hidden = YES;
    self.navigationView.navEditButton.hidden = YES;
    self.navigationView.navSearchButton.hidden = YES;
    self.navigationView.navCartButton.hidden = YES;
    self.navigationView.navTitleTextLabel.hidden = NO;
    self.navigationView.navTitleTextLabel.text = [@"Add to meal plan" capitalizedString];

    // Do any additional setup after loading the view.
    self.searchView.layer.cornerRadius = 13.0;
    self.searchView.clipsToBounds = YES;
    self.addButton.layer.cornerRadius = 10.0;
    self.addButton.clipsToBounds = YES;
    self.recipeArray = [NSMutableArray new];
    self.searchTextField.delegate = self;
    self.searchTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.addedRecipeArray = [NSMutableArray new];
    self.addButton.hidden = YES;
    [self callWebServiceToGetDetails];
    
    self.addToMealCollectionView.showsVerticalScrollIndicator = NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)callWebServiceToGetDetails
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        NSDictionary *params = @{@"mealtime":self.mealType};
        
        [self.networkManager startGETRequestWithAPI:@"cloverlistrecipe/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             NSString *errorStr = [responseDictionary objectForKey:@"msg"];
             
             if ([errorCode isEqualToString:@"200"])
             {
                 NSArray *recipeDictArray =[responseDictionary objectForKey:@"receipes"];
                 for (int i = 0;i<recipeDictArray.count ; i++)
                 {
                     CLRecipe *recipe = [CLRecipe getInformationFromDictionary:[recipeDictArray objectAtIndex:i]];
                     [self.recipeArray addObject:recipe];
                 }
                 self.addToMealCollectionView.hidden = NO;
                 [self.addToMealCollectionView reloadData];
             }
             else
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
             }

             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }

}

#pragma mark UITableView Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return (self.isSearchSelected) ? self.recipeFilteredArray.count : self.recipeArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CLRecipe *recipe = (self.isSearchSelected) ?[self.recipeFilteredArray objectAtIndex:indexPath.row] : [self.recipeArray objectAtIndex:indexPath.row];
        CLRecipeCollectionViewLGridCell *cell = (CLRecipeCollectionViewLGridCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CLRecipeCollectionViewLGridCell" forIndexPath:indexPath];
    cell.delegate = self;
    
    if ([self.addedRecipeArray containsObject:recipe.recipeId])
    {
        cell.plusButton.hidden = NO;
        cell.layer.borderWidth = 2.0;
        cell.layer.borderColor = [[UIColor redColor] CGColor];
        
        [cell configureGridCellWithRecipeDetails:recipe WithBool:NO];
    }
    else
    {
        cell.plusButton.hidden = YES;
        cell.layer.borderColor = [[UIColor clearColor] CGColor];

        [cell configureGridCellWithRecipeDetails:recipe WithBool:YES];
    }
    
    if (self.addedRecipeArray.count)
    {
        self.addButton.hidden = NO;
    }
    else
    {
        self.addButton.hidden = YES;
    }
    cell.layer.shouldRasterize = YES;
    cell.layer.rasterizationScale = [UIScreen mainScreen].scale;

    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake((self.view.frame.size.width-30)/2, 125);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
//    if (IS_IOS_8_OR_LATER)
//    {
        return UIEdgeInsetsMake(10, 10, 10 ,10);
//    }
//    return UIEdgeInsetsMake(10, 18, 18 ,18);
    
}


-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 8;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CLRecipe *recipe;
    if (self.isSearchSelected)
    {
        recipe = [self.recipeFilteredArray objectAtIndex:indexPath.row];
        
    }
    else
    {
        recipe = [self.recipeArray objectAtIndex:indexPath.row];
    }
    
    CLRecipeDetailsViewController *recipeDetailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeDetailsViewController"];
    recipeDetailsVC.isFromAddToMealPLanner = YES;
    recipeDetailsVC.selectedRecipe = recipe;
    recipeDetailsVC.delegate = self;
    recipeDetailsVC.addToMealPlannerArray = self.addedRecipeArray;
    
    [self.navigationController pushViewController:recipeDetailsVC animated:YES];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([textField.text isEqualToString:@""]) {
        self.isSearchSelected = NO;
    }
    else
    {
        self.isSearchSelected = YES;
    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    self.navigationView.navSearchTextFiled.hidden = YES;
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    [textField resignFirstResponder];
    self.navigationView.navSearchTextFiled.hidden = YES;
    [self.addToMealCollectionView reloadData];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
    if ([resultingString isEmptyString])
    {
        self.isSearchSelected = NO;
        
    }
    else
    {
        self.isSearchSelected = YES;
    }
    
    [self loadSearchResultRecipe:resultingString];
    return YES;
}

-(void)loadSearchResultRecipe:(NSString *)searchText
{//LIKE[cd]
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"CAST (recipeTitle,'NSString') contains[cd] %@",searchText];
    self.recipeFilteredArray = [self.recipeArray filteredArrayUsingPredicate:predicate];
    [self.addToMealCollectionView reloadData];
}


- (IBAction)addButtonAction:(id)sender
{
    NSString *recipeId = [NSString stringWithFormat:@"%@",[self.addedRecipeArray componentsJoinedByString:@","]];
    NSDictionary *params = @{@"recipe":recipeId,
                             @"user":self.userManager.user.userId,
                             @"mealtype":self.mealType,
                             @"mealdate":self.selectedDate};
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading...";
    [hud show:YES];
    [self.networkManager startGETRequestWithAPI:@"addtomeal/" andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
        [hud hide:YES];
        NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
        NSString *msg =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"msg"]];
        if ([errorCode isEqualToString:@"200"])
        {
            [self.delegate loadMealPlannerWithUpdate];
            [self.navigationController popViewControllerAnimated:YES];
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
    } andFailure:^(NSString *errorMessage) {
        [hud hide:YES];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }];

}

#pragma mark - CLRecipeCollectionViewLGridCellDelegate Methods

-(void)gridViewSelectedRecipe:(CLRecipe *)selectedrecipe
{
    if ([self.addedRecipeArray containsObject:selectedrecipe.recipeId])
    {
        [self.addedRecipeArray removeObject:selectedrecipe.recipeId];
    }
    else
    {
        [self.addedRecipeArray addObject:selectedrecipe.recipeId];
    }
    
    [self.addToMealCollectionView reloadData];
}

-(void)removeFromMealPlannerWithRecipe:(CLRecipe *)recipe
{
    [self addAndRemoveMealPlannerButtonTappedWithRecipe:recipe];
}

#pragma mark - CLRecipeDetailsViewControllerDelegate Methods

-(void)addAndRemoveMealPlannerButtonTappedWithRecipe:(CLRecipe *)recipe
{
    if ([self.addedRecipeArray containsObject:recipe.recipeId])
    {
        [self.addedRecipeArray removeObject:recipe.recipeId];
    }
    else
    {
        [self.addedRecipeArray addObject:recipe.recipeId];
    }

    [self.addToMealCollectionView reloadData];
}

@end
