//
//  CLAddToMealPlannerViewController.h
//  Clover
//
//  Created by Ajith Kumar on 28/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLBaseViewController.h"

@protocol  CLAddToMealPlannerViewControllerDelegate<NSObject>

@optional

-(void)loadMealPlannerWithUpdate;

@end
@interface CLAddToMealPlannerViewController : CLBaseViewController<UICollectionViewDataSource,UICollectionViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UICollectionView *addToMealCollectionView;

@property (nonatomic, strong) NSString *mealType;
@property (nonatomic, strong) NSString *selectedDate;

@property (nonatomic, weak) id<CLAddToMealPlannerViewControllerDelegate>delegate;
@end
