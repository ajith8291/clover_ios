//
//  CLMealPlannerViewController.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 05/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLBaseViewController.h"

@interface CLMealPlannerViewController : CLBaseViewController
@property (weak, nonatomic) IBOutlet UIView *swapView;
@property (weak, nonatomic) IBOutlet UILabel *swapViewLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *dateScrollView;
@property (weak, nonatomic) IBOutlet UIScrollView *weeklyScrollView;
@property (weak, nonatomic) IBOutlet UITableView *mealPlannerTableView;
@property (weak, nonatomic) IBOutlet UIView *popUpView;

@property (nonatomic, assign) BOOL isFromSettings;
@end
