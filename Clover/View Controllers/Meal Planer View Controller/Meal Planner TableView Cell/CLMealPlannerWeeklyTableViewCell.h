//
//  CLMealPlannerWeeklyTableViewCell.h
//  Clover
//
//  Created by Ajith Kumar on 31/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLMealPlanerGridView.h"

@protocol CLMealPlannerWeeklyTableViewCellDelegate  <NSObject>

@optional

-(void)addButtonTappedWeeklyViewWithDate:(NSString *)dateStr andMealType:(NSString *)mealType;
-(void)removeRecipeFromMealPlannerWith:(NSString *)mealPlanId;
-(void)gridViewTappedWithRecipe:(NSString *)recipeId;

-(void)scrollViewSelcetedStopAnimating;

@end

@interface CLMealPlannerWeeklyTableViewCell : UITableViewCell <CLMealPlanerGridViewDelegate>


@property (weak, nonatomic) IBOutlet UIImageView *mealPlannerCategoryImageView;
@property (weak, nonatomic) IBOutlet UILabel *mealPlannerTypeLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *mealPlannerGridScrollView;

-(void)configureMealPlannerWeeklyWithdataWithDateArray:(NSArray *)dateArray andPage:(NSInteger)page andIndexPath:(int)index andRecipeArray:(NSArray *)recipeArray withWidth:(CGFloat )width;
@property (nonatomic, assign) BOOL isDailyViewSelected;
@property (nonatomic, strong) NSArray *daysArray;
@property (nonatomic, strong) NSArray *mealTypeArray;
@property (nonatomic, assign) int index;

@property (nonatomic, strong)NSString *dateString;
@property (nonatomic, strong) NSString *mealType;

@property (nonatomic, weak) id<CLMealPlannerWeeklyTableViewCellDelegate> delegate;

@property (nonatomic, strong) UILabel *nameLabel;

@end
