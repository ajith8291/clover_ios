//
//  CLMealPlannerTableViewCell.h
//  Clover
//
//  Created by Ajith Kumar on 16/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLMealPlanerGridView.h"

@protocol CLMealPlannerTableViewCellDelegate <NSObject>

@optional

-(void)addButtonTappedWithMealType:(int )index;
-(void)addButtonTappedWeeklyViewWithDate:(NSString *)dateStr andMealType:(NSString *)mealType;
-(void)removeRecipeFromMealPlannerWith:(NSString *)mealPlanId;

-(void)gridViewTappedWithRecipe:(NSString *)recipeId;
-(void)scrollViewSelcetedStopAnimating;

@end

@interface CLMealPlannerTableViewCell : UITableViewCell<CLMealPlanerGridViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *mealPlannerCategoryImageView;
@property (weak, nonatomic) IBOutlet UILabel *mealPlannerTypeLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *mealPlannerGridScrollView;

@property (nonatomic, strong) NSString *mealType;
-(void)configureMealPlannerBreakFastWithdataWithRecipeArray:(NSArray *)recipeArray andIndex:(int)index withWidth :(CGFloat )width;


@property (nonatomic, assign) BOOL isDailyViewSelected;
@property (nonatomic, strong) NSArray *daysArray;
@property (nonatomic, strong) NSArray *mealTypeArray;
@property (nonatomic, weak) id<CLMealPlannerTableViewCellDelegate>delegate;

@property (nonatomic, assign) int index;

@property (nonatomic, strong)NSString *dateString;

@property (nonatomic, strong) UILabel *nameLabel;

@end
