//
//  CLMealPlannerTableViewCell.m
//  Clover
//
//  Created by Ajith Kumar on 16/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLMealPlannerTableViewCell.h"
#import "CLMealPlanerGridView.h"
#import "CLMealPlan.h"
#import "NSString+DataValidator.h"
#import "CLConstants.h"
@implementation CLMealPlannerTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.daysArray = @[@"MON",@"TUE",@"WED",@"THU",@"FRI",@"SAT",@"SUN"];
    self.mealTypeArray = @[@"Breakfast",@"Snacks",@"Lunch",@"Dinner"];
    
    UITapGestureRecognizer *scrollTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTapped)];
    [self.mealPlannerGridScrollView addGestureRecognizer:scrollTap];


}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}




-(void)configureMealPlannerBreakFastWithdataWithRecipeArray:(NSArray *)recipeArray andIndex:(int)index withWidth:(CGFloat)width
{
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.frame = CGRectMake(width-110, 37, 150, 22);
    self.nameLabel.text = @"Add Recipe";
    self.nameLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:14.0];
    self.nameLabel.textColor = CL_BLUE_COLOR;
    self.nameLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *addRecipeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addRecipeTapped)];
    [self.nameLabel addGestureRecognizer:addRecipeTap];
    
    self.mealPlannerGridScrollView.frame = CGRectMake(0, self.mealPlannerGridScrollView.frame.origin.y, self.frame.size.width, 95);

    for (UIView *view in self.mealPlannerGridScrollView.subviews)
    {
        [view removeFromSuperview];
    }
//    self.mealPlannerCategoryImageView.hidden = YES;
//    self.mealPlannerTypeLabel.translatesAutoresizingMaskIntoConstraints = YES;
//    self.mealPlannerTypeLabel.frame = CGRectMake(12, self.mealPlannerTypeLabel.frame.origin.y, self.mealPlannerTypeLabel.frame.size.width, self.mealPlannerTypeLabel.frame.size.height);
    self.mealPlannerGridScrollView.scrollEnabled = YES;
    self.mealPlannerGridScrollView.translatesAutoresizingMaskIntoConstraints = YES;
    [self.mealPlannerGridScrollView setShowsHorizontalScrollIndicator:NO];

    self.index = index;
    self.mealPlannerTypeLabel.text = [[self.mealTypeArray objectAtIndex:index] uppercaseString];

    if (recipeArray.count)
    {
        self.mealPlannerGridScrollView.hidden = NO;
        int xAxis = 8;
        for (int i=0; i<recipeArray.count; i++)
        {
            CLMealPlan *mealPlan = [CLMealPlan getMealPlanDetailsFrom:[recipeArray objectAtIndex:i]];
            
            CLMealPlanerGridView *gridView = [[[NSBundle mainBundle] loadNibNamed:@"CLMealPlanerGridView" owner:self options:nil]objectAtIndex:0];
            
            gridView.recipeId = [[[recipeArray objectAtIndex:i] objectForKey:@"recipe_list"] objectForKey:@"ID"];
            UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
            UITapGestureRecognizer *gridTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gridTapped:)];
            [gridView addGestureRecognizer:gridTap];
            
            gridView.mealPlanDeleteButton.hidden = YES;
            longPress.numberOfTouchesRequired = 1;
            [gridView addGestureRecognizer:longPress];
            gridView.frame = CGRectMake(xAxis, 0, gridView.frame.size.width, 90);
            gridView.mealPlanId = mealPlan.mealPlanID;
            [gridView configureView:mealPlan.recipeTitle andImageUrl:mealPlan.recipeImageUrl];
            gridView.tag = i+100;
            [self.mealPlannerGridScrollView addSubview:gridView];
            xAxis = xAxis+gridView.frame.size.width;
            self.nameLabel.hidden = YES;

        }
        self.mealPlannerGridScrollView.contentSize = CGSizeMake(xAxis+30, 90);
    }
    else
    {
        [self.mealPlannerGridScrollView addSubview:self.nameLabel];
        self.nameLabel.hidden = NO;
    }

}

#pragma mark UIButton/UITap Action Methods

- (IBAction)addMealButtonAction:(id)sender
{
    if (self.isDailyViewSelected)
    {
        [self.delegate addButtonTappedWithMealType:self.index];
    }
    else
    {
        [self.delegate addButtonTappedWeeklyViewWithDate:self.dateString andMealType:self.mealType];
    }
}

-(void)scrollViewTapped
{
    [self.delegate scrollViewSelcetedStopAnimating];
}

-(void)addRecipeTapped
{
    [self addMealButtonAction:self];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gesture
{
    
    CLMealPlanerGridView *gridView = (CLMealPlanerGridView *)[self.mealPlannerGridScrollView viewWithTag:gesture.view.tag];
    gridView.delegate = self;
    gridView.mealPlanDeleteButton.hidden = NO;
    if (gesture.state == UIGestureRecognizerStateBegan)
    {
        CABasicAnimation* anim = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        [anim setToValue:[NSNumber numberWithFloat:0.0f]];
        [anim setFromValue:[NSNumber numberWithDouble:M_PI/25]]; // rotation angle
        [anim setDuration:0.15];
        [anim setRepeatCount:NSUIntegerMax];
        [anim setAutoreverses:YES];
        [gridView.layer addAnimation:anim forKey:@"iconShake"];
    }
}

-(void)gridTapped:(UITapGestureRecognizer *)tapView
{
    CLMealPlanerGridView *gridView = (CLMealPlanerGridView *)[self.mealPlannerGridScrollView viewWithTag:tapView.view.tag];
    [gridView.layer removeAnimationForKey:@"iconShake"];
    gridView.mealPlanDeleteButton.hidden = YES;

    [self.delegate gridViewTappedWithRecipe:gridView.recipeId];
}

#pragma mark CLMealPlanerGridViewDelegate Methods

-(void)deleteRecipeFromMealPlanWith:(NSString *)mealPlanId
{
    [self.delegate removeRecipeFromMealPlannerWith:mealPlanId];
}

@end
