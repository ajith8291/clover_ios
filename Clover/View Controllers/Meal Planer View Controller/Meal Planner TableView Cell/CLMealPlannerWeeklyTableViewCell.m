//
//  CLMealPlannerWeeklyTableViewCell.m
//  Clover
//
//  Created by Ajith Kumar on 31/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLMealPlannerWeeklyTableViewCell.h"
#import "NSString+DataValidator.h"
#import "CLMealPlanerGridView.h"
#import "CLConstants.h"
@implementation CLMealPlannerWeeklyTableViewCell

- (void)awakeFromNib {
    self.daysArray = @[@"MON",@"TUE",@"WED",@"THU",@"FRI",@"SAT",@"SUN"];
    self.mealTypeArray = @[@"Breakfast",@"Snacks",@"Lunch",@"Dinner"];
    
    // Initialization code
    
    UITapGestureRecognizer *scrollTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(scrollViewTapped)];
    [self.mealPlannerGridScrollView addGestureRecognizer:scrollTap];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


-(void)configureMealPlannerWeeklyWithdataWithDateArray:(NSArray *)dateArray andPage:(NSInteger)page andIndexPath:(int)index andRecipeArray:(NSArray *)recipeArray withWidth:(CGFloat)width
{
    self.nameLabel = [[UILabel alloc] init];
    self.nameLabel.frame = CGRectMake(width-110, 37, 150, 22);
    self.nameLabel.text = @"Add Recipe";
    self.nameLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:14.0];
    self.nameLabel.textColor = CL_BLUE_COLOR;
    self.nameLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *addRecipeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addRecipeTapped)];
    [self.nameLabel addGestureRecognizer:addRecipeTap];

    for (UIView *view in self.mealPlannerGridScrollView.subviews)
    {
        [view removeFromSuperview];
    }
    
    self.mealPlannerCategoryImageView.hidden = YES;
    self.mealPlannerTypeLabel.translatesAutoresizingMaskIntoConstraints = YES;
    self.mealPlannerTypeLabel.frame = CGRectMake(12, self.mealPlannerTypeLabel.frame.origin.y, self.mealPlannerTypeLabel.frame.size.width, self.mealPlannerTypeLabel.frame.size.height);
    self.mealPlannerGridScrollView.scrollEnabled = YES;
    self.mealPlannerGridScrollView.translatesAutoresizingMaskIntoConstraints = YES;
    [self.mealPlannerGridScrollView setShowsHorizontalScrollIndicator:NO];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"MMM dd"];
    NSString *date = [dateFormatter stringFromDate:[dateArray objectAtIndex:(page*7)+index]];
    
    NSDateFormatter *dateStrFormatter = [[NSDateFormatter alloc] init];
    [dateStrFormatter setDateFormat:@"yyyy-MM-dd"];
    self.dateString = [dateStrFormatter stringFromDate:[dateArray objectAtIndex:(page*7)+index]];
    
    
    self.mealPlannerTypeLabel.text = [[NSString stringWithFormat:@"%@, %@",[self.daysArray objectAtIndex:index],date] uppercaseString];
    self.mealPlannerGridScrollView.frame = CGRectMake(0, self.mealPlannerGridScrollView.frame.origin.y, self.frame.size.width, 95);
    int xAxis = 8;
    
    for (int j=1; j<=recipeArray.count; j++)
    {
        NSString *timeStampStr = [NSString stringWithFormat:@"%@",[[recipeArray objectAtIndex:j-1]  objectForKey:@"mealdate"]];
        NSArray *recipeInfoArray = [[recipeArray objectAtIndex:j-1] objectForKey:@"recipelist"];
        for (int i=1; i<=recipeInfoArray.count; i++)
        {
            NSString *dateStr = [timeStampStr makeDateStringFromTimeStamp];
            if ([date isEqualToString:dateStr])
            {
                CLMealPlanerGridView *gridView = [[[NSBundle mainBundle] loadNibNamed:@"CLMealPlanerGridView" owner:self options:nil]objectAtIndex:0];
                gridView.mealPlanDeleteButton.hidden = YES;
                UITapGestureRecognizer *gridTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gridTapped:)];
                [gridView addGestureRecognizer:gridTap];

                UILongPressGestureRecognizer *longPress = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
                longPress.numberOfTouchesRequired = 1;
                [gridView addGestureRecognizer:longPress];
                gridView.mealPlanId = [[recipeInfoArray objectAtIndex:i-1] objectForKey:@"ID"];
                gridView.recipeId = [[recipeInfoArray objectAtIndex:i-1] objectForKey:@"recipe_ID"];
                gridView.frame = CGRectMake(xAxis, 0, gridView.frame.size.width, 90);
                [gridView configureView:[[recipeInfoArray objectAtIndex:i-1] objectForKey:@"recipe_name"] andImageUrl:[[recipeInfoArray objectAtIndex:i-1] objectForKey:@"image"]];
                gridView.tag = i+j+100;
                [self.mealPlannerGridScrollView addSubview:gridView];
                gridView.recipeImageView.backgroundColor = [UIColor cyanColor];
                xAxis = xAxis+gridView.frame.size.width;
                
            }
        }
        
    }
    if (!self.mealPlannerGridScrollView.subviews.count)
    {
        [self.mealPlannerGridScrollView addSubview:self.nameLabel];
    }
    self.mealPlannerGridScrollView.contentSize = CGSizeMake(xAxis+30, 90);
}

#pragma mark UIButton/UITap Action Methods

- (IBAction)addMealButtonAction:(id)sender
{
        [self.delegate addButtonTappedWeeklyViewWithDate:self.dateString andMealType:self.mealType];
}

-(void)addRecipeTapped
{
    [self addMealButtonAction:self];
}


-(void)scrollViewTapped
{
    [self.delegate scrollViewSelcetedStopAnimating];
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gesture
{
    
    CLMealPlanerGridView *gridView = (CLMealPlanerGridView *)[self.mealPlannerGridScrollView viewWithTag:gesture.view.tag];
    gridView.delegate = self;
    gridView.mealPlanDeleteButton.hidden = NO;
    if (gesture.state == UIGestureRecognizerStateBegan)
    {
        CABasicAnimation* anim = [CABasicAnimation animationWithKeyPath:@"transform.rotation"];
        [anim setToValue:[NSNumber numberWithFloat:0.0f]];
        [anim setFromValue:[NSNumber numberWithDouble:M_PI/25]]; // rotation angle
        [anim setDuration:0.15];
        [anim setRepeatCount:NSUIntegerMax];
        [anim setAutoreverses:YES];
        [gridView.layer addAnimation:anim forKey:@"iconShake"];
    }
}

-(void)gridTapped:(UITapGestureRecognizer *)tapView
{
    CLMealPlanerGridView *gridView = (CLMealPlanerGridView *)[self.mealPlannerGridScrollView viewWithTag:tapView.view.tag];
    [gridView.layer removeAnimationForKey:@"iconShake"];
    gridView.mealPlanDeleteButton.hidden = YES;
    
    [self.delegate gridViewTappedWithRecipe:gridView.recipeId];
}


#pragma mark CLMealPlanerGridViewDelegate Methods

-(void)deleteRecipeFromMealPlanWith:(NSString *)mealPlanId
{
    [self.delegate removeRecipeFromMealPlannerWith:mealPlanId];
}


@end
