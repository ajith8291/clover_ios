//
//  CLRecipeDetailsUserPhotoTableViewCell.h
//  Clover
//
//  Created by Ajith Kumar on 13/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLHighlights.h"
@protocol  CLRecipeDetailsUserPhotoTableViewCellDelegate <NSObject>

@optional

-(void)pictureViewTappedWith:(CLHighlights *)highlight;

@end

@interface CLRecipeDetailsUserPhotoTableViewCell : UITableViewCell

// Left View Property

@property (weak, nonatomic) IBOutlet UIView *leftView;
@property (weak, nonatomic) IBOutlet UIView *leftViewContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *leftViewUserImageView;
@property (weak, nonatomic) IBOutlet UILabel *leftViewUserCaptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *leftViewUserNameLabel;

//Right View Property

@property (weak, nonatomic) IBOutlet UIView *rightView;
@property (weak, nonatomic) IBOutlet UIView *rightViewContainerView;
@property (weak, nonatomic) IBOutlet UIImageView *rightViewUserImageView;
@property (weak, nonatomic) IBOutlet UILabel *rightViewUserCaptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *rightViewUserNameLabel;


@property (nonatomic, strong)CLHighlights *leftViewPicture;
@property (nonatomic, strong)CLHighlights *rightViewPicture;

-(void)configureLeftViewWithData:(CLHighlights *)highlight;
-(void)configureRightViewWithData:(CLHighlights *)highlight;

@property (nonatomic, strong) id<CLRecipeDetailsUserPhotoTableViewCellDelegate>delegate;
@end
