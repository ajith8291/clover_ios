//
//  CLRecipeDetailsUserPhotoTableViewCell.m
//  Clover
//
//  Created by Ajith Kumar on 13/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLRecipeDetailsUserPhotoTableViewCell.h"
#import <UIImageView+AFNetworking.h>
#import "CLConstants.h"
#import "UIImage+colors.h"

@implementation CLRecipeDetailsUserPhotoTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    [self setNeedsLayout];
    [self setNeedsDisplay];

    UITapGestureRecognizer *leftViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leftViewTapped)];
    [self.leftView addGestureRecognizer:leftViewTap];
    
    UITapGestureRecognizer *rightViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rightViewTapped)];
    [self.rightView addGestureRecognizer:rightViewTap];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureLeftViewWithData:(CLHighlights *)highlight
{
    self.leftViewPicture = highlight;
    self.leftView.translatesAutoresizingMaskIntoConstraints = YES;
    self.leftView.frame = CGRectMake(10, 0, (self.frame.size.width-30)/2, 155);
    self.leftViewUserCaptionLabel.hidden = YES;
    
    
    self.leftViewUserNameLabel.hidden = YES;
    
    NSString *imgUrl = [[highlight.highlightFeaturedImage objectForKey:@"image"] stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%d",600]];
    imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%d",310]];
    
    UIImage *placeholderImg = [UIImage imageWithColor:[UIColor lightGrayColor]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
    
    __weak UIImageView *weakImg = self.leftViewUserImageView;
    
    [self.leftViewUserImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = image;
             weakImg.alpha = 0.0;
             
             [UIView animateWithDuration:1.0 animations:^{
                 weakImg.alpha = 1.0;
             }];
         });
         
         
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
     }];

}

-(void)configureRightViewWithData:(CLHighlights *)highlight
{
    self.rightViewPicture = highlight;
    self.rightView.translatesAutoresizingMaskIntoConstraints = YES;
    self.rightView.frame = CGRectMake((self.frame.size.width/2)+5, 0, (self.frame.size.width-30)/2, 155);
    
    self.rightViewUserCaptionLabel.hidden = YES;

    self.rightViewUserNameLabel.hidden = YES;
    
    NSString *imgUrl = [[highlight.highlightFeaturedImage objectForKey:@"image"] stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%d",600]];
    imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%d",310]];
    
    UIImage *placeholderImg = [UIImage imageWithColor:[UIColor lightGrayColor]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
    
    __weak UIImageView *weakImg = self.rightViewUserImageView;
    
    [self.rightViewUserImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = image;
             weakImg.alpha = 0.0;
             
             [UIView animateWithDuration:1.0 animations:^{
                 weakImg.alpha = 1.0;
             }];
         });
         
         
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
     }];

}


#pragma mark UITap/UIButton Actions

-(void)leftViewTapped
{
    [self.delegate pictureViewTappedWith:self.leftViewPicture];
}

-(void)rightViewTapped
{
    [self.delegate pictureViewTappedWith:self.rightViewPicture];
}

@end
