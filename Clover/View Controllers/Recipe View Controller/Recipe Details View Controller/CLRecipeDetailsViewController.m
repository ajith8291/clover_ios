//
//  CLRecipeDetailsViewController.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 23/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLRecipeDetailsViewController.h"
#import "CLRecipeDetailsTableViewCell.h"
#import "CLConstants.h"
#import "CLRecipeDetailsIngredientsTableViewCell.h"

#import "CLRecipeDetailsTableViewMethodCell.h"
#import "NSString+DataValidator.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "CLWebViewController.h"
#import "CLRateRecipeView.h"
#import "CLWriteReviewAndUploadPictureView.h"
#import "CLReviewsViewController.h"
#import "CLRecipeDetailsVideoTableViewCell.h"
#import "CLRecipeDetailsUserPhotoTableViewCell.h"
#import "CLNewsViewController.h"
#import "CLRecipeReviewTableViewCell.h"
#import "CLRecipeUserReviewTableViewCell.h"
#import "CLCalenderView.h"
#import "CLBaseViewController.h"
#import <MediaPlayer/MediaPlayer.h>

@interface CLRecipeDetailsViewController ()<CLRecipeDetailsTableViewMethodCellDelegate,CLRateRecipeViewDelegate,CLWriteReviewAndUploadPictureViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate,CLRecipeDetailsTableViewCellDelegate,CLRecipeDetailsVideoTableViewCellDelegate,CLRecipeDetailsUserPhotoTableViewCellDelegate,CLRecipeViewDelegate,UIActionSheetDelegate,CLCalenderViewDelegate>




@property (nonatomic, assign) BOOL isIngredientsSelected;
@property (nonatomic, assign) BOOL isMethodSelected;
@property (nonatomic, assign) BOOL isShareViewSelected;
@property (nonatomic, assign) BOOL isSocialShareViewSelected;
@property (nonatomic, assign) BOOL isMealPlanViewSelected;
@property (nonatomic, assign) BOOL isDetailsLoaded;
@property (nonatomic, assign) BOOL isPhotoUploaded;
@property (nonatomic, strong) NSMutableArray *uploadPhotoArray;

@property (nonatomic, strong) CLRateRecipeView *rateRecipeView;
@property (nonatomic, strong) CLWriteReviewAndUploadPictureView *writeReviewViewAndUploadPictureView;
@property (nonatomic, strong) CLCalenderView *calenderView;

@property (nonatomic, strong) UIView *blackView;
@property (nonatomic, strong) UIView *blackBottomView;
@property (nonatomic, strong) UIView *calenderBlackView;
@property (nonatomic, strong) NSDate *selectedDate;

@end

@implementation CLRecipeDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view.
    self.navigationView.navLogoImageView.hidden = YES;
    self.navigationView.navMenuButton.hidden = YES;
    self.navigationView.navEditButton.hidden = YES;
    self.navigationView.navSearchButton.hidden = YES;
    self.navigationView.navCartButton.hidden = YES;
    self.navigationView.navTitleTextLabel.hidden = NO;
    self.navigationView.navTitleTextLabel.text = [@"recipe detail" capitalizedString];
    if (self.isUserReviewSelected)
    {
        self.isIngredientsSelected = NO;
        self.isMethodSelected = NO;
    }
    else
    {
        self.isIngredientsSelected = YES;
        self.isMethodSelected = NO;
    }
    self.socialShareView.hidden = YES;
    if (self.isPlayButtonTapped)
    {
        CLWebViewController *webVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLWebViewController"];
        webVC.urlString = self.selectedRecipe.recipeVideoUrl;
        [self.navigationController pushViewController:webVC animated:YES];

    }
    UINib *nib = [UINib nibWithNibName:@"CLRecipeUserReviewTableViewCell" bundle:nil];
    [self.recipeDetailsTableView registerNib:nib forCellReuseIdentifier:@"CLRecipeUserReviewTableViewCell"];
    
    self.shareView.hidden = YES;
    self.isShareViewSelected = YES;
    self.isSocialShareViewSelected = YES;
    self.isMealPlanViewSelected = YES;
    [self addTapGesturesToView];
    self.isPictureUploaded = NO;
    self.uploadPhotoArray = [[NSMutableArray new] mutableCopy];
    [self modifyRecipeDetailsView];
    [self getDetailsOfRecipe];
    [self changeFrameOfViews];
    self.mealPlannerView.hidden = YES;
    [self modifyMealPlanView];
    
}

-(void)modifyRecipeDetailsView
{
    if (self.isFromAddToMealPLanner == YES) {
        self.bottomTabView.hidden = YES;
        self.addAndRemoveMealPlannerButton.hidden = NO;
    }
    else
    {
        self.bottomTabView.hidden = NO;
        self.addAndRemoveMealPlannerButton.hidden = YES;
    }
    if ([self.addToMealPlannerArray containsObject:self.selectedRecipe.recipeId])
    {
        [self.addAndRemoveMealPlannerButton setTitle:@"Remove from Meal Planner" forState:UIControlStateNormal];
    }
    else
    {
        [self.addAndRemoveMealPlannerButton setTitle:@"Add to Meal Planner" forState:UIControlStateNormal];
    }

}

-(void)changeFrameOfViews
{
    if (IS_IOS8_OR_GREATER)
    {
        self.recipeDetailsTableView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    else
    {
        self.recipeDetailsTableView.translatesAutoresizingMaskIntoConstraints = YES;
        self.recipeDetailsTableView.frame = CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height-90);
    }
    self.isDetailsLoaded = NO;

        self.addToFavButton.translatesAutoresizingMaskIntoConstraints = YES;
        self.addToShoppingListButton.translatesAutoresizingMaskIntoConstraints = YES;
        self.addToMealPlannerButton.translatesAutoresizingMaskIntoConstraints = YES;
        self.shareButton.translatesAutoresizingMaskIntoConstraints = YES;
        self.moreButton.translatesAutoresizingMaskIntoConstraints = YES;
        
        self.seperator1.translatesAutoresizingMaskIntoConstraints = YES;
        self.seperator2.translatesAutoresizingMaskIntoConstraints = YES;
        self.seperator3.translatesAutoresizingMaskIntoConstraints = YES;
        self.seperator4.translatesAutoresizingMaskIntoConstraints = YES;
        
        
        int width = self.view.frame.size.width/5;
        self.addToFavButton.frame = CGRectMake(0,0 , width-1,40);
        self.addToShoppingListButton.frame = CGRectMake(width,0 , width-1,40);
        self.addToMealPlannerButton.frame = CGRectMake(width*2,0 , width-1,40);
        self.shareButton.frame = CGRectMake(width*3,0, width-1,40);
        self.moreButton.frame = CGRectMake(width*4,0, width,40);
        
        self.socialShareView.translatesAutoresizingMaskIntoConstraints = YES;
        self.socialShareView.frame = CGRectMake(self.shareButton.frame.origin.x-43, self.view.frame.size.height-129, self.shareButton.frame.size.width+86, 90);
        self.seperator1.frame = CGRectMake(self.addToShoppingListButton.frame.origin.x-1,0 , 1,40);
        self.seperator2.frame = CGRectMake(self.addToMealPlannerButton.frame.origin.x-1,0 , 1,40);
        self.seperator3.frame = CGRectMake(self.shareButton.frame.origin.x-1,0 , 1,40);
        self.seperator4.frame = CGRectMake(self.moreButton.frame.origin.x-1,0 , 1,40);
}

-(void)modifyMealPlanView
{
    self.mealPlannerView.layer.cornerRadius = 2.0;
    self.mealPlannerView.layer.borderWidth = 0.5;
    self.mealPlannerView.clipsToBounds = YES;
    self.mealPlannerView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    self.mealPlanCancelButton.layer.cornerRadius = 3.0;
    self.mealPlanCancelButton.clipsToBounds = YES;
    
    self.mealPlanAddMealButton.layer.cornerRadius = 3.0;
    self.mealPlanAddMealButton.clipsToBounds = YES;
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd-MMM-yyyy"];
    NSString *currentDate = [df stringFromDate:[NSDate date]];
    
    self.mealPlanDateLabel.text = currentDate;
    self.selectedDate = [NSDate date];
    self.mealPlanDateLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *dateTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mealPlanDateLabelTapped)];
    [self.mealPlanDateLabel addGestureRecognizer:dateTap];
}

-(void)addTapGesturesToView
{
    self.shareView.layer.cornerRadius = 2.0;
    self.shareView.layer.borderWidth = 0.5;
    self.shareView.clipsToBounds = YES;
    self.shareView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    self.socialShareView.layer.cornerRadius = 2.0;
    self.socialShareView.layer.borderWidth = 0.5;
    self.socialShareView.clipsToBounds = YES;
    self.socialShareView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    

    
    
    UITapGestureRecognizer *rateRecipeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(rateRecipeViewTapped)];
    [self.ratingView addGestureRecognizer:rateRecipeTap];

    UITapGestureRecognizer *addPictureTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(addPictueViewTapped)];
    [self.addPictureView addGestureRecognizer:addPictureTap];
    
    UITapGestureRecognizer *shareFBTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shareFBViewTapped)];
    [self.shareFBView addGestureRecognizer:shareFBTap];
    
    UITapGestureRecognizer *shareTwitterTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(shareTwitterViewTapped)];
    [self.shareTwitterView addGestureRecognizer:shareTwitterTap];
    
    UITapGestureRecognizer *mealTimeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mealPlanSelectMealTimeViewTapped)];
    [self.mealPlanSelecteMealTimeView addGestureRecognizer:mealTimeTap];
    

}

-(void)getDetailsOfRecipe
{
   // http://cloverapi.jumpcatch.com/api/getrecipe/?recipe=67
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        NSString *recipeID;
        
        if (self.selectedRecipe.recipeId == nil) {
            recipeID = self.recipeID;
        }
        else
        {
            recipeID = self.selectedRecipe.recipeId;
        }
        
        NSDictionary *params = @{@"recipe":recipeID,
                                 @"user":self.userManager.user.userId};
        
        [self.networkManager startGETRequestWithAPI:@"getrecipe/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             NSString *msg = [responseDictionary objectForKey:@"msg"];
             self.isDetailsLoaded = YES;
             
             if ([errorCode isEqualToString:@"200"])
             {
                 self.selectedRecipe = [CLRecipe getInformationFromDictionary:[responseDictionary objectForKey:@"recipe_info"]];
                 [self configureAddToFavouriteButton];
                 [self configureAddToShopplingButton];
                 self.uploadPhotoArray = [self.selectedRecipe.uploadPictureArray mutableCopy];
                 [self.recipeDetailsTableView reloadData];

             }
             else
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
             }
             
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }
}

-(void)configureAddToFavouriteButton
{
    if ([self.selectedRecipe.recipeInfavourite isEqualToString:@"yes"])
    {
        [self.addToFavButton setImage:[UIImage imageNamed:@"RemoveFav"] forState:UIControlStateNormal];
    }
    else
    {
        [self.addToFavButton setImage:[UIImage imageNamed:@"addtofavortes"] forState:UIControlStateNormal];
    }

}
-(void)configureAddToShopplingButton
{
    if ([self.selectedRecipe.recipeInShoppingList isEqualToString:@"yes"])
    {
        [self.addToShoppingListButton setImage:[UIImage imageNamed:@"removefromshoplist"] forState:UIControlStateNormal];
    }
    else
    {
        [self.addToShoppingListButton setImage:[UIImage imageNamed:@"addtoshoplist"] forState:UIControlStateNormal];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableViewDelegate Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.isDetailsLoaded)
    {
        return 2;
    }
    return 0;
}

- (CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{

    if (self.isDetailsLoaded)
    {
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_BOLD size:18]};
        NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:self.selectedRecipe.recipeTitle attributes:attributes];
        
        CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-20, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        CGSize size = rect.size;

        if (section == 0)
            return size.height+238.0f;
        return 55.0f;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.isDetailsLoaded)
    {
        if (section == 0)
        {
            NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_BOLD size:18]};
            NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:self.selectedRecipe.recipeTitle attributes:attributes];
            
            CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-20, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
            CGSize size = rect.size;

            UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, size.height+238)];
            UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 8, self.view.frame.size.width-20, size.height)];
            nameLabel.numberOfLines = 2;
            nameLabel.text = self.selectedRecipe.recipeTitle;
            nameLabel.textColor = CL_BLUE_COLOR;
            nameLabel.font = [UIFont fontWithName:ROBOTO_BOLD size:18.0];
            CLRecipeView *recipeView = [[[NSBundle mainBundle] loadNibNamed:@"CLRecipeView" owner:self options:nil]objectAtIndex:0];
            recipeView.frame = CGRectMake(0, size.height+13, self.view.frame.size.width,225);
            recipeView.topPlayButton.hidden = YES;
            [recipeView configureRecipeViewWithRecipe:self.selectedRecipe];
            recipeView.backgroundImageView.hidden = YES;
            recipeView.recipeNameLabel.hidden = YES;
            recipeView.reviewCountLabel.translatesAutoresizingMaskIntoConstraints = YES;
            recipeView.reviewCountLabel.frame = CGRectMake(recipeView.reviewCountLabel.frame.origin.x, 150, recipeView.reviewCountLabel.frame.size.width, recipeView.reviewCountLabel.frame.size.height);
            recipeView.starView.translatesAutoresizingMaskIntoConstraints = YES;
            recipeView.starView.frame = CGRectMake(self.view.frame.size.width-130, 140, recipeView.starView.frame.size.width, recipeView.starView.frame.size.height);
            recipeView.delegate = self;
            [headerView addSubview:nameLabel];
            [headerView addSubview:recipeView];

            return headerView;
        }
        CGFloat width = (self.view.frame.size.width-20)/3;
        
        UIView *seperatorView = [[UIView alloc] initWithFrame:CGRectMake(5, 35, width-7 , 1)];
        seperatorView.backgroundColor = [UIColor blackColor];
        
        
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
        headerView.backgroundColor = [UIColor whiteColor];
        
        UIView *ingredientsView = [[UIView alloc] initWithFrame:CGRectMake(5, 8, width, 40)];
        UILabel *ingredientLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 40)];
        ingredientLabel.text = @"Ingredients";
        ingredientLabel.textAlignment = NSTextAlignmentCenter;
        
        [ingredientsView addSubview:ingredientLabel];
        [ingredientsView addSubview:seperatorView];
        
        UITapGestureRecognizer *ingredientsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingredientsViewTapped)];
        [ingredientsView addGestureRecognizer:ingredientsTap];
        [headerView addSubview:ingredientsView];
        
        
        UIView *methodView = [[UIView alloc] initWithFrame:CGRectMake(width+5, 8, width, 40)];
        UILabel *methodLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 40)];
        methodLabel.text = @"Method";
        methodLabel.textAlignment = NSTextAlignmentCenter;
        
        UIView *userPhotoView = [[UIView alloc] initWithFrame:CGRectMake(width*2+5, 8, width, 40)];
        UILabel *userPhotoLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, width, 40)];
        userPhotoLabel.text = @"User reviews";
        userPhotoLabel.textAlignment = NSTextAlignmentCenter;

        if (self.isIngredientsSelected)
        {
            ingredientLabel.font = [UIFont fontWithName:ROBOTO_BOLD size:17.0];
            ingredientLabel.textColor = [UIColor blackColor];
            methodLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:17.0];
            methodLabel.textColor = [UIColor colorWithRed:113.0/255.0 green:113.0/255.0 blue:113.0/255.0 alpha:1.0];
            userPhotoLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:17.0];
            userPhotoLabel.textColor = [UIColor colorWithRed:113.0/255.0 green:113.0/255.0 blue:113.0/255.0 alpha:1.0];
            
            seperatorView.frame  = CGRectMake(5, 35, width-7 , 1);
            [ingredientsView addSubview:seperatorView];
            
        }
        else if (self.isMethodSelected)
        {
            methodLabel.font = [UIFont fontWithName:ROBOTO_BOLD size:17.0];
            methodLabel.textColor = [UIColor blackColor];
            ingredientLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:17.0];
            ingredientLabel.textColor = [UIColor colorWithRed:113.0/255.0 green:113.0/255.0 blue:113.0/255.0 alpha:1.0];
            userPhotoLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:17.0];
            userPhotoLabel.textColor = [UIColor colorWithRed:113.0/255.0 green:113.0/255.0 blue:113.0/255.0 alpha:1.0];
            
            seperatorView.frame  = CGRectMake(5, 35, width-10 , 1);
            [methodView addSubview:seperatorView];
        }
        else
        {
            userPhotoLabel.font = [UIFont fontWithName:ROBOTO_BOLD size:17.0];
            userPhotoLabel.textColor = [UIColor blackColor];
            ingredientLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:17.0];
            ingredientLabel.textColor = [UIColor colorWithRed:113.0/255.0 green:113.0/255.0 blue:113.0/255.0 alpha:1.0];
            methodLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:17.0];
            methodLabel.textColor = [UIColor colorWithRed:113.0/255.0 green:113.0/255.0 blue:113.0/255.0 alpha:1.0];
            
            seperatorView.frame  = CGRectMake(0, 35, width , 1);
            [userPhotoView addSubview:seperatorView];
        }
        
        [methodView addSubview:methodLabel];
        [userPhotoView addSubview:userPhotoLabel];
        
        UITapGestureRecognizer *methodTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(methodViewTapped)];
        [methodView addGestureRecognizer:methodTap];
        [headerView addSubview:methodView];
        
        UITapGestureRecognizer *userPhotoTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(userPhotoViewTapped)];
        [userPhotoView addGestureRecognizer:userPhotoTap];
        [headerView addSubview:userPhotoView];
        return headerView;

    }
    return nil;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isDetailsLoaded) {
        if (section == 0) {
            return 0;
        }
        else
        {
            if (self.isIngredientsSelected)
            {
                return self.selectedRecipe.recipeIngredientsArray.count;
            }
            else if(self.isMethodSelected)
            {
                return self.selectedRecipe.recipeMethodArray.count;
            }
            else
            {
                return self.uploadPhotoArray.count;
            }
            
        }
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 0) {
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_BOLD size:18]};
        self.selectedRecipe.recipeTitle = [NSString stringWithFormat:@"%@",self.selectedRecipe.recipeTitle];
        NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:self.selectedRecipe.recipeTitle attributes:attributes];
        
        CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-20, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        CGSize size = rect.size;

        return size.height+250;
    }
    if (self.isIngredientsSelected)
    {
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:13]};
        NSString *ingredientStr = [NSString stringWithFormat:@"%@",[[self.selectedRecipe.recipeIngredientsArray objectAtIndex:indexPath.row] objectForKey:@"ingredient"]];

        NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:ingredientStr attributes:attributes];
        
        CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-120, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        CGSize size = rect.size;
        return size.height+10;
    }
    else if(self.isMethodSelected)
    {
        
        if (indexPath.row == self.selectedRecipe.recipeMethodArray.count)
        {
            if ([self.selectedRecipe.recipeVideoUrl isEqualToString:@""]) {
                return 0;
            }
            return 200;
        }
        else
        {
            NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:16]};
            NSString *methodtStr = [NSString stringWithFormat:@"%@",[[self.selectedRecipe.recipeMethodArray objectAtIndex:indexPath.row] objectForKey:@"step"]];
            
            NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:methodtStr attributes:attributes];
            
            CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-35, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
            CGSize size = rect.size;
            return size.height+5;
        }
    }
    else
    {
        NSArray *pictureArray;
        pictureArray = self.uploadPhotoArray;
        CLUploadPicture *picture = [CLUploadPicture getUploadPictureDetailsFrom:[pictureArray objectAtIndex:indexPath.row]];
        
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:17]};
        NSString *recipenameStr = picture.user.userName;
        NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:recipenameStr attributes:attributes];
        CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-150, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        
        CGSize size = rect.size;
        
        NSDictionary *CommentsAttributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:15]};
        NSString *commentsStr = picture.uploadPictureCaption;
        
        NSAttributedString *CommentsAttributedtext = [[NSAttributedString alloc] initWithString:commentsStr attributes:CommentsAttributes];
        CGRect rect1 = [CommentsAttributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-150, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        CGSize size1 = rect1.size;
        
        return size.height+size1.height+50;
    }
    return 0;

}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isDetailsLoaded) {
        if (indexPath.section == 0) {
            CLRecipeDetailsTableViewCell *cell = (CLRecipeDetailsTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CLRecipeDetailsTableViewCell" forIndexPath:indexPath];
            cell.frame = CGRectMake(0, 0, self.view.frame.size.width, cell.frame.size.height);
            [cell configureCellWithRecipeDetails:self.selectedRecipe];
            cell.delegate = self;
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            return cell;
        }
        else
        {
            if (self.isIngredientsSelected)
            {
                CLRecipeDetailsIngredientsTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"CLRecipeDetailsIngredientsTableViewCell" forIndexPath:indexPath];
                [cell configureIngredientsCellWith:[self.selectedRecipe.recipeIngredientsArray objectAtIndex:indexPath.row]];
                cell.frame = CGRectMake(0, 0, self.view.frame.size.width, cell.frame.size.height);
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else if(self.isMethodSelected)
            {
                
                CLRecipeDetailsTableViewMethodCell *cell = (CLRecipeDetailsTableViewMethodCell *)[tableView dequeueReusableCellWithIdentifier:@"CLRecipeDetailsTableViewMethodCell" forIndexPath:indexPath];
                cell.StepsLabel.text = [NSString stringWithFormat:@"%ld.",(long)indexPath.row+1];
                cell.recipeMethodDetailsLabel.text = [[self.selectedRecipe.recipeMethodArray objectAtIndex:indexPath.row] objectForKey:@"step"];
                cell.frame = CGRectMake(0, 0, self.view.frame.size.width, cell.frame.size.height);
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                return cell;
            }
            else
            {
                CLRecipeUserReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CLRecipeUserReviewTableViewCell" forIndexPath:indexPath];
                CLUploadPicture *picture = [CLUploadPicture getUploadPictureDetailsFrom:[self.uploadPhotoArray objectAtIndex:indexPath.row]];
                
                cell.frame = CGRectMake(0, 0, self.view.frame.size.width, cell.frame.size.height);
                [cell configureCellWithData:picture];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;

                return cell;

            }
        }

    }
    return nil;
}


#pragma mark UITap / UIButton Action Methods

-(void)ingredientsViewTapped
{
    [self changeBackgroundAndImageOfButton];
    [self changeBackgroundColorOfShare];
    [self changeBackgroundOfAddToMealPlanButton];
    self.isIngredientsSelected = YES;
    self.isMethodSelected = NO;
    [self.recipeDetailsTableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
    self.shareView.hidden = YES;
    self.isShareViewSelected = YES;
    self.isSocialShareViewSelected = YES;
    self.isMealPlanViewSelected= YES;

}

-(void)methodViewTapped
{
    [self changeBackgroundAndImageOfButton];
    [self changeBackgroundColorOfShare];
    [self changeBackgroundOfAddToMealPlanButton];


    self.isIngredientsSelected = NO;
    self.isMethodSelected = YES;
    [self.recipeDetailsTableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
    self.shareView.hidden = YES;
    self.isShareViewSelected = YES;
    self.isSocialShareViewSelected = YES;
    self.isMealPlanViewSelected= YES;

}

-(void)userPhotoViewTapped
{
    [self changeBackgroundAndImageOfButton];
    [self changeBackgroundColorOfShare];
    [self changeBackgroundOfAddToMealPlanButton];


    self.isIngredientsSelected = NO;
    self.isMethodSelected = NO;
    [self.recipeDetailsTableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
    self.shareView.hidden = YES;
    self.isShareViewSelected = YES;
    self.isSocialShareViewSelected = YES;
    self.isMealPlanViewSelected= YES;

}
- (IBAction)addToFavouriteButtonAction:(id)sender
{
    [self changeBackgroundAndImageOfButton];
    [self changeBackgroundOfAddToMealPlanButton];

    [self removeBlackView];
    [self changeBackgroundColorOfShare];
    if ([self.selectedRecipe.recipeInfavourite isEqualToString:@"yes"])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Would you like to remove this recipe from your favourites" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alertView.tag = 101;
        [alertView show];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Would you like to Add this recipe to your favourites" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alertView.tag = 201;
        [alertView show];
    }
}

- (IBAction)addToShoppingListButtonAction:(id)sender
{
    [self removeBlackView];
    [self changeBackgroundAndImageOfButton];
    [self changeBackgroundOfAddToMealPlanButton];
    [self changeBackgroundColorOfShare];
    if ([self.selectedRecipe.recipeInShoppingList isEqualToString:@"yes"])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Would you like to remove this recipe from your shopping list" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alertView.tag = 102;
        [alertView show];
    }
    else
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Would you like to Add this recipe to your shopping list" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
        alertView.tag = 202;
        [alertView show];
    }

}


- (IBAction)addToMealPlanButtonAction:(id)sender
{
    [self addBlackBottomView];
    [self changeBackgroundAndImageOfButton];
    [self changeBackgroundColorOfShare];
    [self.view bringSubviewToFront:self.mealPlannerView];
    [self.view bringSubviewToFront:self.bottomView];
    if (self.isMealPlanViewSelected)
    {
        self.mealPlannerView.hidden = NO;
        
        [self.addToMealPlannerButton setImage:[UIImage imageNamed:@"addtomealplannerblue"] forState:UIControlStateNormal];
        self.addToMealPlannerButton.backgroundColor = [UIColor whiteColor];

    }
    else
    {
        self.mealPlannerView.hidden = YES;
        [self.addToMealPlannerButton setImage:[UIImage imageNamed:@"addtomealplanner"] forState:UIControlStateNormal];
        self.addToMealPlannerButton.backgroundColor = CL_BLUE_COLOR;
    }
    self.isMealPlanViewSelected = !self.isMealPlanViewSelected;
    
//    [self bounceOutAnimationStoped:self.mealPlannerView];
}



- (IBAction)shareButtonAction:(id)sender
{
    [self addBlackBottomView];
    [self changeBackgroundAndImageOfButton];
    [self changeBackgroundOfAddToMealPlanButton];
    [self.view bringSubviewToFront:self.socialShareView];
    
    if (self.isSocialShareViewSelected)
    {
        self.socialShareView.hidden = NO;
        [self.shareButton setImage:[UIImage imageNamed:@"shareblue"] forState:UIControlStateNormal];
        self.shareButton.backgroundColor = [UIColor whiteColor];

    }
    else
    {
        self.socialShareView.hidden = YES;
        [self.shareButton setImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
        self.shareButton.backgroundColor = CL_BLUE_COLOR;
    }
    self.isSocialShareViewSelected = ! self.isSocialShareViewSelected;
//    [self bounceOutAnimationStoped:self.socialShareView];
}

- (IBAction)showMoreButtonAction:(id)sender
{
    [self addBlackBottomView];
    [self changeBackgroundColorOfShare];
    [self changeBackgroundOfAddToMealPlanButton];
    [self.view bringSubviewToFront:self.shareView];
    if ( self.isShareViewSelected)
    {
        self.shareView.hidden = NO;
        [self.moreButton setImage:[UIImage imageNamed:@"OptionsSelected.png"] forState:UIControlStateNormal];
        self.moreButton.backgroundColor = [UIColor whiteColor];
    }
    else
    {
        self.shareView.hidden = YES;
        [self.moreButton setImage:[UIImage imageNamed:@"OptionsUnselected.png"] forState:UIControlStateNormal];
        self.moreButton.backgroundColor = CL_BLUE_COLOR;
    }
     self.isShareViewSelected = ! self.isShareViewSelected;
//    [self bounceOutAnimationStoped:self.shareView];
}

-(void)changeBackgroundAndImageOfButton
{
    self.shareView.hidden = YES;
    self.isShareViewSelected = YES;
    self.isMealPlanViewSelected = YES;
    [self.moreButton setImage:[UIImage imageNamed:@"OptionsUnselected.png"] forState:UIControlStateNormal];
    self.moreButton.backgroundColor = CL_BLUE_COLOR;
}

-(void)changeBackgroundColorOfShare
{
    self.socialShareView.hidden = YES;
    self.isSocialShareViewSelected = YES;
    self.isMealPlanViewSelected = YES;
    [self.shareButton setImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
    self.shareButton.backgroundColor = CL_BLUE_COLOR;
}

-(void)changeBackgroundOfAddToMealPlanButton
{
    self.mealPlannerView.hidden = YES;
    self.isShareViewSelected = YES;
    self.isSocialShareViewSelected = YES;
    [self.addToMealPlannerButton setImage:[UIImage imageNamed:@"addtomealplanner"] forState:UIControlStateNormal];
    self.addToMealPlannerButton.backgroundColor = CL_BLUE_COLOR;
}

-(void)rateRecipeViewTapped
{
    [self addBlackView];
    [self changeBackgroundAndImageOfButton];
    [self changeBackgroundColorOfShare];
    self.shareView.hidden = YES;
    self.isShareViewSelected = YES;
    self.isSocialShareViewSelected = YES;


    self.rateRecipeView = [[[NSBundle mainBundle] loadNibNamed:@"CLRateRecipeView" owner:self options:nil] objectAtIndex:0];
    self.rateRecipeView.recipe = self.selectedRecipe;
    [self.rateRecipeView configureRatingStarWithUserRate:self.selectedRecipe];
    self.rateRecipeView.delegate = self;
    self.rateRecipeView.center = self.view.center;
    [self ViewZoomInEffect:self.rateRecipeView];
    [self.view addSubview:self.rateRecipeView];
    
}

-(void)addPictueViewTapped
{
    [self addBlackView];
    [self changeBackgroundAndImageOfButton];
    [self changeBackgroundColorOfShare];

    self.shareView.hidden = YES;
    self.isShareViewSelected = YES;
    self.isSocialShareViewSelected = YES;

    self.writeReviewViewAndUploadPictureView = [[[NSBundle mainBundle] loadNibNamed:@"CLWriteReviewAndUploadPictureView" owner:self options:nil] objectAtIndex:0];
    //    self.writeReviewView.recipe = self.selectedRecipe;
    //    [self.writeReviewView configureRatingStarWithUserRate:self.selectedRecipe];
    self.writeReviewViewAndUploadPictureView.delegate = self;
    self.writeReviewViewAndUploadPictureView.center = self.view.center;
    [self ViewZoomInEffect:self.writeReviewViewAndUploadPictureView];

    [self.view addSubview:self.writeReviewViewAndUploadPictureView];

}

-(void)shareFBViewTapped
{
    [self changeBackgroundColorOfShare];
    [self removeBlackView];
    [self.sharedManager shareViaFBWithContent:[NSString stringWithFormat:@"%@",self.selectedRecipe.recipeGuide] inViewController:self WithVideoType:@"Recipe"];
}

-(void)shareTwitterViewTapped
{
    [self changeBackgroundColorOfShare];
    [self removeBlackView];
    [self.sharedManager shareViaTwitterWithContent:[NSString stringWithFormat:@"%@",self.selectedRecipe.recipeGuide] inViewController:self WithVideoType:@"Recipe"];
}

- (IBAction)addToMealPlannerButtonAction:(id)sender
{
    [self.delegate addAndRemoveMealPlannerButtonTappedWithRecipe:self.selectedRecipe];
    [self.navigationController popViewControllerAnimated:YES];
}


#pragma mark CLRecipeDetailsTableViewMethodCellDelegate Methods

-(void)videoPlayButtonTapped
{
    CLWebViewController *webVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLWebViewController"];
    webVC.urlString = self.selectedRecipe.recipeVideoUrl;
    [self.navigationController pushViewController:webVC animated:YES];
}

#pragma mark Favourite List Web Service

-(void)addToFavoutiteListOfUser
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        
        NSDictionary *params = @{@"user":self.userManager.user.userId,
                                 @"recipe":self.selectedRecipe.recipeId};
        
        [self.networkManager startGETRequestWithAPI:@"addfavourites/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             NSString *msg = [responseDictionary objectForKey:@"msg"];
             
             if ([errorCode isEqualToString:@"200"])
             {
                 if (self.isfromFavourites)
                 {
                     [self.delegate loadfavouritesDetails];
                     [self.navigationController popViewControllerAnimated:NO];
                 }
                 else
                 {
                     UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [errorAlert show];
                     [self.addToFavButton setImage:[UIImage imageNamed:@"RemoveFav"] forState:UIControlStateNormal];
                     self.selectedRecipe.recipeInfavourite = @"yes";
                 }
             }
             else
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
             }
             
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }
}

-(void)removeFromFavoutiteListOfUser
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        
        NSDictionary *params = @{@"user":self.userManager.user.userId,
                                 @"recipe":self.selectedRecipe.recipeId};
        
        [self.networkManager startGETRequestWithAPI:@"removefavourites/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             NSString *msg = [responseDictionary objectForKey:@"msg"];
             
             if ([errorCode isEqualToString:@"200"])
             {
                 if (self.isfromFavourites)
                 {
                     [self.delegate loadfavouritesDetails];
                     [self.navigationController popViewControllerAnimated:YES];
                 }
                 else
                 {
                     UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [errorAlert show];
                     [self.addToFavButton setImage:[UIImage imageNamed:@"addtofavortes"] forState:UIControlStateNormal];
                     self.selectedRecipe.recipeInfavourite = @"no";
                 }
             }
             else
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
             }
             
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }
}

#pragma mark Shopping List Web Service

-(void)addToShoppingListOfUser
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        
        NSDictionary *params = @{@"user":self.userManager.user.userId,
                                 @"recipe":self.selectedRecipe.recipeId};
        
        [self.networkManager startGETRequestWithAPI:@"addtolist/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             NSString *msg = [responseDictionary objectForKey:@"msg"];
             self.selectedRecipe.recipeShoppingListId = [responseDictionary objectForKey:@"list_id"];
             if ([errorCode isEqualToString:@"200"])
             {
                 if (self.isFromShoppingDetails)
                 {
                     [self.delegate loadShoppingList];
                     [self.navigationController popViewControllerAnimated:NO];
                 }
                 else
                 {
                     UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:[msg capitalizedString] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [errorAlert show];
                     [self.addToShoppingListButton setImage:[UIImage imageNamed:@"removefromshoplist"] forState:UIControlStateNormal];
                     self.selectedRecipe.recipeInShoppingList = @"yes";
                 }
             }
             else
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
             }
             
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }
}

-(void)removeFromShoppingListOfUser
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        
        NSDictionary *params = @{@"list":self.selectedRecipe.recipeShoppingListId,
                                 @"user":self.userManager.user.userId};

        
        [self.networkManager startGETRequestWithAPI:@"deletelist/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             NSString *msg = [responseDictionary objectForKey:@"msg"];
             
             if ([errorCode isEqualToString:@"200"])
             {
                 if (self.isFromShoppingDetails) {
                     [self.delegate loadShoppingList];
                     [self.navigationController popViewControllerAnimated:YES];
                 }
                 else
                 {
                     UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:[msg capitalizedString] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [errorAlert show];
                     [self.addToShoppingListButton setImage:[UIImage imageNamed:@"addtoshoplist"] forState:UIControlStateNormal];
                     self.selectedRecipe.recipeInShoppingList = @"no";
                 }

             }
             else
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
             }
             
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }
}


#pragma mark UIAlertViewDelegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            break;
        case 1:
            if (alertView.tag == 101)
            {
                [self removeFromFavoutiteListOfUser];
            }
            else if (alertView.tag == 102)
            {
                [self removeFromShoppingListOfUser];
            }
            else if (alertView.tag == 201)
            {
                [self addToFavoutiteListOfUser];

            }
            else if (alertView.tag == 202)
            {
                [self addToShoppingListOfUser];
            }
            break;
        default:
            break;
    }
}


#pragma mark CLRateRecipeViewDelegate Methods

-(void)postButtonTappedWithRating:(NSString *)rate
{
    [self removeBlackView];
    [self ViewZoomOutEffect:self.rateRecipeView];
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        
        NSDictionary *params = @{@"user":self.userManager.user.userId,
                                 @"recipe":self.selectedRecipe.recipeId,
                                 @"urating":rate};
        
        [self.networkManager startGETRequestWithAPI:@"rating/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSLog(@"%@",responseDictionary);
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             NSString *msg = [responseDictionary objectForKey:@"msg"];
             self.selectedRecipe.recipeRating = [NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"rating"]];
             self.selectedRecipe.recipeUserRating = rate;
             
             CLRecipeDetailsTableViewCell *cell = (CLRecipeDetailsTableViewCell *)[self.recipeDetailsTableView dequeueReusableCellWithIdentifier:@"CLRecipeDetailsTableViewCell"];
             [cell configureCellWithRecipeDetails:self.selectedRecipe];
             [self.recipeDetailsTableView reloadData];
             
             
             if ([errorCode isEqualToString:@"200"])
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Your rating has been saved successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
                 [self.delegate addedRecipeReviewAndRatings];
             }
             else
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
             }
             
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
        
    }
//    http://cloverapi.jumpcatch.com/api/rating/?user=19&recipe=68&urating=1
    
}

-(void)cancelButtonTapped
{
    [self removeBlackView];
    [self ViewZoomOutEffect:self.rateRecipeView];

}


#pragma mark CLWriteReviewAndUploadPictureViewDelegate methods

-(void)submitReviewButtonTappedWithReview:(NSString *)review
{
    [self removeBlackView];
    self.writeReviewViewAndUploadPictureView.hidden = YES;
//    http://cloverapi.jumpcatch.com/api/addreview/?user4&recipe=69&comment=asample%20comment
    if ([self isNetworkIsReachable])
    {
        [self ViewZoomOutEffect:self.writeReviewViewAndUploadPictureView];
        {
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.dimBackground = YES;
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = @"Loading...";
            [hud show:YES];
            
            
            NSDictionary *params = @{@"user":self.userManager.user.userId,
                                     @"recipe":self.selectedRecipe.recipeId,
                                     @"comment":review};
            
            [self.networkManager startGETRequestWithAPI:@"addreview/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
             {
                 [hud hide:YES];
                 NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
                 NSString *msg = [responseDictionary objectForKey:@"msg"];
                 
                 self.selectedRecipe.recipeCommentCount = [self.selectedRecipe.recipeCommentCount stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
                 
                 NSString *countStr = ([self.selectedRecipe.recipeCommentCount isEqualToString:@""])?@"0":self.selectedRecipe.recipeCommentCount;
                 int count = [countStr intValue];
                 
                 if ([errorCode isEqualToString:@"200"])
                 {
                     UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:[msg capitalizedString] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [errorAlert show];
                     count++;
                     self.selectedRecipe.recipeCommentCount = [NSString stringWithFormat:@"%d",count];
                     CLRecipeDetailsTableViewCell *cell = (CLRecipeDetailsTableViewCell *)[self.recipeDetailsTableView dequeueReusableCellWithIdentifier:@"CLRecipeDetailsTableViewCell"];
                     [cell configureCellWithRecipeDetails:self.selectedRecipe];
                     [self.recipeDetailsTableView reloadData];
                     [self.delegate addedRecipeReviewAndRatings];
                     
                 }
                 else
                 {
                     UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [errorAlert show];
                 }
                 
                 
             } andFailure:^(NSString *errorMessage) {
                 [hud hide:YES];
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [alert show];
             }];
            
        }
        }
}

-(void)writeReviewCancelButtonTapped
{
    [self removeBlackView];
    self.isPictureUploaded = NO;
    [self ViewZoomOutEffect:self.writeReviewViewAndUploadPictureView];
    [self changeBackgroundAndImageOfButton];
    [self changeBackgroundColorOfShare];

}

-(void)uploadPictureButtonTappedWith:(UIImage *)recipeImage andCaption:(NSString *)caption
{
    if (self.isPictureUploaded == NO && [caption isEqualToString:@""])
    {
        recipeImage = nil;
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"No Photo/Review to upload" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        [self removeBlackView];
        if ([self isNetworkIsReachable])
        {
            [self ViewZoomOutEffect:self.writeReviewViewAndUploadPictureView];

            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.dimBackground = YES;
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = @"Loading...";
            [hud show:YES];
            if (self.isPictureUploaded)
            {
                [self.networkManager uploadImage:recipeImage withUserID:nil WithSuccessHandler:^(NSArray *result, NSString *resultString, BOOL apiStatus) {
                    [hud hide:YES];
                    if (![caption isEmptyString])
                    {
                        [self addReviewPostedByUser:caption withPictureUrl:resultString];
                    }
                    else
                    {
                        [self addReviewPostedByUser:@"" withPictureUrl:resultString];
                    }
                } WithErrorHandler:^(NSError *error, NSString *errorMessage, BOOL apiStatus) {
                    [hud hide:YES];
                    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [errorAlert show];
                }];
                
            }
            else
            {
                [hud hide:YES];
                [self addReviewPostedByUser:caption withPictureUrl:@""];
            }
        }

    }
    
}

-(void)addReviewPostedByUser:(NSString *)review withPictureUrl:(NSString *)url
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading...";
    [hud show:YES];
    NSDictionary *params;
//    if (![url isEmptyString] && ![review isEmptyString])
//    {
        params = @{@"user":self.userManager.user.userId,
                   @"recipe":self.selectedRecipe.recipeId,
                   @"comment":review,
                   @"picture":url};

//    }
//    else if(![url isEmptyString] )
//    {
//        params = @{@"user":self.userManager.user.userId,
//                   @"recipe":self.selectedRecipe.recipeId,
//                   @"comment":@"",
//                   @"picture":url};
//    }
//    if (![review isEmptyString])
//    {
//        params = @{@"user":self.userManager.user.userId,
//                   @"recipe":self.selectedRecipe.recipeId,
//                   @"comment":review
//                   };
//    }
    [self.networkManager startGETRequestWithAPI:@"addpicturecomment/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
     {
         [hud hide:YES];
         NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
         NSString *msg = [responseDictionary objectForKey:@"msg"];
         
         if ([errorCode isEqualToString:@"200"])
         {
             NSDictionary *pictureDict = @{@"comment_user":[responseDictionary objectForKey:@"review_id"],
                                           @"comment_date":[responseDictionary objectForKey:@"review_date"],
                                           @"pic_comment":[responseDictionary objectForKey:@"review_content"],
                                           @"comment_picture":[responseDictionary objectForKey:@"review_picture"]};
             [self.uploadPhotoArray insertObject:pictureDict atIndex:0];
             [self.recipeDetailsTableView reloadData];
             UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Your Review has been uploaded successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [errorAlert show];
         }
         else
         {
             UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [errorAlert show];
         }
         
         
     } andFailure:^(NSString *errorMessage) {
         [hud hide:YES];
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
         [alert show];
     }];
}

- (void)getAllUploadedPicture
{
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        //http://cloverapi.jumpcatch.com/api/getpicturecomments/?recipe=67
        NSDictionary *params = @{@"recipe":self.selectedRecipe.recipeId};
        
        [self.networkManager startGETRequestWithAPI:@"getpicturecomments" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
        {
            [hud hide:YES];
            NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
            NSString *msg = [responseDictionary objectForKey:@"msg"];
            
            if ([errorCode isEqualToString:@"200"])
            {
                self.uploadPhotoArray = [responseDictionary objectForKey:@"picture_comments"];
                self.isPhotoUploaded = YES;
                self.selectedRecipe.recipePictureCount = [NSString stringWithFormat:@"%lu",(unsigned long)self.uploadPhotoArray.count];
                [self.recipeDetailsTableView reloadData];
            }
            else
            {
                UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [errorAlert show];
            }
        } andFailure:^(NSString *errorMessage)
        {
            [hud hide:YES];
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [errorAlert show];
            
        }];

    }
}

-(void)captureAPictureFromCamera
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }

}

-(void)loadImageFromCameraRoll
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }

}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = info[UIImagePickerControllerOriginalImage];
        self.writeReviewViewAndUploadPictureView.cameraImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.writeReviewViewAndUploadPictureView.cameraImageView.image = image;
        self.isPictureUploaded = YES;
    }
}
#pragma mark Adding/Removing BlackView

-(void)addBlackView
{
    if (!self.blackView)
    {
        self.blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.blackView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        UITapGestureRecognizer *blackViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(blackViewTapped)];
        [self.blackView addGestureRecognizer:blackViewTap];
        [self.view addSubview:self.blackView];
    }
    else
    {
        self.blackView.hidden = NO;
    }
}

-(void)addCalenderBlackView
{
    if (!self.calenderBlackView)
    {
        self.calenderBlackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.calenderBlackView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        UITapGestureRecognizer *blackViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(calenderBlackViewTapped)];
        [self.calenderBlackView addGestureRecognizer:blackViewTap];
        [self.view addSubview:self.calenderBlackView];
    }
    else
    {
        self.calenderBlackView.hidden = NO;
    }
}
-(void)addBlackBottomView
{
    if (!self.blackBottomView)
    {
        self.blackBottomView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height-40)];
        self.blackBottomView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        UITapGestureRecognizer *blackViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(bottomBlackViewTapped)];
        [self.blackBottomView addGestureRecognizer:blackViewTap];
        [self.view addSubview:self.blackBottomView];
    }
    else
    {
        self.blackBottomView.hidden = NO;
    }
}

-(void)removeBlackView
{
    [self.blackView removeFromSuperview];
    self.blackView = nil;
    [self.blackBottomView removeFromSuperview];
    self.blackBottomView = nil;

}

-(void)removeCalenderBlackView
{
    [self.calenderBlackView removeFromSuperview];
    self.calenderBlackView = nil;
}

#pragma mark CLRecipeViewCellDelegate methods

-(void)loadReviewViewControllerWithSelectedRecipe:(CLRecipe *)recipe
{
    self.isIngredientsSelected = NO;
    self.isMethodSelected = NO;
    [self.recipeDetailsTableView reloadData];
}

-(void)playButtonTappedWithRecipe:(CLRecipe *)selectedRecipe
{
    CLWebViewController *webVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLWebViewController"];
    webVC.urlString = selectedRecipe.recipeVideoUrl;
    [self.navigationController pushViewController:webVC animated:YES];
}


- (IBAction)mealPlanCancelButtonAction:(id)sender
{
    [self changeBackgroundOfAddToMealPlanButton];
    [self removeBlackView];
}


- (IBAction)mealPlanAddMealButtonAction:(id)sender
{
    [self changeBackgroundOfAddToMealPlanButton];
    [self removeBlackView];
    
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"yyyy-MM-dd"];
    NSString *selectedDate =[df stringFromDate:self.selectedDate];

    NSDictionary *params = @{@"recipe":self.selectedRecipe.recipeId,
                             @"user":self.userManager.user.userId,
                             @"mealtype":self.mealTimePopUpbutton.titleLabel.text,
                             @"mealdate":selectedDate};
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading...";
    [hud show:YES];
    [self.networkManager startGETRequestWithAPI:@"addtomeal/" andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
        [hud hide:YES];
        NSString *msg = [responseDictionary objectForKey:@"msg"];
        NSString *errorCode =[NSString stringWithFormat:@"%@", [responseDictionary objectForKey:@"errorcode"]];
        if ([errorCode isEqualToString:@"200"])
        {
            if (self.isfromFavourites)
            {
                [self.delegate loadMealPlannerView];
                [self.navigationController popViewControllerAnimated:YES];
            }
            else
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }
        

        
    } andFailure:^(NSString *errorMessage) {
        [hud hide:YES];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }];
}

- (IBAction)mealPlanSelectDateButtonTapped:(id)sender
{
    [self addCalenderBlackView];
    self.calenderView = [[[NSBundle mainBundle] loadNibNamed:@"CLCalenderView" owner:self options:nil] objectAtIndex:0];
    self.calenderView.center = self.view.center;
    self.calenderView.delegate = self;
    [self.view addSubview:self.calenderView];
    [self ViewZoomInEffect:self.calenderView];
}

-(void)mealPlanDateLabelTapped
{
    [self addCalenderBlackView];
    self.calenderView = [[[NSBundle mainBundle] loadNibNamed:@"CLCalenderView" owner:self options:nil] objectAtIndex:0];
    self.calenderView.center = self.view.center;
    self.calenderView.delegate = self;
    [self.view addSubview:self.calenderView];
    [self ViewZoomInEffect:self.calenderView];
}

- (IBAction)mealPlanSelectMealTimeButtonAction:(id)sender
{
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Breakfast",@"Lunch",@"Snacks",@"Dinner", nil];
    [actionSheet showInView:self.view];
}

-(void)mealPlanSelectMealTimeViewTapped
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Breakfast",@"Lunch",@"Snacks",@"Dinner", nil];
    [actionSheet showInView:self.view];
}

-(void)blackViewTapped
{
    [self ViewZoomOutEffect:self.writeReviewViewAndUploadPictureView];
    [self ViewZoomOutEffect:self.rateRecipeView];
    [self.blackView removeFromSuperview];
    self.blackView = nil;
    [self removeBlackView];
}

-(void)bottomBlackViewTapped
{
    self.socialShareView.hidden = YES;
    self.shareView.hidden = YES;
    self.mealPlannerView.hidden = YES;
    [self changeBackgroundOfAddToMealPlanButton];
    [self changeBackgroundColorOfShare];
    [self changeBackgroundAndImageOfButton];
    [self removeBlackView];

}

-(void)calenderBlackViewTapped
{
    [self.calenderBlackView removeFromSuperview];
    self.calenderBlackView = nil;
    [self ViewZoomOutEffect:self.calenderView];
}


#pragma mark CLCalenderViewDelegate Methods

-(void)doneButtonTappedWithSelectedDate:(NSString *)dateStr andDate:(NSDate *)date
{
    [self ViewZoomOutEffect:self.calenderView];
    [self removeCalenderBlackView];
    self.selectedDate = date;
    self.mealPlanDateLabel.text = dateStr;
}

#pragma mark UIActionSheet Delegate

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            if (actionSheet.tag == 123)
            {
                [self.sharedManager shareViaFBWithContent:[NSString stringWithFormat:@"%@",self.selectedRecipe.recipeVideoUrl] inViewController:self WithVideoType:@"Video"];
            }
            else
            {
                [self.mealTimePopUpbutton setTitle:@"Breakfast" forState:UIControlStateNormal];
            }
            break;
            
        case 1:
            if (actionSheet.tag == 123) {
                [self.sharedManager shareViaTwitterWithContent:[NSString stringWithFormat:@"%@",self.selectedRecipe.recipeVideoUrl] inViewController:self WithVideoType:@"Video"];
            }
            else
            {
                [self.mealTimePopUpbutton setTitle:@"Lunch" forState:UIControlStateNormal];
            }
            break;
            
        case 2:
            [self.mealTimePopUpbutton setTitle:@"Snacks" forState:UIControlStateNormal];
            break;
            
        case 3:
            [self.mealTimePopUpbutton setTitle:@"Dinner" forState:UIControlStateNormal];
            break;
            
        case 4:
            break;
            
        default:
            break;
    }
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
