//
//  CLRecipeDetailsTableViewCell.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 23/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLRecipeDetailsTableViewCell.h"
#import "UIImage+colors.h"
#import <UIImageView+AFNetworking.h>
#import "CLRecipe.h"


@implementation CLRecipeDetailsTableViewCell

- (void)awakeFromNib {
    // Initialization code
} 

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCellWithRecipeDetails: (CLRecipe *)recipe
{
    self.recipeTitlelabel.text = recipe.recipeTitle;

    self.contentView.frame = CGRectMake(0, 0, self.frame.size.width, 230) ;
    self.containerView.frame = CGRectMake(0, 0, self.frame.size.width, 230) ;
    
    CLRecipeView *recipeView = [[[NSBundle mainBundle] loadNibNamed:@"CLRecipeView" owner:self options:nil]objectAtIndex:0];
    recipeView.frame = CGRectMake(0, 0, self.frame.size.width,230);
    recipeView.topPlayButton.hidden = YES;
    [recipeView configureRecipeViewWithRecipe:recipe];
    recipeView.backgroundImageView.hidden = YES;
    recipeView.recipeNameLabel.hidden = YES;
    recipeView.reviewCountLabel.translatesAutoresizingMaskIntoConstraints = YES;
    recipeView.reviewCountLabel.frame = CGRectMake(recipeView.reviewCountLabel.frame.origin.x, 150, recipeView.reviewCountLabel.frame.size.width, recipeView.reviewCountLabel.frame.size.height);
    recipeView.starView.translatesAutoresizingMaskIntoConstraints = YES;
    recipeView.starView.frame = CGRectMake(self.containerView.frame.size.width-130, 140, recipeView.starView.frame.size.width, recipeView.starView.frame.size.height);
    recipeView.delegate = self;
    [self.containerView addSubview:recipeView];
}

#pragma mark CLRecipeViewDelegate Methods

-(void)loadReviewViewControllerWithSelectedRecipe:(CLRecipe *)recipe
{
    [self.delegate recipeReviewsLabelIsTappedWithRecipe:recipe];
}

-(void)playButtonTappedWithRecipe:(CLRecipe *)selectedRecipe
{
    [self.delegate playButtonTappedWithRecipe:selectedRecipe];
}

@end
