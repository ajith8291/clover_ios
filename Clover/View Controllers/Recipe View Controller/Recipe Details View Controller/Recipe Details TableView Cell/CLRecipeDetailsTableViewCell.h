//
//  CLRecipeDetailsTableViewCell.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 23/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLRecipe.h"
#import "CLRecipeView.h"

@protocol  CLRecipeDetailsTableViewCellDelegate<NSObject>

@optional
-(void)recipeReviewsLabelIsTappedWithRecipe:(CLRecipe *)recipe;
-(void)playButtonTappedWithRecipe:(CLRecipe *)recipe;
@end


@interface CLRecipeDetailsTableViewCell : UITableViewCell<CLRecipeViewDelegate>
@property (weak, nonatomic) IBOutlet UILabel *recipeTitlelabel;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property(nonatomic, weak) id<CLRecipeDetailsTableViewCellDelegate>delegate;

-(void)configureCellWithRecipeDetails: (CLRecipe *)recipe;
@end
