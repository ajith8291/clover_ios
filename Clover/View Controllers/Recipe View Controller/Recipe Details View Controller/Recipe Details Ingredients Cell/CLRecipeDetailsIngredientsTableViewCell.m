//
//  CLRecipeDetailsIngredientsTableViewCell.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 12/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLRecipeDetailsIngredientsTableViewCell.h"

@implementation CLRecipeDetailsIngredientsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

-(void)configureIngredientsCellWith:(NSDictionary *)ingredientsDict
{
    self.ingredientsQuantityLabel.text = [ingredientsDict objectForKey:@"quantity"];
    self.ingredientsNameLabel.text = [NSString stringWithFormat:@"%@",[ingredientsDict objectForKey:@"ingredient"]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
