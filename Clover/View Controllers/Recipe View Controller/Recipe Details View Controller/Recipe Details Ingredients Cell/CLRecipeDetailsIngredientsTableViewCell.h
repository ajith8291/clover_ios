//
//  CLRecipeDetailsIngredientsTableViewCell.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 12/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLRecipeDetailsIngredientsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *ingredientsQuantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *ingredientsNameLabel;

-(void)configureIngredientsCellWith:(NSDictionary *)ingredientsDict;
@end
