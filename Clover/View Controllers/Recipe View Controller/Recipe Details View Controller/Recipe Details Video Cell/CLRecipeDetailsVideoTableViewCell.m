//
//  CLRecipeDetailsVideoTableViewCell.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 12/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLRecipeDetailsVideoTableViewCell.h"
#import "CLConstants.h"
#import <UIImageView+AFNetworking.h>
#import "UIImage+colors.h"

@implementation CLRecipeDetailsVideoTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCellWithRecipe:(CLRecipe *)recipe
{
    UIImageView *recipeImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 5, self.frame.size.width-26, 183)];
    recipeImageView.backgroundColor = CL_BLUE_COLOR;
    NSString *imgUrl = [recipe.recipeImage stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%d",600]];
    imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%d",370]];
    
    UIImage *placeholderImg = [UIImage imageWithColor:[UIColor lightGrayColor]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
    
    __weak UIImageView *weakImg = recipeImageView;
    
    [recipeImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = image;
             weakImg.alpha = 0.0;
             
             [UIView animateWithDuration:1.0 animations:^{
                 weakImg.alpha = 1.0;
             }];
         });
         
         
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
         
     }];
    UIView *blackView =[[UIView alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width-26, 183)];
    blackView.backgroundColor = [UIColor blackColor];
    blackView.alpha = 0.4;
    
    UIImageView *playImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"play"]];
    playImageView.frame = CGRectMake(self.frame.size.width/2-108/2, 55, 75, 75);
    
    recipeImageView.layer.cornerRadius = 3.0;
    [recipeImageView addSubview:blackView];
    
    UITapGestureRecognizer *playTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(playImageViewTapped)];
    
    [self.containerView addSubview:recipeImageView];
    [self.containerView addGestureRecognizer:playTap];
    [self.containerView addSubview:playImageView];
}

#pragma mark UITap/UIButton Action Methods

-(void)playImageViewTapped
{
    [self.delegate videoPlayButtonTapped];
}

@end
