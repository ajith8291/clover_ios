//
//  CLRecipeDetailsVideoTableViewCell.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 12/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLRecipe.h"

@protocol CLRecipeDetailsVideoTableViewCellDelegate <NSObject>

-(void)videoPlayButtonTapped;

@end

@interface CLRecipeDetailsVideoTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *containerView;

-(void)configureCellWithRecipe:(CLRecipe *)recipe;

@property (nonatomic, weak) id<CLRecipeDetailsVideoTableViewCellDelegate>delegate;
@end
