//
//  CLRecipeDetailsTableViewMethodCell.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 24/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLRecipe.h"

@protocol CLRecipeDetailsTableViewMethodCellDelegate <NSObject>

@optional
-(void)videoPlayButtonTapped;

@end

@interface CLRecipeDetailsTableViewMethodCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *recipeMethodDetailsLabel;
@property (weak, nonatomic) IBOutlet UILabel *StepsLabel;

@property (nonatomic, weak) id<CLRecipeDetailsTableViewMethodCellDelegate>delegate;

-(void)configureCellWithRecipeDetails :(NSString *)method;
@end
