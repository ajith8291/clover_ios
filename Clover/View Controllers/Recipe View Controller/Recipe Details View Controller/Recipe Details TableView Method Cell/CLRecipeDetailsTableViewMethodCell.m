//
//  CLRecipeDetailsTableViewMethodCell.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 24/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLRecipeDetailsTableViewMethodCell.h"
#import "CLConstants.h"
#import "UIImage+colors.h"
#import <UIImageView+AFNetworking.h>

@implementation CLRecipeDetailsTableViewMethodCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCellWithRecipeDetails :(CLRecipe *)recipe WithFrame:(CGRect)rect
{
    self.recipeMethodDetailsLabel.text = @"";// recipe.recipeMethodToPrepare;
}

@end
