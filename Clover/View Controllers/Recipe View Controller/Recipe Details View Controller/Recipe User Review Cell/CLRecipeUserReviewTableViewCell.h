//
//  CLRecipeUserReviewTableViewCell.h
//  Clover
//
//  Created by Ajith Kumar on 21/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLUploadPicture.h"

@interface CLRecipeUserReviewTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *profileImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *uploadedImageView;
@property (weak, nonatomic) IBOutlet UILabel *reviewLabel;

-(void)configureCellWithData:(CLUploadPicture *)picture;
@end
