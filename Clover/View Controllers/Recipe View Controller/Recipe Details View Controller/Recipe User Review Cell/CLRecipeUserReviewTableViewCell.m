//
//  CLRecipeUserReviewTableViewCell.m
//  Clover
//
//  Created by Ajith Kumar on 21/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLRecipeUserReviewTableViewCell.h"
#import <UIImageView+AFNetworking.h>
#import "NSString+DataValidator.h"
#import "UIImage+colors.h"

@implementation CLRecipeUserReviewTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.profileImageView.layer.cornerRadius = self.profileImageView.frame.size.width/2;
    self.profileImageView.clipsToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCellWithData:(CLUploadPicture *)picture
{
    self.userNameLabel.text = picture.user.userName;
    self.dateLabel.text = [picture.uploadDate getDateFromString];
    self.reviewLabel.text = picture.uploadPictureCaption;
    if ([picture.uploadPicture isEmptyString])
    {
        self.uploadedImageView.hidden = YES;
        [self uploadUserImageView:picture.user.profileURL];
    }
    else
    {
        self.uploadedImageView.hidden = NO;

        NSString *imgUrl = [picture.uploadPicture stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%d",65]];
        imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%d",65]];
        
        UIImage *placeholderImg = [UIImage imageWithColor:[UIColor lightGrayColor]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
        
        __weak UIImageView *weakImg = self.uploadedImageView;
        
        self.uploadedImageView.contentMode = UIViewContentModeCenter;
        
        [self.uploadedImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             CGSize newSize = CGSizeMake(65.0f, 65.0f);

             UIGraphicsBeginImageContext(newSize);
             [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
             UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
             UIGraphicsEndImageContext();

             
             dispatch_async(dispatch_get_main_queue(), ^{
                 weakImg.image = newImage;
                 weakImg.alpha = 0.0;
                 
                 [UIView animateWithDuration:1.0 animations:^{
                     weakImg.alpha = 1.0;
                 }];
             });
             
             
         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
             
             
         }];
        // User ImageView
        
        [self uploadUserImageView:picture.user.profileURL];
    }
}

-(void)uploadUserImageView:(NSString *)profilrUrl
{ 
    NSString *imgUrl = [profilrUrl stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%d",90]];
    imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%d",90]];
    
    UIImage *placeholderImg = [UIImage imageNamed:@"userProfileNew"];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
    
    __weak UIImageView *weakImg = self.profileImageView;
    
    [self.profileImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         CGSize newSize = CGSizeMake(45.0f, 45.0f);
         
         UIGraphicsBeginImageContext(newSize);
         [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
         UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
         UIGraphicsEndImageContext();
         
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = newImage;
             weakImg.alpha = 0.0;
             
             [UIView animateWithDuration:1.0 animations:^{
                 weakImg.alpha = 1.0;
             }];
         });
         
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
         
     }];
}

@end
