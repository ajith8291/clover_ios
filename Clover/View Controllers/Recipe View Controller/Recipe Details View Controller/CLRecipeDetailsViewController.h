//
//  CLRecipeDetailsViewController.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 23/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLBaseViewController.h"
#import "CLRecipe.h"

@protocol  CLRecipeDetailsViewControllerDelegate<NSObject>

@optional

-(void)addedRecipeReviewAndRatings;
- (void)loadShoppingList;
-(void)loadfavouritesDetails;
-(void) loadMealPlannerView;
- (void) addAndRemoveMealPlannerButtonTappedWithRecipe:(CLRecipe *)recipe;

@end

@interface CLRecipeDetailsViewController : CLBaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *recipeDetailsTableView;
@property (nonatomic, strong) CLRecipe *selectedRecipe;

@property (weak, nonatomic) IBOutlet UIView *shareView;
@property (weak, nonatomic) IBOutlet UIView *ratingView;
@property (weak, nonatomic) IBOutlet UIView *addPictureView;

@property (weak, nonatomic) IBOutlet UIButton *addToFavButton;
@property (weak, nonatomic) IBOutlet UIButton *addToShoppingListButton;
@property (weak, nonatomic) IBOutlet UIButton *addToMealPlannerButton;
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (weak, nonatomic) IBOutlet UIButton *moreButton;
@property (weak, nonatomic) IBOutlet UIView *seperator1;
@property (weak, nonatomic) IBOutlet UIView *seperator2;
@property (weak, nonatomic) IBOutlet UIView *seperator3;
@property (weak, nonatomic) IBOutlet UIView *seperator4;

@property (nonatomic, assign) BOOL isPictureUploaded;
@property (nonatomic, assign) BOOL isPlayButtonTapped;
@property (nonatomic, assign) BOOL isfromFavourites;
@property (nonatomic, assign) BOOL isUserReviewSelected;
@property (nonatomic, assign) BOOL isFromShoppingDetails;
@property (nonatomic, assign) BOOL isFromAddToMealPLanner;

@property (nonatomic, strong) NSString *recipeID;
@property (nonatomic, weak) id<CLRecipeDetailsViewControllerDelegate>delegate;
@property (weak, nonatomic) IBOutlet UIView *socialShareView;
@property (weak, nonatomic) IBOutlet UIView *shareFBView;
@property (weak, nonatomic) IBOutlet UIView *shareTwitterView;


//Meal Planner view property

@property (weak, nonatomic) IBOutlet UIView *mealPlannerView;

@property (weak, nonatomic) IBOutlet UIButton *mealPlanSelecteDateButton;
@property (weak, nonatomic) IBOutlet UILabel *mealPlanDateLabel;
@property (weak, nonatomic) IBOutlet UIView *mealPlanSelecteMealTimeView;
@property (weak, nonatomic) IBOutlet UIButton *mealTimePopUpbutton;
@property (weak, nonatomic) IBOutlet UIButton *mealPlanCancelButton;
@property (weak, nonatomic) IBOutlet UIButton *mealPlanAddMealButton;

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIView *bottomTabView;
@property (weak, nonatomic) IBOutlet UIButton *addAndRemoveMealPlannerButton;
@property (nonatomic, strong) NSMutableArray *addToMealPlannerArray;

@end


