//
//  CLRecipeViewController.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 05/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLBaseViewController.h"

@protocol  CLRecipeViewControllerDelegate<NSObject>

@optional

-(void)loadFavouritesWithNewDetails;
-(void)loadShoppingListWithNewDetails;

@end

@interface CLRecipeViewController : CLBaseViewController<UICollectionViewDataSource,UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *recipeCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *listViewButton;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (nonatomic, strong) NSMutableArray *recipeArray;
@property (nonatomic, strong) NSMutableDictionary *filterOptionDictionary;
@property (nonatomic, assign) BOOL isFromShoppingList;
@property (nonatomic, assign) BOOL isFromFavourites;
@property (nonatomic, strong) NSString *searchText;

@property (nonatomic, weak) id <CLRecipeViewControllerDelegate> delegate;
@end
