//
//  CLRecipeCollectionViewListCell.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 05/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLRecipeCollectionViewListCell.h"
#import "UIImage+colors.h"
#import <UIImageView+AFNetworking.h>

@implementation CLRecipeCollectionViewListCell

-(void)awakeFromNib
{
    self.favImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *favtap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(favImageViewTapped)];
    [self.favImageView addGestureRecognizer:favtap];
    self.contentView.translatesAutoresizingMaskIntoConstraints = YES;
    self.contentView.layer.cornerRadius = 2.0;
}

-(void)configureListCellWithRecipeDetails :(CLRecipe *)recipe
{
    CLRecipeView *recipeView = [[[NSBundle mainBundle] loadNibNamed:@"CLRecipeView" owner:self options:nil]objectAtIndex:0];
    recipeView.frame = CGRectMake(0, 0, self.frame.size.width,225);
    [recipeView configureRecipeViewWithRecipe:recipe];
    recipeView.delegate = self;
    recipeView.backgroundColor = [UIColor clearColor];
    recipeView.backgroundImageView.hidden = YES;
    recipeView.contentView.backgroundColor = [UIColor clearColor];
    [self.contentView addSubview:recipeView];
}

#pragma mark UIButton/ Tap Action Methods

-(void)favImageViewTapped
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Would you like to remove this recipe from your favourites" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alertView.tag = 101;
    [alertView show];
}


#pragma mark UIAlertViewDelegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            break;
        case 1:
            if (alertView.tag == 101)
            {
                [self.delegate removeRecipeFromFavouriteList:self.selectedRecipe];
            }
            
            break;
        default:
            break;
    }
}

#pragma mark CLRecipeViewDelegate Methods

-(void)loadReviewViewControllerWithSelectedRecipe:(CLRecipe *)recipe
{
    [self.delegate recipeReviewsLabelIsTappedWithRecipe:recipe];
}

@end
