//
//  CLRecipeCollectionViewLGridCell.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 05/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLRecipe.h"
#import "CLRecipeGridView.h"

@protocol  CLRecipeCollectionViewLGridCellDelegate<NSObject>

@optional

-(void)gridViewPlayButtonTappedWithRecipe:(CLRecipe *)selectedRecipe;
-(void)gridViewSelectedRecipe:(CLRecipe *)selectedrecipe;
- (void) removeFromMealPlannerWithRecipe :(CLRecipe *)recipe;
@end

@interface CLRecipeCollectionViewLGridCell : UICollectionViewCell<CLRecipeGridViewDelegate>


-(void)configureGridCellWithRecipeDetails:(CLRecipe *)recipe;
-(void)configureGridCellWithRecipeDetails:(CLRecipe *)recipe WithBool:(BOOL)flag;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;

@property(nonatomic, weak) id<CLRecipeCollectionViewLGridCellDelegate>delegate;
@end
