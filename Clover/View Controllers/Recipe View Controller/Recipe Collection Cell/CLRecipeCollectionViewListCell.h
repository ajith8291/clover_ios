//
//  CLRecipeCollectionViewListCell.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 05/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLRecipe.h"
#import "CLRecipeView.h"

@protocol  CLRecipeCollectionViewListCellDelegate<NSObject>

@optional
-(void)removeRecipeFromFavouriteList:(CLRecipe *)favRecipe;
-(void)recipeReviewsLabelIsTappedWithRecipe:(CLRecipe *)recipe;
@end

@interface CLRecipeCollectionViewListCell : UICollectionViewCell<UIAlertViewDelegate,CLRecipeViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *favImageView;

@property (nonatomic, strong) CLRecipe *selectedRecipe;
-(void)configureListCellWithRecipeDetails :(CLRecipe *)recipe;

@property (nonatomic, weak) id<CLRecipeCollectionViewListCellDelegate>delegate;
@end
