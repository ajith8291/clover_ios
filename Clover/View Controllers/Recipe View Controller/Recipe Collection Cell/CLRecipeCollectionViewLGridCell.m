//
//  CLRecipeCollectionViewLGridCell.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 05/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLRecipeCollectionViewLGridCell.h"
#import "UIImage+colors.h"
#import <UIImageView+AFNetworking.h>
#import "CLRecipeGridView.h"
#import "CLRecipe.h"
#import "CLConstants.h"


@interface CLRecipeCollectionViewLGridCell ()

@property (nonatomic, strong) CLRecipe *selectedRecipe;

@end

@implementation CLRecipeCollectionViewLGridCell

-(void)awakeFromNib
{
//    UILongPressGestureRecognizer *longPressTap = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(addToMealPLannerArray:)];
//    [longPressTap setMinimumPressDuration:1.0];
//    [self addGestureRecognizer:longPressTap];
}

-(void)configureGridCellWithRecipeDetails:(CLRecipe *)recipe
{
    self.selectedRecipe = recipe;
    self.contentView.frame = CGRectMake(0, 0, 145, 125);
    CLRecipeGridView *recipeGridView = [[[NSBundle mainBundle] loadNibNamed:@"CLRecipeGridView" owner:self options:nil]objectAtIndex:0];
    recipeGridView.frame = CGRectMake(0, 3, recipeGridView.frame.size.width,recipeGridView.frame.size.height);
    recipeGridView.delegate = self;
    [recipeGridView configureRecipeDetailsWithRecipe:recipe];
    [self.contentView addSubview:recipeGridView];
    
}

-(void)configureGridCellWithRecipeDetails:(CLRecipe *)recipe WithBool:(BOOL)flag
{
    self.selectedRecipe = recipe;
    self.contentView.frame = CGRectMake(0, 0, 145, 125);
    CLRecipeGridView *recipeGridView = [[[NSBundle mainBundle] loadNibNamed:@"CLRecipeGridView" owner:self options:nil]objectAtIndex:0];
    recipeGridView.frame = CGRectMake(0, 3, recipeGridView.frame.size.width,recipeGridView.frame.size.height);
    recipeGridView.delegate = self;
    [recipeGridView configureRecipeDetailsWithRecipe:recipe WithBool:flag];
    [self.contentView addSubview:recipeGridView];
    
}

#pragma mark UIGesture Methods

-(void)addToMealPLannerArray :(UILongPressGestureRecognizer *)sender
{
//    if (sender.state == UIGestureRecognizerStateEnded)
//    {
//        self.layer.borderWidth = 2.0;
//        self.layer.borderColor = [[UIColor clearColor] CGColor];
//        [self.delegate gridViewSelectedRecipe:self.selectedRecipe];
//    }
//    else if (sender.state == UIGestureRecognizerStateBegan)
//    {
//        self.layer.borderWidth = 2.0;
//        self.layer.borderColor = [CL_BLUE_COLOR CGColor];
//    }
}

#pragma mark UIButton Action Methods

- (IBAction)removeFromMealPlannerButtonAction:(id)sender
{
    [self.delegate removeFromMealPlannerWithRecipe:self.selectedRecipe];
}


#pragma mark CLRecipeGridViewDelegate Methods


-(void)playButtonTappedWithRecipe:(CLRecipe *)recipe
{
    [self.delegate gridViewPlayButtonTappedWithRecipe:recipe];
}

-(void)removeRecipeFromTheList:(CLRecipe *)recipe
{
    [self.delegate removeFromMealPlannerWithRecipe:recipe];

}

@end
