//
//  ALFilterSearchTableViewCell.m
//  Clover
//
//  Created by Ajith Kumar on 16/04/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLFilterSearchTableViewCell.h"

@implementation CLFilterSearchTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.searchTextField.autocorrectionType =UITextAutocorrectionTypeNo;
    self.searchTextField.leftViewMode = UITextFieldViewModeAlways;
    self.searchTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 2, 0)];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


//#pragma mark UITextFieldDelegate Methods
//
//-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
//{
//    
//    return YES;
//}
//
//-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
//{
//    [textField resignFirstResponder];
//    return YES;
//}
//
//-(BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    [textField resignFirstResponder];
//    return YES;
//}
//
//-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
//{
//    NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
//    [self.delegate searchFieldTextDidChage:resultingString];
//    return YES;
//}

@end
