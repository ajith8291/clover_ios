  //
//  CLRecipeFilterViewController.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 01/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLRecipeFilterViewController.h"
#import "CLRecipeFilterTableViewCell.h"
#import "CLConstants.h"
#import "CLRecipeViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "CLIngredientsFilterView.h"
#import "CLFilterSearchTableViewCell.h"

@interface CLRecipeFilterViewController ()<CLIngredientsFilterViewDelegate,CLFilterSearchTableViewCellDelegate,UITextFieldDelegate>
{
    CGPoint keyboardOffset;
}
@property (nonatomic, strong) NSMutableDictionary *mealTimeDictionary;
@property (nonatomic, strong) NSMutableDictionary *cuisinesDictionary;
@property (nonatomic, strong) NSMutableDictionary *DiataryRequirementsDictionary;
@property (nonatomic, strong) NSMutableDictionary *ingredientDictionary;
@property (nonatomic, strong) NSMutableDictionary *preparationTimeDictionary;
@property (nonatomic, strong) NSMutableDictionary *filterOptionDictionary;

@property(nonatomic) BOOL isMealTimeSelected;
@property(nonatomic) BOOL isCuisinesSelected;
@property(nonatomic) BOOL isDietarySelected;
@property(nonatomic) BOOL isIngredientsSelected;
@property(nonatomic) BOOL isPreparingTimeSelected;
@property(nonatomic) BOOL isDataLoaded;

@property(nonatomic, strong) NSMutableArray *ingredientArray;
@property(nonatomic, strong) NSMutableArray *ingredientIDArray;
@property (nonatomic, strong) CLIngredientsFilterView *ingredientsFilterView;
@property (nonatomic, strong) UIView *blackView;
@property (nonatomic, strong) NSString *searchText;
@end

@implementation CLRecipeFilterViewController

{
    UIButton *dropDownButton;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    self.filterOptionDictionary = [NSMutableDictionary new];
    NSMutableArray *mealTimeArray = [NSMutableArray new];
    NSMutableArray *cuisineArray = [NSMutableArray new];
    NSMutableArray *diatryArray = [NSMutableArray new];
    NSMutableArray *ingredientsArray = [NSMutableArray new];
    NSMutableArray *preparationTimeArray = [NSMutableArray new];
    
    [self.filterOptionDictionary setObject:mealTimeArray forKey:@"mealtime"];
    [self.filterOptionDictionary setObject:cuisineArray forKey:@"cuisines"];
    [self.filterOptionDictionary setObject:diatryArray forKey:@"dietry"];
    [self.filterOptionDictionary setObject:ingredientsArray forKey:@"ingredient"];
    [self.filterOptionDictionary setObject:preparationTimeArray forKey:@"cookingtime"];
    
    
    // Do any additional setup after loading the view.
    self.navigationView.navTitleTextLabel.text = @"Search Recipes";
    self.navigationView.navLogoImageView.hidden = YES;
    self.navigationView.navSearchButton.hidden = YES;
    self.navigationView.navBackButton.hidden = YES;
    self.navigationView.navEditButton.hidden = YES;
    self.navigationView.navCartButton.hidden = YES;
    self.submitView.layer.cornerRadius = 3.0;
    self.ingredientArray = [NSMutableArray new];
    
    UINib *nib = [UINib nibWithNibName:@"CLRecipeFilterTableViewCell" bundle:nil];
    
    [self.recipeFilterTableView registerNib:nib forCellReuseIdentifier:@"CLRecipeFilterTableViewCell"];
    UITapGestureRecognizer *submitTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(submitViewTapped)];
    [self.submitView addGestureRecognizer:submitTap];
    self.submitView.hidden = YES;
    
    if (self.networkManager.filterDictionary.count == 0) {
        [self callWebServiceToLoadRecipeFilter];
    }
    else
    {
        [self loadFilterDictionaryLocally:self.networkManager.filterDictionary];
    }
    [self.recipeFilterTableView setShowsVerticalScrollIndicator:NO];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.recipeFilterTableView reloadData];

    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)callWebServiceToLoadRecipeFilter
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        [self.networkManager startGETRequestWithAPI:@"cloverfilters" andParameters:nil withSuccess:^(NSDictionary *responseDictionary) {
            [hud hide:YES];
            
            [self loadFilterDictionaryLocally:responseDictionary];
            self.networkManager.filterDictionary = [NSDictionary dictionaryWithDictionary:responseDictionary];
            
        } andFailure:^(NSString *errorMessage) {
            [hud hide:YES];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [alert show];
        }];
    }

}

-(void)loadFilterDictionaryLocally:(NSDictionary *)responseDictionary;
{
    self.filterDictionary = [NSMutableDictionary dictionaryWithDictionary:responseDictionary];
    [self.filterDictionary removeObjectForKey:@"mealtype"];
    self.mealTimeDictionary = [[self.filterDictionary objectForKey:@"mealtime"] mutableCopy];
    [self.mealTimeDictionary removeObjectForKey:@"menuorder"];
    
    self.cuisinesDictionary = [[self.filterDictionary objectForKey:@"cuisines"] mutableCopy];
    [self.cuisinesDictionary removeObjectForKey:@"menuorder"];
    
    self.DiataryRequirementsDictionary = [[self.filterDictionary objectForKey:@"dietry"] mutableCopy];
    [self.DiataryRequirementsDictionary removeObjectForKey:@"menuorder"];
    
    self.ingredientDictionary = [[self.filterDictionary objectForKey:@"ingredient"] mutableCopy];
    [self.ingredientDictionary removeObjectForKey:@"menuorder"];
    
    self.preparationTimeDictionary = [[self.filterDictionary objectForKey:@"cookingtime"] mutableCopy];
    [self.preparationTimeDictionary removeObjectForKey:@"menuorder"];
    self.submitView.hidden = NO;
    self.isDataLoaded = YES;
    [self.recipeFilterTableView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self.recipeFilterTableView reloadData];
}


#pragma mark UITableViewDelegate/ Datasource mathods

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
        // Create an instance of cell
    if (indexPath.section == 5) {
        CLFilterSearchTableViewCell *cell = (CLFilterSearchTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CLFilterSearchTableViewCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.searchTextField.delegate = self;
        cell.delegate = self;
        return cell;
    }
    CLRecipeFilterTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"CLRecipeFilterTableViewCell"];

    cell.categoryNameLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:17.0];
    NSArray *mealTimeArray = [self.mealTimeDictionary objectForKey:@"data"];
    NSArray *cuisineArray = [self.cuisinesDictionary objectForKey:@"data"];
    NSArray *dietaryArray = [self.DiataryRequirementsDictionary objectForKey:@"data"];
    NSArray *preparingTimeArray = [self.preparationTimeDictionary objectForKey:@"data"];
    BOOL isThereInArrayList = NO;

    switch (indexPath.section)
    {
        case 0:
            cell.categoryNameLabel.text = mealTimeArray[indexPath.row];
            if ([[self.filterOptionDictionary objectForKey:@"mealtime"]  containsObject:cell.categoryNameLabel.text])
            {
                [cell.tickImageView setImage:[UIImage imageNamed:@"tickblue"]];
            }
            else
            {
                [cell.tickImageView setImage:[UIImage imageNamed:@"tiick"]];
            }
            break;
        case 1:
            cell.categoryNameLabel.text = cuisineArray[indexPath.row];
            if ([[self.filterOptionDictionary objectForKey:@"cuisines"]  containsObject:cell.categoryNameLabel.text])
            {
                [cell.tickImageView setImage:[UIImage imageNamed:@"tickblue"]];
            }
            else
            {
                [cell.tickImageView setImage:[UIImage imageNamed:@"tiick"]];
            }
            break;
        case 2:
            cell.categoryNameLabel.text = dietaryArray[indexPath.row];
            if ([[self.filterOptionDictionary objectForKey:@"dietry"] containsObject:cell.categoryNameLabel.text])
            {
                [cell.tickImageView setImage:[UIImage imageNamed:@"tickblue"]];
            }
            else
            {
                [cell.tickImageView setImage:[UIImage imageNamed:@"tiick"]];
            }
            break;
        case 3:
            cell.categoryNameLabel.text = [self.ingredientArray[indexPath.row] objectForKey:@"name"];
            for (int i = 0; i<self.ingredientArray.count; i++)
            {
                NSString *name = [[self.ingredientArray objectAtIndex:i] objectForKey:@"name"];
                if ([name isEqualToString:cell.categoryNameLabel.text])
                {
                    [cell.tickImageView setImage:[UIImage imageNamed:@"tickblue"]];
                    isThereInArrayList = YES;
                }
            }
            
            if (!isThereInArrayList)
            {
                [cell.tickImageView setImage:[UIImage imageNamed:@"tickgrey"]];
            }

            break;
        case 4:
            cell.categoryNameLabel.text = preparingTimeArray[indexPath.row];
            if ([[self.filterOptionDictionary objectForKey:@"cookingtime"] containsObject:cell.categoryNameLabel.text])
            {
                [cell.tickImageView setImage:[UIImage imageNamed:@"tickblue"]];
            }
            else
            {
                [cell.tickImageView setImage:[UIImage imageNamed:@"tiick"]];
            }
            break;
        default:
            break;
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    
//    NSArray *headerTitleArray = self.filterDictionary.allKeys;
//    NSLog(@"%@",self.filterDictionary);
//    NSLog(@"%@", [self.filterDictionary objectForKey:[headerTitleArray objectAtIndex:section]]);
//    
//    if ([self.indexArray containsObject:[headerTitleArray objectAtIndex:section]]) {
//        if (![[self.filterDictionary objectForKey:[headerTitleArray objectAtIndex:section]] isKindOfClass:[NSDictionary class]])
//        {
//            return 0;
//        }
//        return [[[self.filterDictionary objectForKey:[headerTitleArray objectAtIndex:section]] allKeys] count ];    }
//    else
//    {
//        return 0;
//    }
    
  
 
    
    
    
    
    
    if (section == 0) {
        if (!self.isMealTimeSelected) {
            return 0;
        }
        else
        {
            return [[self.mealTimeDictionary objectForKey:@"data"] count];
        }
    }
    else if (section == 1) {
        if (!self.isCuisinesSelected) {
            return 0;
        }
        else
        {
            return [[self.cuisinesDictionary objectForKey:@"data"] count];
        }
    }
    else if (section == 2) {
        if (!self.isDietarySelected) {
            return 0;
        }
        else
        {
            return [[self.DiataryRequirementsDictionary objectForKey:@"data"] count];
        }
    }
    else if (section == 3) {
        if (!self.isIngredientsSelected) {
            return 0;
        }
        else
        {
            return self.ingredientArray.count;
        }
    }
    else if (section == 4) {
        if (!self.isPreparingTimeSelected) {
            return 0;
        }
        else
        {
            return [[self.preparationTimeDictionary objectForKey:@"data"] count];
        }
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 5) {
        return 1;
    }
    return 62;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView;
{
    if (self.isDataLoaded) {
        return self.filterDictionary.count+1;
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.section == 5) {
        return 60.0;
    }
    return 40.0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    /* Create custom view to display section... */
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 62)];
    headerView.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0];
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 12, tableView.frame.size.width, 50)];
    [view setBackgroundColor:[UIColor whiteColor]];
    headerView.tag = section;
    
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(52,0,215, 50)];
    titleLabel.textColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
    titleLabel.backgroundColor = [UIColor clearColor];
    titleLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:20.0];
    
    UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(2, 2, 46, 45)];
    imageView.contentMode = UIViewContentModeCenter;
    UIImageView *plusImageView = [[UIImageView alloc] init];
    plusImageView.userInteractionEnabled = YES;
   dropDownButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-60, 15, 23, 23)];
    
    UIView *bottomSeperatorView = [[UIView alloc] initWithFrame:CGRectMake(13, 50, tableView.frame.size.width-26, 2)];
    bottomSeperatorView.backgroundColor = [UIColor colorWithRed:192.0/255.0 green:192.0/255.0 blue:192.0/255.0 alpha:1.0];
    //Tap action for header Viewt
    

    UITapGestureRecognizer *mealTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mealTimeSectionSelected)];
    UITapGestureRecognizer *cuisineTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cuisinesSectionSelected)];
    UITapGestureRecognizer *dietaryTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dietarySectionSelected)];
    UITapGestureRecognizer *showIngredientTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showIngredientsViewPopUp)];
    UITapGestureRecognizer *prepareTimeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(preparingTimeSectionSelected)];
    
    UITapGestureRecognizer *ingredientTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(ingredientsSectionSelected)];

    [plusImageView addGestureRecognizer:showIngredientTap];
    
    NSString *titleString;
    switch (section) {
        case 0:
            if (self.isMealTimeSelected) {
                [dropDownButton setImage:[UIImage imageNamed:@"arrowtop"] forState:UIControlStateNormal];
                bottomSeperatorView.hidden = NO;
            }
            else
            {
                [dropDownButton setImage:[UIImage imageNamed:@"arrowdown"] forState:UIControlStateNormal];
                bottomSeperatorView.hidden = YES;
            }
            titleString = @"Meal Time";
            [imageView setImage:[UIImage imageNamed:@"mealtime"]];
            [dropDownButton addTarget:self action:@selector(mealTimeSectionSelected) forControlEvents:UIControlEventTouchDown];
            [headerView addGestureRecognizer:mealTap];
            break;
        case 1:
            if (self.isCuisinesSelected) {
                [dropDownButton setImage:[UIImage imageNamed:@"arrowtop"] forState:UIControlStateNormal];
                bottomSeperatorView.hidden = NO;
            }
            else
            {
                [dropDownButton setImage:[UIImage imageNamed:@"arrowdown"] forState:UIControlStateNormal];
                bottomSeperatorView.hidden = YES;
            }
            titleString = @"Cuisine";
            [imageView setImage:[UIImage imageNamed:@"cuisines"]];
            [dropDownButton addTarget:self action:@selector(cuisinesSectionSelected) forControlEvents:UIControlEventTouchDown];
            [headerView addGestureRecognizer:cuisineTap];
            break;
        case 2:
            if (self.isDietarySelected) {
                [dropDownButton setImage:[UIImage imageNamed:@"arrowtop"] forState:UIControlStateNormal];
                bottomSeperatorView.hidden = NO;
            }
            else
            {
                [dropDownButton setImage:[UIImage imageNamed:@"arrowdown"] forState:UIControlStateNormal];
                bottomSeperatorView.hidden = YES;
            }
            titleString = @"Dietary Requirements";
            [imageView setImage:[UIImage imageNamed:@"mealcontents.png"]];
            [dropDownButton addTarget:self action:@selector(dietarySectionSelected) forControlEvents:UIControlEventTouchDown];
            [headerView addGestureRecognizer:dietaryTap];
            break;
        case 3:
            if (self.isIngredientsSelected) {
                [dropDownButton setImage:[UIImage imageNamed:@"arrowtop"] forState:UIControlStateNormal];
            }
            else
            {
                [dropDownButton setImage:[UIImage imageNamed:@"arrowdown"] forState:UIControlStateNormal];
            }
            titleString = @"Ingredient";
            [imageView setImage:[UIImage imageNamed:@"ingrediwnts"]];
            [dropDownButton addTarget:self action:@selector(ingredientsSectionSelected) forControlEvents:UIControlEventTouchDown];
            [plusImageView setImage:[UIImage imageNamed:@"PlusButton"]];
            plusImageView.contentMode = UIViewContentModeCenter;
            if (self.ingredientArray.count)
            {
                dropDownButton.hidden = NO;
                plusImageView.frame = CGRectMake(self.view.frame.size.width-105, 0, 50, 50);
            }
            else
            {
                plusImageView.frame = CGRectMake(self.view.frame.size.width-75, 0, 50, 50);
                dropDownButton.hidden = YES;
            }
            [view addSubview:plusImageView];
            [headerView addGestureRecognizer:ingredientTap];

            break;
        case 4:
            if (self.isPreparingTimeSelected) {
                [dropDownButton setImage:[UIImage imageNamed:@"arrowtop"] forState:UIControlStateNormal];
                bottomSeperatorView.hidden = NO;
            }
            else
            {
                [dropDownButton setImage:[UIImage imageNamed:@"arrowdown"] forState:UIControlStateNormal];
                bottomSeperatorView.hidden = YES;
            }
            titleString = @"Preparation Time";
            [imageView setImage:[UIImage imageNamed:@"preptimeu"]];
            imageView.frame = CGRectMake(6, 2, 46, 45);

            [dropDownButton addTarget:self action:@selector(preparingTimeSectionSelected) forControlEvents:UIControlEventTouchDown];
            [headerView addGestureRecognizer:prepareTimeTap];
            break;
        case 5:
            return headerView;
            break;
        default:
            break;
    }
    titleLabel.text = titleString;
    [view addSubview:dropDownButton];
    [view addSubview:imageView];
    [view addSubview:titleLabel];
    [view addSubview:bottomSeperatorView];
    [headerView addSubview:view];
    return headerView;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL isThereInArrayList = NO;
    CLRecipeFilterTableViewCell *cell = (CLRecipeFilterTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    switch (indexPath.section) {
        case 0:
            if ([[self.filterOptionDictionary objectForKey:@"mealtime"] containsObject:cell.categoryNameLabel.text])
            {
                [[self.filterOptionDictionary objectForKey:@"mealtime"]  removeObject:cell.categoryNameLabel.text];
//                [self.filterOptionDictionary removeObjectForKey:@"mealtime"];
            }
            else
            {
                [[self.filterOptionDictionary objectForKey:@"mealtime"] addObject:cell.categoryNameLabel.text];
//                [self.filterOptionDictionary setObject:cell.categoryNameLabel.text forKey:@"mealtime"];
            }
            break;
        case 1:
            if ([[self.filterOptionDictionary objectForKey:@"cuisines"] containsObject:cell.categoryNameLabel.text])
            {
//                [self.filterOptionDictionary removeObjectForKey:@"cuisines"];
                [[self.filterOptionDictionary objectForKey:@"cuisines"] removeObject:cell.categoryNameLabel.text];

            }
            else
            {
//                [self.filterOptionDictionary setObject:cell.categoryNameLabel.text forKey:@"cuisines"];
                [[self.filterOptionDictionary objectForKey:@"cuisines"]  addObject:cell.categoryNameLabel.text];
            }

            break;
        case 2:
            if ([[self.filterOptionDictionary objectForKey:@"dietry"] containsObject:cell.categoryNameLabel.text])
            {
                [[self.filterOptionDictionary objectForKey:@"dietry"] removeObject:cell.categoryNameLabel.text];
//                [self.filterOptionDictionary removeObjectForKey:@"dietry"];
            }
            else
            {
//                [self.filterOptionDictionary setObject:cell.categoryNameLabel.text forKey:@"dietry"];
                [[self.filterOptionDictionary objectForKey:@"dietry"]  addObject:cell.categoryNameLabel.text];

            }

            break;
        case 3:
            
            for (int i = 0; i<self.ingredientArray.count; i++)
            {
                NSString *name = [[self.ingredientArray objectAtIndex:i] objectForKey:@"name"];
                if ([name isEqualToString:cell.categoryNameLabel.text])
                {
                    [self.ingredientArray removeObjectAtIndex:i];
                    isThereInArrayList = YES;
                }
            }
            
            break;
        case 4:
            if ([[self.filterOptionDictionary objectForKey:@"cookingtime"]  containsObject:cell.categoryNameLabel.text])
            {
//                [self.filterOptionDictionary removeObjectForKey:@"cookingtime"];
                [[self.filterOptionDictionary objectForKey:@"cookingtime"]  removeObject:cell.categoryNameLabel.text];
            }
            else
            {
//                [self.filterOptionDictionary setObject:cell.categoryNameLabel.text forKey:@"cookingtime"];
                [[self.filterOptionDictionary objectForKey:@"cookingtime"]  addObject:cell.categoryNameLabel.text];
            }
            break;

        default:
            break;
    }
    [self.filterOptionDictionary setObject:self.ingredientArray forKey:@"ingredient"];
    [self.recipeFilterTableView reloadData];
}


//-(UIView *)returnIngredientHeaderView
//{
//    UIView *ingredientView = [[UIView alloc] initWithFrame:CGRectMake(48, 7, 232, 35)];
//    ingredientView.backgroundColor = [UIColor colorWithRed:232.0/255.0 green:232.0/255.0 blue:232.0/255.0 alpha:1.0];
//    ingredientView.layer.cornerRadius = 2.0;
//    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(6, 0, 210, 35)];
//    titleLabel.backgroundColor = [UIColor clearColor];
//    titleLabel.textColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
//    titleLabel.text = @"Ingredients";
//    titleLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:22.0];
//    UIImageView *searchImageView = [[UIImageView alloc] initWithFrame:CGRectMake(190, -1, 37, 37)];
//    [searchImageView setImage:[UIImage imageNamed:@"search"]];
//    [ingredientView addSubview:titleLabel];
//    [ingredientView addSubview:searchImageView];
//
//    return ingredientView;
//}

//-(void)headerViewTapped:(UIGestureRecognizer *)gesture
//{
//    UIView *headerView = gesture.view;
//    
//    NSArray *headerTitleArray = self.filterDictionary.allKeys;
//    
//    
//    if ([self.indexArray containsObject:[headerTitleArray  objectAtIndex:headerView.tag]]) {
//        [self.indexArray removeObject:[headerTitleArray  objectAtIndex:headerView.tag]];
//        [dropDownButton setImage:[UIImage imageNamed:@"arrowdown"] forState:UIControlStateNormal];
//    }
//    else
//    {
//        [self.indexArray addObject:[headerTitleArray  objectAtIndex:headerView.tag]];
//        [dropDownButton setImage:[UIImage imageNamed:@"arrowtop"] forState:UIControlStateNormal];
//    }
//    [self.recipeFilterTableView reloadSections:[NSIndexSet indexSetWithIndex:headerView.tag] withRowAnimation:UITableViewRowAnimationFade];
//}

#pragma mark - Button/tap methods

-(void)mealTimeSectionSelected
{
    self.isMealTimeSelected = !self.isMealTimeSelected;
    [self.recipeFilterTableView reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

-(void)cuisinesSectionSelected
{
    self.isCuisinesSelected = !self.isCuisinesSelected;
    [self.recipeFilterTableView reloadSections:[NSIndexSet indexSetWithIndex:1] withRowAnimation:UITableViewRowAnimationFade];
}

-(void)dietarySectionSelected
{
    self.isDietarySelected = !self.isDietarySelected;
    [self.recipeFilterTableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationFade];
}

-(void)ingredientsSectionSelected
{
    if (self.ingredientArray.count)
    {
        self.isIngredientsSelected = !self.isIngredientsSelected;
        [self.recipeFilterTableView reloadSections:[NSIndexSet indexSetWithIndex:3] withRowAnimation:UITableViewRowAnimationFade];
    }

}

-(void)showIngredientsViewPopUp
{
    [self addBlackView];
    self.ingredientsFilterView = [[[NSBundle mainBundle] loadNibNamed:@"CLIngredientsFilterView" owner:self options:nil] objectAtIndex:0];
    self.ingredientsFilterView.delegate = self;
    [self.ingredientsFilterView configureViewWithArray:(NSMutableArray *)[self.ingredientDictionary objectForKey:@"data"]];
    self.ingredientsFilterView.ingredientsFilterArray = self.ingredientArray;
    self.ingredientsFilterView.center = self.view.center;
    [self ViewZoomInEffect:self.ingredientsFilterView];
    [self.view addSubview:self.ingredientsFilterView];
}

-(void)preparingTimeSectionSelected
{
    self.isPreparingTimeSelected = !self.isPreparingTimeSelected;
    [self.recipeFilterTableView reloadSections:[NSIndexSet indexSetWithIndex:4] withRowAnimation:UITableViewRowAnimationFade];
}


-(void)submitViewTapped
{
    CLRecipeViewController *recipeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeViewController"];
    recipeVC.filterOptionDictionary = self.filterOptionDictionary;
    recipeVC.searchText = self.searchText;
    [self.navigationController pushViewController:recipeVC animated:YES];
}

#pragma mark CLIngredientsFilterViewDelegate methods

-(void)ingredientSaveButtonTapped:(NSMutableArray *)ingredientArray withIdArray:(NSMutableArray *)idArray
{
    [self ViewZoomOutEffect:self.ingredientsFilterView];
    self.ingredientArray = ingredientArray;
    self.ingredientIDArray = idArray;
    [self.filterOptionDictionary setObject:self.ingredientArray forKey:@"ingredient"];
    [self removeBlackView];
    [self.recipeFilterTableView reloadData];
}

#pragma mark - CLFilterSearchTableViewCellDelegate Methods

-(void)searchFieldTextDidChage:(NSString *)searchText
{
    self.searchText = searchText;
}


#pragma mark UITextFieldDelegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
    self.searchText = resultingString;
    return YES;
}

#pragma mark - Keyboard Notification methods

- (void)keyboardWillShow:(NSNotification*)notification
{
    keyboardOffset = self.recipeFilterTableView.contentOffset;
    NSDictionary *info = [notification userInfo];
    CGSize kbSize = [[info objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.recipeFilterTableView.contentOffset = CGPointMake(0,self.recipeFilterTableView.contentSize.height-kbSize.height);
}

- (void)keyboardWillHide:(NSNotification*)notification
{
    self.recipeFilterTableView.contentOffset = keyboardOffset;
}

#pragma mark black view custom methods

-(void)addBlackView
{
    if (!self.blackView)
    {
        self.blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.blackView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        UITapGestureRecognizer *blackTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(blackViewTapped)];
        [self.blackView addGestureRecognizer:blackTap];
        [self.view addSubview:self.blackView];
    }
    else
    {
        self.blackView.hidden = NO;
    }
}

-(void)removeBlackView
{
    [self.blackView removeFromSuperview];
    self.blackView = nil;
}

-(void)blackViewTapped
{
    [self.blackView removeFromSuperview];
    self.blackView = nil;
    [self ViewZoomOutEffect:self.ingredientsFilterView];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
