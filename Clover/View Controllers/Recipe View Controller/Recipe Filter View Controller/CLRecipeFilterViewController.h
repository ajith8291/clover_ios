//
//  CLRecipeFilterViewController.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 01/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLBaseViewController.h"

@interface CLRecipeFilterViewController : CLBaseViewController<UITableViewDataSource,UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UITableView *recipeFilterTableView;
@property (weak, nonatomic) IBOutlet UIView *submitView;
@property (weak, nonatomic) IBOutlet UILabel *submitLabel;


@property (nonatomic, strong) NSMutableDictionary *filterDictionary;

@end
