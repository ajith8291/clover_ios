//
//  ALFilterSearchTableViewCell.h
//  Clover
//
//  Created by Ajith Kumar on 16/04/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CLFilterSearchTableViewCellDelegate <NSObject>

@optional

-(void)searchFieldTextDidChage:(NSString *)searchText;

@end

@interface CLFilterSearchTableViewCell : UITableViewCell<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *searchTextField;

@property (nonatomic, weak) id <CLFilterSearchTableViewCellDelegate>delegate;
@end
