//
//  CLRecipeViewController.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 05/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLRecipeViewController.h"
#import "CLRecipeCollectionViewListCell.h"
#import "CLRecipeCollectionViewLGridCell.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "NSString+DataValidator.h"
#import "CLConstants.h"
#import "CLRecipeDetailsViewController.h"
#import "CLRecipeDetailsCollectionViewCell.h"
#import "CLReviewsViewController.h"
#import "CLWebViewController.h"

@interface CLRecipeViewController ()<UITextFieldDelegate,CLRecipeDetailsCollectionViewCellDelegate,UICollectionViewDelegateFlowLayout,CLRecipeDetailsViewControllerDelegate,CLRecipeCollectionViewLGridCellDelegate>



@property (nonatomic, assign) BOOL isListViewSelected;
@property (nonatomic, strong) NSMutableDictionary *paramsDictionary;
@property (nonatomic, strong) UIScrollView *topScrollView;
@property (nonatomic, strong) NSArray *recipeFilteredArray;
@property (nonatomic, assign) BOOL isSearchSelected;
@end

@implementation CLRecipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isListViewSelected = YES;
    self.paramsDictionary = [NSMutableDictionary new];
    
    // Do any additional setup after loading the view.
    self.navigationView.navTitleTextLabel.text = [@"Recipes" capitalizedString];
    self.navigationView.navMenuButton.hidden = YES;
    self.navigationView.navEditButton.hidden = YES;
    self.navigationView.navLogoImageView.hidden = YES;
    self.navigationView.navCartButton.hidden = YES;
    self.navigationView.navSearchButton.translatesAutoresizingMaskIntoConstraints = YES;
    self.navigationView.navSearchButton.frame = CGRectMake(self.view.frame.size.width-44, 5, 40, 40);
    self.navigationView.navSearchTextFiled.delegate = self;
    self.navigationView.navSearchTextFiled.text = self.searchText;
    [self.listViewButton setImage:[UIImage imageNamed:@"thumbviewinactive"] forState:UIControlStateNormal];
    [self.recipeCollectionView registerClass:[CLRecipeDetailsCollectionViewCell class] forCellWithReuseIdentifier:@"CLRecipeDetailsCollectionViewCell"];

    self.topScrollView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-40, 40)];

    if (self.isFromFavourites||self.isFromShoppingList)
    {
        self.recipeCollectionView.translatesAutoresizingMaskIntoConstraints = YES;
        self.recipeCollectionView.frame = CGRectMake(0, 70, self.view.frame.size.width, self.view.frame.size.height-70);
    }
    else
    {
        self.recipeCollectionView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    [self.recipeCollectionView setShowsVerticalScrollIndicator:NO];
    self.topView.hidden = YES;
    [self changeFilterDictionaryToParameterDictionary];
}

-(void)changeFilterDictionaryToParameterDictionary
{
    if ([[self.filterOptionDictionary objectForKey:@"mealtime"] count])
    {
        
        NSString *mealTimeStr = [NSString stringWithFormat:@"%@",[[self.filterOptionDictionary objectForKey:@"mealtime"] componentsJoinedByString:@","]];
        [self.paramsDictionary setObject:mealTimeStr forKey:@"mealtime"];
    }
    if ([[self.filterOptionDictionary objectForKey:@"cuisines"] count])
    {
        
        NSString *mealTimeStr = [NSString stringWithFormat:@"%@",[[self.filterOptionDictionary objectForKey:@"cuisines"] componentsJoinedByString:@","]];
        [self.paramsDictionary setObject:mealTimeStr forKey:@"cuisines"];
    }
    if ([[self.filterOptionDictionary objectForKey:@"dietry"] count])
    {
        
        NSString *mealTimeStr = [NSString stringWithFormat:@"%@",[[self.filterOptionDictionary objectForKey:@"dietry"] componentsJoinedByString:@","]];
        [self.paramsDictionary setObject:mealTimeStr forKey:@"dietry"];
    }
    if ([[self.filterOptionDictionary objectForKey:@"ingredient"] count])
    {
        NSMutableArray *idArray = [NSMutableArray new];
        for (int i = 0; i<[[self.filterOptionDictionary objectForKey:@"ingredient"] count]; i++)
        {
            NSString *recipeId = [[[self.filterOptionDictionary objectForKey:@"ingredient"] objectAtIndex:i] objectForKey:@"id"];
            [idArray addObject:recipeId];
        }
        
        NSString *mealTimeStr = [NSString stringWithFormat:@"%@",[idArray componentsJoinedByString:@","]];
        [self.paramsDictionary setObject:mealTimeStr forKey:@"ingredient"];
    }
    if ([[self.filterOptionDictionary objectForKey:@"cookingtime"] count])
    {
        
        NSString *mealTimeStr = [NSString stringWithFormat:@"%@",[[self.filterOptionDictionary objectForKey:@"cookingtime"] componentsJoinedByString:@","]];
        [self.paramsDictionary setObject:mealTimeStr forKey:@"cookingtime"];
    }
    [self callWebServiceToGetRecipeDetails];
    [self createTopScrollViewWithFilterDictionary];
}

-(void)createTopScrollViewWithFilterDictionary
{
    int xAxis = 5;
    int tag = 400;
    [self.topScrollView setShowsHorizontalScrollIndicator:NO];
    
    for (UIView *views in self.topScrollView.subviews) {
        [views removeFromSuperview];
    }
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:15]};
    
    if ([[self.filterOptionDictionary objectForKey:@"mealtime"] count])
    {
        for (NSString *mealTimeStr in [self.filterOptionDictionary objectForKey:@"mealtime"])
        {
            UIView *filterTagView = [[UIView alloc] init];
            filterTagView.tag = tag;
            CGSize size = [mealTimeStr sizeWithAttributes:attributes];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, size.width+10, size.height)];
            label.text = mealTimeStr;
            [label sizeToFit];
            filterTagView.frame = CGRectMake(xAxis, 0, label.frame.size.width+40, 40);
            xAxis = xAxis + label.frame.size.width+30+5;
            UIImageView *closeImage = [[UIImageView alloc] initWithFrame:CGRectMake(label.frame.size.width, 5, 30, 30)];
            closeImage.image = [UIImage imageNamed:@"close"];
            [filterTagView addSubview:closeImage];
            [filterTagView addSubview:label];
            
            UITapGestureRecognizer *closeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeButtonTappedView:)];
            [filterTagView addGestureRecognizer:closeTap];
            tag++;
            [self.topScrollView addSubview:filterTagView];
        }
    }
    if ([[self.filterOptionDictionary objectForKey:@"cuisines"] count])
    {
        for (NSString *mealTimeStr in [self.filterOptionDictionary objectForKey:@"cuisines"])
        {
            UIView *filterTagView = [[UIView alloc] init];
            filterTagView.tag = tag;
            CGSize size = [mealTimeStr sizeWithAttributes:attributes];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, size.width+10, size.height)];
            label.text = mealTimeStr;
            [label sizeToFit];
            filterTagView.frame = CGRectMake(xAxis, 0, label.frame.size.width+40, 40);
            xAxis = xAxis + label.frame.size.width+30+5;
            UIImageView *closeImage = [[UIImageView alloc] initWithFrame:CGRectMake(label.frame.size.width, 5, 30, 30)];
            closeImage.image = [UIImage imageNamed:@"close"];
            [filterTagView addSubview:closeImage];
            [filterTagView addSubview:label];
            UITapGestureRecognizer *closeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeButtonTappedView:)];
            [filterTagView addGestureRecognizer:closeTap];
            tag++;
            [self.topScrollView addSubview:filterTagView];
        }
    }
    if ([[self.filterOptionDictionary objectForKey:@"dietry"] count])
    {
        for (NSString *mealTimeStr in [self.filterOptionDictionary objectForKey:@"dietry"])
        {
            UIView *filterTagView = [[UIView alloc] init];
            filterTagView.tag = tag;
            CGSize size = [mealTimeStr sizeWithAttributes:attributes];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, size.width+10, size.height)];
            label.text = mealTimeStr;
            [label sizeToFit];
            filterTagView.frame = CGRectMake(xAxis, 0, label.frame.size.width+40, 40);
            xAxis = xAxis + label.frame.size.width+30+5;
            UIImageView *closeImage = [[UIImageView alloc] initWithFrame:CGRectMake(label.frame.size.width, 5, 30, 30)];
            closeImage.image = [UIImage imageNamed:@"close"];
            [filterTagView addSubview:closeImage];
            [filterTagView addSubview:label];
            UITapGestureRecognizer *closeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeButtonTappedView:)];
            [filterTagView addGestureRecognizer:closeTap];
            tag++;
            [self.topScrollView addSubview:filterTagView];
        }
    }
    if ([[self.filterOptionDictionary objectForKey:@"ingredient"] count])
    {
        for (int i = 0; i<[[self.filterOptionDictionary objectForKey:@"ingredient"] count]; i++)
        {
            NSString *mealTimeStr = [[[self.filterOptionDictionary objectForKey:@"ingredient"] objectAtIndex:i] objectForKey:@"name"];
            UIView *filterTagView = [[UIView alloc] init];
            filterTagView.tag = tag;
            CGSize size = [mealTimeStr sizeWithAttributes:attributes];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, size.width+10, size.height)];
            label.text = mealTimeStr;
            [label sizeToFit];
            filterTagView.frame = CGRectMake(xAxis, 0, label.frame.size.width+40, 40);
            xAxis = xAxis + label.frame.size.width+30+5;
            UIImageView *closeImage = [[UIImageView alloc] initWithFrame:CGRectMake(label.frame.size.width, 5, 30, 30)];
            closeImage.image = [UIImage imageNamed:@"close"];
            [filterTagView addSubview:closeImage];
            [filterTagView addSubview:label];
            UITapGestureRecognizer *closeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeButtonTappedView:)];
            [filterTagView addGestureRecognizer:closeTap];
            tag++;
            [self.topScrollView addSubview:filterTagView];
        }
    }
    if ([[self.filterOptionDictionary objectForKey:@"cookingtime"] count])
    {
        for (NSString *mealTimeStr in [self.filterOptionDictionary objectForKey:@"cookingtime"])
        {
            UIView *filterTagView = [[UIView alloc] init];
            filterTagView.tag = tag;
            CGSize size = [mealTimeStr sizeWithAttributes:attributes];
            UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 10, size.width+10, size.height)];
            label.text = mealTimeStr;
            [label sizeToFit];
            filterTagView.frame = CGRectMake(xAxis, 0, label.frame.size.width+40, 40);
            xAxis = xAxis + label.frame.size.width+30+5;
            UIImageView *closeImage = [[UIImageView alloc] initWithFrame:CGRectMake(label.frame.size.width, 5, 30, 30)];
            closeImage.image = [UIImage imageNamed:@"close"];
            [filterTagView addSubview:closeImage];
            [filterTagView addSubview:label];
            UITapGestureRecognizer *closeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeButtonTappedView:)];
            [filterTagView addGestureRecognizer:closeTap];
            tag++;
            [self.topScrollView addSubview:filterTagView];
        }
    }


    self.topScrollView.contentSize = CGSizeMake(xAxis, 40);
    [self.topView addSubview:self.topScrollView];
}


-(void)closeButtonTappedView:(UITapGestureRecognizer *)tapView
{
    UIView *tagView = (UIView *)[self.topView viewWithTag:tapView.view.tag];
    if ([tapView.view tag] == 399) {
        self.searchText = @"";
        [tagView removeFromSuperview];
        [self createTopScrollViewWithFilterDictionary];
        self.isSearchSelected = NO;
        [self.recipeCollectionView reloadData];
    }
    else
    {
        NSString *selectedStr;
        NSLog(@"%@",tagView.subviews);
        
        for (UIView *view in tagView.subviews)
        {
            if ([view isKindOfClass:[UILabel class]])
            {
                UILabel *label = (UILabel *)view;
                selectedStr = label.text;
            }
        }
        if ([[self.filterOptionDictionary objectForKey:@"mealtime"] containsObject:selectedStr])
        {
            [[self.filterOptionDictionary objectForKey:@"mealtime"] removeObject:selectedStr];
        }
        if ([[self.filterOptionDictionary objectForKey:@"cuisines"] containsObject:selectedStr])
        {
            [[self.filterOptionDictionary objectForKey:@"cuisines"] removeObject:selectedStr];
        }
        if ([[self.filterOptionDictionary objectForKey:@"dietry"] containsObject:selectedStr])
        {
            [[self.filterOptionDictionary objectForKey:@"dietry"] removeObject:selectedStr];
        }
        for (int i = 0; i<[[self.filterOptionDictionary objectForKey:@"ingredient"] count]; i++)
        {
            if ([selectedStr isEqualToString:[[[self.filterOptionDictionary objectForKey:@"ingredient"] objectAtIndex:i] objectForKey:@"name"]])
            {
                [[self.filterOptionDictionary objectForKey:@"ingredient"] removeObjectAtIndex:i];
            }
        }
        if ([[self.filterOptionDictionary objectForKey:@"cookingtime"] containsObject:selectedStr])
        {
            [[self.filterOptionDictionary objectForKey:@"cookingtime"] removeObject:selectedStr];
        }
        self.paramsDictionary = [NSMutableDictionary new];
        [self.topScrollView removeFromSuperview];
        [self changeFilterDictionaryToParameterDictionary];

    }
}


-(void)callWebServiceToGetRecipeDetails
{

    if ([self isNetworkIsReachable])
    {
        self.recipeArray = [NSMutableArray new];
        self.recipeCollectionView.hidden = YES;
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        [self.view bringSubviewToFront:hud];
        
        [self.paramsDictionary setObject:self.userManager.user.userId forKey:@"userid"];
        [self.networkManager startGETRequestWithAPI:@"cloverlistrecipe" andParameters:self.paramsDictionary  withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             NSString *errorStr = [responseDictionary objectForKey:@"msg"];
             self.topView.hidden = NO;

             if ([errorCode isEqualToString:@"200"])
             {
                 NSArray *recipeDictArray =[responseDictionary objectForKey:@"receipes"];
                 for (int i = 0;i<recipeDictArray.count ; i++)
                 {
                     CLRecipe *recipe = [CLRecipe getInformationFromDictionary:[recipeDictArray objectAtIndex:i]];
                     [self.recipeArray addObject:recipe];
                 }
                 self.recipeCollectionView.hidden = NO;
                 if (![self.searchText isEmptyString] && self.searchText != nil) {
                     self.isSearchSelected = YES;
                     [self loadSearchResultRecipe:self.searchText];
                 }
                 [self.recipeCollectionView reloadData];
             }
             else
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
             }
             
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
        
    }
}

#pragma mark UITableView Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return (self.isSearchSelected) ? self.recipeFilteredArray.count : self.recipeArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isSearchSelected == NO)
    {
        [self.view endEditing:YES];
    }
    CLRecipe *recipe = (self.isSearchSelected) ?[self.recipeFilteredArray objectAtIndex:indexPath.row] : [self.recipeArray objectAtIndex:indexPath.row];
    if (self.isListViewSelected == YES)
    {
        CLRecipeDetailsCollectionViewCell *cell = (CLRecipeDetailsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CLRecipeDetailsCollectionViewCell" forIndexPath:indexPath];
        cell.topPlayButton.translatesAutoresizingMaskIntoConstraints = YES;
        cell.topPlayButton.frame = CGRectMake(self.view.frame.size.width-55, 10, 40, 40);
        cell.delegate = self;
        cell.favImageView.hidden = YES;
        cell.removeFromListImageView.hidden = YES;
        [cell configureListCellWithRecipeDetails:recipe];
        cell.layer.shouldRasterize = YES;
        cell.layer.rasterizationScale = [UIScreen mainScreen].scale;

        return cell;
    }
    else
    {
        CLRecipeCollectionViewLGridCell *cell = (CLRecipeCollectionViewLGridCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CLRecipeCollectionViewLGridCell" forIndexPath:indexPath];
        cell.delegate = self;
        [cell configureGridCellWithRecipeDetails:recipe];
        cell.layer.shouldRasterize = YES;
        cell.layer.rasterizationScale = [UIScreen mainScreen].scale;
        return cell;
    }
    
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isListViewSelected == YES)
    {
        return CGSizeMake(self.view.frame.size.width, 230);
    }
    return CGSizeMake((self.view.frame.size.width-30)/2, 125);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (IS_IOS8_OR_GREATER)
    {
        if (self.isListViewSelected) {
            return UIEdgeInsetsMake(5, 0, 5, 0);
        }
        return UIEdgeInsetsMake(7, 10, 12, 10);
    }
    if (self.isListViewSelected)
    {
        return UIEdgeInsetsMake(5, 3, 5, 3);
    }
    return UIEdgeInsetsMake(6, 18, 10 ,18);

}


-(CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    if (self.isListViewSelected)
    {
        return 0;
    }
    return 8;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CLRecipe *recipe;
    if (self.isSearchSelected)
    {
        recipe = [self.recipeFilteredArray objectAtIndex:indexPath.row];

    }
    else
    {
        recipe = [self.recipeArray objectAtIndex:indexPath.row];
    }
    CLRecipeDetailsViewController *recipedetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeDetailsViewController"];
    recipedetailVC.selectedRecipe = recipe;
    recipedetailVC.delegate = self;
    recipedetailVC.isfromFavourites = self.isFromFavourites;
    recipedetailVC.isFromShoppingDetails = self.isFromShoppingList;
    [self.navigationController pushViewController:recipedetailVC animated:YES];
}

#pragma mark Tap\ Button Action Methods

- (IBAction)listViewButtonActionMethod:(id)sender
{
    if (!self.isListViewSelected)
    {        [self.listViewButton setImage:[UIImage imageNamed:@"thumbviewinactive"] forState:UIControlStateNormal];
        [UIView transitionWithView:self.recipeCollectionView duration:0.8 options:UIViewAnimationOptionTransitionFlipFromLeft animations:nil completion:nil];

    }
    else
    {
        [self.listViewButton setImage:[UIImage imageNamed:@"listviewactive"] forState:UIControlStateNormal];
        [UIView transitionWithView:self.recipeCollectionView duration:0.8 options:UIViewAnimationOptionTransitionFlipFromRight animations:nil completion:nil];

    }
    self.isListViewSelected = !self.isListViewSelected;
    [self.recipeCollectionView reloadData];
}

- (IBAction)gridViewButtonActionMethod:(id)sender
{
    self.isListViewSelected = NO;
    [self.listViewButton setImage:[UIImage imageNamed:@"listviewinactive"] forState:UIControlStateNormal];
    [self.recipeCollectionView reloadData];
}

#pragma mark CLCustomNavigationViewDelegate

-(void)searchButtonTapped
{
    self.navigationView.navSearchTextFiled.hidden = NO;
    self.navigationView.navSearchTextFiled.autocorrectionType = UITextAutocorrectionTypeNo;
    [self.navigationView.navSearchTextFiled becomeFirstResponder];

}

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if ([textField.text isEqualToString:@""]) {
        self.isSearchSelected = NO;
    }
    else
    {
        self.isSearchSelected = YES;
    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    self.navigationView.navSearchTextFiled.hidden = YES;
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    [textField resignFirstResponder];
    self.navigationView.navSearchTextFiled.hidden = YES;
    [self.recipeCollectionView reloadData];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
    if ([resultingString isEmptyString])
    {
        self.isSearchSelected = NO;
        
    }
    else
    {
        self.isSearchSelected = YES;
    }

    [self loadSearchResultRecipe:resultingString];
    return YES;
}


-(void)loadSearchResultRecipe:(NSString *)searchText
{//LIKE[cd]
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"CAST (recipeTitle,'NSString') contains[cd] %@",searchText];
   self.recipeFilteredArray = [self.recipeArray filteredArrayUsingPredicate:predicate];
    [self.recipeCollectionView reloadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark CLRecipeDetailsCollectionViewCellDelegate methods

-(void)loadReviewViewControllerWithSelectedRecipe:(CLRecipe *)recipe
{
    CLRecipeDetailsViewController *recipedetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeDetailsViewController"];
    recipedetailVC.selectedRecipe = recipe;
    recipedetailVC.delegate = self;
    recipedetailVC.isUserReviewSelected = YES;
    [self.navigationController pushViewController:recipedetailVC animated:YES];
}

-(void)playButtonTappedWithRecipe:(CLRecipe *)recipe
{
    CLRecipeDetailsViewController *detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeDetailsViewController"];
    detailsVC.selectedRecipe = recipe;
    detailsVC.isPlayButtonTapped = YES;
    [self.navigationController pushViewController:detailsVC animated:NO];
}

-(void)gridViewPlayButtonTappedWithRecipe:(CLRecipe *)selectedRecipe
{
    CLRecipeDetailsViewController *detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeDetailsViewController"];
    detailsVC.selectedRecipe = selectedRecipe;
    detailsVC.isPlayButtonTapped = YES;
    [self.navigationController pushViewController:detailsVC animated:NO];
}

#pragma mark CLRecipeDetailsViewControllerDelegate Methods


-(void)addedRecipeReviewAndRatings
{
    [self callWebServiceToGetRecipeDetails];
}

-(void)loadShoppingList
{
    [self.delegate loadShoppingListWithNewDetails];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)loadfavouritesDetails
{
    [self.delegate loadFavouritesWithNewDetails];
    [self.navigationController popViewControllerAnimated:YES];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
