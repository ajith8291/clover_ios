//
//  CLRecipeFilterTableViewCell.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 01/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLRecipeFilterTableViewCell.h"

@implementation CLRecipeFilterTableViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
}

-(void)configureCellWithRecipe
{
    self.tickImageView.hidden = YES;
    self.categoryNameLabel.translatesAutoresizingMaskIntoConstraints = YES;
    self.categoryNameLabel.frame = CGRectMake(20, self.categoryNameLabel.frame.origin.y, self.categoryNameLabel.frame.size.width, self.categoryNameLabel.frame.size.height);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
