//
//  CLRecipeFilterTableViewCell.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 01/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLRecipeFilterTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *tickImageView;
@property (weak, nonatomic) IBOutlet UILabel *categoryNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *idLabel;

-(void)configureCellWithRecipe;

@end
