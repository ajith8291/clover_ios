//
//  CLShoppingListViewController.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 06/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLBaseViewController.h"

@interface CLShoppingListViewController : CLBaseViewController<UICollectionViewDataSource,UICollectionViewDelegate>


@property (weak, nonatomic) IBOutlet UICollectionView *shoppingListCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *noRecipeLabel;

@property (weak, nonatomic) IBOutlet UIButton *addRecipeButton;
@end
