//
//  CLShoppingListViewController.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 06/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLShoppingListViewController.h"
#import "CLConstants.h"
#import "CLShoppingListDetailsViewController.h"
#import "CLRecipe.h"
#import "CLRecipeDetailsCollectionViewCell.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "CLRecipeDetailsViewController.h"
#import "CLRecipeViewController.h"

@interface CLShoppingListViewController ()<CLRecipeDetailsCollectionViewCellDelegate,CLShoppingListDetailsViewControllerDelegate,CLShoppingListDetailsViewControllerDelegate,CLRecipeViewControllerDelegate>


@property (nonatomic, strong) NSMutableArray *shoppingListArray;
@property (nonatomic, strong) NSDictionary *idDictionary;
@property (nonatomic, strong) NSMutableArray *listIdArray;
@property (nonatomic, strong) NSMutableArray *purchasedArray;
@property (nonatomic, assign) BOOL isValueFromWeb;
@property (nonatomic, strong) NSMutableDictionary *purchaseDict;
@end

@implementation CLShoppingListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.purchaseDict = [NSMutableDictionary new];
    
    // Do any additional setup after loading the view.
    self.navigationView.navTitleTextLabel.text = [@"My Shopping List" capitalizedString];
    self.navigationView.navTitleTextLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:18.0];
    self.navigationView.navLogoImageView.hidden = YES;
    self.navigationView.navSearchButton.hidden = YES;
    self.navigationView.navBackButton.hidden = YES;
    self.navigationView.navEditButton.hidden = YES;
    self.navigationView.navCartButton.hidden = YES;
    self.isValueFromWeb = YES;
    [self.shoppingListCollectionView registerClass:[CLRecipeDetailsCollectionViewCell class] forCellWithReuseIdentifier:@"CLRecipeDetailsCollectionViewCell"];
    [self callWebServiceToGetShoppingRecipe];
    
    self.addRecipeButton.layer.cornerRadius = 10.0;
    self.addRecipeButton.clipsToBounds = YES;
    
    [self.shoppingListCollectionView setShowsVerticalScrollIndicator:NO];
    
}


-(void)callWebServiceToGetShoppingRecipe
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        
        NSDictionary *params = @{@"user":self.userManager.user.userId};
        
        [self.networkManager startGETRequestWithAPI:@"shoplist/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
//             NSString *errorStr = [responseDictionary objectForKey:@"msg"];
             
             if ([errorCode isEqualToString:@"200"])
             {
                 self.shoppingListArray = [responseDictionary objectForKey:@"shopping_list"];
                 self.idDictionary = [responseDictionary objectForKey:@"shopping_list"];
                 if (self.shoppingListArray.count)
                 {
                     self.noRecipeLabel.hidden = YES;
                     self.shoppingListCollectionView.hidden = NO;
                     [self.shoppingListCollectionView reloadData];
                     [self getShoppingListIdArray];
                 }
                 else
                 {
                     self.noRecipeLabel.hidden = NO;
                     self.shoppingListCollectionView.hidden = YES;
                     [self.shoppingListCollectionView reloadData];
                 }
             }
             else
             {
                 self.noRecipeLabel.hidden = NO;
                 self.shoppingListCollectionView.hidden = YES;
                 [self.shoppingListCollectionView reloadData];

//                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                 [errorAlert show];
             }
             
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [errorAlert show];

         }];
    }
}

-(void)getShoppingListIdArray
{
    self.listIdArray = [NSMutableArray new];
    
    for (int i=0; i<self.self.shoppingListArray.count; i++)
    {
        NSString *listId = [NSString stringWithFormat:@"%@",[[self.shoppingListArray objectAtIndex:i] objectForKey:@"ID"]];
        [self.listIdArray addObject:listId];
        [self.purchaseDict setObject:[[_shoppingListArray objectAtIndex:i] objectForKey:@"purchase_list"] forKey:[NSString stringWithFormat:@"%d",i]];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UICollectionViewDatasource and delegate methods

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CLRecipe *recipe = [CLRecipe getInformationFromDictionary:[[self.shoppingListArray objectAtIndex:indexPath.row] objectForKey:@"recipe_info"]];
    CLRecipeDetailsCollectionViewCell *cell = (CLRecipeDetailsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CLRecipeDetailsCollectionViewCell" forIndexPath:indexPath];
    cell.favImageView.hidden = YES;
    cell.delegate = self;
    cell.bottomView.hidden = YES;
    cell.isFromShoppingList = YES;
    cell.reviewCountLabel.backgroundColor = [UIColor purpleColor];
    cell.starView.hidden = YES;
    [cell configureListCellWithRecipeDetails:recipe];
    return cell;
}



-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.shoppingListArray.count;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CLRecipe *recipe = [CLRecipe getInformationFromDictionary:[[self.shoppingListArray objectAtIndex:indexPath.row] objectForKey:@"recipe_info"]];

    CLShoppingListDetailsViewController *shoppingDetailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLShoppingListDetailsViewController"];
    shoppingDetailsVC.purchaseItemArray = [self.purchaseDict objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    shoppingDetailsVC.indexPath = indexPath;
    shoppingDetailsVC.shoppingListId = [self.listIdArray objectAtIndex:indexPath.row];
    shoppingDetailsVC.selecyedRecipe = recipe;
    shoppingDetailsVC.delegate = self;
    [self.navigationController pushViewController:shoppingDetailsVC animated:YES];
    
}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (IS_IOS8_OR_GREATER)
    {
        return UIEdgeInsetsMake(5, 3, 5, 3);
    }
    return UIEdgeInsetsMake(5, 10, 5, 10);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width, 215);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
} 
*/

#pragma mark CLRecipeDetailsCollectionViewCellDelegate methods

-(void)loadReviewViewControllerWithSelectedRecipe:(CLRecipe *)recipe
{
    CLRecipeDetailsViewController *recipedetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeDetailsViewController"];
    recipedetailVC.selectedRecipe = recipe;
    recipedetailVC.isUserReviewSelected = YES;
    [self.navigationController pushViewController:recipedetailVC animated:YES];
}

-(void)playButtonTappedWithRecipe:(CLRecipe *)recipe
{
    CLRecipeDetailsViewController *detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeDetailsViewController"];
    detailsVC.selectedRecipe = recipe;
    detailsVC.isPlayButtonTapped = YES;
    [self.navigationController pushViewController:detailsVC animated:NO];
}

-(void)removeRecipeFromShoppingList:(CLRecipe *)shoppingListRecipe
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        
        NSDictionary *params = @{@"list":shoppingListRecipe.recipeShoppingListId,
                                 @"user":self.userManager.user.userId};
        
        [self.networkManager startGETRequestWithAPI:@"deletelist/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             NSString *msg = [responseDictionary objectForKey:@"msg"];
             
             if ([errorCode isEqualToString:@"200"])
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:[msg capitalizedString] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
                 [self callWebServiceToGetShoppingRecipe];
             }
             else
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
             }
             
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }
}

#pragma mark CLShoppingListDetailsViewControllerDelegate Methods

-(void)updatePurchasedWithManualPurchasedArray:(NSArray *)purchasedArray andIndexPath:(NSIndexPath *)indexPath
{
    [self.purchaseDict setObject:purchasedArray forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
}

-(void)loadShoppingListWithNewDetails
{
    [self callWebServiceToGetShoppingRecipe];
}

- (IBAction)addRecipeButtonAction:(id)sender
{
    CLRecipeViewController *recipeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeViewController"];
    recipeVC.isFromShoppingList = YES;
    recipeVC.delegate = self;
    [self.navigationController pushViewController:recipeVC animated:YES];
}
@end
