//
//  CLIngredientPopUpView.m
//  Clover
//
//  Created by Ajith Kumar on 02/03/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLIngredientPopUpView.h"
#import "CLConstants.h"
#import "NSString+DataValidator.h"

@implementation CLIngredientPopUpView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.popUpTableView.delegate = self;
    self.popUpTableView.dataSource = self;
    self.searchTextField.delegate = self;
    self.layer.cornerRadius = 5.0;
    self.clipsToBounds = YES;
}

-(void)configureViewWithNewData:(NSMutableArray *)dataArray
{
    self.recipeArray = dataArray;
}

#pragma mark - UITablevIewDatasource and Delegate Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:16]};
    NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:[self.recipeArray objectAtIndex:indexPath.row] attributes:attributes];
    
    CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.frame.size.width-20, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    CGSize size = rect.size;
    return size.height+12;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (self.isSearchSelected) ? self.recipeFilteredArray.count : self.recipeArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *array = (self.isSearchSelected) ?self.recipeFilteredArray  : self.recipeArray;

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.textLabel.text = [array objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:16.0];
    cell.textLabel.numberOfLines=0;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [self.delegate recipeSelectedWithRecipeTitle:cell.textLabel.text];
}

#pragma mark - UIButton/UITap Action

- (IBAction)closeButtonTapped:(id)sender
{
    [self.delegate recipePopUpViewCloseButtonTapped];
}

#pragma mark UITextFieldDelegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    [textField resignFirstResponder];
    self.isSearchSelected = NO;
    [self.popUpTableView reloadData];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
    if ([resultingString isEmptyString])
    {
        self.isSearchSelected = NO;
        
    }
    else
    {
        self.isSearchSelected = YES;
    }
    [self loadSearchResultIngredients:resultingString];
    
    return YES;
}

-(void) loadSearchResultIngredients:(NSString *)searchText
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF CONTAINS[cd] %@",searchText];
    self.recipeFilteredArray = (NSMutableArray *)[self.recipeArray filteredArrayUsingPredicate:predicate];
    [self.popUpTableView reloadData];
    
}

@end
