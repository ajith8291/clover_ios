//
//  CLIngredientPopUpView.h
//  Clover
//
//  Created by Ajith Kumar on 02/03/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol  CLIngredientPopUpViewDelegate<NSObject>

@optional

-(void)recipePopUpViewCloseButtonTapped;

-(void)recipeSelectedWithRecipeTitle:(NSString *)recipeName;

@end

@interface CLIngredientPopUpView : UIView<UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *popUpTableView;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;
@property (nonatomic, assign) BOOL isSearchSelected;


@property (nonatomic, strong)NSMutableArray *recipeArray;
@property (nonatomic, strong)NSMutableArray *recipeFilteredArray;

@property (nonatomic, weak) id<CLIngredientPopUpViewDelegate>delegate;

-(void)configureViewWithNewData:(NSMutableArray *)dataArray;
@end
