//
//  CLShoppingListDetailsViewController.h
//  Clover
//
//  Created by Ajith Kumar on 14/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLBaseViewController.h"
#import "CLRecipe.h"

@protocol CLShoppingListDetailsViewControllerDelegate  <NSObject>

@optional

-(void)updatePurchasedWithManualPurchasedArray:(NSMutableArray *)purchasedArray andIndexPath:(NSIndexPath *)indexPath;
-(void)loadShoppingListWithNewDetails;
@end
@interface CLShoppingListDetailsViewController : CLBaseViewController<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *shoppingItemsTableView;
@property (nonatomic, strong) NSArray *purchaseItemArray;
@property (nonatomic, strong) NSString *shoppingListId;
@property (nonatomic, strong) NSIndexPath *indexPath;
@property (nonatomic,weak) id<CLShoppingListDetailsViewControllerDelegate>delegate;
@property (nonatomic, strong) CLRecipe *selecyedRecipe;

@property (weak, nonatomic) IBOutlet UIView *filterView;
@property (weak, nonatomic) IBOutlet UILabel *filterViewLabel;
@property (weak, nonatomic) IBOutlet UILabel *noRecipeLabel;
@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UILabel *filterIngredientLabel;

@end
