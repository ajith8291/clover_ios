//
//  CLShoppingListDetailsViewController.m
//  Clover
//
//  Created by Ajith Kumar on 14/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLShoppingListDetailsViewController.h"
#import "CLConstants.h"
#import "CLRecipeFilterTableViewCell.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "CLRecipeDetailsViewController.h"
#import "CLIngredientPopUpView.h"
#import "CLRecipeViewController.h"

@interface CLShoppingListDetailsViewController ()<CLRecipeDetailsViewControllerDelegate,CLIngredientPopUpViewDelegate,CLRecipeViewControllerDelegate,UIAlertViewDelegate>

@property(nonatomic, strong)NSMutableArray *itemsPurchasedArray;
@property (nonatomic, strong) NSMutableArray *purchasedManulaArray;
@property (nonatomic, strong) NSArray *allIngredientsArray;
@property (nonatomic, strong) NSMutableArray *allRecipeArray;
@property (nonatomic, strong) NSMutableArray *recipeDetailsArray;
@property (nonatomic, strong) UIView *blackView;
@property (nonatomic, strong) CLIngredientPopUpView *ingredientPopUpView;
@property (nonatomic, assign) BOOL isAllRecipeSelected;
@property (nonatomic, strong) NSMutableArray *headerArray;
@property (nonatomic, assign) BOOL isLoaded;
@end

@implementation CLShoppingListDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationView.navTitleTextLabel.text = [@"My Shopping List Details" capitalizedString];
    self.navigationView.navTitleTextLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:18.0];
    self.navigationView.navLogoImageView.hidden = YES;
    self.navigationView.navSearchButton.hidden = YES;
    self.navigationView.navBackButton.hidden = YES;
    self.navigationView.navEditButton.hidden = YES;
    self.navigationView.navCartButton.hidden = YES;
    
    self.itemsPurchasedArray = [NSMutableArray new];
    self.purchasedManulaArray = [NSMutableArray new];
    self.filterView.layer.cornerRadius = 3.0;
    self.filterView.hidden = YES;
    self.filterViewLabel.hidden = YES;
    self.filterIngredientLabel.hidden = YES;
    self.noRecipeLabel.hidden = YES;
    self.addButton.hidden = YES;
    UITapGestureRecognizer *recipeFilterTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(recipeFilterViewTapped)];
    [self.filterView addGestureRecognizer:recipeFilterTap];
    
    self.filterView.clipsToBounds = YES;
    self.isAllRecipeSelected = YES;
    self.addButton.layer.cornerRadius = 3.0;
    self.addButton.clipsToBounds = YES;
    self.headerArray = [NSMutableArray new];
    [self loadAllIngredients];

}

-(void)loadAllIngredients
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        NSDictionary *params = @{@"user":self.userManager.user.userId,
                                 @"mode":@"ingredient"};
        
        [self.networkManager startGETRequestWithAPI:@"shoplist/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             if ([errorCode isEqualToString:@"200"])
             {
                 self.allIngredientsArray = [responseDictionary objectForKey:@"shopping_list"];
                 if (self.allIngredientsArray.count) {
                     self.filterView.hidden = NO;
                     self.filterViewLabel.hidden = NO;
                     self.filterIngredientLabel.hidden = NO;
                     self.shoppingItemsTableView.hidden = NO;

                     [self makeRecipeArray];
                 }
                 else
                 {
                     self.filterView.hidden = YES;
                     self.filterIngredientLabel.hidden = YES;
                     self.noRecipeLabel.hidden = NO;
                     self.addButton.hidden = NO;
                     self.allIngredientsArray = nil;
                     self.shoppingItemsTableView.hidden = YES;
                 }
                 self.isLoaded = YES;
             }
             else
             {
                 self.filterView.hidden = YES;
                 self.filterIngredientLabel.hidden = YES;
                 self.noRecipeLabel.hidden = NO;
                 self.addButton.hidden = NO;
                 self.allIngredientsArray = nil;
                 self.shoppingItemsTableView.hidden = YES;
                 self.isLoaded = NO;

             }
             [self.shoppingItemsTableView reloadData];
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [errorAlert show];
             
         }];
    }
}

-(void)makeRecipeArray
{
    self.allRecipeArray = [NSMutableArray new];
    
    for (int i = 0 ; i<self.allIngredientsArray.count; i++)
    {
        NSDictionary *ingredientDict = [self.allIngredientsArray objectAtIndex:i];
        NSArray *recipeArray = [ingredientDict objectForKey:@"recipe"];
        for (int j = 0 ; j<recipeArray.count; j++)
        {
            if (![self.allRecipeArray containsObject:[[recipeArray objectAtIndex:j] objectForKey:@"recipe_name"]])
            {
                [self.allRecipeArray addObject:[[recipeArray objectAtIndex:j] objectForKey:@"recipe_name"]];
            }
        }
    }
    
    [self.allRecipeArray insertObject:@"All Recipes" atIndex:0];
    [self modifyPurchasedArray];
    self.filterViewLabel.text = @"All Recipes";
    [self.shoppingItemsTableView reloadData];
}

-(void) modifyPurchasedArray
{
    for (int i = 0 ; i<self.allIngredientsArray.count; i++)
    {
        NSDictionary *ingredientDict = [self.allIngredientsArray objectAtIndex:i];
        NSArray *recipeArray = [ingredientDict objectForKey:@"recipe"];
        for (int j = 0 ; j<recipeArray.count; j++)
        {
            if ([[[recipeArray objectAtIndex:j] objectForKey:@"purchased"] isEqualToString:@"yes"])
            {
                NSString *ingredientStr = [NSString stringWithFormat:@"%@ %@",[[recipeArray objectAtIndex:j] objectForKey:@"quantity"],[[recipeArray objectAtIndex:j] objectForKey:@"ingredient"]];
                [self.itemsPurchasedArray addObject:ingredientStr];
            }
        }
    }
    [[NSUserDefaults standardUserDefaults] setObject:self.itemsPurchasedArray forKey:@"previousArray"];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UITableViewDatasouce /UITableViewDelegate Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (indexPath.section == 0) {
        return 0;
    }
    if (self.isAllRecipeSelected) {
        NSAttributedString *attributedtext;
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:16]};
        if (![[[self.allIngredientsArray objectAtIndex:indexPath.section-1] objectForKey:@"recipe"] count]>1)
        {
            NSString *ingredientStr = [NSString stringWithFormat:@"%@ %@",[[[self.allIngredientsArray objectAtIndex:indexPath.section-1] objectForKey:@"recipe"] objectForKey:@"quantity"],[[[self.allIngredientsArray objectAtIndex:indexPath.section-1] objectForKey:@"recipe"] objectForKey:@"ingredient"]];
            attributedtext = [[NSAttributedString alloc] initWithString:ingredientStr attributes:attributes];
        }
        else
        {
            attributedtext = [[NSAttributedString alloc] initWithString:[[self.allIngredientsArray objectAtIndex:indexPath.section-1] objectForKey:@"ingredient"] attributes:attributes];
        }
        CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-120, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
        CGSize size = rect.size;
        
        return size.height+15;
    }
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section == 0) {
        return 0;
    }
    if (self.isAllRecipeSelected)
    {
        if ([[[self.allIngredientsArray objectAtIndex:section-1] objectForKey:@"recipe"] count]>1)
        {
            return [[[self.allIngredientsArray objectAtIndex:section-1] objectForKey:@"recipe"] count];
        }
    }
    return 0;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CLRecipeFilterTableViewCell *cell =  (CLRecipeFilterTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CLRecipeFilterTableViewCell"];
    
    NSString *ingredientStr = [NSString stringWithFormat:@"%@ %@",[[[[self.allIngredientsArray objectAtIndex:indexPath.section-1] objectForKey:@"recipe"] objectAtIndex:indexPath.row]objectForKey:@"quantity"],[[[[self.allIngredientsArray objectAtIndex:indexPath.section-1] objectForKey:@"recipe"] objectAtIndex:indexPath.row] objectForKey:@"ingredient"]];
    
    cell.categoryNameLabel.text =ingredientStr;
    NSAttributedString *theAttributedString;
    theAttributedString = [[NSAttributedString alloc] initWithString:ingredientStr
                                                          attributes:@{NSStrikethroughStyleAttributeName:
                                                                           [NSNumber numberWithInteger:NSUnderlineStyleSingle]}];
    if ([self.itemsPurchasedArray containsObject:cell.categoryNameLabel.text])
    {
        [cell.tickImageView setImage:[UIImage imageNamed:@"tickblue"]];
        cell.categoryNameLabel.attributedText = theAttributedString;
    }
    else
    {
        [cell.tickImageView setImage:[UIImage imageNamed:@"tickgrey"]];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        return 58;
    }
    NSArray *ingredientArray;
    NSString *ingredientStr;
    NSAttributedString *attributedtext;
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:16]};
    
    if (self.isAllRecipeSelected) {
        ingredientArray = self.allIngredientsArray;
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:16]};
        if ([[[ingredientArray objectAtIndex:section-1] objectForKey:@"recipe"] count]==1)
        {
            ingredientStr = [NSString stringWithFormat:@"%@ %@",[[[[ingredientArray objectAtIndex:section-1] objectForKey:@"recipe"] objectAtIndex:0] objectForKey:@"quantity"],[[[[ingredientArray objectAtIndex:section-1] objectForKey:@"recipe"] objectAtIndex:0]objectForKey:@"ingredient"]];
            attributedtext = [[NSAttributedString alloc] initWithString:ingredientStr attributes:attributes];
        }
        else
        {
            ingredientStr = [[ingredientArray objectAtIndex:section-1] objectForKey:@"ingredient"];
            if (ingredientStr == nil) {
                ingredientStr = [[ingredientArray objectAtIndex:section-1] objectForKey:@"ingredient_type"];
            }
            attributedtext = [[NSAttributedString alloc] initWithString:ingredientStr  attributes:attributes];
        }
    }
    else
    {
        ingredientArray = self.recipeDetailsArray;
        ingredientStr = [[ingredientArray objectAtIndex:section-1] objectForKey:@"ingredient"];
        attributedtext = [[NSAttributedString alloc] initWithString:ingredientStr  attributes:attributes];

    }

    CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-80, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    CGSize size = rect.size;
    return size.height+20;
}

-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    if (self.isLoaded) {
        if (self.allIngredientsArray.count) {
            if (self.isAllRecipeSelected) {
                return self.allIngredientsArray.count+1;
            }
            return self.recipeDetailsArray.count;
        }
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (section == 0) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 58)];
        headerView.backgroundColor = [UIColor colorWithRed:240.0/255.0 green:240.0/255.0 blue:240.0/255.0 alpha:1.0];
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 58)];
        [view setBackgroundColor:[UIColor whiteColor]];
        headerView.tag = section;
        
        UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(56,0,215, 50)];
        titleLabel.textColor = [UIColor colorWithRed:54.0/255.0 green:53.0/255.0 blue:54.0/255.0 alpha:1.0];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:20.0];
        titleLabel.text = [@"items to buy" uppercaseString];
        
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(13, 2, 46, 45)];
        imageView.contentMode = UIViewContentModeCenter;
        [imageView setImage:[UIImage imageNamed:@"shoppinglist-new-grey"]];
        if (!self.isAllRecipeSelected) {
            UIImageView *recipeimageView = [[UIImageView alloc] initWithFrame:CGRectMake(tableView.frame.size.width-90, 3, 46, 45)];
            recipeimageView.contentMode = UIViewContentModeCenter;
            [recipeimageView setImage:[UIImage imageNamed:@"recipes-new-grey"]];
            recipeimageView.userInteractionEnabled = YES;
            
            UITapGestureRecognizer *recipeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(recipeimageViewTapped)];
            [recipeimageView addGestureRecognizer:recipeTap];
            [view addSubview:recipeimageView];
            
            UIImageView *removeFromListImageView = [[UIImageView alloc] initWithFrame:CGRectMake(tableView.frame.size.width-55, 3, 46, 45)];
            removeFromListImageView.contentMode = UIViewContentModeCenter;
            [removeFromListImageView setImage:[UIImage imageNamed:@"shoppinglist-remove"]];
            removeFromListImageView.userInteractionEnabled = YES;
            
            UITapGestureRecognizer *removeRecipeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeRecipeImageViewTapped)];
            [removeFromListImageView addGestureRecognizer:removeRecipeTap];
            [view addSubview:removeFromListImageView];
        }
        
        UIView *bottomSeperatorView = [[UIView alloc] initWithFrame:CGRectMake(13, 50, tableView.frame.size.width-26, 2)];
        bottomSeperatorView.backgroundColor = [UIColor colorWithRed:192.0/255.0 green:192.0/255.0 blue:192.0/255.0 alpha:1.0];
        [view addSubview:imageView];
        [view addSubview:titleLabel];
        [view addSubview:bottomSeperatorView];
        [headerView addSubview:view];
        return headerView;
    }
    
    NSAttributedString *attributedtext;
    NSString *ingredientStr;
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:16]};
    
    int count=0;
    NSArray *ingredientArray;
    if (self.isAllRecipeSelected) {
        ingredientArray = self.allIngredientsArray;
        if ([[[ingredientArray objectAtIndex:section-1] objectForKey:@"recipe"] count]==1)
        {
            ingredientStr = [NSString stringWithFormat:@"%@ %@",[[[[ingredientArray objectAtIndex:section-1] objectForKey:@"recipe"] objectAtIndex:0] objectForKey:@"quantity"],[[[[ingredientArray objectAtIndex:section-1] objectForKey:@"recipe"] objectAtIndex:0]objectForKey:@"ingredient"]];
            attributedtext = [[NSAttributedString alloc] initWithString:ingredientStr attributes:attributes];
        }
        else
        {
            for (int i = 0; i< [[[ingredientArray objectAtIndex:section-1] objectForKey:@"recipe"] count]; i++)
            {
                NSString *ingredientTextStr = [NSString stringWithFormat:@"%@ %@",[[[[ingredientArray objectAtIndex:section-1] objectForKey:@"recipe"] objectAtIndex:i] objectForKey:@"quantity"],[[[[ingredientArray objectAtIndex:section-1] objectForKey:@"recipe"] objectAtIndex:i]objectForKey:@"ingredient"]];
                if ([self.itemsPurchasedArray containsObject:ingredientTextStr])
                {
                    count ++;
                }
            }
            ingredientStr = [[ingredientArray objectAtIndex:section-1] objectForKey:@"ingredient"];
            if (ingredientStr == nil) {
                ingredientStr = [[ingredientArray objectAtIndex:section-1] objectForKey:@"ingredient_type"];
            }
            attributedtext = [[NSAttributedString alloc] initWithString:ingredientStr  attributes:attributes];
        }
    }
    else
    {
        ingredientArray = self.recipeDetailsArray;
        
        ingredientStr = [NSString stringWithFormat:@"%@ %@",[[ingredientArray objectAtIndex:section-1] objectForKey:@"quantity"],[[ingredientArray objectAtIndex:section-1] objectForKey:@"ingredient"]];
        attributedtext = [[NSAttributedString alloc] initWithString:ingredientStr  attributes:attributes];
    }
    

    CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-80, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    
    UIView *headeView = [[UIView alloc] initWithFrame:CGRectMake(10, 0, rect.size.width, rect.size.height+20)];
    headeView.backgroundColor = [UIColor whiteColor];
    
    UIImageView *tickImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 30, 30)];
    UILabel *ingredientLabel = [[UILabel alloc] initWithFrame:CGRectMake(45, 10, self.view.frame.size.width-80, rect.size.height+10)];
    
    [tickImageView setImage:[UIImage imageNamed:@"tickgrey"]];
    tickImageView.contentMode = UIViewContentModeCenter;
    
    NSAttributedString *theAttributedString;
    theAttributedString = [[NSAttributedString alloc] initWithString:ingredientStr
                                                          attributes:@{NSStrikethroughStyleAttributeName:
                                                                           [NSNumber numberWithInteger:NSUnderlineStyleSingle]}];
    
    if (count == [[[self.allIngredientsArray objectAtIndex:section-1] objectForKey:@"recipe"] count]) {
        [tickImageView setImage:[UIImage imageNamed:@"tickblue"]];
        ingredientLabel.attributedText = theAttributedString;

    }
    else
    {
        if ([self.itemsPurchasedArray containsObject:ingredientStr])
        {
            [tickImageView setImage:[UIImage imageNamed:@"tickblue"]];
            ingredientLabel.attributedText = theAttributedString;
        }
        else
        {
            [tickImageView setImage:[UIImage imageNamed:@"tickgrey"]];
            ingredientLabel.text = ingredientStr;
        }
    }
    
    ingredientLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:16.0];
    ingredientLabel.numberOfLines = 0;
    
    
    
    [headeView addSubview:tickImageView];
    [headeView addSubview:ingredientLabel];
    headeView.tag = section;
    UITapGestureRecognizer *headerViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(headerViewTapped:)];
    [headeView addGestureRecognizer:headerViewTap];
    return headeView;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CLRecipeFilterTableViewCell *cell = (CLRecipeFilterTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];

    if ([self.itemsPurchasedArray containsObject:cell.categoryNameLabel.text])
    {
        [self.itemsPurchasedArray removeObject:cell.categoryNameLabel.text];
    }
    else
    {
        [self.itemsPurchasedArray addObject:cell.categoryNameLabel.text];
    }
    [self.shoppingItemsTableView reloadData];
}

-(void)menuButtonTapped
{
    NSArray *array = [[NSUserDefaults standardUserDefaults] objectForKey:@"previousArray"];
    if (array.count == self.itemsPurchasedArray.count)
    {
        [self showLeftMenuItems];

    }
    else
    {
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Would you like to save changes" delegate:self cancelButtonTitle:@"Discard" otherButtonTitles:@"Save", nil];
        errorAlert.tag = 100;
        [errorAlert show];

    }
}

- (IBAction)addRecipeAction:(id)sender
{
    CLRecipeViewController *recipeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeViewController"];
    recipeVC.isFromShoppingList = YES;
    recipeVC.delegate = self;
    [self.navigationController pushViewController:recipeVC animated:YES];
}

-(void)addPurchasedItemsToRecipeWith:(NSDictionary *)params
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        
        [self.networkManager startGETRequestWithAPI:@"purchaselist/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             NSString *errorStr = [responseDictionary objectForKey:@"msg"];
             
             if ([errorCode isEqualToString:@"200"])
             {
                 [self showLeftMenuItems];
             }
             else
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
             }
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [errorAlert show];
             
         }];
    }

}


#pragma mark UITap/UIbutton action methods

-(void)recipeimageViewTapped
{
    CLRecipeDetailsViewController *recipedetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeDetailsViewController"];
    recipedetailVC.recipeID = [[self.recipeDetailsArray objectAtIndex:0] objectForKey:@"recipe"];
    recipedetailVC.delegate = self;
    recipedetailVC.isFromShoppingDetails = YES;
    [self.navigationController pushViewController:recipedetailVC animated:YES];
}

-(void)removeRecipeImageViewTapped
{
    NSString *message = [NSString stringWithFormat:@"Would u like to remove %@ from Shopping list",[[self.recipeDetailsArray objectAtIndex:0] objectForKey:@"recipe_name"]];
    
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:message delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [alertView show];
}

-(void)recipeFilterViewTapped
{
    [self addBlackView];
    self.ingredientPopUpView = [[[NSBundle mainBundle] loadNibNamed:@"CLIngredientPopUpView" owner:self options:nil]objectAtIndex:0];
    if (self.allRecipeArray.count>8) {
        self.ingredientPopUpView.frame = CGRectMake(10, 10, 300, 8*35+80);
    }
    else
    {
        self.ingredientPopUpView.frame = CGRectMake(10, 10, 300, self.allRecipeArray.count*35+80);
    }
    self.ingredientPopUpView.center = self.view.center;
    [self.ingredientPopUpView configureViewWithNewData:self.allRecipeArray];
    self.ingredientPopUpView.delegate = self;
    [self.view addSubview:self.ingredientPopUpView];
    [self ViewZoomInEffect:self.ingredientPopUpView];
}

#pragma mark CLRecipeDetailsViewControllerDelegate Mathods

-(void)loadShoppingList
{
    [self loadAllIngredients];
}


-(void)addBlackView
{
    if (!self.blackView)
    {
        self.blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.blackView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        UITapGestureRecognizer *blackViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(blackViewTapped)];
        [self.blackView addGestureRecognizer:blackViewTap];
        [self.view addSubview:self.blackView];
    }
    else
    {
        self.blackView.hidden = NO;
    }
}

-(void)removeBlackView
{
    [self.blackView removeFromSuperview];
    self.blackView = nil;
}

-(void)blackViewTapped
{
    [self ViewZoomOutEffect:self.ingredientPopUpView];
    [self removeBlackView];
}


#pragma mark - CLIngredientPopUpViewDelegate Methods

-(void)recipePopUpViewCloseButtonTapped
{
    [self ViewZoomOutEffect:self.ingredientPopUpView];
    [self removeBlackView];
}

-(void)recipeSelectedWithRecipeTitle:(NSString *)recipeName
{
    self.recipeDetailsArray = [NSMutableArray new];
    [self ViewZoomOutEffect:self.ingredientPopUpView];
    [self removeBlackView];
    self.filterViewLabel.text = recipeName;
    if (![recipeName isEqualToString:@"All Recipes"])
    {
        self.isAllRecipeSelected = NO;
        for (int i = 0 ; i<self.allIngredientsArray.count; i++)
        {
            NSDictionary *ingredientDict = [self.allIngredientsArray objectAtIndex:i];
            NSArray *recipeArray = [ingredientDict objectForKey:@"recipe"];
            for (int j = 0 ; j<recipeArray.count; j++)
            {
                if ([[[recipeArray objectAtIndex:j] objectForKey:@"recipe_name"] isEqualToString:recipeName])
                {
                    [self.recipeDetailsArray addObject:[recipeArray objectAtIndex:j]];
                }
            }
        }
    }  
    else
    {
        self.isAllRecipeSelected = YES;
    }
    [self.shoppingItemsTableView reloadData];
}

-(void)headerViewTapped:(UITapGestureRecognizer *)tapView
{
    UIView *headerView = [self.view viewWithTag:tapView.view.tag];
    
    int index = tapView.view.tag-1;
    if (self.isAllRecipeSelected)
    {
        if ([[[self.allIngredientsArray objectAtIndex:tapView.view.tag-1] objectForKey:@"recipe"] count]>1)
        {
            if ([self.headerArray containsObject:[[self.allIngredientsArray objectAtIndex:index] objectForKey:@"ingredient"]])
            {
                [self.headerArray removeObject:[[self.allIngredientsArray objectAtIndex:index] objectForKey:@"ingredient"]];
                
                for (int i = 0; i<[[[self.allIngredientsArray objectAtIndex:index] objectForKey:@"recipe"] count]; i++)
                {
                    NSString *ingredientStr = [NSString stringWithFormat:@"%@ %@",[[[[self.allIngredientsArray objectAtIndex:index] objectForKey:@"recipe"] objectAtIndex:i] objectForKey:@"quantity"],[[[[self.allIngredientsArray objectAtIndex:index] objectForKey:@"recipe"] objectAtIndex:i]objectForKey:@"ingredient"]];
                    if ([self.itemsPurchasedArray containsObject:ingredientStr]) {
                        [self.itemsPurchasedArray removeObject:ingredientStr];
                    }
                }
            }
            else
            {
                for (int i = 0; i<[[[self.allIngredientsArray objectAtIndex:index] objectForKey:@"recipe"] count]; i++)
                {
                    NSString *ingredientStr = [NSString stringWithFormat:@"%@ %@",[[[[self.allIngredientsArray objectAtIndex:index] objectForKey:@"recipe"] objectAtIndex:i] objectForKey:@"quantity"],[[[[self.allIngredientsArray objectAtIndex:index] objectForKey:@"recipe"] objectAtIndex:i]objectForKey:@"ingredient"]];
                    if (![self.itemsPurchasedArray containsObject:ingredientStr]) {
                        [self.itemsPurchasedArray addObject:ingredientStr];
                    }
                }

                [self.headerArray addObject:[[self.allIngredientsArray objectAtIndex:index] objectForKey:@"ingredient"]];
            }
        }
        else
        {
            for (UILabel *label in headerView.subviews) {
                if ([label isKindOfClass:[UILabel class]]) {
                    if ([self.itemsPurchasedArray containsObject:label.text]) {
                        [self.itemsPurchasedArray removeObject:label.text];
                    }
                    else
                    {
                        [self.itemsPurchasedArray addObject:label.text];
                    }
                }
            }
        }
    }
    else
    {
        for (UILabel *label in headerView.subviews) {
            if ([label isKindOfClass:[UILabel class]]) {
                if ([self.itemsPurchasedArray containsObject:label.text]) {
                    [self.itemsPurchasedArray removeObject:label.text];
                }
                else
                {
                    [self.itemsPurchasedArray addObject:label.text];
                }
            }
        }
    }
    
    [self.shoppingItemsTableView reloadData];
}

-(void)showLeftMenuItems
{
    [self.leftMenuView loadDetailsOfLeftMenuView];
    self.leftMenuView.hidden = NO;
    self.leftMenuBlackView.hidden = NO;
    [self.view bringSubviewToFront:self.leftMenuBlackView];
    [UIView animateWithDuration:0.2 animations:^{
        [self.view bringSubviewToFront:self.leftMenuView];
        self.leftMenuView.frame = CGRectMake(0, 0, 210, self.view.frame.size.height-10);
    }];
}

#pragma mark- UIAlertView Delegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex)
    {
        case 0:
            if (alertView.tag == 100) {
                [self modifyPurchasedArray];
                [self showLeftMenuItems];
                [self.shoppingItemsTableView reloadData];
            }
        break;
        case 1:
            if (alertView.tag == 100)
            {
                [self updateSelectedIngredients];
            }
            else
            {
                [self removeRecipeFromShoppingList];
            }
        break;
        
        default:
        break;
    }
}

-(void)updateSelectedIngredients
{
    NSMutableArray *paramsArray = [NSMutableArray new];
    
    for (int i = 0 ; i<self.allIngredientsArray.count; i++)
    {
        NSDictionary *ingredientDict = [self.allIngredientsArray objectAtIndex:i];
        NSArray *recipeArray = [ingredientDict objectForKey:@"recipe"];
        for (int j = 0 ; j<recipeArray.count; j++)
        {
            NSString *ingredientStr = [NSString stringWithFormat:@"%@ %@",[[[[self.allIngredientsArray objectAtIndex:i] objectForKey:@"recipe"] objectAtIndex:j] objectForKey:@"quantity"],[[[[self.allIngredientsArray objectAtIndex:i] objectForKey:@"recipe"] objectAtIndex:j]objectForKey:@"ingredient"]];
            
            NSString *purchaseString;
            if ([self.itemsPurchasedArray containsObject:ingredientStr]) {
                purchaseString = @"yes";
            }
            else
            {
                purchaseString = @"no";
            }
            NSString *paramsStr = [NSString stringWithFormat:@"%@_%@_%@",[[recipeArray objectAtIndex:j] objectForKey:@"list_id"],[[recipeArray objectAtIndex:0] objectForKey:@"list_key"],purchaseString];
            [paramsArray addObject:paramsStr];
        }
    }
    NSString *paramsStr = [NSString stringWithFormat:@"%@",[paramsArray componentsJoinedByString:@","]];
    NSDictionary *params = @{@"list":paramsStr};
    
    [self addPurchasedItemsToRecipeWith:params];
}
-(void)removeRecipeFromShoppingList
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        NSDictionary *params = @{@"list":[[self.recipeDetailsArray objectAtIndex:0] objectForKey:@"list_id"],
                                 @"user":self.userManager.user.userId};
        
        [self.networkManager startGETRequestWithAPI:@"deletelist/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             NSString *msg = [responseDictionary objectForKey:@"msg"];
             
             if ([errorCode isEqualToString:@"200"])
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:[msg capitalizedString] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
                 self.isAllRecipeSelected = YES;
                 self.filterViewLabel.text = @"ALL Recipes";
                 [self loadAllIngredients];
             }
             else
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
             }
             
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }

}

-(void)loadShoppingListWithNewDetails
{
    [self loadAllIngredients];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
