//
//  CLHelpViewController.m
//  Clover
//
//  Created by Ajith Kumar on 03/02/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLHelpViewController.h"
#import "CLConstants.h"
#import "CLAppDelegate.h"

@interface CLHelpViewController ()<UIScrollViewDelegate>
@property (nonatomic,strong) NSArray *cautionArray;
@property (nonatomic, strong) CLAppDelegate *appDelegate;
@end

@implementation CLHelpViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    self.gotItButton.hidden = YES;

    self.cautionArray = @[@"Register via Facebook, Twitter or manually and create a new account",@"Use the menu on left to navigate",@"Recipes can be filtered by Mealtime, Cuisine, Dietary Requirements, Ingredients and Preparation time",@"Rate or review a recipe or add it to your favourites, shopping list, meal plan or share it with your friend",@"Use Clover Club to access latest news from the show, tips and tricks, as well as fabulous competitions"];

    self.cautionLabel.text = [self.cautionArray objectAtIndex:0];
    [self.view bringSubviewToFront:self.skipButton];

    // Do any additional setup after loading the view.
    [self configureScrollViewByAddingAllImages];
    self.mainScrollView.pagingEnabled = YES;
    self.mainScrollView.showsHorizontalScrollIndicator = NO;
    self.mainScrollView.showsVerticalScrollIndicator = NO;
    
    self.appDelegate = [[UIApplication sharedApplication] delegate];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    self.bottomImageView.translatesAutoresizingMaskIntoConstraints  = YES;
    self.bottomImageView.frame = CGRectMake(0, 700, self.bottomImageView.frame.size.width, self.bottomImageView.frame.size.height);

    [UIView animateWithDuration:0.5 animations:^{
        self.bottomImageView.frame = CGRectMake(0, self.view.frame.size.height-200, self.bottomImageView.frame.size.width, self.bottomImageView.frame.size.height);
    }];
    
    [[UIApplication  sharedApplication] setStatusBarHidden:YES];
    
}

-(void)configureScrollViewByAddingAllImages
{
    UIImageView *previousImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, (self.view.frame.size.height-191)/2, 22, 22)];
    [previousImageView setImage:[UIImage imageNamed:@"leftnew"]];
    previousImageView.tag = 101;
    previousImageView.hidden = YES;
    [self.view addSubview:previousImageView];
    
    UIImageView *nextImageView = [[UIImageView alloc] initWithFrame:CGRectMake(self.view.frame.size.width-22, (self.view.frame.size.height-191)/2, 22, 22)];
    [nextImageView setImage:[UIImage imageNamed:@"rightnew"]];
    nextImageView.tag = 102;
    nextImageView.hidden = NO;
    [self.view addSubview:nextImageView];

    CGFloat xAxis = 0;
    for (int i = 0; i<5; i++)
    {
        UIImageView *onboardImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xAxis, -20, self.view.frame.size.width, self.view.frame.size.height-180)];
        [onboardImageView setImage:[UIImage imageNamed:[NSString stringWithFormat:@"screenshot%d.png",i+1]]];
        
        [self.mainScrollView addSubview:onboardImageView];
        xAxis= xAxis + self.view.frame.size.width;
        
        onboardImageView.alpha = 0.2;
        [UIView animateWithDuration:0.5 animations:^{
            onboardImageView.alpha = 1.0;
        }];
        
    }
    UILabel *swipeLabel = [[UILabel alloc] init];
    
    if (IS_IPHONE_4) {
        swipeLabel.frame = CGRectMake(2, 0, self.view.frame.size.width-4, 30);
        swipeLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:13.0];

    }
    else if (IS_IPHONE_6)
    {
        swipeLabel.frame = CGRectMake(10, 2, self.view.frame.size.width-20, 60);
        swipeLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:17.0];

    }
    else
    {
        swipeLabel.frame = CGRectMake(10, 0, self.view.frame.size.width-20, 40);
        swipeLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:15.0];
    }
    
    swipeLabel.numberOfLines = 0;
    swipeLabel.textAlignment = NSTextAlignmentCenter;
    swipeLabel.textColor = CL_BLUE_COLOR;
    swipeLabel.text = @"Scroll to the right to learn about the app or skip below";
    [self.mainScrollView bringSubviewToFront:swipeLabel];
    [self.mainScrollView addSubview:swipeLabel];
    
    self.mainScrollView.contentSize = CGSizeMake(xAxis, self.mainScrollView.frame.size.height-100);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
//    for (UIView *view in self.mainScrollView.subviews)
//    {
//        if ([view isKindOfClass:[UIImageView class]])
//        {
//            view.alpha = 0.2;
//            [UIView animateWithDuration:0.2 animations:^{
//                view.alpha = 1.0;
//            }];
//        }
//    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    
    int page = scrollView.contentOffset.x/self.view.frame.size.width;
    UIImageView *imageView = (UIImageView *)[self.view viewWithTag:101];
    
    if (page != 0)
    {
        imageView.hidden = NO;
    }
    else
    {
        imageView.hidden = YES;
    }
    UIImageView *nextImageView = (UIImageView *)[self.view viewWithTag:102];
    if (page==4) {
        nextImageView.hidden = YES;
    }
    else
    {
        nextImageView.hidden = NO;
    }
  
    self.pageControl.currentPage = page;
    
    [UIView animateWithDuration:0.2 animations:^{
            self.cautionLabel.alpha = 0.0;
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.2 animations:^{
            self.cautionLabel.text = [self.cautionArray objectAtIndex:page];
            self.cautionLabel.alpha = 1.0;
            if (page == 4) {
                self.gotItButton.hidden = NO;
                self.pageControl.hidden = YES;
                self.skipButton.hidden = YES;

                [self.view bringSubviewToFront:self.gotItButton];
            }
            else
            {
                self.gotItButton.hidden = YES;
                self.pageControl.hidden = NO;
                self.skipButton.hidden = NO;
            }
        }];
    }];
}

- (IBAction)gotItButtonAction:(id)sender
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    if (self.isFromLeftMenu) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        [self.appDelegate loadInitialViewController];
    }
}

- (IBAction)skipButtonAction:(id)sender
{
    [[UIApplication sharedApplication] setStatusBarHidden:NO];
    if (self.isFromLeftMenu) {
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else
    {
        [self.appDelegate loadInitialViewController];
    }
}

@end
