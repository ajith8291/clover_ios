//
//  CLHomeNewsAndEventsTableViewCell.m
//  Clover
//
//  Created by Ajith Kumar on 16/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLHomeNewsAndEventsTableViewCell.h"
#import "NSString+DataValidator.h"
#import <UIImageView+AFNetworking.h>
#import "UIImage+colors.h"

@implementation CLHomeNewsAndEventsTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCellWithData:(CLNewsAndEvents *)newsEvents
{
    self.eventNameLabel.text = [NSString stringWithFormat:@"%@",newsEvents.newsTitle];
    self.eventDateLabel.text = [newsEvents.publishDate getDateFromString];
    self.eventContentLabel.text = newsEvents.newsContent;
    
    NSString *imgUrl = [newsEvents.imageURL stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%d",600]];
    imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%d",370]];
    
    UIImage *placeholderImg = [UIImage imageWithColor:[UIColor lightGrayColor]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
    
    __weak UIImageView *weakImg = self.eventImageView;
    
    [self.eventImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = image;
             weakImg.alpha = 0.0;
             
             [UIView animateWithDuration:1.0 animations:^{
                 weakImg.alpha = 1.0;
             }];
         });
         
         
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
         
     }];

}

@end
