//
//  CLHomeNewsAndEventsTableViewCell.h
//  Clover
//
//  Created by Ajith Kumar on 16/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLNewsAndEvents.h"

@interface CLHomeNewsAndEventsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *eventImageView;
@property (weak, nonatomic) IBOutlet UILabel *eventNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *eventContentLabel;

-(void)configureCellWithData:(CLNewsAndEvents *)newsEvents;
@end
