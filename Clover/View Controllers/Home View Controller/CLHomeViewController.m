//
//  ViewController.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLHomeViewController.h"
#import "CLCustomNavigationView.h"
#import "CLHomeTableViewCell.h"
#import "CLAppDelegate.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "NSString+DataValidator.h"
#import "CLConstants.h"
#import "CLRecipeDetailsViewController.h"
#import "CLReviewsViewController.h"
#import "CLHomeNewsAndEventsTableViewCell.h"
#import "CLWebViewController.h"
#import "CLSettingsViewController.h"
#import "CLRecipeView.h"
#import "CLNewsAndEvents.h"
#import "CLNewsViewController.h"

@interface CLHomeViewController ()<CLCustomNavigationViewDelegate,CLHomeTableViewCellDelegate,CLRecipeDetailsViewControllerDelegate,CLRecipeViewDelegate,UIScrollViewDelegate,UIActionSheetDelegate>


@property (nonatomic, strong) CLCustomNavigationView *navigationView;
@property (nonatomic, strong) NSArray *recipeArray;
@property (nonatomic, strong) NSArray *newsArray;
@property (nonatomic, assign) BOOL isLoaded;
@end

@implementation CLHomeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Do any additional setup after loading the view, typically from a nib.
    self.homeTabelView.translatesAutoresizingMaskIntoConstraints = YES;
    
    self.homeTabelView.frame = CGRectMake(0, 340, self.view.frame.size.width, self.view.frame.size.height-340);
    BOOL isFirstTimeregistration =  [[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstTimeRegistration"];

    if (isFirstTimeregistration)
    {
        CLSettingsViewController *settingsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLSettingsViewController"];
        [self.navigationController pushViewController:settingsVC animated:NO];
        
    }
    self.navigationView.navTitleTextLabel.hidden = YES;
    self.navigationView.navBackButton.hidden = YES;
    self.navigationView.navEditButton.hidden = YES;
    self.navigationView.navSearchButton.hidden = YES;
    self.navigationView.navCartButton.hidden = YES;
    self.pageControl.hidden = YES;
    [self.homeTabelView setShowsVerticalScrollIndicator:NO];
    [self callWebServiceToFetchHomeDetails];
}

-(void)callWebServiceToFetchHomeDetails
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        
        [self.networkManager startGETRequestWithAPI:@"cloverhome" andParameters:nil withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             NSString *errorStr = [responseDictionary objectForKey:@"msg"];
             
             if ([errorCode isEqualToString:@"200"])
             {
                 self.recipeArray = [responseDictionary objectForKey:@"receipes"];
                 self.newsArray = [responseDictionary objectForKey:@"news"];
                 [self createScrollImagesForRecipeWithImageArray:self.recipeArray];
                 self.isLoaded = YES;
                 self.pageControl.hidden = NO;
                 [self.homeTabelView reloadData];
             }
             else
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
             }
             
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSUserDefaults standardUserDefaults] setInteger:0  forKey:@"previousIndexPath"];
    [self.leftMenuView.leftMenuTableView reloadData];
    self.navigationController.navigationBarHidden = YES;
    self.leftMenuBlackView.hidden = YES;
     
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark UITableViewDelegate Methods


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.newsArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CLNewsAndEvents *newsEvent = [CLNewsAndEvents getnewsAndEventsInformationFrom:[self.newsArray objectAtIndex:indexPath.row]];
    
    CLHomeNewsAndEventsTableViewCell *cell = (CLHomeNewsAndEventsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"CLHomeNewsAndEventsTableViewCell" forIndexPath:indexPath];
    [cell configureCellWithData:newsEvent];
    cell.frame = CGRectMake(0, 0, self.view.frame.size.width, cell.frame.size.height);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CLNewsAndEvents *newsEvent = [CLNewsAndEvents getnewsAndEventsInformationFrom:[self.newsArray objectAtIndex:indexPath.row]];
    CLNewsViewController *newsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLNewsViewController"];
    newsVC.newsId = newsEvent.newsId;
    [self.navigationController pushViewController:newsVC animated:YES];

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CLNewsAndEvents *newsEvent = [CLNewsAndEvents getnewsAndEventsInformationFrom:[self.newsArray objectAtIndex:indexPath.row]];
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:18]};
    NSString *recipenameStr = [NSString stringWithFormat:@"%@",newsEvent.newsTitle];
    
    NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:recipenameStr attributes:attributes];
    CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-103, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    
    CGSize size = rect.size;
        

    return size.height+70;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if (self.isLoaded)
    {
        return 50;
    }
    return 0;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    if (self.isLoaded) {
        UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0,0, self.view.frame.size.width, 50)];
        UIImageView *bgImageView = [[UIImageView alloc] initWithFrame:headerView.frame];
        [bgImageView setImage:[UIImage imageNamed:@"Background.png"]];
        [headerView addSubview:bgImageView];
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-10, 50)];
        label.text = @"News & Events";
        label.font = [UIFont fontWithName:ROBOTO_BOLD size:18.0];
        label.textColor = CL_BLUE_COLOR;
        [headerView addSubview:label];
        
        UIButton *seeAllButton = [[UIButton alloc] initWithFrame:CGRectMake(self.view.frame.size.width-60, 0, 50, 50)];
        [seeAllButton setTitle:@"See All" forState:UIControlStateNormal];
        [seeAllButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        seeAllButton.titleLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:14.0];
        [seeAllButton addTarget:self action:@selector(buttonSelected) forControlEvents:UIControlEventTouchUpInside];
        [headerView addSubview:seeAllButton];
        seeAllButton.tag = 1100;

        headerView.backgroundColor = [UIColor clearColor];
        return headerView;
    }
    return nil;
}


-(void)buttonSelected
{
    UIButton *seeAllButton = (UIButton *)[self.view viewWithTag:1100];
    static BOOL isSeeAllSelected = YES;
    self.homeTabelView.translatesAutoresizingMaskIntoConstraints = YES;
    if (isSeeAllSelected)
    {
//        [UIView transitionWithView:self.homeTabelView duration:0.8 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
        [UIView animateWithDuration:0.5 animations:^{
            self.homeTabelView.frame = CGRectMake(0, 70, self.view.frame.size.width, self.view.frame.size.height-70);
            self.pageControl.hidden = YES;
            self.headerNameLabel.hidden = YES;
            self.containerView.hidden = YES;
            [seeAllButton setTitle:@"Hide" forState:UIControlStateNormal];
        }];
    }
    else
    {
        self.pageControl.hidden = NO;
        self.headerNameLabel.hidden = NO;
        self.containerView.hidden = NO;
        
//        [UIView transitionWithView:self.homeTabelView duration:0.8 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
        [UIView animateWithDuration:0.5 animations:^{
            self.homeTabelView.frame = CGRectMake(0, 340, self.view.frame.size.width, self.view.frame.size.height-340);
            [seeAllButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            [seeAllButton setTitle:@"See All" forState:UIControlStateNormal];
            
        }];
    }
    
    isSeeAllSelected = !isSeeAllSelected;
}


-(void)popularRecipeTapped:(UITapGestureRecognizer *)tapView
{
    NSInteger tag = tapView.view.tag;
    [self popularRecipeSelectedAtInder:tag];
}


-(void)popularRecipeSelectedAtInder:(NSInteger)position
{
    CLRecipe *recipe =[CLRecipe getInformationFromDictionary:[self.recipeArray objectAtIndex:position]];
    CLRecipeDetailsViewController *recipedetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeDetailsViewController"];

    recipedetailVC.selectedRecipe = recipe;
    recipedetailVC.delegate = self;
    [self.navigationController pushViewController:recipedetailVC animated:YES];

}

- (void)createScrollImagesForRecipeWithImageArray:(NSArray *)imageArray
{
    self.headerNameLabel.text = @"Popular Recipe";
    float xAxis = 0;
    if (self.recipeScrolView == nil)
    {
        self.recipeScrolView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, self.view.frame.size.width, 226)];
    }
    self.recipeScrolView.delegate = self;
    
    self.recipeScrolView.pagingEnabled = YES;
    self.recipeScrolView.scrollEnabled = YES;
    int count;
    if (imageArray.count>5)
    {
        count = 5;
    }
    else
    {
        count = imageArray.count;
    }
    self.pageControl.numberOfPages = count;
    self.pageControl.userInteractionEnabled = NO;
    self.recipeScrolView.contentSize = CGSizeMake(self.view.frame.size.width * count, self.recipeScrolView .frame.size.height);
    [self.recipeScrolView setShowsHorizontalScrollIndicator:NO];
    
    for (int im = 0; im < count; im++)
    {
        CLRecipe *recipe =[CLRecipe getInformationFromDictionary:[imageArray objectAtIndex:im]];
        CLRecipeView *recipeView = [[[NSBundle mainBundle] loadNibNamed:@"CLRecipeView" owner:self options:nil]objectAtIndex:0];
        recipeView.isFromHome = YES;
        recipeView.frame = CGRectMake(xAxis, 0, self.view.frame.size.width,recipeView.frame.size.height);
        [recipeView configureRecipeViewWithRecipe:recipe];
        recipeView.tag = im;
        recipeView.delegate = self;
        UITapGestureRecognizer *recipeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(popularRecipeTapped:)];
        [recipeView addGestureRecognizer:recipeTap];
        [self.recipeScrolView addSubview:recipeView];
        
        xAxis = xAxis + self.view.frame.size.width;
    }
    [self.containerView addSubview:self.recipeScrolView];
    
}

#pragma mark - CLRecipeViewDelegate

-(void)loadReviewViewControllerWithSelectedRecipe:(CLRecipe *)recipe
{
    CLRecipeDetailsViewController *recipedetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeDetailsViewController"];
    recipedetailVC.selectedRecipe = recipe;
    recipedetailVC.delegate = self;
    recipedetailVC.isUserReviewSelected = YES;
    [self.navigationController pushViewController:recipedetailVC animated:YES];
}


-(void)playButtonTappedWithRecipe:(CLRecipe *)selectedRecipe
{
    CLRecipeDetailsViewController *detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeDetailsViewController"];
    detailsVC.selectedRecipe = selectedRecipe;
    detailsVC.isPlayButtonTapped = YES;
    [self.navigationController pushViewController:detailsVC animated:NO];
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.recipeScrolView )
    {
        int page = scrollView.contentOffset.x/310;
        self.pageControl.currentPage = page;
    }
}


#pragma mark CLRecipeDetailsViewControllerDelegate Methods

-(void)addedRecipeReviewAndRatings
{
    [self callWebServiceToFetchHomeDetails];
}


@end
