//
//  CLHomeTableViewCell.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 27/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLHomeTableViewCell.h"
#import <UIImageView+AFNetworking.h>
#import "UIImage+colors.h"
#import "CLRecipe.h"
#import "CLConstants.h"
#import "CLRecipeView.h"


@implementation CLHomeTableViewCell

- (void)awakeFromNib {
    // Initialization code
    self.imageScrollView.translatesAutoresizingMaskIntoConstraints = YES;
    self.recipeImageView.hidden = YES;
    self.bottomView.hidden = YES;

}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark PageControl Action Methods

- (IBAction)pageControlActionMethod:(id)sender
{
//    UIPageControl *pgControl = sender;
//    
//    int page = pgControl.currentPage;
//    
//    CGRect frame = self.imageScrollView.frame;
//    frame.origin.x = frame.size.width * page;
//    frame.origin.y = 0;
//    
//    [self.imageScrollView scrollRectToVisible:frame animated:YES];

}

-(void)configureRecipeCellWithArray:(NSArray *)recipeArray
{
    [self createScrollImagesForRecipeWithImageArray:recipeArray];
    
}


- (void)createScrollImagesForRecipeWithImageArray:(NSArray *)imageArray
{
    float xAxis = 0;
    if (self.recipeScrolView == nil)
    {
        self.recipeScrolView = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 44, self.frame.size.width, 226)];
    }
    self.recipeScrolView.delegate = self;
    
    self.recipeScrolView.pagingEnabled = YES;
    self.recipeScrolView.scrollEnabled = YES;
    int count;
    if (imageArray.count>5)
    {
        count = 5;
    }
    else
    {
        count = imageArray.count;
    }
    self.pageControl.numberOfPages = count;
    self.pageControl.userInteractionEnabled = NO;
    self.recipeScrolView.contentSize = CGSizeMake(self.frame.size.width * count, self.recipeScrolView .frame.size.height);
    [self.recipeScrolView setShowsHorizontalScrollIndicator:NO];

    for (int im = 0; im < count; im++)
    {
        CLRecipe *recipe =[CLRecipe getInformationFromDictionary:[imageArray objectAtIndex:im]];
        CLRecipeView *recipeView = [[[NSBundle mainBundle] loadNibNamed:@"CLRecipeView" owner:self options:nil]objectAtIndex:0];
        recipeView.isFromHome = YES;
        recipeView.frame = CGRectMake(xAxis, 0, self.frame.size.width,recipeView.frame.size.height);
        [recipeView configureRecipeViewWithRecipe:recipe];
        recipeView.tag = im;
        recipeView.delegate = self;
        UITapGestureRecognizer *recipeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(popularRecipeTapped:)];
        [recipeView addGestureRecognizer:recipeTap];
        [self.recipeScrolView addSubview:recipeView];

        xAxis = xAxis + self.frame.size.width;
    }
    [self.contentView addSubview:self.recipeScrolView];
    
}


#pragma mark - Scrollview delegate
- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.recipeScrolView )
    {
        int page = scrollView.contentOffset.x/310;
        self.pageControl.currentPage = page;
    }
}


#pragma mark UIButton/Tap Action methods

-(void)popularRecipeTapped:(UITapGestureRecognizer *)tapView
{
    NSInteger tag = tapView.view.tag;
    [self.delegate popularRecipeSelectedAtInder:tag];
}


#pragma mark CLRecipeViewDelegate Methods

-(void)loadReviewViewControllerWithSelectedRecipe:(CLRecipe *)recipe
{
    [self.delegate loadReviewOfSelectedRecipe:recipe];
}

-(void)playButtonTappedWithRecipe:(CLRecipe *)selectedRecipe
{
    [self.delegate playVideoOfSelectedRecipe:selectedRecipe];
}

@end
