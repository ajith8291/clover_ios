//
//  CLHomeTableViewCell.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 27/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLRecipe.h"
#import "CLRecipeView.h"
@protocol CLHomeTableViewCellDelegate <NSObject>

@optional
-(void)popularRecipeSelectedAtInder:(NSInteger) position;
-(void)loadReviewOfSelectedRecipe:(CLRecipe *)recipe;
-(void)playVideoOfSelectedRecipe:(CLRecipe *)recipe;
@end

@interface CLHomeTableViewCell : UITableViewCell<UIScrollViewDelegate,CLRecipeViewDelegate>


@property (weak, nonatomic) IBOutlet UIScrollView *imageScrollView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UILabel *headerViewLabel;
@property (weak, nonatomic) IBOutlet UIImageView *recipeImageView;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *bottomViewTitleNameLabel;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

-(void)configureRecipeCellWithArray: (NSArray *)recipeArray;

@property (nonatomic, strong) UIScrollView *recipeScrolView;

@property (nonatomic, weak) id<CLHomeTableViewCellDelegate>delegate;
@end
