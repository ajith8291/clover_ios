//
//  CLUploadedPictureViewController.m
//  Clover
//
//  Created by Ajith Kumar on 13/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLNewsViewController.h"
#import <UIImageView+AFNetworking.h>
#import "NSString+DataValidator.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "CLNewsAndEvents.h"
#import "NSString+DataValidator.h"
#import "CLConstants.h"
#import "UIImage+colors.h"

@interface CLNewsViewController ()
@property (nonatomic, strong) CLNewsAndEvents *newsObj;
@property (nonatomic, assign) BOOL a;

@end

@implementation CLNewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationView.navLogoImageView.hidden = YES;
    self.navigationView.navMenuButton.hidden = YES;
    self.navigationView.navEditButton.hidden = YES;
    self.navigationView.navSearchButton.hidden = YES;
    self.navigationView.navTitleTextLabel.hidden = NO;
    self.navigationView.navCartButton.hidden = YES;
    self.navigationView.navTitleTextLabel.text = [@"News" capitalizedString];
    [self callWebServiceToGetDetails];
}

-(void)callWebServiceToGetDetails
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        NSDictionary *params = @{@"news":self.newsId};
        
        [self.networkManager startGETRequestWithAPI:@"singlenews/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
//             self.gallaryArray = [[responseDictionary objectForKey:@"album"] objectForKey:@"gallery"];
             self.newsObj = [CLNewsAndEvents getnewsAndEventsInformationFrom:[responseDictionary objectForKey:@"news"]];
             [self configureViewDetails];
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }
    
}


-(void)configureViewDetails
{
    self.newsTitleLabel.text = self.newsObj.newsTitle;
    self.dateAddedLabel.text = [self.newsObj.publishDate getDateFromString];
    self.newsObj.newsContent = [NSString stringWithFormat:@"%@",self.newsObj.newsContent];
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:16]};
    NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:self.newsObj.newsContent attributes:attributes];
    
    CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-20, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    CGSize size = rect.size;
    self.newsContentLabel.translatesAutoresizingMaskIntoConstraints = YES;
    self.newsContentLabel.frame = CGRectMake(self.newsContentLabel.frame.origin.x, self.newsContentLabel.frame.origin.y, self.view.frame.size.width-20, size.height);
    
    self.newsContentLabel.text = self.newsObj.newsContent;
    self.newsObj.featuredImageURL = [self.newsObj.featuredImageURL stringByReplacingOccurrencesOfString:@"(null)" withString:@""];

    if ([self.newsObj.featuredImageURL isEqualToString:@""])
    {
        [self.pictureImageView setImage:[UIImage imageNamed:@"no-image"]];
    }
    else
    {
        NSString *imgUrl = [self.newsObj.featuredImageURL stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%d",600]];
        imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%d",370]];
        
        UIImage *placeholderImg = [UIImage imageWithColor:[UIColor lightGrayColor]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
        
        __weak UIImageView *weakImg = self.pictureImageView;
        
        [self.pictureImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 weakImg.image = image;
                 weakImg.alpha = 0.0;
                 
                 [UIView animateWithDuration:1.0 animations:^{
                     weakImg.alpha = 1.0;
                 }];
             });
             
             
         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
             
             
         }];
    }
    
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, self.newsContentLabel.frame.origin.y+self.newsContentLabel.frame.size.height);
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
