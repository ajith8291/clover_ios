//
//  CLUploadedPictureViewController.h
//  Clover
//
//  Created by Ajith Kumar on 13/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLHighlights.h"
#import "CLNewsAndEvents.h"
#import "CLBaseViewController.h"

@interface CLNewsViewController : CLBaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *pictureImageView;
@property (weak, nonatomic) IBOutlet UILabel *newsTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateAddedLabel;
@property (weak, nonatomic) IBOutlet UILabel *newsContentLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *mainScrollView;

@property (nonatomic, strong) NSString *newsId;

@end
