//
//  CLReviewsViewController.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 08/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLBaseViewController.h"
#import "CLRecipe.h"

@interface CLReviewsViewController : CLBaseViewController<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *reviewTableView;
@property (nonatomic, strong)CLRecipe *selectedRecipe;
@property (nonatomic, strong)NSArray *commentArray;
@end
