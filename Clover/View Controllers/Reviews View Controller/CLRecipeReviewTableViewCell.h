//
//  CLRecipeReviewTableViewCell.h
//  Clover
//
//  Created by Ajith Kumar on 19/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLComments.h"
#import "CLUploadPicture.h"

@protocol  CLRecipeReviewTableViewCellDelegate<NSObject>

@optional

-(void)recipeNameTappedWith:(NSString *)recipeID;

@end

@interface CLRecipeReviewTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *reviewLabel;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIImageView *uploadedImageView;
@property (weak, nonatomic) IBOutlet UILabel *recipeNameLabel;

@property (nonatomic, strong) CLComments *selectedComments;

-(void)configureCellWithData:(CLComments *)comments;

-(void)configureCellWithDataWithPicture:(CLUploadPicture *)picture;

@property (nonatomic, assign) BOOL isFromSettings;

@property (nonatomic, weak) id<CLRecipeReviewTableViewCellDelegate>delegate;
@end
