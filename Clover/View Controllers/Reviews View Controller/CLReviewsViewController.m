//
//  CLReviewsViewController.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 08/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLReviewsViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "CLComments.h"
#import "CLConstants.h"
#import "CLReviewTableViewCell.h"
#import "CLRecipeReviewTableViewCell.h"
#import "NSString+DataValidator.h"
#import "CLRecipeDetailsViewController.h"


@interface CLReviewsViewController ()<CLRecipeReviewTableViewCellDelegate>

@property (nonatomic, strong) CLComments *comments;

@end

@implementation CLReviewsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationView.navLogoImageView.hidden = YES;
    self.navigationView.navMenuButton.hidden = YES;
    self.navigationView.navEditButton.hidden = YES;
    self.navigationView.navSearchButton.hidden = YES;
    self.navigationView.navCartButton.hidden = YES;
    self.navigationView.navTitleTextLabel.hidden = NO;
    self.navigationView.navTitleTextLabel.text = [@"recipe reviews" capitalizedString];
    if (IS_IOS8_OR_GREATER)
    {
        self.reviewTableView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    else
    {
        self.reviewTableView.translatesAutoresizingMaskIntoConstraints = YES;
        self.reviewTableView.frame = CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height-50);
    }
    UINib *nib = [UINib nibWithNibName:@"CLRecipeReviewTableViewCell" bundle:nil];
    
    [self.reviewTableView registerNib:nib forCellReuseIdentifier:@"CLRecipeReviewTableViewCell"];


}

//-(void)callWebServiceToGetRecipesReview
//{
//    if ([self isNetworkIsReachable])
//    {
//        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
//        hud.dimBackground = YES;
//        hud.mode = MBProgressHUDModeIndeterminate;
//        hud.labelText = @"Loading...";
//        [hud show:YES];
//        
//        //  http://cloverapi.jumpcatch.com/api/getreviews/?recipe=67
//        
//        NSDictionary *params = @{@"recipe":self.selectedRecipe.recipeId};
//        
//        [self.networkManager startGETRequestWithAPI:@"getreviews/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
//         {
//             [hud hide:YES];
//             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
//             NSString *msg = [responseDictionary objectForKey:@"msg"];
//             
//             if ([errorCode isEqualToString:@"200"])
//             {
//                 NSArray *reviewsCommentArray = [responseDictionary objectForKey:@"comments"];
//                 for (int i = 0; i<reviewsCommentArray.count; i++)
//                 {
//                     CLComments *comments = [CLComments getCommentsdetailsFromDictionary:[reviewsCommentArray objectAtIndex:i]];
//                     [self.reviewsArray addObject:comments];
//                 }
//                 [self.reviewTableView reloadData];
//             }
//             else
//             {
//                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//                 [errorAlert show];
//             }
//             
//             
//         } andFailure:^(NSString *errorMessage) {
//             [hud hide:YES];
//             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
//             [alert show];
//         }];
//
//    }
//}

#pragma mark UITableVewController Datasource/Delegate Methods


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.commentArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.comments =[CLComments getCommentsdetailsFromDictionary:[self.commentArray objectAtIndex:indexPath.row]];
    CLRecipeReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CLRecipeReviewTableViewCell"];
    cell.isFromSettings = YES;
    cell.dateLabel.text = [self.comments.commentDate getDateFromString];
    cell.reviewLabel.text = self.comments .commentTitle;
    cell.nameLabel.textColor = CL_BLUE_COLOR;
    cell.recipeNameLabel.textColor = CL_BLUE_COLOR;
    cell.frame = CGRectMake(0, 0, self.view.frame.size.width, cell.frame.size.height);
    [cell configureCellWithData:self.comments];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.delegate = self;
    if ([self.comments.commentPictureUrl isEmptyString])
    {
        cell.recipeNameLabel.text = self.comments.recipeName;
        cell.nameLabel.hidden = YES;
    }
    else
    {
        cell.nameLabel.text = self.comments.recipeName;
        cell.recipeNameLabel.hidden = YES;
    }
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    self.comments =[CLComments getCommentsdetailsFromDictionary:[self.commentArray objectAtIndex:indexPath.row]];
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:17]};
    NSString *recipenameStr = self.comments.recipeName;
    NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:recipenameStr attributes:attributes];
    CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-70, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    
    if ([self.comments.commentPictureUrl isEmptyString])
    {
        rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-20, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    }
    else
    {
        rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-70, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    }
    CGSize size = rect.size;
    
    NSDictionary *CommentsAttributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:15]};
    NSString *commentsStr = self.comments.commentTitle;
    NSAttributedString *CommentsAttributedtext = [[NSAttributedString alloc] initWithString:commentsStr attributes:CommentsAttributes];
    CGRect rect1 = [CommentsAttributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-20, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    CGSize size1 = rect1.size;


    return size.height+size1.height+45;
}



- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark CLRecipeReviewTableViewCellDelegate Methods

-(void)recipeNameTappedWith:(NSString *)recipeID
{
    CLRecipeDetailsViewController *detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeDetailsViewController"];
    detailsVC.recipeID = recipeID;
    [self.navigationController pushViewController:detailsVC animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
