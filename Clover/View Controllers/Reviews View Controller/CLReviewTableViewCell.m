//
//  CLReviewTableViewCell.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 08/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLReviewTableViewCell.h"
#import "NSString+DataValidator.h"

@implementation CLReviewTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCellWithComments:(CLComments *)comments
{
    self.userNameLabel.text = comments.user.userName;
    self.dateLabel.text =[[NSString stringWithFormat:@"%@",comments.commentDate] getDateFromString];
    self.reviewLabel.text = [NSString stringWithFormat:@"%@",comments.commentTitle];
}
@end
