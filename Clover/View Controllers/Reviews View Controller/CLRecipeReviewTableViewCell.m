//
//  CLRecipeReviewTableViewCell.m
//  Clover
//
//  Created by Ajith Kumar on 19/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLRecipeReviewTableViewCell.h"
#import "NSString+DataValidator.h"
#import <UIImageView+AFNetworking.h>
#import "UIImage+colors.h"

@implementation CLRecipeReviewTableViewCell

- (void)awakeFromNib {
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCellWithData:(CLComments *)comments
{
    if (self.isFromSettings) {
        self.nameLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *recipeTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(recipeTitleTapped)];
        [self.nameLabel addGestureRecognizer:recipeTap];
        
        self.recipeNameLabel.userInteractionEnabled = YES;
        UITapGestureRecognizer *recipeNameTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(recipeTitleTapped)];
        [self.recipeNameLabel addGestureRecognizer:recipeNameTap];
    }

    self.selectedComments = comments;
    if ([comments.commentPictureUrl isEmptyString]) {
        self.uploadedImageView.hidden = YES;
    }
    else
    {
        NSString *imgUrl = [comments.commentPictureUrl stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%d",600]];
        imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%d",370]];
        
        UIImage *placeholderImg = [UIImage imageWithColor:[UIColor lightGrayColor]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
        
        __weak UIImageView *weakImg = self.uploadedImageView;
        
        [self.uploadedImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 weakImg.image = image;
                 weakImg.alpha = 0.0;
                 
                 [UIView animateWithDuration:1.0 animations:^{
                     weakImg.alpha = 1.0;
                 }];
             });
             
             
         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
             
             
         }];
    }
    
}

-(void)configureCellWithDataWithPicture:(CLUploadPicture *)picture
{
    NSString *imgUrl = [picture.uploadPicture stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%d",600]];
    imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%d",370]];
    
    UIImage *placeholderImg = [UIImage imageWithColor:[UIColor lightGrayColor]];
    
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
    
    __weak UIImageView *weakImg = self.uploadedImageView;
    
    [self.uploadedImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
     {
         
         dispatch_async(dispatch_get_main_queue(), ^{
             weakImg.image = image;
             weakImg.alpha = 0.0;
             
             [UIView animateWithDuration:1.0 animations:^{
                 weakImg.alpha = 1.0;
             }];
         });
         
         
     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
         
         
     }];
}

#pragma mark UITap/UIButton Action Methods

-(void)recipeTitleTapped
{
    [self.delegate recipeNameTappedWith:self.selectedComments.recipeId];
}

@end
