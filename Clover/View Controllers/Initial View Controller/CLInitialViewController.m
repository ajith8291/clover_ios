//
//  CLInitialViewController.m
//  Clover
//
//  Created by Ajith Kumar on 26/02/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLInitialViewController.h"
#import "CLLoginViewController.h"
#import "CLRegistrationViewController.h"
#import "CLRecoverPasswordView.h"
#import "CLAppDelegate.h"
#import <STTwitter/STTwitter.h>
#import <MBProgressHUD/MBProgressHUD.h>

@interface CLInitialViewController ()<CLRecoverPasswordViewDelegate,FBLoginViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UIView *facebookView;
@property (weak, nonatomic) IBOutlet UIView *twitterView;
@property (weak, nonatomic) IBOutlet UIView *loginView;
@property (weak, nonatomic) IBOutlet UIView *signUpView;
@property (weak, nonatomic) IBOutlet UIView *forgotPasswordView;


@property (nonatomic, strong) UIView *blackView;
@property (nonatomic, strong) CLRecoverPasswordView *recoverPasswordView;
@property (nonatomic, strong) UIScrollView *containerScrollView;
@property (nonatomic, strong) CLAppDelegate *appDelegate;

@property (nonatomic, strong) NSString *twitterUserId;
@property (nonatomic, strong) NSString *twitterScreen;
@property (nonatomic, strong) CLNetworkManager *networkManager;
@property (nonatomic, strong) CLUserManager *userManager;

@end

@implementation CLInitialViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationController.navigationBarHidden = YES;
    [self configureViewAndAddGesture];
    [self addContainerScrollView];
    self.appDelegate = [[UIApplication sharedApplication] delegate];
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isNotFirstRun"];
    [self facebookLoginFlow];
    self.containerView.center = self.view.center;
    self.networkManager = [CLNetworkManager sharedManager];
    self.userManager = [CLUserManager sharedManager];
}

-(void)configureViewAndAddGesture
{
    UITapGestureRecognizer *twitterTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(signUpTwitterViewTapped)];
    [self.twitterView addGestureRecognizer:twitterTap];
    
    UITapGestureRecognizer *loginTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loginViewTapped)];
    [self.loginView addGestureRecognizer:loginTap];

    UITapGestureRecognizer *signUpTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(signUpViewTapped)];
    [self.signUpView addGestureRecognizer:signUpTap];

    UITapGestureRecognizer *forgotPasswordTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(forgotPasswordViewTapped)];
    [self.forgotPasswordView addGestureRecognizer:forgotPasswordTap];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)addContainerScrollView
{
    self.containerScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    self.containerScrollView.contentSize = CGSizeMake(320, 568);
    self.containerView.translatesAutoresizingMaskIntoConstraints = YES;
    
    [self.containerScrollView addSubview:self.containerView];
    [self.view addSubview:self.containerScrollView];
}

#pragma mark - UIButton/UITap Action

-(void)signUpTwitterViewTapped
{
    if ([self.appDelegate isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        
        ACAccountStore *accountStore = [[ACAccountStore alloc] init];
        
        // Create an account type that ensures Twitter accounts are retrieved.
        ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        
        // Request access from the user to use their Twitter accounts.
        [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
            if(granted) {
                // Get the list of Twitter accounts.
                NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
                // For the sake of brevity, we'll assume there is only one Twitter account present.
                // You would ideally ask the user which account they want to tweet from, if there is more than one Twitter account present.
                if ([accountsArray count] > 0) {
                    [hud hide:YES];
                    // Grab the initial Twitter account to tweet from.
                    ACAccount *twitterAccount = [accountsArray objectAtIndex:0];
                    NSLog(@"Account details %@",twitterAccount);
                    self.twitterUserId = [[twitterAccount valueForKey:@"properties"] valueForKey:@"user_id"];
                    self.twitterScreen = [twitterAccount valueForKey:@"username"];
                    NSLog(@"user id %@",self.twitterUserId);
                    [self getProfileDetailsFromTwitter];
                    
                }
                else
                {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"There are no Twitter accounts configured. You can add or create a twitter account in Settings." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alertView show];
                    [hud hide:YES];
                }
            }
            [hud hide:YES];
        }];
    }

}

- (void) getProfileDetailsFromTwitter
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading...";
    [hud show:YES];
    
    STTwitterAPI *twitter = [STTwitterAPI twitterAPIAppOnlyWithConsumerKey:@"r2SP7Za01Z7Kr5QMwerBSnkQe" consumerSecret:@"NBd80I5Y3mA3lC1A6njfqevUMSAQniwdRwUKa0zaKmoJzPexUG"]; //twitterAPIApplicationOnlyWithConsumerKey:@"" consumerSecret:@""];
    __block NSString *token;
    [twitter verifyCredentialsWithSuccessBlock:^(NSString *bearerToken) {
        NSLog(@"%@",bearerToken);
        token = bearerToken;
        [hud hide:YES];
        [twitter getUsersShowForUserID:self.twitterUserId orScreenName:nil includeEntities:nil successBlock:^(NSDictionary *user) {
            NSString *profileImageURLString = [user valueForKey:@"profile_image_url"];
            NSString *location = [user valueForKey:@"location"];
            NSString *name = [user valueForKey:@"name"];
            NSString *email = [user valueForKey:@"screen_name"];
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.dimBackground = YES;
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = @"Loading...";
            [hud show:YES];
            
            
            NSDictionary *params = @{@"email":email,
                                     @"uname":name,
                                     @"location":location,
                                     @"profileimg":profileImageURLString,
                                     @"social":@"true",
                                     @"twitter":@"yes",
                                     @"devicetoken":[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"],
                                     @"devicetype":@"ios",
                                     @"twaccesstoken":token
                                     };
            
            
            [self.networkManager startGETRequestWithAPI:@"cloversignin/" andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
                NSLog(@"Response : %@",responseDictionary);
                [hud hide:YES];
                NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
                NSString *errorStr = [responseDictionary objectForKey:@"msg"];
                
                if ([errorCode isEqualToString:@"200"])
                {
                    NSDictionary *userInfo = [responseDictionary objectForKey:@"userinfo"];
                    self.userManager.user = [CLUser getUserInformationFrom:userInfo];
                    CLAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                    [appDelegate loadHomeViewController];
                }
                else
                {
                    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [errorAlert show];
                }
                
            } andFailure:^(NSString *errorMessage) {
                [hud hide:YES];
                UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [errorAlert show];
                NSLog(@"Error : %@",errorMessage);
            }];
            
            
        } errorBlock:^(NSError *error) {
            NSLog(@"Error : %@", error.localizedDescription);
            //
            [hud hide:YES];
        }];
        
    } errorBlock:^(NSError *error) {
        NSLog(@"Error: %@", error.localizedDescription);
        //
        [hud hide:YES];
    }];
}


#pragma mark Facebook Login methods

-(void)facebookLoginFlow
{
    if ([self.appDelegate isNetworkIsReachable])
    {
        FBLoginView *loginView = [[FBLoginView alloc] initWithReadPermissions: @[@"public_profile", @"email",@"user_location",@"user_hometown"]];//@"user_friends",@"user_birthday", @"user_location"
        loginView.delegate = self;
        
        loginView.frame = CGRectMake(30, 315, 260, 40);
        loginView.backgroundColor = [UIColor colorWithRed:0.39 green:0.27 blue:0.24 alpha:1];
        
        for (id obj in loginView.subviews)
        {
            if ([obj isKindOfClass:[UIButton class]])
            {
                UIButton *loginButton =  (UIButton *)obj;
                {
//                                    loginButton.layer.cornerRadius = 3.0;
//                                    loginButton.layer.borderColor = [UIColor colorWithRed:194.0/255.0 green:194.0/255.0 blue:194.0/255.0 alpha:1.0].CGColor;
//                                    loginButton.layer.borderWidth = 1.3;
//                                    loginButton.layer.masksToBounds=YES;
//                    
                    loginButton.contentMode=UIViewContentModeCenter;
                    [loginButton setImage:[UIImage imageNamed:@"facebook-btn"] forState:UIControlStateNormal];
                    [loginButton setImage:[UIImage imageNamed:@"facebook-btn"] forState:UIControlStateHighlighted];
                    
                }
                [loginButton setBackgroundImage:nil forState:UIControlStateHighlighted];
                [loginButton setBackgroundImage:nil forState:UIControlStateNormal];
            }
            if ([obj isKindOfClass:[UILabel class]])
            {
                UILabel * loginLabel =  obj;
                loginLabel.frame = CGRectMake(0, 0, 0, 0);
            }
        }
        
        loginView.backgroundColor=[UIColor clearColor];
        [self.containerView addSubview:loginView];
    }
}

-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading...";
    [hud show:YES];
    
    NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [user objectID]];
    NSString *fbAccessToken = [[[FBSession activeSession] accessTokenData] accessToken];
    
    NSString *emailId = [user objectForKey:@"email"];
    NSString *userName = [user objectForKey:@"name"];
    //    NSString *userLocation = [[user objectForKey:@"location"] objectForKey:@"name"];
    
    //                             @"fbaccesstoken":fbAccessToken,
    
    // @"location":userLocation,
    
    NSDictionary *params = @{@"email":emailId,
                             @"fbaccesstoken":fbAccessToken,
                             @"uname":userName,
                             @"profileimg": userImageURL,
                             @"social":@"true",
                             @"fb":@"yes",
                             @"devicetoken":[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"],
                             @"devicetype":@"ios"
                             };
    
    [self.networkManager startGETRequestWithAPI:@"cloversignin/" andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
        NSLog(@"Response : %@",responseDictionary);
        [hud hide:YES];
        NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
        NSString *errorStr = [responseDictionary objectForKey:@"msg"];
        
        if ([errorCode isEqualToString:@"200"])
        {
            NSDictionary *userInfo = [responseDictionary objectForKey:@"userinfo"];
            self.userManager.user = [CLUser getUserInformationFrom:userInfo];
            CLAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
            [appDelegate loadHomeViewController];
        }
        else
        {
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [errorAlert show];
        }
        
    } andFailure:^(NSString *errorMessage) {
        [hud hide:YES];
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [errorAlert show];
        NSLog(@"Error : %@",errorMessage);
    }];
    
}


-(void)loginViewTapped
{
    CLLoginViewController *loginVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLLoginViewController"];
    [self.navigationController pushViewController:loginVC animated:YES];
}

-(void)signUpViewTapped
{
    CLRegistrationViewController *registrationVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRegistrationViewController"];
    [self.navigationController pushViewController:registrationVC animated:YES];
}

-(void)forgotPasswordViewTapped
{
    [self addBlackView];
    self.recoverPasswordView = [[[NSBundle mainBundle] loadNibNamed:@"CLRecoverPasswordView" owner:self options:nil] objectAtIndex:0];
    self.recoverPasswordView.center = self.view.center;
    self.recoverPasswordView.delegate = self;
    [self.view addSubview:self.recoverPasswordView];
    [self.appDelegate ViewZoomInEffect:self.recoverPasswordView];
}


#pragma mark black view custom methods

-(void)addBlackView
{
    if (!self.blackView)
    {
        self.blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.blackView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        [self.view addSubview:self.blackView];
    }
    else
    {
        self.blackView.hidden = NO;
    }
}

-(void)removeBlackView
{
    [self.blackView removeFromSuperview];
    self.blackView = nil;
}

#pragma CLRecoverPasswordViewDelegate Methods

-(void)recoverPasswordCloseButtonTapped
{
    [self.appDelegate ViewZoomOutEffect:self.recoverPasswordView];
    [self removeBlackView];
}

-(void)recoverPasswordResetPasswordButtonTapped
{
    if ([self.appDelegate isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        NSDictionary *params = @{@"email":self.recoverPasswordView.emailIdTextField.text};
        
        [self.networkManager startGETRequestWithAPI:@"cloverreset/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             NSString *errorStr = [responseDictionary objectForKey:@"msg"];
             [hud hide:YES];
             if ([errorCode isEqualToString:@"200"])
             {
                 [self removeBlackView];
                 [self.appDelegate ViewZoomOutEffect:self.recoverPasswordView];
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"An email has been successfully sent" message:@"Please check your email to retrieve new password" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [alert show];
             }
             else
             {
                 [hud hide:YES];
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [alert show];
             }
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
             
         }];
    }
}


@end
