//
//  CLBaseViewController.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLNetworkManager.h"
#import "CLSharedManager.h"
#import "CLUserManager.h"
#import "CLCustomNavigationView.h"
#import "CLLeftMenuView.h"

//typedef enum
//{
//    CLFilterTypeRecipe,
//
//}CLFilterType;

@interface CLBaseViewController : UIViewController<CLCustomNavigationViewDelegate,CLLeftMenuViewDelegate>

@property (nonatomic, strong) CLNetworkManager *networkManager;
@property (nonatomic, strong) CLSharedManager *sharedManager;
@property (nonatomic, strong) CLUserManager *userManager;
@property (nonatomic, strong) CLCustomNavigationView *navigationView;
@property (nonatomic, strong) CLLeftMenuView *leftMenuView;

@property (nonatomic, strong) UIView *statusView;
@property (nonatomic, strong) UIView *leftMenuBlackView;
-(void)loadNavigationViewDetails;
-(void)loadLeftMenuViewDetails;
-(BOOL)isNetworkIsReachable;

//ANIMATION Effects
- (void)bounceOutAnimationStoped:(UIView*)aview;
- (void)ViewZoomInEffect:(UIView *)aView;
- (void)ViewZoomOutEffect:(UIView *)aView;
- (void)rotateViewCompletely:(UIView *)aView;

@end
