//
//  CLBaseViewController.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLBaseViewController.h"
#import "CLRecipeFilterViewController.h"
#import <AFNetworking/AFNetworking.h>
#import "CLSettingsViewController.h"
#import "CLAppDelegate.h"
#import <FacebookSDK/FacebookSDK.h>
#import "CLMealPlannerViewController.h"
#import "CLFavouritesViewController.h"
#import "CLShoppingListDetailsViewController.h"
#import "CLConstants.h"
#import "Reachability.h"
#import "CLCloverClubViewController.h"
#import "CLHelpViewController.h"
#import "CLRecipeView.h"

@interface CLBaseViewController ()<UIActionSheetDelegate,CLRecipeViewDelegate>
@property (nonatomic, strong) NSString *recipeVideoUrl;
@end

@implementation CLBaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.networkManager = [CLNetworkManager sharedManager];
    self.sharedManager = [CLSharedManager sharedManager];
    self.userManager = [CLUserManager sharedManager];
    [self loadNavigationViewDetails];
    [self loadLeftMenuViewDetails];
    [self loadLeftMenuBlackView];
    
    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.leftMenuBlackView addGestureRecognizer:swipeleft];

}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.leftMenuView.hidden = YES;
}
#pragma mark Loading Navigation Bar View

-(void)loadNavigationViewDetails
{
    self.statusView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 20)];
    self.statusView.backgroundColor = CL_BLUE_COLOR;
    [self.view addSubview:self.statusView];

    self.navigationView = [[[NSBundle mainBundle] loadNibNamed:@"CLCustomNavigationView" owner:self options:nil] objectAtIndex:0];
    self.navigationView.frame = CGRectMake(0, 20, self.view.frame.size.width, 50);
    self.navigationView.delegate = self;
    [self.view addSubview:self.navigationView];
}

#pragma mark Loading Left Menu View

-(void)loadLeftMenuViewDetails
{
    self.leftMenuView = [[[NSBundle mainBundle] loadNibNamed:@"CLLeftMenuView" owner:self options:nil] objectAtIndex:0];
    self.leftMenuView.frame = CGRectMake(-210, 0, 210, self.view.frame.size.height-10);
    self.leftMenuView.delegate = self;
    [self.view addSubview:self.leftMenuView];
}

-(void)loadLeftMenuBlackView
{
    self.leftMenuBlackView = [[UIView alloc] initWithFrame:self.view.frame];
    self.leftMenuBlackView.backgroundColor = [UIColor blackColor];
    self.leftMenuBlackView.alpha = 0.2;
    
    UITapGestureRecognizer *blackViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(leftMenuBlackViewTapped)];
    [self.leftMenuBlackView addGestureRecognizer:blackViewTap];
    self.leftMenuBlackView.hidden = YES;
    
    [self.view addSubview:self.leftMenuBlackView];
}


#pragma mark CLCustomNavigationViewDelegate methods

-(void)menuButtonTapped
{
    [self.leftMenuView loadDetailsOfLeftMenuView];
    self.leftMenuView.hidden = NO;
    self.leftMenuBlackView.hidden = NO;
    [self.view bringSubviewToFront:self.leftMenuBlackView];
    [UIView animateWithDuration:0.2 animations:^{
        [self.view bringSubviewToFront:self.leftMenuView];
        self.leftMenuView.frame = CGRectMake(0, 0, 210, self.view.frame.size.height-10);
    }];
}

-(void)searchButtonTapped
{
    
}

-(void)shoppingCartButtonTapped
{
    
}

-(void)backButtonTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark CLLeftMenuViewDelegate Methods

-(void)menuTappedATIndex:(NSIndexPath *)indexPath
{
    [UIView animateWithDuration:0.2 animations:^{
        self.leftMenuView.frame = CGRectMake(-210, 0, 210, self.view.frame.size.height-10);
    } completion:^(BOOL finished) {
        self.leftMenuBlackView.hidden = YES;
        switch (indexPath.row)
        {
            case 0:
                [self.navigationController popToRootViewControllerAnimated:NO];
                break;
                
            case 1:
                [self loadRecipeFilterViewController];
                break;
                
            case 2:
                [self loadMealPlannerViewController];
                break;
            case 3:
                [self loadShoppingListViewController];
                break;
            case 4:
                [self loadFavouritesViewController];
                break;
            case 5:
                [self loadCloverClubViewController];
                break;
            case 6:
                [self loadSettingsViewController];
                break;
            case 7:
                [self loadHelpViewController];
                break;
            case 8:
                [self logoutAction];
                break;
            default:
                [self.navigationController popToRootViewControllerAnimated:NO];
                break;
        }
        
    }];
}

-(void)loadRecipeFilterViewController
{
//    UINavigationController *navCtr = self.navigationController;
//    [navCtr popToRootViewControllerAnimated:NO];
    CLRecipeFilterViewController *filterVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeFilterViewController"];
    [self.navigationController pushViewController:filterVC animated:NO];
}

-(void)loadMealPlannerViewController
{
    CLMealPlannerViewController *mealPlannerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLMealPlannerViewController"];
    [self.navigationController pushViewController:mealPlannerVC animated:NO];
}

-(void)loadFavouritesViewController
{
    CLFavouritesViewController *favVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLFavouritesViewController"];
    [self.navigationController pushViewController:favVC animated:NO];
}

-(void)loadShoppingListViewController
{
    CLShoppingListDetailsViewController *shoppingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLShoppingListDetailsViewController"];
    [self.navigationController pushViewController:shoppingVC animated:NO];
}

-(void)loadSettingsViewController
{
    CLSettingsViewController *settingsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLSettingsViewController"];
    [self.navigationController pushViewController:settingsVC animated:NO];
}

-(void)loadCloverClubViewController
{
    CLCloverClubViewController *cloverClubVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLCloverClubViewController"];
    [self.navigationController pushViewController:cloverClubVC animated:NO];
}

-(void)loadHelpViewController
{
    CLHelpViewController *helpVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLHelpViewController"];
    helpVC.isFromLeftMenu = YES;
    [self.navigationController pushViewController:helpVC animated:NO];
}

-(void)logoutAction
{
    CLAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
    
    if ([self.userManager.user.fbsignup isEqualToString:@"yes"]||[self.userManager.user.fbconnected isEqualToString:@"yes"])
    {
        [FBSession.activeSession closeAndClearTokenInformation];
    }
    
    [self.userManager logOutUser];
    [appDelegate loadInitialViewController];
}

#pragma mark Tap gesture Action Method

-(void)leftMenuBlackViewTapped
{
    self.leftMenuBlackView.hidden = YES;
    [UIView animateWithDuration:0.2 animations:^{
        self.leftMenuView.frame = CGRectMake(-210, 0, 210, self.view.frame.size.height-10);
    }];
}




#pragma mark Network Reachability Method

-(BOOL)isNetworkIsReachable
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus astatus = [reachability currentReachabilityStatus];
    BOOL status;
    if(astatus == NotReachable)
    {
        //No internet
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Please check your internet connection and try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        status =  NO;
        
    }
    else if (astatus == ReachableViaWiFi)
    {
        //WiFi
        status =  YES;
    }
    else if (astatus == ReachableViaWWAN)
    {
        status =  YES;
    }else
        status =  NO;
    return status;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

-(void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    [UIView animateWithDuration:0.2 animations:^{
        self.leftMenuView.frame = CGRectMake(-210, 0, 210, self.view.frame.size.height-10);
    } completion:^(BOOL finished) {
        self.leftMenuBlackView.hidden = YES;
    }];
}


#pragma mark Bounce animation

- (void)bounceOutAnimationStoped:(UIView*)aview
{
    [UIView animateWithDuration:0.2 animations:
     ^(void){
         aview.transform = CGAffineTransformScale(CGAffineTransformIdentity,0.8, 0.8);
         aview.alpha = 0.8;
     }
                     completion:^(BOOL finished){
                         [self bounceInAnimationStoped:aview];
                     }];
}

- (void)bounceInAnimationStoped:(UIView*)aview
{
    [UIView animateWithDuration:0.2 animations:
     ^(void){
         aview.transform = CGAffineTransformScale(CGAffineTransformIdentity,1, 1);
         aview.alpha = 1.0;
     }
                     completion:^(BOOL finished){
                         //[self animationStoped];
                     }];
}

-(void)ViewZoomInEffect:(UIView *)aView
{
    
    aView.alpha = 0.0f;
    aView.transform = CGAffineTransformMakeScale(0.1,0.1);
    [UIView animateWithDuration:0.3 animations:^{
        aView.transform = CGAffineTransformMakeScale(1.0,1.0);
        aView.alpha = 1.0f;
    }];

}

-(void)ViewZoomOutEffect:(UIView *)aView
{
    
    aView.alpha = 1.0f;
    aView.transform = CGAffineTransformMakeScale(1.0,1.0);
    [UIView animateWithDuration:0.3 animations:^{
        aView.transform = CGAffineTransformMakeScale(0.1,0.1);
        aView.alpha = 0.0f;
    }];
    
}

-(void)rotateViewCompletely:(UIView *)aView
{
    CABasicAnimation *rotation;
    rotation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotation.fromValue = [NSNumber numberWithFloat:0];
    rotation.toValue = [NSNumber numberWithFloat:(2*M_PI)];
    rotation.duration = 1.0; // Speed
    rotation.repeatCount = 1; // Repeat forever. Can be a finite number.
    [aView.layer addAnimation:rotation forKey:@"Spin"];
    [aView.layer setZPosition:100];
}

-(void)shareButtonTappedWithVideoUrl:(NSString *)videoUrl
{
    self.recipeVideoUrl = videoUrl;
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Share video on" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Facebook",@"Twitter", nil];
    actionSheet.tag = 123;
    [actionSheet showInView:self.view];
}

#pragma mark - UIActionSheetDelegate Method

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            NSLog(@"FaceBook\n%@",self.recipeVideoUrl);
            [self shareFBViewTapped];
            break;
        case 1:
            NSLog(@"Twitter\n%@",self.recipeVideoUrl);
            [self shareTwitterViewTapped];
            break;
        default:
            break;
    }
}

-(void)shareFBViewTapped
{
    [self.sharedManager shareViaFBWithContent:[NSString stringWithFormat:@"%@",self.recipeVideoUrl] inViewController:self WithVideoType:@"Video"];
}

-(void)shareTwitterViewTapped
{
    [self.sharedManager shareViaTwitterWithContent:[NSString stringWithFormat:@"%@",self.recipeVideoUrl] inViewController:self WithVideoType:@"Video"];
}


@end
