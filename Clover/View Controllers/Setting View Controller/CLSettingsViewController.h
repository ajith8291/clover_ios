//
//  CLSettingsViewController.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 04/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLBaseViewController.h"
#import "CLChangePasswordView.h"

@interface CLSettingsViewController : CLBaseViewController<UINavigationControllerDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate>

@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (weak, nonatomic) IBOutlet UIView *locationView;
@property (weak, nonatomic) IBOutlet UITextField *locationTextField;
@property (weak, nonatomic) IBOutlet UIImageView *cameraImageView;
@property (weak, nonatomic) IBOutlet UITextView *summaryTextView;

@property (weak, nonatomic) IBOutlet UILabel *recipeLabel;
@property (weak, nonatomic) IBOutlet UILabel *favouriteLabel;
@property (weak, nonatomic) IBOutlet UILabel *mealPlanLabel;

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UIButton *twitterDisconnectButton;
@property (weak, nonatomic) IBOutlet UIButton *facebookDisconnectButton;
@property (weak, nonatomic) IBOutlet UIView *changePasswordView;
@property (weak, nonatomic) IBOutlet UIScrollView *mainSrollView;
@property (weak, nonatomic) IBOutlet UIImageView *locationImageView;
@property (weak, nonatomic) IBOutlet UIImageView *mobileImageView;
@property (weak, nonatomic) IBOutlet UIView *mobileSeperatorView;

@property (nonatomic, strong) CLChangePasswordView *changePasswordDetailsView;
@property (weak, nonatomic) IBOutlet UIView *recipeCountView;
@property (weak, nonatomic) IBOutlet UIView *favCountView;
@property (weak, nonatomic) IBOutlet UIView *mealPlanView;
@property (weak, nonatomic) IBOutlet UIView *recipeSeperator;
@property (weak, nonatomic) IBOutlet UIView *favSeperator;
@property (weak, nonatomic) IBOutlet UIButton *sendFeedbackButton;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordButton;

@end
