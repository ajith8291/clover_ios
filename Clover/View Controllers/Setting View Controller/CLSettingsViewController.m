//
//  CLSettingsViewController.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 04/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLSettingsViewController.h"
#import "CLUserManager.h"
#import "CLUser.h"
#import "CLConstants.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "NSString+DataValidator.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "CLFavouritesViewController.h"
#import "CLMealPlannerViewController.h"
#import "CLReviewsViewController.h"
#import "CLFetchLocationManager.h"
#import "CLSendFeedbackView.h"
@interface CLSettingsViewController () <CLCustomNavigationViewDelegate, UITextFieldDelegate, UITextViewDelegate,CLChangePasswordViewDelegate,UIAlertViewDelegate,CLFetchLocationManagerDelegate,CLSendFeedbackViewDelegate>


@property (nonatomic, strong) MBProgressHUD *hud;
@property (nonatomic, assign) BOOL isEditButtonSelected;
@property (nonatomic, assign) BOOL isTextViewSelected;
@property (nonatomic, strong) UIView *blackView;
@property (nonatomic, assign) BOOL isImageUploaded;
@property (nonatomic, strong) UIView *locationSeperatorView;
@property (nonatomic, strong) NSArray *pictureCommentArray;

@property (nonatomic, assign) BOOL isDisardSelected;
@property (nonatomic, assign) BOOL isSaveSelected;
@property (nonatomic, strong) CLLocation *userLocation;
@property (nonatomic, strong) CLSendFeedbackView *sendFeedBackView;
@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;

@end

@implementation CLSettingsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.navigationBarHidden = YES;
    self.navigationView.navLogoImageView.hidden = YES;
    self.navigationView.navSearchButton.hidden = YES;
    self.navigationView.navCartButton.hidden = YES;
    self.navigationView.navTitleTextLabel.hidden = YES;
    self.navigationView.navTitleTextField.hidden = NO;
    self.navigationView.navTitleTextField.textAlignment = NSTextAlignmentCenter;
   self.navigationView.navTitleTextField.font = [UIFont fontWithName:ROBOTO_REGULAR size:22.0];
    self.navigationView.navTitleTextField.text = self.userManager.user.userName;
    self.navigationView.delegate = self;
    self.navigationView.navBackButton.hidden = YES;
    // Do any additional setup after loading the view.
    
    [self.hud hide:YES];
    self.navigationView.navTitleTextField.enabled = NO;
    self.navigationView.navTitleTextField.delegate = self;
    self.navigationView.navTitleTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.mobileTextField.enabled = NO;
    self.emailTextField.enabled = NO;
    self.locationTextField.enabled = NO;
    self.summaryTextView.editable = NO;
    self.containerView.hidden = YES;
    [self changePropertyOfViews];
    [self configureViewsAndAddGestures];
    
    [self loadPersonalDetailsOfUser];
    [self changeFrameOfViews];
    
    UITapGestureRecognizer *locationTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loadCurrentLocationOfUser)];
    [self.locationImageView addGestureRecognizer:locationTap];

    
    [self printFrameOfView];
    self.mainSrollView.showsVerticalScrollIndicator = NO;
}

-(void)printFrameOfView
{
    NSLog(@"%@",NSStringFromCGRect(self.locationView.frame));
}

-(void)changeFrameOfViews
{
    self.mainSrollView.translatesAutoresizingMaskIntoConstraints = YES;
    if (IS_IOS8_OR_GREATER)
    {
        self.mainSrollView.frame = CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height-50);
        
    }
    else
    {
        self.mainSrollView.frame = CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height-50);
    }
    self.containerView.translatesAutoresizingMaskIntoConstraints = YES;
    self.containerView.frame  = CGRectMake(0, self.containerView.frame.origin.y, self.view.frame.size.width,self.view.frame.size.height);
    
    self.cameraImageView.translatesAutoresizingMaskIntoConstraints = YES;
    self.cameraImageView.frame = CGRectMake(self.view.center.x-self.cameraImageView.frame.size.width/2 , self.cameraImageView.frame.origin.y, self.cameraImageView.frame.size.width, self.cameraImageView.frame.size.height);
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

-(void)configureViewsAndAddGestures
{
    self.locationSeperatorView = [[UIView alloc] init];
    self.locationSeperatorView.frame = CGRectMake(self.locationTextField.frame.origin.x, 20, 37, 1);
    self.locationSeperatorView.backgroundColor = [UIColor whiteColor];
    self.locationSeperatorView.hidden = YES;
    [self.locationView addSubview:self.locationSeperatorView];
    self.isEditButtonSelected = YES;
    
    
    self.recipeCountView.translatesAutoresizingMaskIntoConstraints = YES;
    self.favCountView.translatesAutoresizingMaskIntoConstraints = YES;
    self.mealPlanView.translatesAutoresizingMaskIntoConstraints = YES;
    self.recipeSeperator.translatesAutoresizingMaskIntoConstraints = YES;
    self.favSeperator.translatesAutoresizingMaskIntoConstraints = YES;
    
    CGFloat width = (self.view.frame.size.width)/3;
    self.recipeCountView.frame = CGRectMake(0,self.recipeCountView.frame.origin.y , width,30);
    self.favCountView.frame = CGRectMake(width,self.recipeCountView.frame.origin.y , width,30);
    self.mealPlanView.frame = CGRectMake(width*2,self.recipeCountView.frame.origin.y , width,30);
    
    self.recipeSeperator.frame = CGRectMake(width-1,8 , 1,18);
    self.favSeperator.frame = CGRectMake(width-1,8 , 1,18);

    UITapGestureRecognizer *reviewsTap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(reviewViewTapped)];
    [self.recipeCountView addGestureRecognizer:reviewsTap];
    
    UITapGestureRecognizer *favTap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(favCountViewTapped)];
    [self.favCountView addGestureRecognizer:favTap];
    
    UITapGestureRecognizer *mealPlannerTap =[[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(mealPlannerViewTapped)];
    [self.mealPlanView addGestureRecognizer:mealPlannerTap];

}

-(void)loadPersonalDetailsOfUser
{
    if ([self isNetworkIsReachable])
    {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.dimBackground = YES;
        self.hud.mode = MBProgressHUDModeIndeterminate;
        self.hud.labelText = @"Loading...";
        [self.hud show:YES];
        
        int userId = [self.userManager.user.userId intValue];
        
        NSDictionary *params = @{@"userid":[NSNumber numberWithInt:userId]};
        
        [self.networkManager startGETRequestWithAPI:@"cloverprofile/" andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
            NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
            NSString *errorStr = [responseDictionary objectForKey:@"msg"];
            [self.hud hide:YES];
            NSLog(@"%@",responseDictionary);
            
            if ([errorCode isEqualToString:@"200"])
            {
                NSDictionary *userInfo = [responseDictionary objectForKey:@"userinfo"];
                self.userManager.user = [CLUser getUserInformationFrom:userInfo];
                self.pictureCommentArray = [userInfo objectForKey:@"picture_comments"];
                [self configureViewWithUserDetails];
                self.containerView.hidden = NO;
                BOOL isFirstTimeregistration =  [[NSUserDefaults standardUserDefaults] boolForKey:@"isFirstTimeRegistration"];
                if (isFirstTimeregistration)
                {
                    [[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"isFirstTimeRegistration"];
                    self.isEditButtonSelected = YES;
                    [self userDetailsEditButtonTapped];
                }

            }
            else
            {
                UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [errorAlert show];
            }
            
        } andFailure:^(NSString *errorMessage) {
            [self.hud hide:YES];
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [errorAlert show];
        }];
    }
}

-(void)configureViewWithUserDetails
{
    [self.navigationView.navEditButton setImage:[UIImage imageNamed:@"EditProfile"] forState:UIControlStateNormal];
    [self.view endEditing:YES];
    if ([self.userManager.user.fbsignup isEqualToString:@"yes"]||[self.userManager.user.twsignup isEqualToString:@"yes"]) {
        self.changePasswordView.hidden = YES;
    }
    else
    {
        self.changePasswordView.hidden = NO;
    }
    self.locationImageView.userInteractionEnabled = NO;
    self.mainSrollView.contentSize = CGSizeMake(320, self.containerView.frame.size.height);
    [self.mainSrollView scrollRectToVisible:CGRectMake(0, -20, self.mainSrollView.frame.size.width, self.mainSrollView.frame.size.height) animated:YES];
    self.cameraImageView.userInteractionEnabled = NO;
    self.navigationView.navTitleTextField.enabled = NO;
    self.mobileTextField.enabled = NO;
    self.locationTextField.enabled = NO;
    self.summaryTextView.editable = NO;
    self.mobileSeperatorView.hidden = YES;
    self.summaryTextView.layer.borderColor = [[UIColor clearColor] CGColor];
    self.navigationView.navNameSeperatorView.hidden = YES;
    self.locationSeperatorView.hidden = YES;
    
    self.navigationView.navTitleTextField.text = self.userManager.user.userName;
    self.userManager.user.userLocation = [self.userManager.user.userLocation stringByReplacingOccurrencesOfString:@"null" withString:@""];
    self.userManager.user.userMobileNumber = [self.userManager.user.userMobileNumber stringByReplacingOccurrencesOfString:@"null" withString:@""];
    self.userManager.user.summary = [self.userManager.user.summary stringByReplacingOccurrencesOfString:@"null" withString:@""];

    if ([self.userManager.user.userLocation isEmptyString])
    {
        self.locationImageView.hidden = YES;
    }
    
    if ([self.userManager.user.userMobileNumber isEmptyString])
    {
        self.mobileImageView.hidden = YES;
    }
    if ([self.userManager.user.twsignup isEqualToString:@"yes"])
    {
        self.emailTextField.text = self.userManager.user.userName;
    }
    else
    {
        self.emailTextField.text = self.userManager.user.userEmail;
    }
    self.locationTextField.text = self.userManager.user.userLocation;
    self.mobileTextField.text = self.userManager.user.userMobileNumber;
    self.summaryTextView.text = self.userManager.user.summary;
    //recipe Label Details
    
    self.cameraImageView.contentMode = UIViewContentModeScaleAspectFill;
    [self.cameraImageView setImageWithURL:[NSURL URLWithString:self.userManager.user.profileURL] placeholderImage:nil];

    NSMutableAttributedString *recipeCountAtrStr = [[NSMutableAttributedString alloc] initWithString:self.userManager.user.reviewCount];
    [recipeCountAtrStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, recipeCountAtrStr.length)];
    [recipeCountAtrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:ROBOTO_REGULAR size:25.0] range:NSMakeRange(0, recipeCountAtrStr.length)];
    
    NSString *reviewStr;
    if ([self.userManager.user.reviewCount isEqualToString:@"0"]||[self.userManager.user.reviewCount isEqualToString:@"1"])
    {
        reviewStr = @" Review";
    }
    else
    {
        reviewStr = @" Reviews";
    }
    
    NSMutableAttributedString *recipeAtrStr = [[NSMutableAttributedString alloc] initWithString:reviewStr];
    [recipeAtrStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:213.0/255.0 green:211.0/255.0 blue:216.0/255.0 alpha:1.0] range:NSMakeRange(0, recipeAtrStr.length)];
    [recipeAtrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:ROBOTO_REGULAR size:12.0] range:NSMakeRange(0, recipeAtrStr.length)];
    [recipeCountAtrStr appendAttributedString:recipeAtrStr];
    
    self.recipeLabel.attributedText = recipeCountAtrStr;
    
    //favourites Label Details
    NSMutableAttributedString *favouriteCountAtrStr = [[NSMutableAttributedString alloc] initWithString:self.userManager.user.favouriteCount];
    [favouriteCountAtrStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, favouriteCountAtrStr.length)];
    [favouriteCountAtrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:ROBOTO_REGULAR size:25.0] range:NSMakeRange(0, favouriteCountAtrStr.length)];
    
    NSString *favouritesStr;
    if ([self.userManager.user.favouriteCount isEqualToString:@"0"]||[self.userManager.user.favouriteCount isEqualToString:@"1"])
    {
        favouritesStr = @" Favourite";
    }
    else
    {
        favouritesStr = @" Favourites";
    }

    NSMutableAttributedString *favouriteAtrStr = [[NSMutableAttributedString alloc] initWithString:favouritesStr];
    
    [favouriteAtrStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:213.0/255.0 green:211.0/255.0 blue:216.0/255.0 alpha:1.0] range:NSMakeRange(0, favouriteAtrStr.length)];
    [favouriteAtrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:ROBOTO_REGULAR size:12.0] range:NSMakeRange(0, favouriteAtrStr.length)];
    [favouriteCountAtrStr appendAttributedString:favouriteAtrStr];
    
    self.favouriteLabel.attributedText = favouriteCountAtrStr;
    
    //Meal Plan Label Details
    NSMutableAttributedString *mealPlanCountAtrStr = [[NSMutableAttributedString alloc] initWithString:self.userManager.user.mealPlannerCount];
    [mealPlanCountAtrStr addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, mealPlanCountAtrStr.length)];
    [mealPlanCountAtrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:ROBOTO_REGULAR size:25.0] range:NSMakeRange(0, mealPlanCountAtrStr.length)];
    
    NSString *mealPlannerStr;
    if ([self.userManager.user.mealPlannerCount isEqualToString:@"0"]||[self.userManager.user.mealPlannerCount isEqualToString:@"1"])
    {
        mealPlannerStr = @" Meal Plan";
    }
    else
    {
        mealPlannerStr = @" Meal Plans";
    }

    
    NSMutableAttributedString *mealPlanAtrStr = [[NSMutableAttributedString alloc] initWithString:@" Meal Plans"];
    [mealPlanAtrStr addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRed:213.0/255.0 green:211.0/255.0 blue:216.0/255.0 alpha:1.0] range:NSMakeRange(0, mealPlanAtrStr.length)];
    [mealPlanAtrStr addAttribute:NSFontAttributeName value:[UIFont fontWithName:ROBOTO_REGULAR size:12.0] range:NSMakeRange(0, mealPlanAtrStr.length)];
    [mealPlanCountAtrStr appendAttributedString:mealPlanAtrStr];
    
    self.mealPlanLabel.attributedText = mealPlanCountAtrStr;
    
    if ([self.userManager.user.fbsignup isEqualToString:@"yes"])
    {
        self.facebookDisconnectButton.enabled = NO;
    }
    else
    {
        self.facebookDisconnectButton.enabled = YES;
    }
    if ([self.userManager.user.twsignup isEqualToString:@"yes"])
    {
        [self.twitterDisconnectButton setTitle:@"DISCONNECT" forState:UIControlStateNormal];
        self.twitterDisconnectButton.enabled = NO;

    }
    else
    {
        [self.twitterDisconnectButton setTitle:@"CONNECT" forState:UIControlStateNormal];
        self.twitterDisconnectButton.enabled = YES;
    }
    
    if ([self.userManager.user.fbconnected isEqualToString:@"yes"])
    {
        [self.facebookDisconnectButton setTitle:@"DISCONNECT" forState:UIControlStateNormal];
    }
    else
    {
        [self.facebookDisconnectButton setTitle:@"CONNECT" forState:UIControlStateNormal];
    }
    
    if ([self.userManager.user.twconnected isEqualToString:@"yes"])
    {
        [self.twitterDisconnectButton setTitle:@"DISCONNECT" forState:UIControlStateNormal];
    }
    else
    {
        [self.twitterDisconnectButton setTitle:@"CONNECT" forState:UIControlStateNormal];
    }
    


}

-(void)changePropertyOfViews
{
    self.cameraImageView.translatesAutoresizingMaskIntoConstraints = YES;
    self.cameraImageView.frame = CGRectMake(125, 10, 70, 70);
    self.cameraImageView.layer.cornerRadius = self.cameraImageView.frame.size.width/2;
    self.cameraImageView.layer.borderWidth = 3.0;
    self.cameraImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.cameraImageView.layer.masksToBounds = YES;

    
    self.changePasswordButton.layer.cornerRadius = 3.0;
    self.changePasswordButton.layer.borderWidth = 1.0;
    self.changePasswordButton.layer.borderColor = [[UIColor colorWithRed:0 green:32.0/255.0 blue:148.0/255.0 alpha:1.0] CGColor];

    self.sendFeedbackButton.layer.cornerRadius = 3.0;
    self.sendFeedbackButton.clipsToBounds = YES;
    
    self.twitterDisconnectButton.layer.cornerRadius = 3.0;
    self.twitterDisconnectButton.layer.borderWidth = 1.0;
    self.twitterDisconnectButton.layer.borderColor = [[UIColor colorWithRed:0 green:32.0/255.0 blue:148.0/255.0 alpha:1.0] CGColor];

    self.facebookDisconnectButton.layer.cornerRadius = 3.0;
    self.facebookDisconnectButton.layer.borderWidth = 1.0;
    self.facebookDisconnectButton.layer.borderColor = [[UIColor colorWithRed:0 green:32.0/255.0 blue:148.0/255.0 alpha:1.0] CGColor];
    
    self.changePasswordView.layer.cornerRadius = 2.0;
    
    self.cameraImageView.userInteractionEnabled = NO;
    UITapGestureRecognizer *cameraImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cameraImageViewTapped)];
    [self.cameraImageView addGestureRecognizer:cameraImageViewTap];

}

#pragma mark Tap/ Button Action methods

- (IBAction)twitterDisconnectButtonAction:(id)sender
{
    [self.view endEditing:YES];
    self.mainSrollView.contentSize = CGSizeMake(320,self.containerView.frame.size.height);
    [self.mainSrollView scrollRectToVisible:CGRectMake(0, -20, self.mainSrollView.frame.size.width, self.mainSrollView.frame.size.height) animated:YES];

    if ([self isNetworkIsReachable])
    {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.dimBackground = YES;
        self.hud.mode = MBProgressHUDModeIndeterminate;
        self.hud.labelText = @"Loading...";
        [self.hud show:YES];
        //http://cloverapi.jumpcatch.com/api/cloverconnect/?userid=4&option=disconnect&type=fb
        int userId = [self.userManager.user.userId intValue];
        NSString *fbConnected;
        if ([self.userManager.user.twconnected isEqualToString:@"yes"])
        {
            fbConnected = @"disconnect";
        }
        else
        {
            fbConnected = @"connect";
            
        }
        //    NSString
        NSDictionary *params = @{@"userid":[NSNumber numberWithInt:userId],
                                 @"option":fbConnected,
                                 @"type":@"twitter",
                                 @"accesstoken":self.userManager.user.fbaccesstoken};
        [self.networkManager startGETRequestWithAPI:@"cloverconnect/" andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
            
            NSLog(@"%@",responseDictionary);
            NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
            NSString *errorStr = [responseDictionary objectForKey:@"msg"];
            [self.hud hide:YES];
            
            if ([errorCode isEqualToString:@"200"])
            {
                if ([[responseDictionary objectForKey:@"option"] isEqualToString:@"connect"])
                {
                    self.userManager.user.twconnected = @"yes";
                    [self.twitterDisconnectButton setTitle:@"DISCONNECT" forState:UIControlStateNormal];
                }
                else
                {
                    self.userManager.user.twconnected = @"no";
                    [self.twitterDisconnectButton setTitle:@"CONNECT" forState:UIControlStateNormal];
                }
            }
            else
            {
                UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [errorAlert show];
            }
            
        } andFailure:^(NSString *errorMessage) {
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [errorAlert show];
            
        }];
    }

}

- (IBAction)facebookDisconnectButtonAction:(id)sender
{
    [self.view endEditing:YES];
    self.mainSrollView.contentSize = CGSizeMake(320, self.containerView.frame.size.height);
    [self.mainSrollView scrollRectToVisible:CGRectMake(0, -20, self.mainSrollView.frame.size.width, self.mainSrollView.frame.size.height) animated:YES];
    
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.hud.dimBackground = YES;
    self.hud.mode = MBProgressHUDModeIndeterminate;
    self.hud.labelText = @"Loading...";
    [self.hud show:YES];
//http://cloverapi.jumpcatch.com/api/cloverconnect/?userid=4&option=disconnect&type=fb
    int userId = [self.userManager.user.userId intValue];
    NSString *fbConnected;
    if ([self.userManager.user.fbconnected isEqualToString:@"yes"])
    {
        fbConnected = @"disconnect";
    }
    else
    {
        fbConnected = @"connect";

    }
//    NSString
    NSDictionary *params = @{@"userid":[NSNumber numberWithInt:userId],
                             @"option":fbConnected,
                             @"type":@"fb",
                             @"accesstoken":self.userManager.user.fbaccesstoken};
    [self.networkManager startGETRequestWithAPI:@"cloverconnect/" andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
        
        NSLog(@"%@",responseDictionary);
        NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
        NSString *errorStr = [responseDictionary objectForKey:@"msg"];
        [self.hud hide:YES];
        
        if ([errorCode isEqualToString:@"200"])
        {
            if ([[responseDictionary objectForKey:@"option"] isEqualToString:@"connect"])
            {
                self.userManager.user.fbconnected = @"yes";
                [self.facebookDisconnectButton setTitle:@"DISCONNECT" forState:UIControlStateNormal];
            }
            else
            {
                self.userManager.user.fbconnected = @"no";
                [self.facebookDisconnectButton setTitle:@"CONNECT" forState:UIControlStateNormal];
            }
        }
        else
        {
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [errorAlert show];
        }

    } andFailure:^(NSString *errorMessage) {
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [errorAlert show];

    }];
}

-(void)cameraImageViewTapped
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Image" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                  @"Camera",
                                  @"Gallery",
                                  nil];
    [actionSheet showInView:self.view];
}


-(void)loadCurrentLocationOfUser
{
    if ([self isNetworkIsReachable])
    {
        CLFetchLocationManager *manager = [CLFetchLocationManager sharedManager];
        manager.delegate = self;
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(self.locationTextField.frame.origin.x+self.locationTextField.frame.size.width, self.locationTextField.frame.origin.y+5, 20, 20)];
        self.activityIndicator.color = [UIColor whiteColor];
        [self.activityIndicator startAnimating];
        [self.locationView addSubview:self.activityIndicator];
        [manager initializeLocationManager];
    }

}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIACtionSheetDelegate Marthods

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            [self loadImageFromCamera];
            break;
        case 1:
            [self loadImageFromGallery];
            break;
        default:
            break;
    }
}

-(void)loadImageFromCamera
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
    
}

-(void)loadImageFromGallery
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = info[UIImagePickerControllerOriginalImage];
        self.cameraImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.cameraImageView.image = image;
        self.isImageUploaded = YES;
    }
}

#pragma mark CLCustomNavigationViewDelegateMethods

-(void)userDetailsEditButtonTapped
{
    if (self.isEditButtonSelected == YES)
    {
        self.cameraImageView.userInteractionEnabled = YES;
        self.locationImageView.userInteractionEnabled = YES;

        [self.navigationView.navEditButton setImage:[UIImage imageNamed:@"Check"] forState:UIControlStateNormal];
        self.navigationView.navTitleTextField.enabled = YES;
        self.mobileTextField.enabled = YES;
        self.locationTextField.enabled = YES;
        self.summaryTextView.editable = YES;
        self.locationImageView.hidden = NO;
        self.mobileImageView.hidden = NO;
        self.mobileSeperatorView.hidden = NO;
        self.navigationView.navNameSeperatorView.hidden = NO;
        self.locationSeperatorView.hidden = NO;
        
        self.navigationView.navTitleTextField.text = self.userManager.user.userName;
        self.mobileTextField.text = self.userManager.user.userMobileNumber;
        
        self.summaryTextView.translatesAutoresizingMaskIntoConstraints = YES;
        self.summaryTextView.layer.borderWidth = 1.0;
        self.summaryTextView.layer.borderColor = [[UIColor colorWithRed:101.0/255.0 green:64.0/255.0 blue:81.0/255.0 alpha:1.0] CGColor];
        
        
        if ([self.locationTextField.text isEmptyString])
        {
            self.locationSeperatorView.frame = CGRectMake(self.locationTextField.frame.origin.x, 20, 37, 1);
        }
        else
        {
            self.locationSeperatorView.frame = CGRectMake(self.locationTextField.frame.origin.x, 20, self.locationTextField.frame.size.width, 1);
        }
        self.isDisardSelected = NO;
    }
    else
    {
        if (self.isDisardSelected) {
            [self configureViewWithUserDetails];
        }
        else
        {
            [self updateUserInformation];
        }
    }
    self.isEditButtonSelected =!self.isEditButtonSelected;
}

-(void)updateUserInformation
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        
        
        NSMutableDictionary *params =[NSMutableDictionary dictionaryWithDictionary:@{@"userid" : self.userManager.user.userId,
                                                                                     @"uname" : self.navigationView.navTitleTextField.text,
                                                                                     @"location" : self.locationTextField.text,
                                                                                     @"mobilenumber" : self.mobileTextField.text,
                                                                                     @"summary" : self.summaryTextView.text
                                                                                     }];
        [self.networkManager startGETRequestWithAPI:@"cloverupdate/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSLog(@"Response : %@",responseDictionary);
             NSString *errorStr = [NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             if ([errorStr isEqualToString:@"200"])
             {
                 NSDictionary *userInfo = [responseDictionary objectForKey:@"userinfo"];
                 self.userManager.user = [CLUser getUserInformationFrom:userInfo];
                 if (self.isImageUploaded == YES)
                 {
                     [self uploadProfileImageOfUser];
                 }
                 else
                 {
                     NSDictionary *userInfo = [responseDictionary objectForKey:@"userinfo"];
                     self.userManager.user = [CLUser getUserInformationFrom:userInfo];
                     [self configureViewWithUserDetails];
                     if (self.isSaveSelected)
                     {
                         [self menuButtonTapped];
                         self.isSaveSelected = NO;
                     }
                     else
                     {
                         UIAlertView *succesAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Your information uploaded successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                         [succesAlert show];
                     }
                 }
             }
             else
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
             }
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             NSLog(@"Error : %@",errorMessage);
             UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [errorAlert show];
         }];
    }

}

-(void)uploadProfileImageOfUser
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        [self.networkManager uploadImage:self.cameraImageView.image withUserID:self.userManager.user.userId WithSuccessHandler:^(NSArray *result, NSString *resultString, BOOL apiStatus) {
            NSLog(@"%@",result);
            [hud hide:YES];
            self.userManager.user.profileURL = resultString;
            [self configureViewWithUserDetails];
            [self.leftMenuView loadDetailsOfLeftMenuView];
            if (self.isSaveSelected)
            {
                [self menuButtonTapped];
                self.isSaveSelected = NO;
            }
            else
            {
                
                UIAlertView *succesAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Your information uploaded successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [succesAlert show];
            }
            
        } WithErrorHandler:^(NSError *error, NSString *errorMessage, BOOL apiStatus) {
            [hud hide:YES];
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [errorAlert show];
        }];
    }
}

#pragma mark UITextFieldDelegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.mobileTextField)
    {
        self.mainSrollView.contentSize = CGSizeMake(320, 700);
        [self.mainSrollView scrollRectToVisible:CGRectMake(0, 80, self.mainSrollView.frame.size.width, self.mainSrollView.frame.size.height) animated:YES];
        UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
        [keyboardDoneButtonView sizeToFit];
        UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStyleBordered target:self
                                                                      action:@selector(doneClicked)];
        //    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
        
        [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                          [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                          doneButton,
                                          nil]];
        keyboardDoneButtonView.tintColor = [UIColor blackColor];
        textField.inputAccessoryView = keyboardDoneButtonView;

    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    
    [textField resignFirstResponder];
    self.mainSrollView.contentSize = CGSizeMake(320, self.containerView.frame.size.height);
    [self.mainSrollView scrollRectToVisible:CGRectMake(0, -20, self.mainSrollView.frame.size.width, self.mainSrollView.frame.size.height) animated:YES];
    if (textField == self.locationTextField)
    {
        NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:15]};
        CGSize size = [textField.text sizeWithAttributes:attributes];
        self.locationSeperatorView.frame = CGRectMake(self.locationTextField.frame.origin.x, 20, size.width, 1);
    }
    
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField == self.mobileTextField)
    {
        self.mainSrollView.contentSize = CGSizeMake(320, self.containerView.frame.size.height);
        [self.mainSrollView scrollRectToVisible:CGRectMake(0, -20, self.mainSrollView.frame.size.width, self.mainSrollView.frame.size.height) animated:YES];
    }
    return YES;
}

-(BOOL) textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (textField == self.mobileTextField) {
        if (textField.text.length >= 10 && range.length == 0)
        {
            return NO;
        }
        return YES;
    }
    return YES;
}
#pragma mark UITextViewDelegate Methods

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    self.isTextViewSelected = YES;
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked)];
    //    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                      [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                      doneButton,
                                      nil]];
    keyboardDoneButtonView.tintColor = [UIColor blackColor];
    textView.inputAccessoryView = keyboardDoneButtonView;

    return YES;
}

-(void)doneClicked
{
    [self.view endEditing:YES];
}



-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    return YES;
}


#pragma mark KeyBoard hiding methods

- (void)keyboardWillShow:(NSNotification *)note
{
    if (self.isTextViewSelected == YES)
    {
        
//        NSDictionary *userInfo = note.userInfo;
//        NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
//        UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
//        
//        CGRect keyboardFrameEnd = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
//        keyboardFrameEnd = [self.view convertRect:keyboardFrameEnd fromView:nil];
//        
//        [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
//            self.view.frame = CGRectMake(0, -keyboardFrameEnd.size.height, self.view.frame.size.width, self.view.frame.size.height);
//        } completion:nil];
    }
}

- (void)keyboardWillHide:(NSNotification *)note
{
    if (self.isTextViewSelected == YES)
    {
//        NSDictionary *userInfo = note.userInfo;
//        NSTimeInterval duration = [userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];
//        UIViewAnimationCurve curve = [userInfo[UIKeyboardAnimationCurveUserInfoKey] integerValue];
//        
//        CGRect keyboardFrameEnd = [userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
//        keyboardFrameEnd = [self.view convertRect:keyboardFrameEnd fromView:nil];
//        
//        [UIView animateWithDuration:duration delay:0 options:UIViewAnimationOptionBeginFromCurrentState | curve animations:^{
//            self.view.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//        } completion:nil];
        self.isTextViewSelected = NO;
    }
}

#pragma mark black view custom methods

-(void)addBlackView
{
    if (!self.blackView)
    {
        self.blackView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
        self.blackView.backgroundColor = [UIColor colorWithRed:0 green:0 blue:0 alpha:0.5];
        [self.view addSubview:self.blackView];
    }
    else
    {
        self.blackView.hidden = NO;
    }
}

-(void)removeBlackView
{
    [self.blackView removeFromSuperview];
    self.blackView = nil;
}

#pragma mark CLChangePasswordViewDelegate Methods

-(void)changePasswordButtonTapped
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        // http://cloverapi.jumpcatch.com/api/cloverupdate/?userid=1&uname=riys&location=chennai&mobilenumber=9944919105&upwd=demouser&summary=demoss&profileimg=http://demo.com/profileimg.jpg
        
        NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:@{@"upwd" : self.changePasswordDetailsView.oldPasswordTextField.text,
                                                                                      @"npwd" : self.changePasswordDetailsView.passwordTextField.text,
                                                                                      @"userid" : self.userManager.user.userId
                                                                                      }];
        [self.networkManager startGETRequestWithAPI:@"cloverupdate/" andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
            [hud hide:YES];
            NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
            NSString *errorStr = [responseDictionary objectForKey:@"msg"];
            [hud hide:YES];
            if ([errorCode isEqualToString:@"200"])
            {
                self.changePasswordDetailsView.hidden = YES;
                [self removeBlackView];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Profile Updated" message:@"Password changed successfully" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
            else
            {
                [hud hide:YES];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [alert show];
            }
            
        } andFailure:^(NSString *errorMessage) {
            [hud hide:YES];
            NSLog(@"Error : %@",errorMessage);
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [errorAlert show];
        }]; 
    }

}

-(void)changePasswordCloseButtonTapped
{
    [self removeBlackView];
    [self ViewZoomOutEffect:self.changePasswordDetailsView];
}

#pragma mark UITap/UIButton Action

-(void)reviewViewTapped
{
    CLReviewsViewController *reviewVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLReviewsViewController"];
    reviewVC.commentArray = self.pictureCommentArray;
    [self.navigationController pushViewController:reviewVC animated:YES];
    
}

-(void)favCountViewTapped
{
    CLFavouritesViewController *favVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLFavouritesViewController"];
    favVC.isFromSettings = YES;
    [self.navigationController pushViewController:favVC animated:YES];
}

-(void)mealPlannerViewTapped
{
    CLMealPlannerViewController *mealPlannerVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLMealPlannerViewController"];
    mealPlannerVC.isFromSettings = YES;
    [self.navigationController pushViewController:mealPlannerVC animated:YES];
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark CLCustomNavigationViewDelegate methods

-(void)menuButtonTapped
{
    if (self.isEditButtonSelected==NO)
    {
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Would you like to save changes" delegate:self cancelButtonTitle:@"Discard" otherButtonTitles:@"Save", nil];
        errorAlert.tag = 100;
        [errorAlert show];
    }
    else
    {
        [self.leftMenuView loadDetailsOfLeftMenuView];
        self.leftMenuView.hidden = NO;
        self.leftMenuBlackView.hidden = NO;
        [self.view bringSubviewToFront:self.leftMenuBlackView];
        [UIView animateWithDuration:0.2 animations:^{
            [self.view bringSubviewToFront:self.leftMenuView];
            self.leftMenuView.frame = CGRectMake(0, 0, 210, self.view.frame.size.height-10);
        }];
    }
}

#pragma mark UIAlertView Delegate Methods 

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            if (alertView.tag == 100)
            {
                self.isDisardSelected = YES;
                self.isSaveSelected = NO;
                [self userDetailsEditButtonTapped];
                [self menuButtonTapped];
            }
            break;
            
        case 1:
            self.isDisardSelected = NO;
            self.isSaveSelected = YES;
            [self userDetailsEditButtonTapped];

            break;
        default:
            break;
    }
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
}

#pragma mark -
#pragma mark - NYCLocation delegate methods

- (void)didFetchUserLocationWithLocation:(CLLocation *)userLocation
{
    self.userLocation = userLocation;
    [self getLocationOfUser];
}
- (void)failedToFetchUserLocationWithErrorTitle:(NSString *)title andErrorMessage:(NSString *)message
{
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
}


-(void)getLocationOfUser
{
    NSString *latLng = [NSString stringWithFormat:@"%f,%f",self.userLocation.coordinate.latitude,self.userLocation.coordinate.longitude];
    
    NSString *esc_addr =  [latLng stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *locationStr;
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    NSMutableDictionary *data = [NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding]options:NSJSONReadingMutableContainers error:nil];
    NSMutableArray *dataArray = (NSMutableArray *)[data valueForKey:@"results" ];
    if (dataArray.count == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please Enter a valid address" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        locationStr = [[dataArray objectAtIndex:4] objectForKey:@"formatted_address"];
    }
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
    self.locationTextField.text = locationStr;
    
}

- (IBAction)changePasswordButtonAction:(id)sender
{
    [self addBlackView];
    self.changePasswordDetailsView = [[[NSBundle mainBundle] loadNibNamed:@"CLChangePasswordView" owner:self options:nil] objectAtIndex:0];
    self.changePasswordDetailsView.center = self.view.center;
    self.changePasswordDetailsView.delegate  = self;
    [self ViewZoomInEffect:self.changePasswordDetailsView];
    [self.view addSubview:self.changePasswordDetailsView];
}

- (IBAction)sendFeedbackButtonAction:(id)sender
{
    [self addBlackView];
    self.sendFeedBackView = [[[NSBundle mainBundle] loadNibNamed:@"CLSendFeedbackView" owner:self options:nil]objectAtIndex:0];
    self.sendFeedBackView.center = self.view.center;
    self.sendFeedBackView.delegate = self;
    [self ViewZoomInEffect:self.sendFeedBackView];
    [self.sendFeedBackView configureYAxisOfView];
    [self.view addSubview:self.sendFeedBackView];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark- CLSendFeedbackViewDelegate Methods

-(void)uploadUserFeedbackEmail:(NSString *)emailId andMessage:(NSString *)message
{
    [self removeBlackView];
    [self ViewZoomOutEffect:self.sendFeedBackView];
    
    if ([self isNetworkIsReachable])
    {
        self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        self.hud.dimBackground = YES;
        self.hud.mode = MBProgressHUDModeIndeterminate;
        self.hud.labelText = @"Loading...";
        [self.hud show:YES];
        
        
     //   http://cloverapi.jumpcatch.com/api/contactus/?sender=riyaz&email=riyaz@demo.com&message=asamplemessage
        
        CLUserManager *userManager = [CLUserManager sharedManager];
        
        NSDictionary *params = @{@"sender":userManager.user.userName,
                                 @"email":emailId,
                                 @"message":message};
        
        [self.networkManager startGETRequestWithAPI:@"contactus/" andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
            NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
            NSString *errorStr = [responseDictionary objectForKey:@"msg"];
            [self.hud hide:YES];
            NSLog(@"%@",responseDictionary);
            
            if ([errorCode isEqualToString:@"200"])
            {
                UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [errorAlert show];
            }
            else
            {
                UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [errorAlert show];
            }
            
        } andFailure:^(NSString *errorMessage) {
            [self.hud hide:YES];
            UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            [errorAlert show];
        }];
    }

}

-(void)feedBackCloseButtonTapped
{
    [self removeBlackView];
    [self ViewZoomOutEffect:self.sendFeedBackView];
}

@end
