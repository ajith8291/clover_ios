//
//  CLWebViewController.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 08/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLWebViewController.h"
#import "CLConstants.h"
#import "NSString+DataValidator.h"

#import <MBProgressHUD/MBProgressHUD.h>

@interface CLWebViewController ()<UIWebViewDelegate>
@property (nonatomic, strong) MBProgressHUD *hud;
@end

@implementation CLWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationView.navMenuButton.hidden = YES;
    self.navigationView.navEditButton.hidden = YES;
    self.navigationView.navLogoImageView.hidden = YES;
    self.navigationView.navSearchButton.hidden = YES;
    self.navigationView.navCartButton.hidden = YES;
    self.hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    if (IS_IOS8_OR_GREATER)
    {
        self.webView.translatesAutoresizingMaskIntoConstraints = NO;
    }
    else
    {
        self.webView.translatesAutoresizingMaskIntoConstraints = YES;
        self.webView.frame = CGRectMake(0, 50, self.view.frame.size.width, self.view.frame.size.height-50);
    }
    if ([self isNetworkIsReachable])
    {
        if (self.titleStr == nil) {
            self.navigationView.navTitleTextLabel.text = [@"Videos" capitalizedString];
            [self loadWebViewWithData];
        }
        else
        {
            self.navigationView.navTitleTextLabel.text = [self.titleStr capitalizedString];
            [self loadTermsAndConditionPage];
        }
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)loadTermsAndConditionPage
{
    self.webView.scalesPageToFit = YES;
    self.webView.delegate = self;
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.urlString]]];
}

-(void)loadWebViewWithData
{

//    NSString *urls=[self.urlString stringByAppendingString:@"?autoplay=1"];
 //    NSURL *url = [NSURL URLWithString:@"https://www.youtube.com/embed/FglqN1jd1tM?autoplay=1"];

 //   [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
    self.webView.scalesPageToFit = YES;
    self.webView.delegate = self ;
    [self.webView setAllowsInlineMediaPlayback:YES];
    self.webView.allowsInlineMediaPlayback=YES;
    self.webView.mediaPlaybackRequiresUserAction = NO;

    [self embedYouTube:self.urlString frame:self.webView.frame];
}

- (void)embedYouTube:(NSString*)urls frame:(CGRect)frame {
    
    CGFloat leftMargin = (self.view.frame.size.width - 300)/2;
    urls=[urls stringByAppendingString:@"?autoplay=1"];
    NSString *htmlString =[NSString stringWithFormat: @"<html><head> <meta name = \"viewport\" content = \"initial-scale = 1.0, user-scalable = no, width = 300\"/></head> <body style=\"margin-top:%fpx;margin-left:%fpx\"><div><object width=\"212\" height=\"212\"><param name=\"movie\"value=\"%@\">",leftMargin,leftMargin,urls];
    
    NSString *htm2=[NSString stringWithFormat:@"</param><param name=\"wmode\" value=\"transparent\"></param><embed src=\"%@\"type=\"application/x-shockwave-flash\" wmode=\"transparent\" width=\"212\" height=\"212\"></embed></object></div></body></html>",urls];
    
    NSString* html = [NSString stringWithFormat:@"%@%@",htmlString,htm2];
    float version = [[[UIDevice currentDevice] systemVersion] floatValue];
    
    
    if(version<5.0)
    {
        
        NSRange r=[urls rangeOfString:@"v="];
        NSRange ran=NSMakeRange(r.location+2, [urls length]-r.location-2);
        NSString *vid=[urls substringWithRange:ran];
        html=[NSString stringWithFormat:@"<embed id=\"yt\" src=\"http://www.youtube.com/watch?v=%@&fs=0\" type=\"application/x-shockwave-flash\" width=\"300\" height=\"300\"></embed>", vid];
    }
    [self.webView loadHTMLString:html baseURL:nil];
    
//    NSString* embedHTML = @"\
//    <html><head>\
//    <style type=\"text/css\">\
//    body {\
//        background-color: transparent;\
//    color: white;\
//    }\
//    </style>\
//    </head><body style=\"margin:0\">\
//    <embed id=\"yt\" src=\"%@\" type=\"application/x-shockwave-flash\" \
//    width=\"%0.0f\" height=\"%0.0f\"></embed>\
//    </body></html>";
//    
//    NSString* html = [NSString stringWithFormat:embedHTML, urls, frame.size.width, frame.size.height];
//    self.webView.backgroundColor = [UIColor purpleColor];
//    [self.webView loadHTMLString:html baseURL:nil];
}

#pragma mark UIWebViewDelegate Methods

-(void)webViewDidStartLoad:(UIWebView *)webView
{
    [MBProgressHUD showHUDAddedTo:self.webView animated:YES];
    [self.hud show:YES];
}

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [MBProgressHUD hideHUDForView:self.webView animated:YES];
    [self.hud hide:YES];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [MBProgressHUD hideHUDForView:self.webView animated:YES];

}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
