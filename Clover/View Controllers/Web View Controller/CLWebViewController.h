//
//  CLWebViewController.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 08/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLBaseViewController.h"

@interface CLWebViewController : CLBaseViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (nonatomic, strong) NSString *titleStr;
@property (nonatomic, strong) NSString *urlString;
@end
