//
//  CLFavouritesViewController.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 06/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLBaseViewController.h"

@interface CLFavouritesViewController : CLBaseViewController<UICollectionViewDataSource,UICollectionViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *favouriteCollectionview;
@property (weak, nonatomic) IBOutlet UILabel *noRecipeLabel;

@property (weak, nonatomic) IBOutlet UIButton *addRecipeButton;
@property (nonatomic, assign) BOOL isFromSettings;
@end
