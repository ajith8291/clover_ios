//
//  CLFavouritesViewController.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 06/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLFavouritesViewController.h"
#import "CLRecipeCollectionViewListCell.h"
#import "CLConstants.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "CLRecipeDetailsViewController.h"
#import "CLReviewsViewController.h"
#import "CLRecipeDetailsCollectionViewCell.h"
#import "CLWebViewController.h"
#import "CLRecipeViewController.h"
@interface CLFavouritesViewController ()<CLRecipeCollectionViewListCellDelegate,CLRecipeDetailsViewControllerDelegate,CLRecipeDetailsCollectionViewCellDelegate,CLRecipeViewControllerDelegate>


@property (nonatomic, strong) NSMutableArray *favouriteRecipeArray;
@property (nonatomic, assign) BOOL isrecipeRemoved;
@end

@implementation CLFavouritesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.isrecipeRemoved = NO;
    // Do any additional setup after loading the view.
    self.navigationView.navTitleTextLabel.text = [@"My Favourites" capitalizedString];
    self.navigationView.navTitleTextLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:18.0];
    self.navigationView.navLogoImageView.hidden = YES;
    self.navigationView.navSearchButton.hidden = YES;
    self.navigationView.navEditButton.hidden = YES;
    self.navigationView.navCartButton.hidden = YES;
    
    if (self.isFromSettings)
    {
        self.navigationView.navBackButton.hidden = NO;
        self.navigationView.navMenuButton.hidden= YES;
    }
    else
    {
        self.navigationView.navBackButton.hidden = YES;
        self.navigationView.navMenuButton.hidden= NO;
    }

    
    self.noRecipeLabel.numberOfLines = 2;
    self.noRecipeLabel.frame = CGRectMake(0, 0, self.view.frame.size.width-80, 100);
    self.noRecipeLabel.center = self.view.center;
    self.addRecipeButton.layer.cornerRadius = 10.0;
    self.addRecipeButton.clipsToBounds = YES;
    [self.favouriteCollectionview setShowsVerticalScrollIndicator:NO];
    [self callWebServiceToGetFovouriteRecipe];
    [self.favouriteCollectionview registerClass:[CLRecipeDetailsCollectionViewCell class] forCellWithReuseIdentifier:@"CLRecipeDetailsCollectionViewCell"];
}


-(void)callWebServiceToGetFovouriteRecipe
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        
        NSDictionary *params = @{@"user":self.userManager.user.userId};
        
        [self.networkManager startGETRequestWithAPI:@"getfavourites/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
//             NSString *errorStr = [responseDictionary objectForKey:@"msg"];
             
             if ([errorCode isEqualToString:@"200"])
             {
                 self.favouriteRecipeArray = [responseDictionary objectForKey:@"recipes"];
                 self.noRecipeLabel.hidden = YES;
                 self.favouriteCollectionview.hidden = NO;
                 [self.favouriteCollectionview reloadData];
             }
             else
             {
                 self.noRecipeLabel.hidden = NO;
                 self.favouriteCollectionview.hidden = YES;
                 [self.favouriteCollectionview reloadData];
             }
             
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             self.favouriteRecipeArray = nil;
             [self.favouriteCollectionview reloadData];
             self.favouriteCollectionview.hidden = YES;
             self.noRecipeLabel.hidden = NO;
             
         }];
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UICollectionViewDatasource and delegate methods

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    CLRecipe *recipe = [CLRecipe getInformationFromDictionary:[[self.favouriteRecipeArray objectAtIndex:indexPath.row] objectForKey:@"recipe_info"]];
    CLRecipeDetailsCollectionViewCell *cell = (CLRecipeDetailsCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CLRecipeDetailsCollectionViewCell" forIndexPath:indexPath];
    cell.removeFromListImageView.hidden = YES;

    cell.delegate = self;
    [cell configureListCellWithRecipeDetails:recipe];
    return cell;
//    CLRecipeFilterTableViewCell *cell =  [collectionView dequeueReusableCellWithIdentifier:@"CLRecipeFilterTableViewCell"];
//    cell.categoryNameLabel.text = [self.selectedRecipe.recipeIngredientsArray objectAtIndex:indexPath.row];
//    [cell configureCellWithRecipe];
//    cell.selectionStyle = UITableViewCellSelectionStyleNone;
//    return cell;

    
//    CLRecipe *recipe = [CLRecipe getInformationFromDictionary:[self.favouriteRecipeArray objectAtIndex:indexPath.row]];
//    CLRecipeCollectionViewListCell *cell = (CLRecipeCollectionViewListCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"CLRecipeCollectionViewListCell" forIndexPath:indexPath];
//    cell.frame = CGRectMake(0, cell.frame.origin.y, self.view.frame.size.width, cell.frame.size.height);
//    [cell configureListCellWithRecipeDetails:recipe WithRect:cell.frame];
//    cell.delegate = self;
//    return cell;
}

//- (CLRecipeDetailsCollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
//    CLRecipeDetailsCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CLRecipeDetailsCollectionViewCell" forIndexPath:indexPath];
//    return cell;
//}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.favouriteRecipeArray.count;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    CLRecipe *recipe = [CLRecipe getInformationFromDictionary:[[self.favouriteRecipeArray objectAtIndex:indexPath.row] objectForKey:@"recipe_info" ]];
    CLRecipeDetailsViewController *recipedetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeDetailsViewController"];
    recipedetailVC.selectedRecipe = recipe;
    recipedetailVC.isfromFavourites = YES;
    recipedetailVC.delegate = self;
    [self.navigationController pushViewController:recipedetailVC animated:YES];

}
- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    if (IS_IOS8_OR_GREATER)
    {
        return UIEdgeInsetsMake(5, 3, 5, 3);
    }
    return UIEdgeInsetsMake(5, 10, 5, 10);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return CGSizeMake(self.view.frame.size.width, 230);
}

#pragma mark CLRecipeCollectionViewListCellDelegate Methods

-(void)removeRecipeFromFavouriteList:(CLRecipe *)favRecipe
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        
        NSDictionary *params = @{@"user":self.userManager.user.userId,
                                 @"recipe":favRecipe.recipeId};
        
        [self.networkManager startGETRequestWithAPI:@"removefavourites/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             NSString *msg = [responseDictionary objectForKey:@"msg"];
             
             if ([errorCode isEqualToString:@"200"])
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:[msg capitalizedString] delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
                 [self callWebServiceToGetFovouriteRecipe];
                 
             }
             else
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
             }
             
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
        
    }
}

#pragma mark CLRecipeDetailsViewControllerDelegate Methods

-(void)loadfavouritesDetails
{
    [self callWebServiceToGetFovouriteRecipe];
}

-(void)addedRecipeReviewAndRatings
{
    [self callWebServiceToGetFovouriteRecipe];
}

#pragma mark CLRecipeDetailsCollectionViewCellDelegate methods

-(void)loadReviewViewControllerWithSelectedRecipe:(CLRecipe *)recipe
{
    CLRecipeDetailsViewController *recipedetailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeDetailsViewController"];
    recipedetailVC.selectedRecipe = recipe;
    recipedetailVC.delegate = self;
    recipedetailVC.isUserReviewSelected = YES;
    [self.navigationController pushViewController:recipedetailVC animated:YES];
}

-(void)playButtonTappedWithRecipe:(CLRecipe *)recipe
{
    CLRecipeDetailsViewController *detailsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeDetailsViewController"];
    detailsVC.selectedRecipe = recipe;
    detailsVC.isPlayButtonTapped = YES;
    [self.navigationController pushViewController:detailsVC animated:NO];
}

- (IBAction)addRecipeButtonAction:(id)sender
{
    CLRecipeViewController *recipeVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLRecipeViewController"];
    recipeVC.isFromFavourites = YES;
    recipeVC.delegate = self;
    [self.navigationController pushViewController:recipeVC animated:YES];
}


-(void)loadFavouritesWithNewDetails
{
    [self callWebServiceToGetFovouriteRecipe];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
