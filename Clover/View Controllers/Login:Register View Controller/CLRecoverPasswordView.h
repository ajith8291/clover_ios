//
//  CLRecoverPasswordView.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 04/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  CLRecoverPasswordViewDelegate<NSObject>

@optional

-(void)recoverPasswordCloseButtonTapped;
-(void)recoverPasswordResetPasswordButtonTapped;

@end

@interface CLRecoverPasswordView : UIView<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailIdTextField;
@property (weak, nonatomic) IBOutlet UIButton *resetPasswordButton;

@property (nonatomic, weak) id<CLRecoverPasswordViewDelegate>delegate;
@end
