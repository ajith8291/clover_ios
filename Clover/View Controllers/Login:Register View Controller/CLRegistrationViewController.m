//
//  CLRegistrationViewController.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLRegistrationViewController.h"
#import "CLLoginViewController.h"
#import "NSString+DataValidator.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import <STTwitter/STTwitter.h>
#import "CLUserManager.h"
#import "CLAppDelegate.h"
#import "Reachability.h"
#import "CLWebViewController.h"
#import "CLFetchLocationManager.h"
#import <SystemConfiguration/SystemConfiguration.h>
#import <CoreLocation/CoreLocation.h>

@interface CLRegistrationViewController ()<FBLoginViewDelegate,CLFetchLocationManagerDelegate>

@property (nonatomic, strong) UIScrollView *containerScrollView;
@property (nonatomic, strong) CLNetworkManager *networkManager;
@property (nonatomic, strong) NSString *twitterUserId;
@property (nonatomic, strong) NSString *twitterScreen;
@property (nonatomic, strong) CLUserManager *userManager;
@property (nonatomic, assign) BOOL isImageUploaded;
@property (nonatomic, assign) BOOL isCheckBoxSelected;
@property (nonatomic, strong) CLLocation *userLocation;

@property (nonatomic, strong) UIActivityIndicatorView *activityIndicator;


@end


@implementation CLRegistrationViewController

-(void)viewDidLoad
{
    [super viewDidLoad];

    [self addTapGesturesToView];
    [self addContainerScrollView];
    [self setTextFieldAttributes];
    
    self.cameraImageView.layer.cornerRadius = self.cameraImageView.frame.size.height/2.0;
    self.cameraImageView.layer.borderWidth = 1.5;
    self.cameraImageView.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.cameraImageView.layer.masksToBounds = YES;
    self.networkManager = [CLNetworkManager sharedManager];
    [self facebookLoginFlow];
    self.userManager = [CLUserManager sharedManager];
    self.containerView.center = self.view.center;
    self.termsAndConditionTextField.enabled = NO;
    self.isCheckBoxSelected = YES;
    
    UITapGestureRecognizer *termsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(termsAndConditionViewTapped)];
    [self.termsAndConditionView addGestureRecognizer:termsTap];
    
    self.locationIconImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *locationTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loactionIconViewtapped)];
    [self.locationIconImageView addGestureRecognizer:locationTap];
    
    [self.view bringSubviewToFront:self.backButton];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if ([self isNetworkIsReachable])
    {
        CLFetchLocationManager *manager = [CLFetchLocationManager sharedManager];
        manager.delegate = self;
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(self.locationTextField.frame.origin.x+self.locationTextField.frame.size.width, self.locationTextField.frame.origin.y+8, 20, 20)];
        self.activityIndicator.color = [UIColor whiteColor];
        [self.activityIndicator startAnimating];
        [self.locationView addSubview:self.activityIndicator];
        
        [manager initializeLocationManager];
    }

}

#pragma mark Custom methods

-(void)addTapGesturesToView
{
    UITapGestureRecognizer *createAccountViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(createAccountViewTapped)];
    [self.createAccountView addGestureRecognizer:createAccountViewTap];
    
    
//    UITapGestureRecognizer *facebookLoginTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(facebookLoginViewTapped)];
//    [self.facebookLoginView addGestureRecognizer:facebookLoginTap];
    
    UITapGestureRecognizer *twitterLoginViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(twitterLoginViewTappped)];
    [self.twitterLoginView addGestureRecognizer:twitterLoginViewTap];
    
    UITapGestureRecognizer *createNewViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(loginViewTapped)];
    [self.loginView addGestureRecognizer:createNewViewTap];
    
    UITapGestureRecognizer *passwordImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(passwordImageViewTapped)];
    self.passwordImageView.userInteractionEnabled = YES;
    [self.passwordImageView addGestureRecognizer:passwordImageViewTap];
    
    UITapGestureRecognizer *cameraImageViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cameraImageViewTapped)];
    self.cameraImageView.userInteractionEnabled = YES;
    [self.cameraImageView addGestureRecognizer:cameraImageViewTap];
}

-(void)addContainerScrollView
{
    self.containerScrollView = [[UIScrollView alloc] initWithFrame:self.view.frame];
    self.containerScrollView.contentSize = CGSizeMake(320, 568);
    self.containerView.translatesAutoresizingMaskIntoConstraints = YES;
    
    [self.containerScrollView addSubview:self.containerView];
    [self.view addSubview:self.containerScrollView];
    
}

-(void)setTextFieldAttributes
{
    self.nameTextField.delegate = self;
    self.emailTextField.delegate = self;
    self.locationTextField.delegate = self;
    self.mobileTextField.delegate = self;
    self.passwordtextField.delegate = self;
    
    self.nameTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.locationTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.mobileTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.passwordtextField.autocorrectionType = UITextAutocorrectionTypeNo;
    
    self.nameTextField.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:@"Name" attributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.emailTextField.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.locationTextField.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:@"Location" attributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.mobileTextField.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:@"Mobile Number" attributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    self.passwordtextField.attributedPlaceholder = [[NSMutableAttributedString alloc] initWithString:@"Password" attributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    
    self.nameTextField.alpha = 0.8;
    self.emailTextField.alpha = 0.8;
    self.locationTextField.alpha = 0.8;
    self.mobileTextField.alpha = 0.8;
    self.passwordtextField.alpha = 0.8;
}

#pragma mark Tap/Button Action Methods

-(void)createAccountViewTapped
{
    [self.view endEditing:YES];
    self.containerScrollView.contentSize = CGSizeMake(320, 568);
    [self.containerScrollView scrollRectToVisible:CGRectMake(0, 0, self.containerScrollView.frame.size.width, self.containerScrollView.frame.size.height) animated:YES];

    if ([self.nameTextField.text isEmptyString]||[self.emailTextField.text isEmptyString]||[self.locationTextField.text isEmptyString]||[self.mobileTextField.text isEmptyString]||[self.passwordtextField.text isEmptyString])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"All fields are Mandatory" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if (![self.emailTextField.text validateEmail])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Invalid Email Id" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if (![self.mobileTextField.text validatePhoneNumber])
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Invalid Mobile number" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else if (self.isCheckBoxSelected==NO)
    {
        UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Accept Terms and Conditions" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        NSMutableDictionary *params =[NSMutableDictionary dictionaryWithDictionary:@{@"uname" : self.nameTextField.text,
                                 @"uemail" : self.emailTextField.text,
                                 @"upwd" : self.passwordtextField.text,
                                 @"location" : self.locationTextField.text,
                                 @"mobilenumber" : self.mobileTextField.text,
                                 @"profileimg":@"",
                                 @"devicetoken":[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"],
                                 @"devicetype":@"ios"
                                 }];
        [self postUserDetailsToServer:params];
        
    }
    
}

- (IBAction)checkButtonAction:(id)sender
{
    if (self.isCheckBoxSelected)
    {
        [self.checkButton setImage:[UIImage imageNamed:@"UncheckNew"] forState:UIControlStateNormal];
    }
    else
    {
        [self.checkButton setImage:[UIImage imageNamed:@"CheckNew"] forState:UIControlStateNormal];
    }
    self.isCheckBoxSelected = !self.isCheckBoxSelected;
} 

-(void)loactionIconViewtapped
{
    if ([self isNetworkIsReachable])
    {
        CLFetchLocationManager *manager = [CLFetchLocationManager sharedManager];
        manager.delegate = self;
        self.activityIndicator = [[UIActivityIndicatorView alloc] initWithFrame:CGRectMake(self.locationTextField.frame.origin.x+self.locationTextField.frame.size.width, self.locationTextField.frame.origin.y+8, 20, 20)];
        self.activityIndicator.color = [UIColor whiteColor];
        [self.activityIndicator startAnimating];
        [self.locationView addSubview:self.activityIndicator];

        [manager initializeLocationManager];
    }
}

- (IBAction)backButtonAction:(id)sender
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark Facebook Login methods

-(void)facebookLoginFlow
{
    if ([self isNetworkIsReachable]) {
        FBLoginView *loginView = [[FBLoginView alloc] initWithReadPermissions: @[@"public_profile", @"email",@"user_location",@"user_hometown"]];//@"user_friends",@"user_birthday", @"user_location"
        loginView.delegate = self;
        loginView.frame = self.facebookLoginView.frame;
        
        loginView.frame = CGRectMake(0, 455, 159, 50);
        
        
        for (id obj in loginView.subviews)
        {
            if ([obj isKindOfClass:[UIButton class]])
            {
                UIButton *loginButton =  (UIButton *)obj;
                {
                    //                loginButton.layer.cornerRadius = 3.0;
                    //                loginButton.layer.borderColor = [UIColor colorWithRed:194.0/255.0 green:194.0/255.0 blue:194.0/255.0 alpha:1.0].CGColor;
                    //                loginButton.layer.borderWidth = 1.3;
                    //                loginButton.layer.masksToBounds=YES;
                    
                    loginButton.contentMode=UIViewContentModeCenter;
                    [loginButton setImage:[UIImage imageNamed:@"facebook-btn"] forState:UIControlStateNormal];
                    [loginButton setImage:[UIImage imageNamed:@"facebook-btn"] forState:UIControlStateHighlighted];
                    
                }
                [loginButton setBackgroundImage:nil forState:UIControlStateHighlighted];
                [loginButton setBackgroundImage:nil forState:UIControlStateNormal];
            }
            if ([obj isKindOfClass:[UILabel class]])
            {
                UILabel * loginLabel =  obj;
                loginLabel.frame = CGRectMake(0, 0, 0, 0);
            }
        }
        
        loginView.backgroundColor=[UIColor clearColor];
        [self.containerView addSubview:loginView];
    }
}

-(void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        NSString *userImageURL = [NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large", [user objectID]];
        NSString *fbAccessToken = [[[FBSession activeSession] accessTokenData] accessToken];
        
        NSString *emailId = [user objectForKey:@"email"];
        NSString *userName = [user objectForKey:@"name"];
        //    NSString *userLocation = [[user objectForKey:@"location"] objectForKey:@"name"];
        
        //                             @"fbaccesstoken":fbAccessToken,
        //                             @"location":userLocation,
        
        NSDictionary *params = @{@"uemail":emailId,
                                 @"uname":userName,
                                 @"profileimg": userImageURL,
                                 @"fbaccesstoken":fbAccessToken,
                                 @"social":@"true",
                                 @"fb":@"yes",
                                 @"devicetoken":[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"],
                                 @"devicetype":@"ios"
                                 };
        
        [self.networkManager startGETRequestWithAPI:@"cloversignup/" andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
            NSLog(@"Response : %@",responseDictionary);
            [hud hide:YES];
            NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
            NSString *errorStr = [responseDictionary objectForKey:@"msg"];
            
            if ([errorCode isEqualToString:@"200"])
            {
                NSDictionary *userInfo = [responseDictionary objectForKey:@"userinfo"];
                self.userManager.user = [CLUser getUserInformationFrom:userInfo];
                CLAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                [appDelegate loadHomeViewController];
            }
            else
            {
                UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [errorAlert show];
            }
            
        } andFailure:^(NSString *errorMessage) {
            [hud hide:YES];
            //        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:@"Error!!" message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
            //        [errorAlert show];
            NSLog(@"Error : %@",errorMessage);
        }];
    }
}


-(void)facebookLoginViewTapped
{
    NSLog(@"Facebook Login View Tapped");
}

-(void)twitterLoginViewTappped
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        
        ACAccountStore *accountStore = [[ACAccountStore alloc] init];
        
        // Create an account type that ensures Twitter accounts are retrieved.
        ACAccountType *accountType = [accountStore accountTypeWithAccountTypeIdentifier:ACAccountTypeIdentifierTwitter];
        
        // Request access from the user to use their Twitter accounts.
        [accountStore requestAccessToAccountsWithType:accountType options:nil completion:^(BOOL granted, NSError *error) {
            if(granted) {
                // Get the list of Twitter accounts.
                NSArray *accountsArray = [accountStore accountsWithAccountType:accountType];
                // For the sake of brevity, we'll assume there is only one Twitter account present.
                // You would ideally ask the user which account they want to tweet from, if there is more than one Twitter account present.
                if ([accountsArray count] > 0) {
                    [hud hide:YES];
                    // Grab the initial Twitter account to tweet from.
                    ACAccount *twitterAccount = [accountsArray objectAtIndex:0];
                    NSLog(@"Account details %@",twitterAccount);
                    self.twitterUserId = [[twitterAccount valueForKey:@"properties"] valueForKey:@"user_id"];
                    self.twitterScreen = [twitterAccount valueForKey:@"username"];
                    NSLog(@"user id %@",self.twitterUserId);
                    [self getProfileDetailsFromTwitter];
                    
                }
                else
                {
                    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"There are no Twitter accounts configured. You can add or create a twitter account in Settings." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [alertView show];
                    [hud hide:YES];
                }
            }
            [hud hide:YES];
        }];
    }
}
- (void) getProfileDetailsFromTwitter
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading...";
    [hud show:YES];
    
    STTwitterAPI *twitter = [STTwitterAPI twitterAPIAppOnlyWithConsumerKey:@"r2SP7Za01Z7Kr5QMwerBSnkQe" consumerSecret:@"NBd80I5Y3mA3lC1A6njfqevUMSAQniwdRwUKa0zaKmoJzPexUG"]; //twitterAPIApplicationOnlyWithConsumerKey:@"" consumerSecret:@""];
    __block NSString *token;
    [twitter verifyCredentialsWithSuccessBlock:^(NSString *bearerToken) {
        NSLog(@"%@",bearerToken);
        token = bearerToken;
        [hud hide:YES];
        [twitter getUsersShowForUserID:self.twitterUserId orScreenName:nil includeEntities:nil successBlock:^(NSDictionary *user) {
            NSString *profileImageURLString = [user valueForKey:@"profile_image_url"];
            NSString *location = [user valueForKey:@"location"];
            NSString *name = [user valueForKey:@"name"];
            NSString *email = [user valueForKey:@"screen_name"];
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            hud.dimBackground = YES;
            hud.mode = MBProgressHUDModeIndeterminate;
            hud.labelText = @"Loading...";
            [hud show:YES];
            
            
            NSDictionary *params = @{@"email":email,
                                     @"uname":name,
                                     @"location":location,
                                     @"profileimg":profileImageURLString,
                                     @"social":@"true",
                                     @"twitter":@"yes",
                                     @"devicetoken":[[NSUserDefaults standardUserDefaults] objectForKey:@"deviceToken"],
                                     @"devicetype":@"ios",
                                     @"twaccesstoken":token
                                     };
            
            
            [self.networkManager startGETRequestWithAPI:@"cloversignin/" andParameters:params withSuccess:^(NSDictionary *responseDictionary) {
                NSLog(@"Response : %@",responseDictionary);
                [hud hide:YES];
                NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
                NSString *errorStr = [responseDictionary objectForKey:@"msg"];
                
                if ([errorCode isEqualToString:@"200"])
                {
                    NSDictionary *userInfo = [responseDictionary objectForKey:@"userinfo"];
                    self.userManager.user = [CLUser getUserInformationFrom:userInfo];
                    CLAppDelegate *appDelegate = [[UIApplication sharedApplication] delegate];
                    [appDelegate loadHomeViewController];
                }
                else
                {
                    UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                    [errorAlert show];
                }
                
            } andFailure:^(NSString *errorMessage) {
                [hud hide:YES];
                UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                [errorAlert show];
                NSLog(@"Error : %@",errorMessage);
            }];
            
            
        } errorBlock:^(NSError *error) {
            NSLog(@"Error : %@", error.localizedDescription);
            //
            [hud hide:YES];
        }];
        
    } errorBlock:^(NSError *error) {
        NSLog(@"Error: %@", error.localizedDescription);
        //
        [hud hide:YES];
    }];
}


-(void)loginViewTapped
{
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)passwordImageViewTapped
{
    if (![self.passwordtextField.text isEmptyString])
    {
        static BOOL isSelected = YES;
        if (isSelected == YES)
        {
            self.passwordtextField.secureTextEntry = NO;
            self.passwordImageView.alpha = 1.0;
        }
        else
        {
            self.passwordtextField.secureTextEntry = YES;
            self.passwordImageView.alpha = 0.5;
        }
        isSelected = !isSelected;
    }
}

-(void)cameraImageViewTapped
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Image" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                  @"Camera",
                                  @"Gallery",
                                  nil];
    [actionSheet showInView:self.view];
}

-(void)doneClicked
{
    [self.view endEditing:YES];
    self.containerScrollView.contentSize = CGSizeMake(320, 568);
    [self.containerScrollView scrollRectToVisible:CGRectMake(0, 0, self.containerScrollView.frame.size.width, self.containerScrollView.frame.size.height) animated:YES];
}

-(void)termsAndConditionViewTapped
{
    CLWebViewController *webVc = [self.storyboard instantiateViewControllerWithIdentifier:@"CLWebViewController"];
    webVc.urlString = @"http://cloverapi.jumpcatch.com/terms/?mode=mobile";
    webVc.titleStr = [@"terms & condition" capitalizedString];
    [self.navigationController pushViewController:webVc animated:YES];
    
}

#pragma mark UITextFieldDelegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.mobileTextField)
    {
        UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
        [keyboardDoneButtonView sizeToFit];
        UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                       style:UIBarButtonItemStyleBordered target:self
                                                                      action:@selector(doneClicked)];
        //    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
        
        [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                          [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                          doneButton,
                                          nil]];
        keyboardDoneButtonView.tintColor = [UIColor blackColor];
        textField.inputAccessoryView = keyboardDoneButtonView;
    }
    if (textField == self.mobileTextField || textField == self.passwordtextField)
    {
        self.containerScrollView.contentSize = CGSizeMake(320, 700);
        [self.containerScrollView scrollRectToVisible:CGRectMake(0, 80, self.containerScrollView.frame.size.width, self.containerScrollView.frame.size.height) animated:YES];
    }
    
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];

    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField == self.mobileTextField || textField == self.passwordtextField)
    {
        self.containerScrollView.contentSize = CGSizeMake(320, 568);
        [self.containerScrollView scrollRectToVisible:CGRectMake(0, 0, self.containerScrollView.frame.size.width, self.containerScrollView.frame.size.height) animated:YES];
    }
    return YES;
}

#pragma mark UIACtionSheetDelegate Marthods

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            [self loadImageFromCamera];
            break;
        case 1:
            [self loadImageFromGallery];
            break;
        default:
            break;
    }
}

-(void)loadImageFromCamera
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
            imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
            imagePicker.allowsEditing = NO;
            [self presentViewController:imagePicker animated:YES completion:nil];
        }
        
}

-(void)loadImageFromGallery
{
    UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeSavedPhotosAlbum]) {
        imagePicker.sourceType =  UIImagePickerControllerSourceTypeSavedPhotosAlbum;
        imagePicker.mediaTypes = @[(NSString *) kUTTypeImage];
        imagePicker.allowsEditing = NO;
        [self presentViewController:imagePicker animated:YES completion:nil];
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    [self dismissViewControllerAnimated:YES completion:nil];
    if ([mediaType isEqualToString:(NSString *)kUTTypeImage]) {
        UIImage *image = info[UIImagePickerControllerOriginalImage];
        self.cameraImageView.contentMode = UIViewContentModeScaleAspectFill;
        self.cameraImageView.image = image;
        self.isImageUploaded = YES;
    }
}

#pragma mark Cell Web Service

-(void)postUserDetailsToServer: (NSMutableDictionary *)params
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        [self.networkManager startGETRequestWithAPI:@"cloversignup/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSLog(@"Response : %@",responseDictionary);
             NSString *errorCode =[NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"errorcode"]];
             NSString *errorStr = [responseDictionary objectForKey:@"msg"];
             
             if ([errorCode isEqualToString:@"200"])
             {
                 NSDictionary *userInfo = [responseDictionary objectForKey:@"userinfo"];
                 self.userManager.user = [CLUser getUserInformationFrom:userInfo];
                 if (self.isImageUploaded == YES)
                 {
                     [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstTimeRegistration"];
                     [self uploadProfileImageOfUser];

                 }
                 else
                 {
                     [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"isFirstTimeRegistration"];
                     CLAppDelegate *appdelegate = [[UIApplication sharedApplication] delegate];
                     [appdelegate loadHomeViewController];
                 }
             }
             else
             {
                 UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorStr delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [errorAlert show];
             }
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             NSLog(@"Error : %@",errorMessage);
             UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [errorAlert show];
         }];
    }

}


-(void)uploadProfileImageOfUser
{
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.dimBackground = YES;
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.labelText = @"Loading...";
    [hud show:YES];
    
    [self.networkManager uploadImage:self.cameraImageView.image withUserID:self.userManager.user.userId WithSuccessHandler:^(NSArray *result, NSString *resultString, BOOL apiStatus) {
        NSLog(@"%@",result);
        [hud hide:YES];
        self.userManager.user.profileURL = resultString;
        CLAppDelegate *appdelegate = [[UIApplication sharedApplication] delegate];
        [appdelegate loadHomeViewController];
    } WithErrorHandler:^(NSError *error, NSString *errorMessage, BOOL apiStatus) {
        [hud hide:YES];
        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [errorAlert show];
    }];
}

-(BOOL)isNetworkIsReachable
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus astatus = [reachability currentReachabilityStatus];
    BOOL status;
    if(astatus == NotReachable)
    {
        //No internet
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Please check your internet connection and try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        status =  NO;
        
    }
    else if (astatus == ReachableViaWiFi)
    {
        //WiFi
        status =  YES;
    }
    else if (astatus == ReachableViaWWAN)
    {
        status =  YES;
    }else
        status =  NO;
    return status;
}

#pragma mark -
#pragma mark - NYCLocation delegate methods

- (void)didFetchUserLocationWithLocation:(CLLocation *)userLocation
{
    self.userLocation = userLocation;
    [self getLocationOfUser];
}
- (void)failedToFetchUserLocationWithErrorTitle:(NSString *)title andErrorMessage:(NSString *)message
{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];
}


-(void)getLocationOfUser
{
    NSString *latLng = [NSString stringWithFormat:@"%f,%f",self.userLocation.coordinate.latitude,self.userLocation.coordinate.longitude];
    
    NSString *esc_addr =  [latLng stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSString *locationStr;
    NSString *req = [NSString stringWithFormat:@"http://maps.google.com/maps/api/geocode/json?sensor=false&address=%@", esc_addr];
    NSString *result = [NSString stringWithContentsOfURL:[NSURL URLWithString:req] encoding:NSUTF8StringEncoding error:NULL];
    NSMutableDictionary *data = [NSJSONSerialization JSONObjectWithData:[result dataUsingEncoding:NSUTF8StringEncoding]options:NSJSONReadingMutableContainers error:nil];
    NSMutableArray *dataArray = (NSMutableArray *)[data valueForKey:@"results" ];
    if (dataArray.count == 0) {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:@"Please Enter a valid address" delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        locationStr = [[dataArray objectAtIndex:4] objectForKey:@"formatted_address"];
    }
    self.locationTextField.text = locationStr;
    [self.activityIndicator stopAnimating];
    [self.activityIndicator removeFromSuperview];

}

@end
