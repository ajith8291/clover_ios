//
//  CLRecoverPasswordView.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 04/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLRecoverPasswordView.h"
#import "CLConstants.h"
#import "NSString+DataValidator.h"


@implementation CLRecoverPasswordView


-(void)awakeFromNib
{
    self.emailIdTextField.layer.cornerRadius = 3.0;
    self.emailIdTextField.leftViewMode = UITextFieldViewModeAlways;
    self.emailIdTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];
    self.emailIdTextField.layer.borderWidth = 1.0;
    self.emailIdTextField.layer.borderColor = [CL_BLUE_COLOR CGColor];
    self.emailIdTextField.delegate = self;
    self.layer.cornerRadius = 2.0;
    self.layer.borderWidth = 1.0;
    self.layer.borderColor = [[UIColor grayColor] CGColor];
}

#pragma mark UIButton Action Methods

- (IBAction)closeButtonAction:(id)sender
{
    [self endEditing:YES];
    [self.delegate recoverPasswordCloseButtonTapped];
}

- (IBAction)resetPasswordButtonAction:(id)sender
{
    if ([self.emailIdTextField.text isEmptyString]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Enter Your email id" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (![self.emailIdTextField.text validateEmail])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Invalid Email Id" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [self.delegate recoverPasswordResetPasswordButtonTapped];
    }
}

#pragma mark UITextFieldDelegate Methods

-(BOOL) textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [self resetPasswordButtonAction:self];
    return YES;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
