//
//  CLHighlightTableViewCell.m
//  Clover
//
//  Created by Ajith Kumar on 23/02/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLHighlightTableViewCell.h"
#import "CLHightlightView.h"

@implementation CLHighlightTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureCellWith:(CLHighlights *)highlight
{
    CLHightlightView *highlightView = [[[NSBundle mainBundle] loadNibNamed:@"CLHightlightView" owner:self options:nil] objectAtIndex:0];
    highlightView.frame = CGRectMake(0, 0, self.frame.size.width, 170);
    [highlightView configureHightlightViewWith:highlight];
    highlightView.delegate = self;
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 5, self.frame.size.width, 170)];
    [view addSubview:highlightView];
    [self addSubview:view];
}


#pragma mark - CLHightlightViewDelegate Methods

-(void)playButtonTappedWith:(CLHighlights *)highlightObj
{
    [self.delegate playButtonTappedWith:highlightObj];
}

-(void)shareButtonTappedWithVideoUrl:(NSString *)videoUrl
{
    [self.delegate shareButtonTappedWithVideoUrl:videoUrl];
}
@end
