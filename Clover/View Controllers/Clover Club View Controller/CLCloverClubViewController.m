//
//  CLCloverClubViewController.m
//  Clover
//
//  Created by Ajith Kumar on 28/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLCloverClubViewController.h"
#import "CLConstants.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "CLHighlights.h"
#import "CLHightlightView.h"
#import "CLWebViewController.h"
#import "CLGalleryViewController.h"
#import "CLNewsViewController.h"
#import "CLCompetitionViewController.h"
#import "CLHomeNewsAndEventsTableViewCell.h"
#import "CLMealPlanerGridView.h"
#import "CLHighlightTableViewCell.h"
#import "CLRecipeDetailsUserPhotoTableViewCell.h"

@interface CLCloverClubViewController ()<UIScrollViewDelegate,CLHightlightViewDelegate,CLHighlightTableViewCellDelegate,CLRecipeDetailsUserPhotoTableViewCellDelegate>



@property (nonatomic, strong) NSArray *highlightArray;
@property (nonatomic, strong) NSArray *galleryArray;
@property (nonatomic, strong) NSArray *newsArray;
@property (nonatomic, assign) BOOL isLoaded;
@property (nonatomic, assign) BOOL isHighlightsSelected;
@property (nonatomic, assign) BOOL isGallerySelected;

@end

@implementation CLCloverClubViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationView.navTitleTextLabel.text = [@"Clover Club" capitalizedString];
    self.navigationView.navTitleTextLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:18.0];
    self.navigationView.navLogoImageView.hidden = YES;
    self.navigationView.navSearchButton.hidden = YES;
    self.navigationView.navBackButton.hidden = YES;
    self.navigationView.navEditButton.hidden = YES;
    self.navigationView.navCartButton.hidden = YES;
    self.highlightArray = [NSArray new];
    self.galleryArray = [NSArray new];
    self.newsArray = [NSArray new];
    
    self.highlightLabel.hidden = YES;
    self.galleryLabel.hidden = YES;
    self.pageControl.hidden = YES;
    self.highlightScrollView.pagingEnabled = YES;
    self.highlightScrollView.showsHorizontalScrollIndicator = NO;
    self.newsTableView.hidden = YES;
    [self callWebServiceToGetDetails];
    self.isHighlightsSelected = YES;
    [self configureSegmentView];
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 7){
        self.newsTableView.contentInset = UIEdgeInsetsMake(5, 0,5, 0);
    }
    
    UINib *nib = [UINib nibWithNibName:@"CLRecipeDetailsUserPhotoTableViewCell" bundle:nil];
    [self.newsTableView registerNib:nib forCellReuseIdentifier:@"CLRecipeDetailsUserPhotoTableViewCell"];
    self.view.backgroundColor = [UIColor whiteColor];
    self.segmentView.hidden = YES;
}

-(void)callWebServiceToGetDetails
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        
        
        [self.networkManager startGETRequestWithAPI:@"cloverclub/" andParameters:nil withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             self.highlightArray = [responseDictionary objectForKey:@"highlights"];
             self.galleryArray = [responseDictionary objectForKey:@"album"];
             self.newsArray = [responseDictionary objectForKey:@"news"];
             self.isLoaded = YES;
             [self.newsTableView reloadData];
             [self configureHighLightView];
             [self configureGalleryView];
             self.view.backgroundColor = CL_BLUE_COLOR;
             self.segmentView.hidden = NO;

             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }
}

-(void)configureSegmentView
{
    self.segmentView.layer.cornerRadius = 5.0;
    self.segmentView.clipsToBounds = YES;
    self.segmentView.layer.borderWidth=1.0;
    self.segmentView.layer.borderColor = [[UIColor whiteColor] CGColor];
    self.highlightView.backgroundColor = [UIColor whiteColor];
    self.highlightsTextLabel.textColor = CL_BLUE_COLOR;
    self.view.backgroundColor = CL_BLUE_COLOR;
    
    CGFloat width = (self.view.frame.size.width-20)/3;
    
    self.highlightView.translatesAutoresizingMaskIntoConstraints = YES;
    self.galleryView.translatesAutoresizingMaskIntoConstraints = YES;
    self.newsView.translatesAutoresizingMaskIntoConstraints = YES;
    self.highlightSeperator.translatesAutoresizingMaskIntoConstraints = YES;
    self.gallerySeperator.translatesAutoresizingMaskIntoConstraints = YES;
    self.highlightsTextLabel.translatesAutoresizingMaskIntoConstraints = YES;
    self.newsTextLabel.translatesAutoresizingMaskIntoConstraints = YES;
    self.galleryTextLabel.translatesAutoresizingMaskIntoConstraints = YES;
    
    self.highlightView.frame = CGRectMake(0, 0, width, self.highlightView.frame.size.height);
    self.galleryView.frame = CGRectMake(width, 0, width, self.highlightView.frame.size.height);
    self.newsView.frame = CGRectMake(width*2, 0, width, self.highlightView.frame.size.height);
    
    self.highlightsTextLabel.frame = CGRectMake(0, 0, width, self.highlightView.frame.size.height);
    self.newsTextLabel.frame = CGRectMake(0, 0, width, self.highlightView.frame.size.height);
    self.galleryTextLabel.frame = CGRectMake(0, 0, width, self.highlightView.frame.size.height);

    
    self.highlightSeperator.frame = CGRectMake(width-1, 0, 1, 30);
    self.gallerySeperator.frame = CGRectMake(width-1, 0, 1, 30);
    
    UITapGestureRecognizer *highlightTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightViewSelected)];
    [self.highlightView addGestureRecognizer:highlightTap];
    
    UITapGestureRecognizer *galleryTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(galleryViewSelected)];
    [self.galleryView addGestureRecognizer:galleryTap];
    
    UITapGestureRecognizer *newsTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(newsViewSelected)];
    [self.newsView addGestureRecognizer:newsTap];
}

-(void)configureHighLightView
{
    self.highlightLabel.hidden = NO;
    self.galleryLabel.hidden = NO;
    self.pageControl.hidden = NO;
    self.newsTableView.hidden = NO;
    self.pageControl.userInteractionEnabled = NO;
    self.highlightScrollView.delegate = self;
    CGFloat xAxis = 0;
    int count;
    if (self.highlightArray.count>7)
    {
        count = 7;
    }
    else
    {
        count = self.highlightArray.count;
    }
    self.pageControl.numberOfPages = count;

    for (int i = 0; i<count; i++)
    {
        CLHighlights *highlightObj = [CLHighlights getInformationFrom:[self.highlightArray objectAtIndex:i]];
        CLHightlightView *highlightView = [[[NSBundle mainBundle] loadNibNamed:@"CLHightlightView" owner:self options:nil] objectAtIndex:0];
        highlightView.frame = CGRectMake(xAxis, 0, self.view.frame.size.width, 170);
        highlightView.delegate = self;
        [highlightView configureHightlightViewWith:highlightObj];
        [self.highlightScrollView addSubview:highlightView];
        xAxis=xAxis+self.view.frame.size.width;
        highlightView.tag = i;
        
        UITapGestureRecognizer *highlightTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(highlightViewTapped:)];
        [highlightView addGestureRecognizer:highlightTap];
    }
    self.highlightScrollView.contentSize = CGSizeMake(xAxis, self.highlightScrollView.frame.size.height);
}

-(void)configureGalleryView
{
    CGFloat xAxis = 10;
    for (int i = 0; i<self.galleryArray.count; i++)
    {
        CLHighlights *highObj = [CLHighlights getInformationFrom:[self.galleryArray objectAtIndex:i]];
        CLMealPlanerGridView *gridView = [[[NSBundle mainBundle] loadNibNamed:@"CLMealPlanerGridView" owner:self options:nil] objectAtIndex:0];
        gridView.frame = CGRectMake(xAxis, 0, 70, 70);
        gridView.mealPlanDeleteButton.hidden = YES;
        [gridView configureView:highObj.highlightPostTitle andImageUrl:[highObj.highlightFeaturedImage objectForKey:@"image"]];
        gridView.recipeNameLabel.hidden = YES;
        xAxis = xAxis +70;
        gridView.tag = i;
        UITapGestureRecognizer *gridViewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(gallaryViewTapped:)];
        [gridView addGestureRecognizer:gridViewTap];
        [self.galleryScrollView addSubview:gridView];
    }
    self.galleryScrollView.contentSize = CGSizeMake(xAxis, self.galleryScrollView.frame.size.height);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView == self.highlightScrollView )
    {
        int page = scrollView.contentOffset.x/self.view.frame.size.width;
        self.pageControl.currentPage = page;
    }
}

#pragma mark UIButton/UItap actions

-(void)highlightViewTapped :(UITapGestureRecognizer *)tapView
{
    CLHighlights *highlightObj = [CLHighlights getInformationFrom:[self.highlightArray objectAtIndex:tapView.view.tag]];
    if ([highlightObj.highlightContenttype isEqualToString:@"gallery"])
    {
        CLGalleryViewController *gallaryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLGalleryViewController"];
        gallaryVC.highLightObj = highlightObj;
        [self.navigationController pushViewController:gallaryVC animated:YES];
    }
    else if ([highlightObj.highlightContenttype isEqualToString:@"news"])
    {
        CLNewsViewController *newsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLNewsViewController"];
        newsVC.newsId = highlightObj.highlightId;
        [self.navigationController pushViewController:newsVC animated:YES];
    }
    else if ([highlightObj.highlightContenttype isEqualToString:@"competitions"])
    {
        CLCompetitionViewController *competitionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLCompetitionViewController"];
        competitionVC.highlightId = highlightObj.highlightId;
        [self.navigationController pushViewController:competitionVC animated:YES];
    }
    
}

-(void)gallaryViewTapped:(UITapGestureRecognizer *)tapView
{
    CLHighlights *highlightObj = [CLHighlights getInformationFrom:[self.galleryArray objectAtIndex:tapView.view.tag]];
    CLGalleryViewController *gallaryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLGalleryViewController"];
    gallaryVC.highLightObj = highlightObj;
    [self.navigationController pushViewController:gallaryVC animated:YES];
}


-(void)playButtonTappedWith:(CLHighlights *)highlightObj
{
    CLWebViewController *webVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLWebViewController"];
    webVC.urlString = highlightObj.highlightVideoUrl;
    [self.navigationController pushViewController:webVC animated:YES];
}


-(void)highlightViewSelected
{
    self.isHighlightsSelected = YES;
    self.isGallerySelected = NO;
    [self changeBackgroundColorOfView:self.highlightView andTextProperty:self.highlightsTextLabel];
    [self.newsTableView reloadData];
    [UIView transitionWithView:self.newsTableView duration:0.8 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];

}

-(void)galleryViewSelected
{
    self.isHighlightsSelected = NO;
    self.isGallerySelected = YES;
    [self changeBackgroundColorOfView:self.galleryView andTextProperty:self.galleryTextLabel];
    [self.newsTableView reloadData];
    [UIView transitionWithView:self.newsTableView duration:0.8 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];

}

-(void)newsViewSelected
{
    self.isHighlightsSelected = NO;
    self.isGallerySelected = NO;
    [self changeBackgroundColorOfView:self.newsView andTextProperty:self.newsTextLabel];
    [self.newsTableView reloadData];
    [UIView transitionWithView:self.newsTableView duration:0.8 options:UIViewAnimationOptionTransitionCrossDissolve animations:nil completion:nil];
}

-(void)changeBackgroundColorOfView : (UIView *)view andTextProperty :(UILabel *)label
{
    NSMutableArray *viewArray = [NSMutableArray arrayWithObjects:self.highlightView, self.galleryView, self.newsView, nil];
    
    view.backgroundColor =[UIColor whiteColor];
    label.textColor = CL_BLUE_COLOR;
    
    [viewArray removeObject:view];
    
    for (UIView *l_view in viewArray)
    {
        l_view.backgroundColor = CL_BLUE_COLOR;
        for (UILabel *l_label in l_view.subviews)
        {
            if ([l_label isKindOfClass:[UILabel class]])
            {
                l_label.textColor = [UIColor whiteColor];
            }
        }
    }
    viewArray = nil;
}


#pragma mark UITableViewDelegate Methods


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (self.isHighlightsSelected)
    {
        return self.highlightArray.count;
    }
    else if(self.isGallerySelected)
    {
        return (self.galleryArray.count/2)+1;
    }
    return self.newsArray.count;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isHighlightsSelected)
    {
        CLHighlights *highlightObj = [CLHighlights getInformationFrom:[self.highlightArray objectAtIndex:indexPath.row]];

        CLHighlightTableViewCell *cell = (CLHighlightTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"CLHighlightTableViewCell"];
        cell.frame = CGRectMake(0, 0, self.view.frame.size.width, cell.frame.size.height);
        [cell configureCellWith:highlightObj];
        cell.delegate = self;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    else if(self.isGallerySelected)
    {
        CLHighlights *highlightLeftObj = [CLHighlights getInformationFrom:[self.galleryArray objectAtIndex:indexPath.row*2]];
        CLRecipeDetailsUserPhotoTableViewCell *cell = (CLRecipeDetailsUserPhotoTableViewCell *)[self.newsTableView dequeueReusableCellWithIdentifier:@"CLRecipeDetailsUserPhotoTableViewCell"];
        cell.frame = CGRectMake(0, 0, self.view.frame.size.width, cell.frame.size.height);
        cell.delegate = self;
        CGFloat count = self.galleryArray.count;
        NSInteger indexCount = ceill(count/2);
        if ((indexCount-1) == indexPath.row)
        {
            if (self.galleryArray.count%2 !=0)
            {
                [cell configureLeftViewWithData:highlightLeftObj];
                cell.rightView.hidden = YES;
            }
            else
            {
                CLHighlights *highlightRightObj = [CLHighlights getInformationFrom:[self.galleryArray objectAtIndex:(indexPath.row*2)+1]];
                [cell configureLeftViewWithData:highlightLeftObj];
                [cell configureRightViewWithData:highlightRightObj];
            }
        }
        else
        {
            CLHighlights *highlightRightObj = [CLHighlights getInformationFrom:[self.galleryArray objectAtIndex:(indexPath.row*2)+1]];
            [cell configureLeftViewWithData:highlightLeftObj];
            [cell configureRightViewWithData:highlightRightObj];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
    CLNewsAndEvents *newsEvent = [CLNewsAndEvents getnewsAndEventsInformationFrom:[self.newsArray objectAtIndex:indexPath.row]];
    CLHomeNewsAndEventsTableViewCell *cell = (CLHomeNewsAndEventsTableViewCell*)[tableView dequeueReusableCellWithIdentifier:@"CLHomeNewsAndEventsTableViewCell" forIndexPath:indexPath];
    [cell configureCellWithData:newsEvent];
    cell.frame = CGRectMake(0, 0, self.view.frame.size.width, cell.frame.size.height);
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isHighlightsSelected) {
        CLHighlights *highlightObj = [CLHighlights getInformationFrom:[self.highlightArray objectAtIndex:indexPath.row]];
        if ([highlightObj.highlightContenttype isEqualToString:@"gallery"])
        {
            CLGalleryViewController *gallaryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLGalleryViewController"];
            gallaryVC.highLightObj = highlightObj;
            [self.navigationController pushViewController:gallaryVC animated:YES];
        }
        else if ([highlightObj.highlightContenttype isEqualToString:@"news"])
        {
            CLNewsViewController *newsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLNewsViewController"];
            newsVC.newsId = highlightObj.highlightId;
            [self.navigationController pushViewController:newsVC animated:YES];
        }
        else if ([highlightObj.highlightContenttype isEqualToString:@"competitions"])
        {
            CLCompetitionViewController *competitionVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLCompetitionViewController"];
            competitionVC.highlightId = highlightObj.highlightId;
            [self.navigationController pushViewController:competitionVC animated:YES];
        }

    }
    else if (self.isGallerySelected)
    {
    }
    else
    {
        CLNewsAndEvents *newsEvent = [CLNewsAndEvents getnewsAndEventsInformationFrom:[self.newsArray objectAtIndex:indexPath.row]];
        CLNewsViewController *newsVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLNewsViewController"];
        newsVC.newsId = newsEvent.newsId;
        [self.navigationController pushViewController:newsVC animated:YES];
    }
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.isHighlightsSelected) {
        return 180;
    }
    else if(self.isGallerySelected)
    {
        return 154;
    }
    CLNewsAndEvents *newsEvent = [CLNewsAndEvents getnewsAndEventsInformationFrom:[self.newsArray objectAtIndex:indexPath.row]];
    
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:18]};
    NSString *recipenameStr = [NSString stringWithFormat:@"%@",newsEvent.newsTitle];
    
    NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:recipenameStr attributes:attributes];
    CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-103, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    
    CGSize size = rect.size;
    return size.height+70;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [tableView setLayoutMargins:UIEdgeInsetsMake(10, 10, 10, 10)];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsMake(10, 10, 10, 10)];
    }
}


-(void)buttonSelected
{
    UIButton *seeAllButton = (UIButton *)[self.view viewWithTag:1100];
    static BOOL isSeeAllSelected = YES;
    self.newsTableView.translatesAutoresizingMaskIntoConstraints = YES;
    if (isSeeAllSelected)
    {
        self.highlightLabel.hidden = YES;
        self.galleryLabel.hidden = YES;
        self.highlightScrollView.hidden = YES;
        self.galleryScrollView.hidden = YES;
        self.pageControl.hidden = YES;
        [UIView animateWithDuration:0.5 animations:^{
            self.newsTableView.frame = CGRectMake(0, 70, self.view.frame.size.width, self.view.frame.size.height-70);
//            [seeAllButton setImage:[UIImage imageNamed:@"closeblacksmall"] forState:UIControlStateNormal];
            [seeAllButton setTitle:@"Hide" forState:UIControlStateNormal];
        }];
    }
    else
    {
        self.highlightLabel.hidden = NO;
        self.galleryLabel.hidden = NO;
        self.highlightScrollView.hidden = NO;
        self.galleryScrollView.hidden = NO;
        self.pageControl.hidden = NO;

        [UIView animateWithDuration:0.5 animations:^{
            self.newsTableView.frame = CGRectMake(0, 400, self.view.frame.size.width, self.view.frame.size.height-400);
            [seeAllButton setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
            [seeAllButton setTitle:@"See All" forState:UIControlStateNormal];

        }];
    }
    
    isSeeAllSelected = !isSeeAllSelected;
}

#pragma mark - CLRecipeDetailsUserPhotoTableViewCellDelegate Methods

-(void)pictureViewTappedWith:(CLHighlights *)highlight
{
    CLGalleryViewController *gallaryVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CLGalleryViewController"];
    gallaryVC.highLightObj = highlight;
    [self.navigationController pushViewController:gallaryVC animated:YES];
}

@end
