//
//  CLCompetitionViewController.h
//  Clover
//
//  Created by Ajith Kumar on 28/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLBaseViewController.h"

@interface CLCompetitionViewController : CLBaseViewController
@property (nonatomic, strong) NSString *highlightId;
@property (weak, nonatomic) IBOutlet UILabel *questionLabel;
@end
