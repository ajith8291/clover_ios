//
//  CLCompetitionViewController.m
//  Clover
//
//  Created by Ajith Kumar on 28/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLCompetitionViewController.h"
#import <MBProgressHUD/MBProgressHUD.h>
#import "CLConstants.h"
#import "NSString+DataValidator.h"

@interface CLCompetitionViewController ()<UIAlertViewDelegate,UITextViewDelegate>

@property (nonatomic, strong) NSString *questionStr;
@property (nonatomic, strong) NSArray *choiceArray;
@property (nonatomic, strong) NSString *keyAnwseredStr;
@property (nonatomic, strong) UIScrollView *mainScrollView;
@property (nonatomic, strong) NSString *selectedChoice;
@property (nonatomic, strong) NSString *competitionType;
@end

@implementation CLCompetitionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationView.navLogoImageView.hidden = YES;
    self.navigationView.navMenuButton.hidden = YES;
    self.navigationView.navEditButton.hidden = YES;
    self.navigationView.navSearchButton.hidden = YES;
    self.navigationView.navCartButton.hidden = YES;
    self.navigationView.navTitleTextLabel.hidden = NO;
    self.navigationView.navTitleTextLabel.text = [@"Competition" capitalizedString];
    self.mainScrollView = [[UIScrollView alloc] init];
    [self callWebServiceToGetDetails];

}
-(void)callWebServiceToGetDetails
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        NSDictionary *params = @{@"qid":self.highlightId,
                                 @"user":self.userManager.user.userId};
        
        [self.networkManager startGETRequestWithAPI:@"getcompetition/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             self.questionStr = [[responseDictionary objectForKey:@"competition"] objectForKey:@"question"];
             self.choiceArray = [[responseDictionary objectForKey:@"competition"] objectForKey:@"choices"];
             self.keyAnwseredStr = [[responseDictionary objectForKey:@"competition"] objectForKey:@"answered"];
             self.competitionType = [[responseDictionary objectForKey:@"competition"] objectForKey:@"questiontype"];

             [self configureViewWithDetails];
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }
    
}

-(void)configureViewWithDetails
{
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:17]};
    self.questionStr = [NSString stringWithFormat:@"%@",self.questionStr];
    NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:self.questionStr attributes:attributes];
    
    CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(self.view.frame.size.width-20, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    CGSize size = rect.size;
    self.questionLabel.translatesAutoresizingMaskIntoConstraints = YES;
    self.questionLabel.frame = CGRectMake(10, 80, self.view.frame.size.width-20, size.height);
    self.questionLabel.text = self.questionStr;
    CGFloat y = self.questionLabel.frame.origin.y+size.height+10;
    if ([self.competitionType isEqualToString:@"choices"])
    {
        self.mainScrollView.frame = CGRectMake(0,y , self.view.frame.size.width, self.view.frame.size.height-y-10);
        [self modifyMainscrollViewWithChoices];
        [self.view addSubview:self.mainScrollView];
    }
    else
    {
        [self configureInputQuestionTypeWith:y];
    }
}

-(void)configureInputQuestionTypeWith:(CGFloat)yAxis
{
    UITextView *textView = [[UITextView alloc] initWithFrame:CGRectMake(20, yAxis, self.view.frame.size.width-40, 80)];
    textView.textColor = [UIColor lightGrayColor];
    textView.alpha = 0.8;
    textView.font = [UIFont fontWithName:ROBOTO_ITALIC size:16.0];
    textView.text = @"write your answer...";
    textView.delegate = self;
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked)];
    //    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                      [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                      doneButton,
                                      nil]];
    keyboardDoneButtonView.tintColor = [UIColor blackColor];
    textView.inputAccessoryView = keyboardDoneButtonView;

    
    textView.layer.cornerRadius = 2.0;
    textView.layer.borderWidth = 0.5;
    textView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    textView.tag = 222;
    
    UIView *submitView = [[UIView alloc] initWithFrame:CGRectMake(40, yAxis+100, self.view.frame.size.width-80, 40)];
    submitView.backgroundColor = CL_BLUE_COLOR;
    UILabel *submitLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-80, 40)];
    submitLabel.textAlignment = NSTextAlignmentCenter;
    submitLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:17.0];
    submitLabel.textColor = [UIColor whiteColor];
    submitLabel.text = @"Submit";
    [submitView addSubview:submitLabel];
    submitView.tag = 100;
    UITapGestureRecognizer *submitTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(submitViewTapped)];
    [submitView addGestureRecognizer:submitTap];
    
    [self.view addSubview:textView];
    [self.view addSubview:submitView];

    
}

-(void)modifyMainscrollViewWithChoices
{
    CGFloat yAxis = 0;
    for (int i =0; i<self.choiceArray.count; i++)
    {
        UIView *choiceView = [[UIView alloc] initWithFrame:CGRectMake(40, yAxis+10, self.view.frame.size.width-80, 60)];
        choiceView.layer.cornerRadius = 3.0;
        choiceView.layer.borderWidth = 2.0;
        choiceView.layer.borderColor = [[UIColor lightGrayColor] CGColor];
        
        UIImageView *tickImageView = [[UIImageView alloc] initWithFrame:CGRectMake(10, 19, 22, 22)];
        [tickImageView setImage:[UIImage imageNamed:@"tickgrey"]];
        UILabel *choiceLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 10, choiceView.frame.size.width-50, 40)];
        choiceLabel.numberOfLines = 2;
        choiceLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:15.0];
        choiceLabel.text = [[self.choiceArray objectAtIndex:i] objectForKey:@"option"];
        yAxis = yAxis+70;
        choiceView.tag = i+1;
        
        UITapGestureRecognizer *choiceTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(choiceViewTapped:)];
        [choiceView addGestureRecognizer:choiceTap];

        
        [choiceView addSubview:tickImageView];
        [choiceView addSubview:choiceLabel];
        [self.mainScrollView addSubview:choiceView];
    }
    UIView *submitView = [[UIView alloc] initWithFrame:CGRectMake(40, yAxis+20, self.view.frame.size.width-80, 40)];
    submitView.backgroundColor = CL_BLUE_COLOR;
    UILabel *submitLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width-80, 40)];
    submitLabel.textAlignment = NSTextAlignmentCenter;
    submitLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:17.0];
    submitLabel.textColor = [UIColor whiteColor];
    submitLabel.text = @"Submit";
    [submitView addSubview:submitLabel];
    submitView.tag = 100;
    UITapGestureRecognizer *submitTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(submitViewTapped)];
    [submitView addGestureRecognizer:submitTap];
    
    [self.mainScrollView addSubview:submitView];
    self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.frame.size.width, yAxis+50);
}

#pragma mark UITap/UIbutton Action

-(void)submitViewTapped
{
    for (UIView *view in self.view.subviews) {
        if (view.tag == 222)
        {
            UITextView *textView = (UITextView *)view;
            self.selectedChoice = textView.text;
        }
    }
    if ([self.keyAnwseredStr isEqualToString:@"yes"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"You already answed for this question" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alert.tag = 999;
        [alert show];
    }
    else
    {
        if ([self.competitionType isEqualToString:@"choices"])
        {
            if (self.selectedChoice == nil)
            {
            }
            else
            {
                [self postUserAnswers];
            }

        }
        else
        {
            if ([self.selectedChoice isEqualToString:@"write your answer..."]||[self.selectedChoice isEmptyString])
            {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Please enter your answer" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                alert.tag = 123;
                [alert show];
            }
            else
            {
                [self postUserAnswers];
            }
        }

    }

}

-(void)postUserAnswers
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        NSDictionary *params = @{@"qid":self.highlightId,
                                 @"user":self.userManager.user.userId,
                                 @"option":self.selectedChoice};
        
        [self.networkManager startGETRequestWithAPI:@"sendanswer/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             NSString *msg = [NSString stringWithFormat:@"%@",[responseDictionary objectForKey:@"msg"]];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:msg delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             alert.tag = 999;
             [alert show];
             
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }
}

-(void)choiceViewTapped:(UITapGestureRecognizer *)tapView
{
    if ([self.keyAnwseredStr isEqualToString:@"yes"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"You already answed for this question" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        alert.tag = 999;
        [alert show];
    }
    else
    {
        UIView *choiceView = (UIView *)[self.mainScrollView  viewWithTag:tapView.view.tag];
        
        for (UIView *view in self.mainScrollView.subviews)
        {
            if (view.tag == choiceView.tag)
            {
                for (UIImageView *imageView in view.subviews)
                {
                    if ([imageView isKindOfClass:[UIImageView class]])
                    {
                        [imageView setImage:[UIImage imageNamed:@"tickblue"]];
                        view.layer.borderColor = [CL_BLUE_COLOR CGColor];
                        self.selectedChoice = [NSString stringWithFormat:@"%ld",(long)view.tag];
                    }
                }
            }
            else
            {
                for (UIImageView *imageView in view.subviews)
                {
                    if ([imageView isKindOfClass:[UIImageView class]])
                    {
                        [imageView setImage:[UIImage imageNamed:@"tickgrey"]];
                        view.layer.borderColor = [[UIColor lightGrayColor] CGColor];
                    }
                }
            }
        }
    }
}


-(void)doneClicked
{
    [self.view endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark UIAlertViewDelegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 999)
    {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/


#pragma mark UITextViewDelegate methods

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"write your answer..."]) {
        textView.textColor = [UIColor colorWithRed:40.0/255.0 green:40.0/255.0 blue:40.0/255.0 alpha:1.0];
        textView.font = [UIFont fontWithName:ROBOTO_REGULAR size:16.0];
        textView.text = @"";
    }
    return YES;
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    return YES;
}

@end
