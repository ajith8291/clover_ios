//
//  CLGalleryViewController.h
//  Clover
//
//  Created by Ajith Kumar on 28/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLBaseViewController.h"
#import "CLHighlights.h"

@interface CLGalleryViewController : CLBaseViewController

@property (weak, nonatomic) IBOutlet UIScrollView *galleryScrollView;

@property (nonatomic, strong) CLHighlights *highLightObj;
@end
