//
//  CLGalleryViewController.m
//  Clover
//
//  Created by Ajith Kumar on 28/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLGalleryViewController.h"
#import <UIImageView+AFNetworking.h>
#import <MBProgressHUD/MBProgressHUD.h>
#import "UIImage+colors.h"

@interface CLGalleryViewController ()

@property (nonatomic, strong)NSArray *gallaryArray;
@end

@implementation CLGalleryViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationView.navLogoImageView.hidden = YES;
    self.navigationView.navMenuButton.hidden = YES;
    self.navigationView.navEditButton.hidden = YES;
    self.navigationView.navSearchButton.hidden = YES;
    self.navigationView.navCartButton.hidden = YES;
    self.navigationView.navTitleTextLabel.hidden = NO;
    self.navigationView.navTitleTextLabel.text = [@"Gallery" capitalizedString];
    self.galleryScrollView.pagingEnabled = YES;
    self.galleryScrollView.showsHorizontalScrollIndicator = NO;
    self.galleryScrollView.translatesAutoresizingMaskIntoConstraints = YES;
    self.galleryScrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
    [self callWebServiceToGetDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//http://cloverapi.jumpcatch.com/api/singlegallery/?gallery=362

-(void)callWebServiceToGetDetails
{
    if ([self isNetworkIsReachable])
    {
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.dimBackground = YES;
        hud.mode = MBProgressHUDModeIndeterminate;
        hud.labelText = @"Loading...";
        [hud show:YES];
        NSDictionary *params = @{@"gallery":self.highLightObj.highlightId};
        
        [self.networkManager startGETRequestWithAPI:@"singlegallery/" andParameters:params withSuccess:^(NSDictionary *responseDictionary)
         {
             [hud hide:YES];
             self.gallaryArray = [[responseDictionary objectForKey:@"album"] objectForKey:@"gallery"];
             if (self.gallaryArray.count) {
                 [self loadViewWithImageGallary];
             }
             else
             {
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"There are no picture in Gallery" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                 [alert show];
             }
             
         } andFailure:^(NSString *errorMessage) {
             [hud hide:YES];
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorMessage delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
             [alert show];
         }];
    }
    
}


-(void)loadViewWithImageGallary
{
    CGFloat xAxis = 0;
    for (int i =0; i<self.gallaryArray.count; i++)
    {
        UIImageView *galleryImageView = [[UIImageView alloc] initWithFrame:CGRectMake(xAxis, self.view.frame.size.height*0.22, self.view.frame.size.width, self.view.frame.size.width)];
        NSString *imageUrl = [[self.gallaryArray objectAtIndex:i] objectForKey:@"image"];
        
        NSString *imgUrl = [imageUrl stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%f",self.view.frame.size.width*2]];
        imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%f",(self.view.frame.size.width*1.04)*2]];
        
        UIImage *placeholderImg = [UIImage imageWithColor:[UIColor lightGrayColor]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
        
        __weak UIImageView *weakImg = galleryImageView;
        
        [galleryImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             CGSize newSize = CGSizeMake(self.view.frame.size.width, self.view.frame.size.width);
             
             UIGraphicsBeginImageContext(newSize);
             
             [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
             UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
             UIGraphicsEndImageContext();
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 weakImg.image = newImage;
                 weakImg.alpha = 0.0;
                 
                 [UIView animateWithDuration:1.0 animations:^{
                     weakImg.alpha = 1.0;
                 }];
             });
             
         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
             
             
         }];
        [self.galleryScrollView addSubview:galleryImageView];
        xAxis=xAxis+self.view.frame.size.width;
    }
    self.galleryScrollView.contentSize = CGSizeMake(xAxis, self.view.frame.size.height-130);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
