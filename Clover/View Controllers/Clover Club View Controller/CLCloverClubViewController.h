//
//  CLCloverClubViewController.h
//  Clover
//
//  Created by Ajith Kumar on 28/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLBaseViewController.h"

@interface CLCloverClubViewController : CLBaseViewController<UITableViewDelegate, UITableViewDataSource>
@property (weak, nonatomic) IBOutlet UILabel *highlightLabel;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UIScrollView *highlightScrollView;

@property (weak, nonatomic) IBOutlet UILabel *galleryLabel;
@property (weak, nonatomic) IBOutlet UIScrollView *galleryScrollView;
@property (weak, nonatomic) IBOutlet UITableView *newsTableView;
@property (weak, nonatomic) IBOutlet UIView *segmentView;
@property (weak, nonatomic) IBOutlet UIView *highlightView;
@property (weak, nonatomic) IBOutlet UIView *galleryView;
@property (weak, nonatomic) IBOutlet UIView *newsView;

@property (weak, nonatomic) IBOutlet UILabel *highlightsTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *galleryTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *newsTextLabel;
@property (weak, nonatomic) IBOutlet UIView *highlightSeperator;
@property (weak, nonatomic) IBOutlet UIView *gallerySeperator;

@end
