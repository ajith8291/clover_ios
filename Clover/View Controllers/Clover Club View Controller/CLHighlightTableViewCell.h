//
//  CLHighlightTableViewCell.h
//  Clover
//
//  Created by Ajith Kumar on 23/02/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLHighlights.h"
#import "CLHightlightView.h"

@protocol CLHighlightTableViewCellDelegate <NSObject>

@optional
-(void)playButtonTappedWith:(CLHighlights *)highlightObj;
@end

@interface CLHighlightTableViewCell : UITableViewCell<CLHightlightViewDelegate>

-(void)configureCellWith:(CLHighlights *)highlight;

@property (nonatomic, strong) id<CLHightlightViewDelegate>delegate;

@end
