//
//  NSString+DataValidator.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DataValidator)
- (BOOL)validateEmail;
- (BOOL)validatePhoneNumber;
- (BOOL)validatePassword;
- (BOOL)isEmptyString;
-(NSString *)getDateFromString;
-(NSString *)makeDateStringFromTimeStamp;
@end
