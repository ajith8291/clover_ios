//
//  UIImage+colors.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 11/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (colors)
+ (UIImage *)imageWithColor:(UIColor *)color;
@end
