//
//  NSString+DataValidator.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "NSString+DataValidator.h"

@implementation NSString (DataValidator)

- (BOOL)validateEmail
{
    NSString *emailRegex = @"[A-Z0-9a-z._-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailPred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailRegex];
    
    return [emailPred evaluateWithObject:self];
}

- (BOOL)validatePhoneNumber
{
    BOOL isPhoneNumber = NO;
    //Validate phone using NSTextCheckingResult
    NSError *error = NULL;
    
    //Assign Data detector with NSTextCheckingType
    NSDataDetector *detector = [NSDataDetector dataDetectorWithTypes:NSTextCheckingTypePhoneNumber error:&error];
    
    NSString *numberWithoutSpace = [self stringByReplacingOccurrencesOfString:@" " withString:@""];
    
    //Set range
    NSRange inputRange = NSMakeRange(0, [numberWithoutSpace length]);
    NSArray *matches = [detector matchesInString:numberWithoutSpace options:0 range:inputRange];
    
    if([matches count] == 0)
    {
        isPhoneNumber = NO;
    }
    
    else
    {
        //Found match , check if matches the whole string
        NSTextCheckingResult *result= (NSTextCheckingResult *)[matches objectAtIndex:0];
        
        if([result resultType] == NSTextCheckingTypePhoneNumber && result.range.location == inputRange.location && result.range.length == inputRange.length)
        {
            //Matched whole thing
            isPhoneNumber = YES;
        }
        else
        {
            isPhoneNumber = NO;
        }
        
    }
    
    return isPhoneNumber;
}


- (BOOL)validatePassword
{
    //    NSString *passwordRegex = @"[A-Z0-9a-z]{2,16}";
    NSString *passReg = @"[A-Z0-9a-z!@#$&*]{6,32}$";
    
    NSPredicate *password = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", passReg];
    
    return [password evaluateWithObject:self];
    
}

- (BOOL)isEmptyString
{
    BOOL isEmp = NO;
    
    if([[self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]] length] == 0)
    {
        isEmp = YES;
    }
    else
    {
        isEmp = NO;
    }
    
    return isEmp;
}

-(NSString *)getDateFromString
{
    NSInteger timestamp = [self integerValue];

    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/YYYY  hh:mm a"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    return stringFromDate;
}

-(NSString *)makeDateStringFromTimeStamp
{
    NSInteger timestamp = [self integerValue];
    
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:timestamp];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM dd"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    return stringFromDate;
}

@end
