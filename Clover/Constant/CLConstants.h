//
//  CLConstants.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#define IS_USER_LOGGED_IN @"ISUSERLOGGEDIN"
#define CL_BLUE_COLOR [UIColor colorWithRed:0.0/255.0 green:83.0/255.0 blue:160.0/255.0 alpha:1.0]
#define Base_URL @"http://cloverapi.jumpcatch.com/api/"

#define IS_IPHONE_4 ([[UIScreen mainScreen] bounds].size.height == 480.0f)
#define IS_IOS8_OR_GREATER (NSFoundationVersionNumber > NSFoundationVersionNumber_iOS_7_1)
#define IS_IPHONE_6 ([[UIScreen mainScreen] bounds].size.height == 667.0f)


#define ROBOTO_REGULAR @"Roboto-Regular" 
#define ROBOTO_LIGHT @"Roboto-Light"
#define ROBOTO_BOLD  @"Roboto-Bold"
#define ROBOTO_MEDIUM  @"Roboto-Medium"
#define ROBOTO_THIN  @"Roboto-Thin"
#define ROBOTO_ITALIC  @"Roboto-Italic"
