//
//  CLMealPlan.m
//  Clover
//
//  Created by Ajith Kumar on 28/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLMealPlan.h"

@implementation CLMealPlan

+(CLMealPlan *)getMealPlanDetailsFrom:(NSDictionary *)data
{
    CLMealPlan *mealPlan = [CLMealPlan new];
    mealPlan.mealPlanID =[NSString stringWithFormat:@"%@",[data objectForKey:@"ID"]];
    mealPlan.mealDate =[NSString stringWithFormat:@"%@",[data objectForKey:@"meal_date"]];
    mealPlan.mealType =[NSString stringWithFormat:@"%@",[data objectForKey:@"meal_type"]];
    NSDictionary *recipeDict = [data objectForKey:@"recipe_list"];
    
    mealPlan.recipeID =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"ID"]];
    mealPlan.recipeTitle =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"post_title"]];
    mealPlan.recipeImageUrl =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"image"]];
    return mealPlan;
}
@end
