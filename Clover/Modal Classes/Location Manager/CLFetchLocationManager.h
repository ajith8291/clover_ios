//
//  CLFetchLocationManager.h
//  Clover
//
//  Created by Ajith Kumar on 11/02/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>


@protocol CLFetchLocationManagerDelegate <NSObject>

@optional
- (void)didFetchUserLocationWithLocation:(CLLocation *)userLocation;
- (void)failedToFetchUserLocationWithErrorTitle:(NSString *)title andErrorMessage:(NSString *)message;
@end


@interface CLFetchLocationManager : NSObject


+ (CLFetchLocationManager *)sharedManager;

- (void)initializeLocationManager;
- (void)startUpdatingUserLocation;

@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, weak) id <CLFetchLocationManagerDelegate>delegate;

@end
