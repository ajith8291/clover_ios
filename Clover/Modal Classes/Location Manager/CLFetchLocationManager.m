//
//  CLFetchLocationManager.m
//  Clover
//
//  Created by Ajith Kumar on 11/02/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLFetchLocationManager.h"
#include "CLConstants.h"

@interface CLFetchLocationManager () <CLLocationManagerDelegate>

@property (nonatomic, strong) CLLocation *userCurrentLocation;
@end


@implementation CLFetchLocationManager



+(CLFetchLocationManager *)sharedManager
{
    static dispatch_once_t onceToken;
    static CLFetchLocationManager *manager = nil;
    
    dispatch_once(&onceToken, ^{
        
        manager = [[CLFetchLocationManager alloc] init];
    });
    
    return manager;
}

#pragma mark - Manager Methods

- (void)initializeLocationManager
{
    if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorized || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined)
    {
        if (self.locationManager == nil)
        {
            self.locationManager = [CLLocationManager new];
            self.locationManager.delegate = self;
            self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
        }
        if (IS_IOS8_OR_GREATER)
        {
            [self.locationManager requestAlwaysAuthorization];
        }
        
        [self.locationManager startUpdatingLocation];
    }
    else
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(failedToFetchUserLocationWithErrorTitle: andErrorMessage:)]) {
            [self.delegate failedToFetchUserLocationWithErrorTitle:nil andErrorMessage:@"Unable to detect Location Services"];
        }
    }
}

- (void)startUpdatingUserLocation
{
    if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusAuthorized || [CLLocationManager authorizationStatus]==kCLAuthorizationStatusNotDetermined) {
        self.locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters;
        
        [self.locationManager startUpdatingLocation];
        
        if ([CLLocationManager deferredLocationUpdatesAvailable])
        {
            [self.locationManager allowDeferredLocationUpdatesUntilTraveled:100 timeout:CLTimeIntervalMax];
        }
    }
}

#pragma mark - CLLocation Delegate
/*
 *  locationManager:didUpdateLocations:
 *
 *  Discussion:
 *    Invoked when new locations are available.  Required for delivery of
 *    deferred locations.  If implemented, updates will
 *    not be delivered to locationManager:didUpdateToLocation:fromLocation:
 *
 *    locations is an array of CLLocation objects in chronological order.
 */
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
    // Stop updating once we have a location
    [self.locationManager stopUpdatingLocation];
    //capture the location...
    self.userCurrentLocation = [locations lastObject];
    if (self.delegate && [self.delegate respondsToSelector:@selector(didFetchUserLocationWithLocation:)]) {
        [self.delegate didFetchUserLocationWithLocation:self.userCurrentLocation];
    }
}

/*
 *  locationManager:didFailWithError:
 *
 *  Discussion:
 *    Invoked when an error has occurred. Error types are defined in "CLError.h".
 */
- (void)locationManager:(CLLocationManager *)manager
       didFailWithError:(NSError *)error
{
    [self.delegate failedToFetchUserLocationWithErrorTitle:nil andErrorMessage:@"Unable to detect Location Services"];
}

@end
