//
//  CLComments.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 08/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLComments.h"

@implementation CLComments

+(CLComments *)getCommentsdetailsFromDictionary:(NSDictionary *)resultDict
{
    CLComments *comment = [CLComments new];
    comment.commentsId =[NSString stringWithFormat:@"%@",[resultDict objectForKey:@"comment_ID"]];
    comment.commentTitle =[NSString stringWithFormat:@"%@",[resultDict objectForKey:@"pic_comment"]];
    comment.commentUserId =[NSString stringWithFormat:@"%@",[resultDict objectForKey:@"comment_user"]];
    comment.commentDate =[NSString stringWithFormat:@"%@",[resultDict objectForKey:@"comment_date"]];
    
    comment.recipeId =[NSString stringWithFormat:@"%@",[resultDict objectForKey:@"recipe_id"]];
    comment.recipeName =[NSString stringWithFormat:@"%@",[resultDict objectForKey:@"recipe_name"]];
    comment.commentPictureUrl =[NSString stringWithFormat:@"%@",[resultDict objectForKey:@"comment_picture"]];
    
    comment.user =[CLUser getUserInformationFrom:[resultDict objectForKey:@"user_info"]];
    return comment;
}
@end
