//
//  CLUploadPicture.h
//  Clover
//
//  Created by Ajith Kumar on 13/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CLUser.h"

@interface CLUploadPicture : NSObject

@property(nonatomic, strong)NSString *uploadDate;
@property(nonatomic, strong)NSString *uploadPicture;
@property(nonatomic, strong)NSString *uploadPictureCaption;
@property(nonatomic, strong)NSString *uploadPictureId;
@property (nonatomic, strong) CLUser *user;

+(CLUploadPicture *)getUploadPictureDetailsFrom:(NSDictionary *)data;
@end
