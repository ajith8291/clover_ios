//
//  CLUser.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CLUser : NSObject

@property (nonatomic, strong) NSString *userId;
@property (nonatomic, strong) NSString *userName;
@property (nonatomic, strong) NSString *userEmail;
@property (nonatomic, strong) NSString *userLocation;
@property (nonatomic, strong) NSString *userMobileNumber;
@property (nonatomic, strong) NSString *profileURL;
@property (nonatomic, strong) NSString *summary;
@property (nonatomic, strong) NSString *twsignup;
@property (nonatomic, strong) NSString *fbsignup;
@property (nonatomic, strong) NSString *fbaccesstoken;
@property (nonatomic, strong) NSString *twaccesstoken;
@property (nonatomic, strong) NSString *fbconnected;
@property (nonatomic, strong) NSString *twconnected;
@property (nonatomic, strong) NSString *reviewCount;
@property (nonatomic, strong) NSString *favouriteCount;
@property (nonatomic, strong) NSString *mealPlannerCount;


+(CLUser *)getUserInformationFrom:(NSDictionary *)userInfo;
@end
