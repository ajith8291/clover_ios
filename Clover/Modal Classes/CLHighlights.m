//
//  CLHighlights.m
//  Clover
//
//  Created by Ajith Kumar on 28/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLHighlights.h"

@implementation CLHighlights

+(CLHighlights *)getInformationFrom:(NSDictionary *)data
{
    CLHighlights *highlight = [CLHighlights new];
    
    highlight.highlightId = [NSString stringWithFormat:@"%@",[data objectForKey:@"ID"]];
    highlight.highlightContenttype = [NSString stringWithFormat:@"%@",[data objectForKey:@"contenttype"]];
    highlight.highlightFeaturedImage = [data objectForKey:@"featured_image"];
    highlight.highlightGallery = [data objectForKey:@"gallery"];
    highlight.highlighGuid = [NSString stringWithFormat:@"%@",[data objectForKey:@"guid"]];
    highlight.highlightPostDate = [NSString stringWithFormat:@"%@",[data objectForKey:@"post_date"]];
    highlight.highlightPostTitle = [NSString stringWithFormat:@"%@",[data objectForKey:@"post_title"]];
    highlight.highlightVideoUrl = [NSString stringWithFormat:@"%@",[data objectForKey:@"video_url"]];
    return highlight;
}
@end
