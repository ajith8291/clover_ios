//
//  CLComments.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 08/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CLUser.h"

@interface CLComments : NSObject
@property (nonatomic, strong) NSString *commentsId;
@property (nonatomic, strong) NSString *commentTitle;//comment_user
@property (nonatomic, strong) NSString *commentUserId;
@property (nonatomic, strong) NSString *commentDate;
@property (nonatomic, strong) NSString *commentPictureUrl;
@property (nonatomic, strong) NSString *recipeId;
@property (nonatomic, strong) NSString *recipeName;

@property (nonatomic, strong) CLUser *user;
+(CLComments *)getCommentsdetailsFromDictionary:(NSDictionary *)resultDict;
@end
