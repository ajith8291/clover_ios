//
//  CLRecipe.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 11/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLRecipe.h"

@implementation CLRecipe

+(CLRecipe *)getInformationFromDictionary:(NSDictionary *)recipeDict
{
    CLRecipe *recipe = [CLRecipe new];
    recipe.recipeId =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"ID"]];
    recipe.recipeCookingTime =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"preparation_time"]];
    recipe.recipeCuisines =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"cuisines"]];
    recipe.recipeDietry =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"dietry"]];
    recipe.recipeGalleryArray =[recipeDict objectForKey:@"gallery"];
    
    recipe.recipeGuide =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"guid"]];
    recipe.recipeImage =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"image"]];
    recipe.recipeIngredientsArray =[recipeDict objectForKey:@"ingredients"];
    
    recipe.recipeMealTimeType =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"meal_time"]];
    recipe.recipeMealType =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"meal_type"]];
    recipe.recipeMethodArray = [recipeDict objectForKey:@"method"];

    
    recipe.recipeName =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"post_name"]];
    recipe.recipeTitle =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"post_title"]];
    recipe.recipePreparationTime =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"preparation_time"]];
    recipe.recipeVideoUrl =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"video_url"]];
    
    if ([recipe.recipeVideoUrl isEqualToString:@"null"]) {
        recipe.recipeVideoUrl = @"";
    }
    recipe.recipeRating =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"rating"]];
    recipe.recipeServingPersons =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"serving_persons"]];
    recipe.recipeType =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"recipe_type"]];
    recipe.recipeIsRated =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"israted"]];
    recipe.recipeCommentCount =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"commentcount"]];
    recipe.recipeUserRating =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"userrating"]];
    
    recipe.recipePictureCount =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"pic_comment_count"]];
    recipe.uploadPictureArray =[recipeDict objectForKey:@"picture_comments"];
    
    recipe.recipeInfavourite =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"infavourite"]];
    recipe.recipeInShoppingList =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"in_list"]];
    recipe.recipeInMealPlan =[NSString stringWithFormat:@"%@",[recipeDict objectForKey:@"meal_plan"]];
    
    recipe.recipeShoppingListId =  [recipeDict objectForKey:@"list_id"];


    
    return recipe;
}
@end
