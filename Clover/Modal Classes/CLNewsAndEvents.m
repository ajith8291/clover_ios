//
//  CLNewsAndEvents.m
//  Clover
//
//  Created by Ajith Kumar on 22/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLNewsAndEvents.h"

@implementation CLNewsAndEvents

+(CLNewsAndEvents *)getnewsAndEventsInformationFrom:(NSDictionary *)newsData
{
    CLNewsAndEvents *news = [CLNewsAndEvents new];
    news.newsId = [NSString stringWithFormat:@"%@",[newsData objectForKey:@"ID"]];
    news.newsTitle = [NSString stringWithFormat:@"%@",[newsData objectForKey:@"news_title"]];
    news.newsContent = [NSString stringWithFormat:@"%@",[newsData objectForKey:@"news_content"]];
    news.publishDate = [NSString stringWithFormat:@"%@",[newsData objectForKey:@"publish_date"]];
    news.imageURL = [NSString stringWithFormat:@"%@",[newsData objectForKey:@"image"]];
    news.featuredImageURL = [NSString stringWithFormat:@"%@",[[newsData objectForKey:@"featured_image"] objectForKey:@"image"]];
    news.galleryArray = [newsData objectForKey:@"gallery"];
    return news;
}
@end
