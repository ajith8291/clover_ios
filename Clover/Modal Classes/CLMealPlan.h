//
//  CLMealPlan.h
//  Clover
//
//  Created by Ajith Kumar on 28/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CLRecipe.h"

@interface CLMealPlan : NSObject

@property (nonatomic, strong) NSString *mealPlanID;
@property (nonatomic, strong) NSString *mealType;
@property (nonatomic, strong) NSString *mealDate;
@property (nonatomic, strong) NSString *recipeID;
@property (nonatomic, strong) NSString *recipeTitle;
@property (nonatomic, strong) NSString *recipeImageUrl;

+(CLMealPlan *)getMealPlanDetailsFrom:(NSDictionary *)data;

@end
