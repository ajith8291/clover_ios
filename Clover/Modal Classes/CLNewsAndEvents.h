//
//  CLNewsAndEvents.h
//  Clover
//
//  Created by Ajith Kumar on 22/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CLNewsAndEvents : NSObject
@property (nonatomic, strong) NSString *newsId;
@property (nonatomic, strong) NSString *newsTitle;
@property (nonatomic, strong) NSString *newsContent;
@property (nonatomic, strong) NSString *publishDate;
@property (nonatomic, strong) NSString *imageURL;
@property (nonatomic, strong) NSString *featuredImageURL;
@property (nonatomic, strong) NSArray *galleryArray;


+(CLNewsAndEvents *)getnewsAndEventsInformationFrom:(NSDictionary *)newsData;
@end
