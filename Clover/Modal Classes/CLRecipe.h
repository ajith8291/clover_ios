//
//  CLRecipe.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 11/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CLRecipe : NSObject

@property(nonatomic, strong)NSString *recipeId;
@property(nonatomic, strong)NSString *recipeCookingTime;
@property(nonatomic, strong)NSString *recipeCuisines;
@property(nonatomic, strong)NSString *recipeDietry;
@property(nonatomic, strong)NSArray *recipeGalleryArray;

@property(nonatomic, strong)NSString *recipeGuide;
@property(nonatomic, strong)NSString *recipeImage;
@property(nonatomic, strong)NSArray *recipeIngredientsArray;

@property(nonatomic, strong)NSString *recipeMealTimeType;
@property(nonatomic, strong)NSString *recipeMealType;
@property(nonatomic, strong)NSArray *recipeMethodArray;
@property(nonatomic, strong)NSString *recipeName;
@property(nonatomic, strong)NSString *recipeTitle;
@property(nonatomic, strong)NSString *recipePreparationTime;
@property(nonatomic, strong)NSString *recipeVideoUrl;
@property(nonatomic, strong)NSString *recipeServingPersons;
@property(nonatomic, strong)NSString *recipeRating;
@property(nonatomic, strong)NSString *recipeType;
@property(nonatomic, strong)NSString *recipeIsRated;
@property(nonatomic, strong)NSString *recipeUserRating;
@property(nonatomic, strong)NSString *recipeCommentCount;
@property(nonatomic, strong)NSString *recipePictureCount;
@property(nonatomic, strong)NSString *recipeInfavourite;
@property(nonatomic, strong)NSString *recipeInShoppingList;
@property(nonatomic, strong)NSString *recipeInMealPlan;

@property(nonatomic, strong)NSString *recipeShoppingListId;

@property (nonatomic, strong) NSMutableArray *uploadPictureArray;

+(CLRecipe *)getInformationFromDictionary: (NSDictionary *)recipeDict;
@end
