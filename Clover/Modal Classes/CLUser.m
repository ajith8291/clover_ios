//
//  CLUser.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLUser.h"

@implementation CLUser

- (void)encodeWithCoder:(NSCoder *)aCoder
{
    [aCoder encodeObject:self.userId forKey:@"userid"];
    [aCoder encodeObject:self.userName forKey:@"displayname"];
    [aCoder encodeObject:self.userEmail forKey:@"useremail"];
    [aCoder encodeObject:self.userLocation forKey:@"location"];
    [aCoder encodeObject:self.userMobileNumber forKey:@"mobilenumber"];
    [aCoder encodeObject:self.profileURL forKey:@"profile_pic"];
    [aCoder encodeObject:self.summary forKey:@"summary"];
    [aCoder encodeObject:self.fbsignup forKey:@"fbsignup"];
    [aCoder encodeObject:self.twsignup forKey:@"twittersignup"];
    [aCoder encodeObject:self.fbaccesstoken forKey:@"fbaccesstoken"];
    [aCoder encodeObject:self.twaccesstoken forKey:@"twaccesstoken"];
    [aCoder encodeObject:self.fbconnected forKey:@"fbconnected"];
    [aCoder encodeObject:self.twconnected forKey:@"twconnected"];
    [aCoder encodeObject:self.reviewCount forKey:@"pic_comment_count"];
    [aCoder encodeObject:self.favouriteCount forKey:@"fav_count"];
    [aCoder encodeObject:self.mealPlannerCount forKey:@"meal_count"];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if (self)
    {
        self.userId = [aDecoder decodeObjectForKey:@"userid"];
        self.userName = [aDecoder decodeObjectForKey:@"displayname"];
        self.userEmail = [aDecoder decodeObjectForKey:@"useremail"];
        self.userLocation = [aDecoder decodeObjectForKey:@"location"];
        self.userMobileNumber = [aDecoder decodeObjectForKey:@"mobilenumber"];
        self.profileURL = [aDecoder decodeObjectForKey:@"profile_pic"];
        self.summary = [aDecoder decodeObjectForKey:@"summary"];
        self.fbsignup = [aDecoder decodeObjectForKey:@"fbsignup"];
        self.twsignup = [aDecoder decodeObjectForKey:@"twittersignup"];
        self.fbaccesstoken = [aDecoder decodeObjectForKey:@"fbaccesstoken"];
        self.twaccesstoken = [aDecoder decodeObjectForKey:@"twaccesstoken"];
        self.fbconnected = [aDecoder decodeObjectForKey:@"fbconnected"];
        self.twconnected = [aDecoder decodeObjectForKey:@"twconnected"];
        self.reviewCount = [aDecoder decodeObjectForKey:@"pic_comment_count"];
        self.favouriteCount = [aDecoder decodeObjectForKey:@"fav_count"];
        self.mealPlannerCount = [aDecoder decodeObjectForKey:@"meal_count"];

    }
    return self;
}


+(CLUser *)getUserInformationFrom:(NSDictionary *)userInfo;
{
    CLUser *user = [CLUser new];
    user.userId = [userInfo objectForKey:@"userid"];
    user.userName = [userInfo objectForKey:@"displayname"];
    user.userEmail = [userInfo objectForKey:@"useremail"];
    user.userLocation = [userInfo objectForKey:@"location"];
    user.userMobileNumber = [userInfo objectForKey:@"mobilenumber"];
    user.profileURL = [userInfo objectForKey:@"profile_pic"];
    user.summary = [userInfo objectForKey:@"summary"];
    user.fbsignup = [userInfo objectForKey:@"fbsignup"];
    user.twsignup = [userInfo objectForKey:@"twittersignup"];
    user.fbaccesstoken = [userInfo objectForKey:@"fbaccesstoken"];
    user.twaccesstoken= [userInfo objectForKey:@"twaccesstoken"];
    user.fbconnected = [userInfo objectForKey:@"fbconnected"];
    user.twconnected = [userInfo objectForKey:@"twconnected"];
    user.reviewCount = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"pic_comment_count"] ];
    user.favouriteCount = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"fav_count"]];
    user.mealPlannerCount = [NSString stringWithFormat:@"%@",[userInfo objectForKey:@"meal_count"] ];
    
    return user;
}

@end
