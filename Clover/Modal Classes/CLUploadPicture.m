//
//  CLUploadPicture.m
//  Clover
//
//  Created by Ajith Kumar on 13/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLUploadPicture.h"
#import "CLUserManager.h"

@implementation CLUploadPicture

+(CLUploadPicture *)getUploadPictureDetailsFrom:(NSDictionary *)data
{
    CLUploadPicture *picture = [CLUploadPicture new];
    picture.uploadPictureId =[NSString stringWithFormat:@"%@",[data objectForKey:@"comment_user"]];
    picture.uploadDate =[NSString stringWithFormat:@"%@",[data objectForKey:@"comment_date"]];
    picture.uploadPictureCaption =[NSString stringWithFormat:@"%@",[data objectForKey:@"pic_comment"]];
    picture.uploadPicture =[NSString stringWithFormat:@"%@",[data objectForKey:@"comment_picture"]];
    if ([data objectForKey:@"user_info"] == nil)
    {
        picture.user = [[CLUserManager sharedManager] user];
    }
    else
    {
        picture.user = [CLUser getUserInformationFrom:[data objectForKey:@"user_info"]];
    }
    return picture;
}
@end
