//
//  CLHighlights.h
//  Clover
//
//  Created by Ajith Kumar on 28/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CLHighlights : NSObject
@property (nonatomic, strong) NSString *highlightId;
@property (nonatomic, strong) NSString *highlightContenttype;
@property (nonatomic, strong) NSString *highlighGuid;
@property (nonatomic, strong) NSString *highlightPostDate;
@property (nonatomic, strong) NSString *highlightPostTitle;
@property (nonatomic, strong) NSString *highlightVideoUrl;
@property (nonatomic, strong) NSDictionary *highlightFeaturedImage;
@property (nonatomic, strong) NSArray *highlightGallery;

+(CLHighlights *)getInformationFrom:(NSDictionary *)data;
@end
