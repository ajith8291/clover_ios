//
//  AppDelegate.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
-(void)loadHomeViewController;
-(void)loadLoginViewController;
-(void)loadInitialViewController;

-(void)ViewZoomInEffect:(UIView *)aView;
-(void)ViewZoomOutEffect:(UIView *)aView;

-(BOOL)isNetworkIsReachable;
@end

