//
//  AppDelegate.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 26/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLAppDelegate.h"
#import "CLUserManager.h"
#import "CLHomeViewController.h"
#import "CLLoginViewController.h"
#import "CLHelpViewController.h"
#import "CLInitialViewController.h"
#import "Reachability.h"

@interface CLAppDelegate ()

@end

@implementation CLAppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
#ifdef __IPHONE_8_0
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge
                                                                                             |UIUserNotificationTypeSound
                                                                                             |UIUserNotificationTypeAlert) categories:nil];
        [application registerForRemoteNotifications];
        [application registerUserNotificationSettings:settings];
        
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [application registerForRemoteNotificationTypes:myTypes];
        
#endif
    } else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [application registerForRemoteNotificationTypes:myTypes];
    }

    [application setStatusBarStyle:UIStatusBarStyleLightContent];
    if ([[CLUserManager sharedManager] hasPreviousLoggedInUser] == YES)
    {
        [self loadHomeViewController];
    }
    else
    {
        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"isNotFirstRun"])
        {
            [self loadInitialViewController];
        }
        else
        {
            [self loadHelpViewController];
        }
    }
    [[NSUserDefaults standardUserDefaults] setInteger:0  forKey:@"previousIndexPath"];
    return YES;
    
}


-(void)loadHelpViewController
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CLHelpViewController *homeVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"CLHelpViewController"];
    UINavigationController *navCtr = [[UINavigationController alloc] initWithRootViewController:homeVC];
    self.window.rootViewController = navCtr;
}


-(void)loadHomeViewController
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CLHomeViewController *homeVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"CLHomeViewController"];
    UINavigationController *navCtr = [[UINavigationController alloc] initWithRootViewController:homeVC];
    self.window.rootViewController = navCtr;
}

-(void)loadLoginViewController
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CLLoginViewController *loginVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"CLLoginViewController"];
    UINavigationController *navCtr = [[UINavigationController alloc] initWithRootViewController:loginVC];
    self.window.rootViewController = navCtr;
   
}

-(void)loadInitialViewController
{
    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    CLInitialViewController *initialVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"CLInitialViewController"];
    UINavigationController *navCtr = [[UINavigationController alloc] initWithRootViewController:initialVC];
    self.window.rootViewController = navCtr;

}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    
 // Call FBAppCall's handleOpenURL:sourceApplication to handle Facebook app responses
    
BOOL wasHandled = [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
    
return wasHandled;
    
}

#pragma mark Zoom Effect

-(void)ViewZoomInEffect:(UIView *)aView
{
    
    aView.alpha = 0.0f;
    aView.transform = CGAffineTransformMakeScale(0.1,0.1);
    [UIView animateWithDuration:0.3 animations:^{
        aView.transform = CGAffineTransformMakeScale(1.0,1.0);
        aView.alpha = 1.0f;
    }];
    
}

-(void)ViewZoomOutEffect:(UIView *)aView
{
    
    aView.alpha = 1.0f;
    aView.transform = CGAffineTransformMakeScale(1.0,1.0);
    [UIView animateWithDuration:0.3 animations:^{
        aView.transform = CGAffineTransformMakeScale(0.1,0.1);
        aView.alpha = 0.0f;
    }];
    
}

-(BOOL)isNetworkIsReachable
{
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus astatus = [reachability currentReachabilityStatus];
    BOOL status;
    if(astatus == NotReachable)
    {
        //No internet
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Connection Error" message:@"Please check your internet connection and try again later." delegate:nil cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        status =  NO;
        
    }
    else if (astatus == ReachableViaWiFi)
    {
        //WiFi
        status =  YES;
    }
    else if (astatus == ReachableViaWWAN)
    {
        status =  YES;
    }else
        status =  NO;
    return status;
}


#pragma mark Notification stuffs


- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)deviceToken
{
    NSLog(@"My token is: %@", deviceToken);
    
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"content---%@", token);
    
    [[NSUserDefaults standardUserDefaults]setObject:token forKey:@"deviceToken"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
}



#ifdef __IPHONE_8_0
- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
    //register to receive notifications
    [application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier forRemoteNotification:(NSDictionary *)userInfo completionHandler:(void(^)())completionHandler
{
    
    NSLog(@"%@",identifier);
    NSLog(@"%@",userInfo);
    //handle the actions
    if ([identifier isEqualToString:@"declineAction"]){
    }
    else if ([identifier isEqualToString:@"answerAction"]){
    }
}
#endif

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    [[NSUserDefaults standardUserDefaults]setObject:error.description forKey:@"deviceToken"];

    NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application
didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:1];
    
    // Show alert for push notifications recevied while the
    // app is running
    //    NSString *message = [[userInfo objectForKey:@"aps"]
    //                         objectForKey:@"alert"];
    //    UIAlertView *alert = [[UIAlertView alloc]
    //                          initWithTitle:@""
    //                          message:message
    //                          delegate:nil
    //                          cancelButtonTitle:@"OK"
    //                          otherButtonTitles:nil];
    //    [alert show];
    
    NSLog(@"Received notification: %@", userInfo);
    //[self addMessageFromRemoteNotification:userInfo];
    
    //    NSString* alertValue = [[userInfo valueForKey:@"aps"] valueForKey:@"badge"];
    //    NSLog(@"my message-- %@",alertValue);
    //    int badgeValue= [alertValue intValue];
    
    //  [[UIApplication sharedApplication] setApplicationIconBadgeNumber:badgeValue];
    
    UIApplicationState state=[application applicationState];
    
    if (state==UIApplicationStateActive) {
        NSLog(@"active");
        
    }
    else if (state==UIApplicationStateBackground)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"GotNotification"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        NSLog(@"run on background");
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UINavigationController *splashVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"Main_storyboard"];
        self.window.rootViewController = splashVC;
    }
    else if (state==UIApplicationStateInactive)
    {
        [[NSUserDefaults standardUserDefaults] setBool:YES  forKey:@"GotNotification"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        
    }
}

@end
