//
//  CLLeftMenuView.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 28/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CLLeftMenuViewDelegate <NSObject>

@optional
-(void)menuTappedATIndex:(NSIndexPath *)indexPath;
-(void)loadSettingsViewController;
@end

@interface CLLeftMenuView : UIView<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *userDetailView;
@property (weak, nonatomic) IBOutlet UIImageView *userProfileImageView;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UITableView *leftMenuTableView;
@property (nonatomic, strong) NSMutableDictionary *indexPathDict;


@property (nonatomic, weak) id<CLLeftMenuViewDelegate>delegate;
-(void)loadDetailsOfLeftMenuView;
@end
