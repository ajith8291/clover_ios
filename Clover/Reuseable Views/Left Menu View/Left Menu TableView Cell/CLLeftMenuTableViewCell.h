//
//  CLLeftMenuTableViewCell.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 28/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CLLeftMenuTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleTextLabel;

@end
