//
//  CLLeftMenuTableViewCell.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 28/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLLeftMenuTableViewCell.h"

@implementation CLLeftMenuTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
