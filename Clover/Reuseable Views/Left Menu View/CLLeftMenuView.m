//
//  CLLeftMenuView.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 28/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLLeftMenuView.h"
#import "CLConstants.h"
#import "CLUserManager.h"
#import "CLUser.h"
#import <UIImageView+AFNetworking.h>


@implementation CLLeftMenuView

-(void)awakeFromNib
{
    self.userProfileImageView.layer.cornerRadius = self.userProfileImageView.frame.size.width/2;
    self.leftMenuTableView.dataSource = self;
    self.leftMenuTableView.delegate = self;
    self.userProfileImageView.layer.masksToBounds = YES;
    
    UITapGestureRecognizer *profileTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(profileViewTapped)];
    [self.userDetailView addGestureRecognizer:profileTap];
    
    [self loadDetailsOfLeftMenuView];
    
    self.indexPathDict = [NSMutableDictionary dictionary];
    [self.indexPathDict setObject:[NSIndexPath indexPathForItem:0 inSection:0] forKey:@"previousIndexPath"];

}

-(id) initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    return self;
}
-(void)loadDetailsOfLeftMenuView
{
    CLUserManager *userManager = [CLUserManager sharedManager];

    self.userNameLabel.text = userManager.user.userName;
    [self.userProfileImageView setImageWithURL:[NSURL URLWithString:userManager.user.profileURL] placeholderImage:nil];
    
//    NSString *imgUrl = [userManager.user.profileURL stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%d",90]];
//    imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%d",90]];
//    
//    UIImage *placeholderImg = [UIImage imageNamed:@"userProfileNew"];
//    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
//    
//    __weak UIImageView *weakImg = self.userProfileImageView;
//    
//    [self.userProfileImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
//     {
//         
//         CGSize newSize = CGSizeMake(46.0f, 46.0f);
//         
//         UIGraphicsBeginImageContext(newSize);
//         [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
//         UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
//         UIGraphicsEndImageContext();
//         
//         
//         dispatch_async(dispatch_get_main_queue(), ^{
//             weakImg.image = newImage;
//             weakImg.alpha = 0.0;
//             
//             [UIView animateWithDuration:1.0 animations:^{
//                 weakImg.alpha = 1.0;
//             }];
//         });
//         
//         
//     } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
//         
//
//     }];

    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
#pragma mark UITableViewDelegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 9;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    }
    cell.textLabel.textColor = [UIColor colorWithRed:67.0/255.0 green:67.0/255.0 blue:67.0/255.0 alpha:1.0];
    cell.textLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:17.0];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;

    NSInteger row = [[NSUserDefaults standardUserDefaults] integerForKey:@"previousIndexPath"];
    NSIndexPath *previousIndexPath = [NSIndexPath indexPathForRow:row inSection:0];
    if (previousIndexPath.row == indexPath.row)
    {
        cell.backgroundColor = CL_BLUE_COLOR;
        cell.textLabel.textColor = [UIColor whiteColor];
        switch (indexPath.row)
        {
            case 0:
                cell.imageView.image = [UIImage imageNamed:@"home-new-white"];
                cell.textLabel.text = @"Home";
                break;
                
            case 1:
                cell.imageView.image = [UIImage imageNamed:@"recipes-new-white"];
                cell.textLabel.text = @"Recipes";
                break;
            case 2:
                cell.imageView.image = [UIImage imageNamed:@"mealplannernew-white"];
                cell.textLabel.text = @"Meal Planner";
                break;
            case 3:
                cell.imageView.image = [UIImage imageNamed:@"shoppinglist-new-white"];
                cell.textLabel.text = @"Shopping List";
                break;
            case 4:
                cell.imageView.image = [UIImage imageNamed:@"favorites-new-white"];
                cell.textLabel.text = @"My Favourites";
                break;
            case 5:
                cell.imageView.image = [UIImage imageNamed:@"cloverclub-new-white"];
                cell.textLabel.text = @"Clover Club";
                break;
            case 6:
                cell.imageView.image = [UIImage imageNamed:@"settings-new-white"];
                cell.textLabel.text = @"Profile Settings";
                break;
            case 7:
                cell.imageView.image = [UIImage imageNamed:@"help-new-white"];
                cell.textLabel.text = @"Help";
                break;
            case 8:
                cell.imageView.image = [UIImage imageNamed:@"logout-new-white"];
                cell.textLabel.text = @"Log Out";
                break;
                
            default:
                break;
        }
        
    }
    else
    {
        cell.backgroundColor = [UIColor clearColor];
        cell.textLabel.textColor = [UIColor colorWithRed:67.0/255.0 green:67.0/255.0 blue:67.0/255.0 alpha:1.0];
        switch (indexPath.row)
        {
            case 0:
                cell.imageView.image = [UIImage imageNamed:@"home-new-grey"];
                cell.textLabel.text = @"Home";
                break;
                
            case 1:
                cell.imageView.image = [UIImage imageNamed:@"recipes-new-grey"];
                cell.textLabel.text = @"Recipes";
                break;
            case 2:
                cell.imageView.image = [UIImage imageNamed:@"mealplanner-new-grey"];
                cell.textLabel.text = @"Meal Planner";  
                break;
            case 3:
                cell.imageView.image = [UIImage imageNamed:@"shoppinglist-new-grey"];
                cell.textLabel.text = @"Shopping List";
                break;
            case 4:
                cell.imageView.image = [UIImage imageNamed:@"favorites-new-grey"];
                cell.textLabel.text = @"My Favourites";
                break;
            case 5:
                cell.imageView.image = [UIImage imageNamed:@"cloverclub-new-grey"];
                cell.textLabel.text = @"Clover Club";
                break;
            case 6:
                cell.imageView.image = [UIImage imageNamed:@"settings-new-grey"];
                cell.textLabel.text = @"Profile Settings";
                break;
            case 7:
                cell.imageView.image = [UIImage imageNamed:@"help-new-grey"];
                cell.textLabel.text = @"Help";
                break;
            case 8:
                cell.imageView.image = [UIImage imageNamed:@"logout-new-grey"];
                cell.textLabel.text = @"Log Out";
                break;
                
            default:
                break;
        }

    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [[NSUserDefaults standardUserDefaults] setInteger:indexPath.row forKey:@"previousIndexPath"];
//    if ([self.indexPathDict objectForKey:@"previousIndexPath"] == nil)
//    {
//        [self.indexPathDict setObject:indexPath forKey:@"previousIndexPath"];
//    }
//    else
//    {
//        NSIndexPath *previousIndex = [self.indexPathDict objectForKey:@"previousIndexPath"];
//        
//        UITableViewCell *previousCell = [tableView cellForRowAtIndexPath:previousIndex];
//        previousCell.backgroundColor = [UIColor clearColor];
//        previousCell.contentView.backgroundColor = [UIColor clearColor];
//        previousCell.textLabel.textColor = [UIColor colorWithRed:67.0/255.0 green:67.0/255.0 blue:67.0/255.0 alpha:1.0];
//        [self.indexPathDict setObject:indexPath forKey:@"previousIndexPath"];
//    }
//    
//    // Make the current cell red
//    UITableViewCell *tableCell = [tableView cellForRowAtIndexPath:indexPath];
//    tableCell.backgroundColor = CL_BLUE_COLOR;
//    tableCell.textLabel.textColor = [UIColor whiteColor];

    [self.leftMenuTableView reloadData];
    [self.delegate menuTappedATIndex:indexPath];
}


-(void)profileViewTapped
{
    [[NSUserDefaults standardUserDefaults] setInteger:6 forKey:@"previousIndexPath"];
    [self.leftMenuTableView reloadData];
    [self.delegate loadSettingsViewController];
}


@end
