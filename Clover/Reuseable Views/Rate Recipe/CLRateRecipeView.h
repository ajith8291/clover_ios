//
//  CLRateRecipeView.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 08/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLRecipe.h"

@protocol  CLRateRecipeViewDelegate<NSObject>

@optional

-(void)postButtonTappedWithRating:(NSString *)rate;
-(void)cancelButtonTapped;

@end

@interface CLRateRecipeView : UIView
@property (nonatomic,strong) CLRecipe *recipe;
@property (weak, nonatomic) IBOutlet UIButton *postButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UIButton *rateButoon1;
@property (weak, nonatomic) IBOutlet UIButton *rateButoon2;
@property (weak, nonatomic) IBOutlet UIButton *rateButoon3;
@property (weak, nonatomic) IBOutlet UIButton *rateButoon4;
@property (weak, nonatomic) IBOutlet UIButton *rateButton5;

@property (nonatomic, strong) NSString *ratingStarStr;
@property (nonatomic, weak) id<CLRateRecipeViewDelegate>delegate;

-(void)configureRatingStarWithUserRate :(CLRecipe *)recipe;
@end
