//
//  CLRateRecipeView.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 08/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLRateRecipeView.h"
#import "CLConstants.h"

@implementation CLRateRecipeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    self.layer.cornerRadius = 5.0;
    self.clipsToBounds = YES;
    self.postButton.titleLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:15.0];
    self.cancelButton.titleLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:15.0];
    self.ratingStarStr = @"0";

}

-(void)configureRatingStarWithUserRate :(CLRecipe *)recipe
{
    self.recipe = recipe;
    if ([self.recipe.recipeUserRating isEqualToString:@"0"]) {
        [self.rateButoon1 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
        [self.rateButoon2 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
        [self.rateButoon3 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
        [self.rateButoon4 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
        [self.rateButton5 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
    }
    else if ([self.recipe.recipeUserRating isEqualToString:@"1"]) {
        [self.rateButoon1 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
        [self.rateButoon2 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
        [self.rateButoon3 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
        [self.rateButoon4 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
        [self.rateButton5 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
    }
    else if ([self.recipe.recipeUserRating isEqualToString:@"2"]) {
        [self.rateButoon1 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
        [self.rateButoon2 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
        [self.rateButoon3 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
        [self.rateButoon4 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
        [self.rateButton5 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
    }
    else if ([self.recipe.recipeUserRating isEqualToString:@"3"]) {
        [self.rateButoon1 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
        [self.rateButoon2 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
        [self.rateButoon3 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
        [self.rateButoon4 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
        [self.rateButton5 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];

    }
    else if ([self.recipe.recipeUserRating isEqualToString:@"4"]) {
        [self.rateButoon1 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
        [self.rateButoon2 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
        [self.rateButoon3 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
        [self.rateButoon4 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
        [self.rateButton5 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
    }
    else if ([self.recipe.recipeUserRating isEqualToString:@"5"]) {
        [self.rateButoon1 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
        [self.rateButoon2 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
        [self.rateButoon3 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
        [self.rateButoon4 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
        [self.rateButton5 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
    }
    
}

- (IBAction)postRecipeRateButtonAction:(id)sender
{
    [self.delegate postButtonTappedWithRating:self.ratingStarStr];
}
- (IBAction)postRecipeRateCancelButton:(id)sender
{
    [self.delegate cancelButtonTapped];
}

#pragma UIButton/Tap Action

- (IBAction)rateButton1ActionMethod:(id)sender
{
    self.ratingStarStr = @"1";//StarSilver.png,StarGold.png
    [self.rateButoon1 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
    [self.rateButoon2 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
    [self.rateButoon3 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
    [self.rateButoon4 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
    [self.rateButton5 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
}

- (IBAction)rateButton2ActionMethod:(id)sender
{
    self.ratingStarStr = @"2";
    [self.rateButoon1 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
    [self.rateButoon2 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
    [self.rateButoon3 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
    [self.rateButoon4 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
    [self.rateButton5 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
}

- (IBAction)rateButton3ActionMethod:(id)sender
{
    self.ratingStarStr = @"3";
    [self.rateButoon1 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
    [self.rateButoon2 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
    [self.rateButoon3 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
    [self.rateButoon4 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
    [self.rateButton5 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];

}

- (IBAction)rateButton4ActionMethod:(id)sender
{
    self.ratingStarStr = @"4";
    [self.rateButoon1 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
    [self.rateButoon2 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
    [self.rateButoon3 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
    [self.rateButoon4 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
    [self.rateButton5 setImage:[UIImage imageNamed:@"RateBigStarUnelected"] forState:UIControlStateNormal];
}

- (IBAction)rateButton5ActionMethod:(id)sender
{
    self.ratingStarStr = @"5";
    [self.rateButoon1 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
    [self.rateButoon2 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
    [self.rateButoon3 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
    [self.rateButoon4 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
    [self.rateButton5 setImage:[UIImage imageNamed:@"RateBigStarSelected"] forState:UIControlStateNormal];
}

@end
