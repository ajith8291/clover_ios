//
//  CLRecipeDetailsCollectionViewCell.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 10/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLRecipeDetailsCollectionViewCell.h"
#import <UIImageView+AFNetworking.h>
#import "NSString+DataValidator.h"
#import "UIImage+colors.h"

@implementation CLRecipeDetailsCollectionViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
    
    self.favImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *favtap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(favImageViewTapped)];
    [self.favImageView addGestureRecognizer:favtap];

    self.removeFromListImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *listTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeFromListImageViewTapped)];
    [self.removeFromListImageView addGestureRecognizer:listTap];

    
    self.bottomView.layer.cornerRadius = 12.0;
    self.bottomView.clipsToBounds = YES;

    self.reviewCountLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *reviewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showReviewViewController)];
    [self.reviewCountLabel addGestureRecognizer:reviewTap];

    
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 225);
    if (self)
    {
        // Initialization code
        NSArray *arrayOfViews = [[NSBundle mainBundle] loadNibNamed:@"CLRecipeDetailsCollectionViewCell" owner:self options:nil];
        
        if ([arrayOfViews count] < 1) { return nil; }
        
        if (![[arrayOfViews objectAtIndex:0] isKindOfClass:[UICollectionViewCell class]]) { return nil; }
        
        self = [arrayOfViews objectAtIndex:0];
    }
    return self;
}

-(void)configureListCellWithRecipeDetails :(CLRecipe *)recipe
{
    
    self.selectedRecipe = recipe;
    if ([self.selectedRecipe.recipeVideoUrl isEmptyString])
    {
        self.topPlayButton.hidden = YES;
        self.shareButtonAction.hidden = YES;
    }
    else
    {
        self.topPlayButton.hidden = NO;
        self.shareButtonAction.hidden = NO;
    }
    
    if ([recipe.recipeRating isEqualToString:@"5"])
    {
        [self.rateStarImagwView1 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView2 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView3 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView4 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImageView5 setImage:[UIImage imageNamed:@"starselected"]];
    }
    else if ([recipe.recipeRating isEqualToString:@"4"])
    {
        [self.rateStarImagwView1 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView2 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView3 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView4 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImageView5 setImage:[UIImage imageNamed:@"starinactive"]];
    }
    else if ([recipe.recipeRating isEqualToString:@"3"])
    {
        [self.rateStarImagwView1 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView2 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView3 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView4 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImageView5 setImage:[UIImage imageNamed:@"starinactive"]];
    }
    else if ([recipe.recipeRating isEqualToString:@"2"])
    {
        [self.rateStarImagwView1 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView2 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView3 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImagwView4 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImageView5 setImage:[UIImage imageNamed:@"starinactive"]];
    }
    else if ([recipe.recipeRating isEqualToString:@"1"])
    {
        [self.rateStarImagwView1 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView2 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImagwView3 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImagwView4 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImageView5 setImage:[UIImage imageNamed:@"starinactive"]];
    }
    else if ([recipe.recipeRating isEqualToString:@"0"])
    {
        [self.rateStarImagwView1 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImagwView2 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImagwView3 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImagwView4 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImageView5 setImage:[UIImage imageNamed:@"starinactive"]];
    }
    
    self.recipeNameLabel.text = [NSString stringWithFormat:@"%@",[recipe.recipeTitle capitalizedString]];
    self.recipeTypeLabel.text = recipe.recipeType;
    self.cookingTimeLabel.text = recipe.recipePreparationTime;
    self.servingPersonLabel.text = recipe.recipeServingPersons;
    
    recipe.recipePictureCount = [recipe.recipePictureCount stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    NSString *count = ([recipe.recipePictureCount isEqualToString:@""])?@"0":recipe.recipePictureCount;
    if ([count isEqualToString:@"0"])
    {
        self.reviewCountLabel.hidden = YES;
    }
    else
    {
        self.reviewCountLabel.hidden = NO;
    }
    NSMutableAttributedString *reviewCountAtrStr = [[NSMutableAttributedString alloc] initWithString:count];
    NSMutableAttributedString *reviewAtrStr = [[NSMutableAttributedString alloc] initWithString:([count isEqualToString:@"0"]||[count isEqualToString:@"1"])?@" review":@" reviews"];
    [reviewCountAtrStr appendAttributedString:reviewAtrStr];
    self.reviewCountLabel.attributedText = reviewCountAtrStr;
    
    if ([recipe.recipeImage isEqualToString:@"null"])
    {
        [self.recipeImageView setImage:[UIImage imageNamed:@"no-image"]];
    }
    else
    {
        NSString *imgUrl = [recipe.recipeImage stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%d",600]];
        imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%d",370]];
        
        UIImage *placeholderImg = [UIImage imageWithColor:[UIColor lightGrayColor]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
        
        __weak UIImageView *weakImg = self.recipeImageView;
        
        [self.recipeImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 weakImg.image = image;
                 weakImg.alpha = 0.0;
                 
                 [UIView animateWithDuration:1.0 animations:^{
                     weakImg.alpha = 1.0;
                 }];
             });
             
             
         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
             
             
         }];
    }
    
    if (self.isFromShoppingList) {
        self.reviewCountLabel.hidden = YES;
        self.topPlayButton.hidden = YES;
        self.recipeNameLabel.translatesAutoresizingMaskIntoConstraints = YES;
        self.recipeNameLabel.frame = CGRectMake(self.recipeNameLabel.frame.origin.x, 170, self.frame.size.width-34, self.recipeNameLabel.frame.size.height);
    }
}

#pragma mark UIButton/Tap Actions

-(void)showReviewViewController
{
    [self.delegate loadReviewViewControllerWithSelectedRecipe:self.selectedRecipe];
}

- (IBAction)topPlayButtonAction:(id)sender
{
    [self.delegate playButtonTappedWithRecipe:self.selectedRecipe];
}
- (IBAction)shareButtonAction:(id)sender
{
    [self.delegate shareButtonTappedWithVideoUrl:self.selectedRecipe.recipeVideoUrl];
}

-(void)favImageViewTapped
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Would you like to remove this recipe from your favourites" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alertView.tag = 101;
    [alertView show];
}

-(void)removeFromListImageViewTapped
{
    UIAlertView *alertView = [[UIAlertView alloc] initWithTitle:nil message:@"Would you like to remove this recipe from your Shopping list" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alertView.tag = 102;
    [alertView show];

}


#pragma mark UIAlertViewDelegate Methods

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            break;
        case 1:
            if (alertView.tag == 101)
            {
                [self.delegate removeRecipeFromFavouriteList:self.selectedRecipe];
            }
            else
            {
                [self.delegate removeRecipeFromShoppingList:self.selectedRecipe];
            }
            break;
        default:
            break;
    }
}

@end
