//
//  CLRecipeDetailsCollectionViewCell.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 10/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "CLRecipeView.h"
#import "CLRecipe.h"

@protocol CLRecipeDetailsCollectionViewCellDelegate<NSObject>

@optional

-(void)loadReviewViewControllerWithSelectedRecipe:(CLRecipe *)recipe;
-(void)removeRecipeFromFavouriteList:(CLRecipe *)favRecipe;
-(void)playButtonTappedWithRecipe:(CLRecipe *)recipe;
-(void)removeRecipeFromShoppingList:(CLRecipe *)shoppingListRecipe;
  
-(void)shareButtonTappedWithVideoUrl :(NSString *)videoUrl;

@end

@interface CLRecipeDetailsCollectionViewCell : UICollectionViewCell<CLRecipeViewDelegate>


-(void)configureListCellWithRecipeDetails :(CLRecipe *)recipe;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property (weak, nonatomic) IBOutlet UIView *recipeContentView;
@property (weak, nonatomic) IBOutlet UIImageView *recipeImageView;
@property (weak, nonatomic) IBOutlet UILabel *recipeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewCountLabel;

// rating stars property
@property (weak, nonatomic) IBOutlet UIImageView *rateStarImagwView1;
@property (weak, nonatomic) IBOutlet UIImageView *rateStarImagwView2;
@property (weak, nonatomic) IBOutlet UIImageView *rateStarImagwView3;
@property (weak, nonatomic) IBOutlet UIImageView *rateStarImagwView4;
@property (weak, nonatomic) IBOutlet UIImageView *rateStarImageView5;

// Bottom View property
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *recipeTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *cookingTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *servingPersonLabel;

@property (nonatomic, strong) CLRecipe *selectedRecipe;

@property (weak, nonatomic) IBOutlet UIImageView *favImageView;
@property (weak, nonatomic) IBOutlet UIImageView *removeFromListImageView;

@property (weak, nonatomic) IBOutlet UIButton *topPlayButton;
@property (weak, nonatomic) IBOutlet UIView *starView;
@property (weak, nonatomic) IBOutlet UIButton *shareButtonAction;

@property (nonatomic, assign) BOOL isFromShoppingList;

@property (nonatomic, weak) id<CLRecipeDetailsCollectionViewCellDelegate>delegate;

@end
