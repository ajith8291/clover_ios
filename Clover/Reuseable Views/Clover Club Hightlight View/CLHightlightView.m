//
//  CLHightlightView.m
//  Clover
//
//  Created by Ajith Kumar on 28/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLHightlightView.h"
#import <UIImageView+AFNetworking.h>
#import "UIImage+colors.h"
#import "NSString+DataValidator.h"
@implementation CLHightlightView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)configureHightlightViewWith:(CLHighlights *)highlightObj
{
    self.selectedHighlightObj = highlightObj;
    self.nameLabel.text = highlightObj.highlightPostTitle;
    self.highlight = highlightObj;
    if ([highlightObj.highlightContenttype isEqualToString:@"video"])
    {
        self.playButton.hidden = NO;
        self.shareButton.hidden = NO;
    }
    else
    {
        self.playButton.hidden = YES;
        self.shareButton.hidden = YES;
    }
    if ([[highlightObj.highlightFeaturedImage objectForKey:@"image"] isEmptyString] || highlightObj.highlightFeaturedImage == nil)
    {
        [self.highLightImageView setImage:[UIImage imageNamed:@"no-image"]];
    }
    else
    {
        NSString *imgUrl = [[highlightObj.highlightFeaturedImage objectForKey:@"image"]stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%f",(self.frame.size.width-20)*2]];
        imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%d",340]];
        
        UIImage *placeholderImg = [UIImage imageWithColor:[UIColor lightGrayColor]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
        
        __weak UIImageView *weakImg = self.highLightImageView;
        
        [self.highLightImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             
             CGSize newSize = CGSizeMake(self.frame.size.width-20, 170.0f);
             
             UIGraphicsBeginImageContext(newSize);
             [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
             UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
             UIGraphicsEndImageContext();
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 weakImg.image = newImage;
                 weakImg.alpha = 0.0;
                 
                 [UIView animateWithDuration:1.0 animations:^{
                     weakImg.alpha = 1.0;
                 }];
             });
             
         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
             
             
         }];
    }
}
- (IBAction)shareButtonAction:(id)sender
{
    [self.delegate shareButtonTappedWithVideoUrl:self.highlight.highlightVideoUrl];
}

- (IBAction)playButtonAction:(id)sender
{
    [self.delegate playButtonTappedWith:self.selectedHighlightObj];
}
@end
