//
//  CLHightlightView.h
//  Clover
//
//  Created by Ajith Kumar on 28/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLHighlights.h"

@protocol CLHightlightViewDelegate <NSObject>

@optional

-(void)playButtonTappedWith:(CLHighlights *)highlightObj;
-(void)shareButtonTappedWithVideoUrl :(NSString *)videoUrl;

@end

@interface CLHightlightView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *highLightImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property (weak, nonatomic) IBOutlet UIButton *shareButton;
@property (nonatomic, strong) CLHighlights *selectedHighlightObj;
-(void)configureHightlightViewWith:(CLHighlights *)highlightObj;

@property (nonatomic, weak) id<CLHightlightViewDelegate>delegate;

@property (nonatomic, strong) CLHighlights *highlight;
@end
