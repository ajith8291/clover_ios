//
//  CLRecipeView.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 09/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLRecipe.h"

@protocol CLRecipeViewDelegate<NSObject>

@optional

-(void)loadReviewViewControllerWithSelectedRecipe:(CLRecipe *)recipe;
-(void)playButtonTappedWithRecipe:(CLRecipe *)selectedRecipe;
-(void)shareButtonTappedWithVideoUrl :(NSString *)videoUrl;
@end

@interface CLRecipeView : UIView

// content View property
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

@property (weak, nonatomic) IBOutlet UIView *contentView;
@property (weak, nonatomic) IBOutlet UIImageView *recipeImageView;
@property (weak, nonatomic) IBOutlet UILabel *recipeNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *reviewCountLabel;

// rating stars property
@property (weak, nonatomic) IBOutlet UIImageView *rateStarImagwView1;
@property (weak, nonatomic) IBOutlet UIImageView *rateStarImagwView2;
@property (weak, nonatomic) IBOutlet UIImageView *rateStarImagwView3;
@property (weak, nonatomic) IBOutlet UIImageView *rateStarImagwView4;
@property (weak, nonatomic) IBOutlet UIImageView *rateStarImageView5;

// Bottom View property
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *recipeTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *cookingTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *servingPersonLabel;
@property (weak, nonatomic) IBOutlet UIView *starView;
@property (weak, nonatomic) IBOutlet UIButton *centrePlayButton;
@property (weak, nonatomic) IBOutlet UIButton *topPlayButton;
 
@property (weak, nonatomic) IBOutlet UIButton *shareButton;
-(void)configureRecipeViewWithRecipe:(CLRecipe *)recipe;
@property (nonatomic, strong) CLRecipe *selectedRecipe;
@property (nonatomic, assign) BOOL isFromHome;

@property (nonatomic, weak) id<CLRecipeViewDelegate>delegate;

@end
