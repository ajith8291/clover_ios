//
//  CLRecipeView.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 09/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLRecipeView.h"
#import "UIImage+colors.h"
#import <UIImageView+AFNetworking.h>
#import "NSString+DataValidator.h"

@implementation CLRecipeView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.contentView.layer.cornerRadius = 2.0;
    self.contentView.clipsToBounds = YES;
    
    self.bottomView.layer.cornerRadius = 12.0;
    self.bottomView.clipsToBounds = YES;
    
    self.reviewCountLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *reviewTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showReviewViewController)];
    [self.reviewCountLabel addGestureRecognizer:reviewTap];
}

-(void)configureRecipeViewWithRecipe:(CLRecipe *)recipe
{
    self.selectedRecipe = recipe;
    if ([self.selectedRecipe.recipeVideoUrl isEmptyString])
    {
        self.centrePlayButton.hidden = YES;
        self.topPlayButton.hidden = YES;
        self.shareButton.hidden = YES;
    }
    else
    {
        if (self.isFromHome)
        {
            self.centrePlayButton.hidden = YES;
            self.topPlayButton.hidden = NO;
        }
        else
        {
            self.centrePlayButton.hidden = NO;
            self.topPlayButton.hidden = YES;
        }
        self.shareButton.hidden = NO;
    }
    

    if ([recipe.recipeRating isEqualToString:@"5"])
    {
        [self.rateStarImagwView1 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView2 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView3 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView4 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImageView5 setImage:[UIImage imageNamed:@"starselected"]];
    }
    else if ([recipe.recipeRating isEqualToString:@"4"])
    {
        [self.rateStarImagwView1 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView2 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView3 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView4 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImageView5 setImage:[UIImage imageNamed:@"starinactive"]];
    }
    else if ([recipe.recipeRating isEqualToString:@"3"])
    {
        [self.rateStarImagwView1 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView2 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView3 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView4 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImageView5 setImage:[UIImage imageNamed:@"starinactive"]];
    }
    else if ([recipe.recipeRating isEqualToString:@"2"])
    {
        [self.rateStarImagwView1 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView2 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView3 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImagwView4 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImageView5 setImage:[UIImage imageNamed:@"starinactive"]];
    }
    else if ([recipe.recipeRating isEqualToString:@"1"])
    {
        [self.rateStarImagwView1 setImage:[UIImage imageNamed:@"starselected"]];
        [self.rateStarImagwView2 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImagwView3 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImagwView4 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImageView5 setImage:[UIImage imageNamed:@"starinactive"]];
    }
    else if ([recipe.recipeRating isEqualToString:@"0"])
    {
        [self.rateStarImagwView1 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImagwView2 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImagwView3 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImagwView4 setImage:[UIImage imageNamed:@"starinactive"]];
        [self.rateStarImageView5 setImage:[UIImage imageNamed:@"starinactive"]];
    }

    
    self.recipeNameLabel.text = [NSString stringWithFormat:@"%@",[recipe.recipeTitle capitalizedString]];
    self.recipeTypeLabel.text = recipe.recipeType;
    self.cookingTimeLabel.text = recipe.recipePreparationTime;
    self.servingPersonLabel.text = recipe.recipeServingPersons;
    
    recipe.recipePictureCount = [recipe.recipePictureCount stringByReplacingOccurrencesOfString:@"(null)" withString:@""];
    NSString *count = ([recipe.recipePictureCount isEqualToString:@""])?@"0":recipe.recipePictureCount;
    if ([count isEqualToString:@"0"])
    {
        self.reviewCountLabel.hidden = YES;
    }
    NSMutableAttributedString *reviewCountAtrStr = [[NSMutableAttributedString alloc] initWithString:count];
    NSMutableAttributedString *reviewAtrStr = [[NSMutableAttributedString alloc] initWithString:([count isEqualToString:@"0"]||[count isEqualToString:@"1"])?@" review":@" reviews"];
    [reviewCountAtrStr appendAttributedString:reviewAtrStr];
    self.reviewCountLabel.attributedText = reviewCountAtrStr;
    
    if ([recipe.recipeImage isEqualToString:@"null"])
    {
        [self.recipeImageView setImage:[UIImage imageNamed:@"no-image"]];
    }
    else
    {
        NSString *imgUrl = [recipe.recipeImage stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%d",600]];
        imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%d",370]];
        
        UIImage *placeholderImg = [UIImage imageWithColor:[UIColor lightGrayColor]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
        
        __weak UIImageView *weakImg = self.recipeImageView;
        
        [self.recipeImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 weakImg.image = image;
                 weakImg.alpha = 0.0;
                 
                 [UIView animateWithDuration:2.5 animations:^{
                     weakImg.alpha = 1.0;
                 }];
             });
             
             
         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
             
             
         }];
    }
    
    self.centrePlayButton.translatesAutoresizingMaskIntoConstraints = YES;
    self.centrePlayButton.frame = CGRectMake(self.frame.size.width/2-75/2, 66, 75, 75);
}

#pragma mark UIButton/Tap Actions

-(void)showReviewViewController
{
    [self.delegate loadReviewViewControllerWithSelectedRecipe:self.selectedRecipe];
}

- (IBAction)centrePlayButtonAction:(id)sender
{
    [self.delegate playButtonTappedWithRecipe:self.selectedRecipe];
}

- (IBAction)topPlayButtonAction:(id)sender
{
    [self.delegate playButtonTappedWithRecipe:self.selectedRecipe];
}

- (IBAction)shareButtonAction:(id)sender
{
    [self.delegate shareButtonTappedWithVideoUrl:self.selectedRecipe.recipeVideoUrl];
}
@end
