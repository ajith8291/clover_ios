//
//  CLRecipeGridView.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 09/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLRecipeGridView.h"
#import <UIImageView+AFNetworking.h>
#import "NSString+DataValidator.h"
#import "UIImage+colors.h"

@implementation CLRecipeGridView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    self.plusButton.layer.cornerRadius = self.plusButton.frame.size.width/2;
    self.plusButton.clipsToBounds = YES;
}

-(void)configureRecipeDetailsWithRecipe:(CLRecipe *)recipe
{
    self.selectedRecipe = recipe;

    NSString *cookingStr = self.selectedRecipe.recipeCookingTime;
//    if (self.selectedRecipe.recipeCookingTime.length >6)
//    {
//        cookingStr= [self.selectedRecipe.recipeCookingTime substringFromIndex:[self.selectedRecipe.recipeCookingTime length]-5];
//    }
//    else
//    {
//        cookingStr = self.selectedRecipe.recipeCookingTime;
//    }

    if ([self.selectedRecipe.recipeVideoUrl isEmptyString])
    {
        self.playButton.hidden = YES;
    }
    else
    {
        self.playButton.hidden = NO;
    }
    self.recipeNameLabel.text = recipe.recipeTitle;
    self.reciprTypeLabel.text = recipe.recipeType;
    self.cookingTimeLabel.text = cookingStr;
    self.servePersonLabel.text = recipe.recipeServingPersons;
    
    if ([recipe.recipeImage isEqualToString:@"null"])
    {
        [self.recipeImageView setImage:[UIImage imageNamed:@"no-image"]];
    }
    else
    {
        NSString *imgUrl = [recipe.recipeImage stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%d",640]];
        imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%d",376]];
        
        UIImage *placeholderImg = [UIImage imageWithColor:[UIColor lightGrayColor]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
        
        __weak UIImageView *weakImg = self.recipeImageView;
        
        [self.recipeImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                weakImg.image = image;
                weakImg.alpha = 0.0;
                
                [UIView animateWithDuration:0.4 animations:^{
                    weakImg.alpha = 1.0;
                }];
            });
            
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            
            
        }];
    }

}

-(void)configureRecipeDetailsWithRecipe:(CLRecipe *)recipe WithBool:(BOOL)flag
{
    
    self.plusButton.hidden = flag;
    self.selectedRecipe = recipe;
    NSString *cookingStr = self.selectedRecipe.recipeCookingTime;
//    if (self.selectedRecipe.recipeCookingTime.length >6)
//    {
//        cookingStr= [self.selectedRecipe.recipeCookingTime substringFromIndex:[self.selectedRecipe.recipeCookingTime length]-5];
//    }
//    else
//    {
//        cookingStr = self.selectedRecipe.recipeCookingTime;
//    }
    if ([self.selectedRecipe.recipeVideoUrl isEmptyString])
    {
        self.playButton.hidden = YES;
    }
    else
    {
        self.playButton.hidden = YES;
    }
    self.recipeNameLabel.text = recipe.recipeTitle;
    self.reciprTypeLabel.text = recipe.recipeType;
    self.cookingTimeLabel.text = cookingStr;
    self.servePersonLabel.text = recipe.recipeServingPersons;
    
    if ([recipe.recipeImage isEqualToString:@"null"])
    {
        [self.recipeImageView setImage:[UIImage imageNamed:@"no-image"]];
    }
    else
    {
        NSString *imgUrl = [recipe.recipeImage stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%d",640]];
        imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%d",376]];
        
        UIImage *placeholderImg = [UIImage imageWithColor:[UIColor lightGrayColor]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
        
        __weak UIImageView *weakImg = self.recipeImageView;
        
        [self.recipeImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image) {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                weakImg.image = image;
                weakImg.alpha = 0.0;
                
                [UIView animateWithDuration:0.4 animations:^{
                    weakImg.alpha = 1.0;
                }];
            });
            
            
        } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
            
            
        }];
    }
    
}

- (IBAction)playButtonAction:(id)sender
{
    [self.delegate playButtonTappedWithRecipe:self.selectedRecipe];
}

- (IBAction)plusButtonAction:(id)sender
{
    [self.delegate removeRecipeFromTheList:self.selectedRecipe];
}

@end
