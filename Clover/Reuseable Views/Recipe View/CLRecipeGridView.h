//
//  CLRecipeGridView.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 09/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CLRecipe.h"

@protocol CLRecipeGridViewDelegate <NSObject>

@optional

-(void)playButtonTappedWithRecipe:(CLRecipe *)recipe;
- (void)removeRecipeFromTheList :(CLRecipe *)recipe;

@end

@interface CLRecipeGridView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *recipeImageView;
@property (weak, nonatomic) IBOutlet UILabel *recipeNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *plusButton;

@property (weak, nonatomic) IBOutlet UILabel *reciprTypeLabel;

@property (weak, nonatomic) IBOutlet UILabel *servePersonLabel;
@property (weak, nonatomic) IBOutlet UILabel *cookingTimeLabel;
@property (weak, nonatomic) IBOutlet UIButton *playButton;

@property(nonatomic, strong) CLRecipe *selectedRecipe;

-(void)configureRecipeDetailsWithRecipe:(CLRecipe *)recipe;
-(void)configureRecipeDetailsWithRecipe:(CLRecipe *)recipe WithBool:(BOOL)flag;

@property (nonatomic, weak) id<CLRecipeGridViewDelegate>delegate;
@end
