//
//  CLIngredientsFilterView.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 16/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLIngredientsFilterView.h"
#import "CLRecipeFilterTableViewCell.h"
#import "CLConstants.h"
#import "NSString+DataValidator.h"

@interface CLIngredientsFilterView()<UITextFieldDelegate>

@property (nonatomic, assign) BOOL isSearchSelected;
@property (nonatomic, strong) NSMutableArray *ingredientsSearchResultArray;
@property (nonatomic, strong) NSMutableArray *ingredientsNameArray;

@end
@implementation CLIngredientsFilterView

-(void)awakeFromNib
{
    self.ingredientsFilterArray = [NSMutableArray new];
    self.ingredientsNameArray = [NSMutableArray new];
    self.ingredientsSearchResultArray = [NSMutableArray new];
    self.searchTextField.delegate = self;
    self.layer.cornerRadius = 5.0;
    self.clipsToBounds = YES;
//    self.layer.borderWidth = 1.0;
//    self.layer.borderColor = [CL_BLUE_COLOR CGColor];
    self.ingredientFilterTableView.delegate = self;
    self.ingredientFilterTableView.dataSource = self;
    UINib *nib = [UINib nibWithNibName:@"CLRecipeFilterTableViewCell" bundle:nil];
    [self.ingredientFilterTableView registerNib:nib forCellReuseIdentifier:@"CLRecipeFilterTableViewCell"];
    
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)configureViewWithArray :(NSMutableArray *)array;
{
    self.ingredientsArray = array;
    [self.ingredientFilterTableView reloadData];
}

#pragma mark UITableViewDelegate Method

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return (self.isSearchSelected) ? self.ingredientsSearchResultArray.count : self.ingredientsArray.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL isThereInArrayList = NO;

    NSMutableArray *array = (self.isSearchSelected) ?self.ingredientsSearchResultArray  : self.ingredientsArray;

    CLRecipeFilterTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"CLRecipeFilterTableViewCell"];
    cell.categoryNameLabel.text = [[array objectAtIndex:indexPath.row] objectForKey:@"name"];
    cell.idLabel.text = [[array objectAtIndex:indexPath.row] objectForKey:@"id"];
    
    for (int i = 0; i<self.ingredientsFilterArray.count; i++)
    {
        NSString *name = [[self.ingredientsFilterArray objectAtIndex:i] objectForKey:@"name"];
        if ([name isEqualToString:cell.categoryNameLabel.text])
        {
            [cell.tickImageView setImage:[UIImage imageNamed:@"tickblue"]];
            isThereInArrayList = YES;
        }
    }
    
    if (!isThereInArrayList)
    {
        [cell.tickImageView setImage:[UIImage imageNamed:@"tickgrey"]];
    }

    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    BOOL isThereInArrayList = NO;
    NSMutableArray *array = (self.isSearchSelected) ?self.ingredientsSearchResultArray  : self.ingredientsArray;
    CLRecipeFilterTableViewCell *cell = (CLRecipeFilterTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
    
    for (int i = 0; i<self.ingredientsFilterArray.count; i++)
    {
        NSString *name = [[self.ingredientsFilterArray objectAtIndex:i] objectForKey:@"name"];
        if ([name isEqualToString:cell.categoryNameLabel.text])
        {
            [self.ingredientsFilterArray removeObjectAtIndex:i];
            isThereInArrayList = YES;
        }
    }
    
    if (!isThereInArrayList)
    {
        [self.ingredientsFilterArray addObject:[array objectAtIndex:indexPath.row]];
    }
    
    [self.ingredientFilterTableView reloadData];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSMutableArray *array = (self.isSearchSelected) ?self.ingredientsSearchResultArray  : self.ingredientsArray;
    NSString *ingredientStr = [[array objectAtIndex:indexPath.row]  objectForKey:@"name"];
    NSDictionary *attributes = @{NSFontAttributeName: [UIFont fontWithName:ROBOTO_REGULAR size:16]};
    NSAttributedString *attributedtext = [[NSAttributedString alloc] initWithString:ingredientStr attributes:attributes];
    
    CGRect rect = [attributedtext boundingRectWithSize:CGSizeMake(220, 10000) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading context:nil];
    CGSize size = rect.size;

    return size.height+15;
}
#pragma UIButton action Method

- (IBAction)saveButtonAction:(id)sender
{
    [self.delegate ingredientSaveButtonTapped:self.ingredientsFilterArray withIdArray:self.ingredientsFilterArray];
}    

#pragma mark UITextFieldDelegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    [textField resignFirstResponder];
    self.isSearchSelected = NO;
    [self.ingredientFilterTableView reloadData];
    return YES;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSString *resultingString = [textField.text stringByReplacingCharactersInRange: range withString: string];
    if ([resultingString isEmptyString])
    {
        self.isSearchSelected = NO;

    }
    else
    {
        self.isSearchSelected = YES;
    }
    [self loadSearchResultIngredients:resultingString];

    return YES;
}

-(void) loadSearchResultIngredients:(NSString *)searchText
{
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"CAST (name,'NSString') contains[cd] %@",searchText];
    self.ingredientsSearchResultArray = (NSMutableArray *)[self.ingredientsArray filteredArrayUsingPredicate:predicate];
    [self.ingredientFilterTableView reloadData];

}

@end
