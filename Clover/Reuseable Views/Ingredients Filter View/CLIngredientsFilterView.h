//
//  CLIngredientsFilterView.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 16/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CLIngredientsFilterViewDelegate <NSObject>

@optional

-(void)ingredientSaveButtonTapped:(NSMutableArray *)ingredientArray withIdArray :(NSMutableArray *)idArray;

@end
@interface CLIngredientsFilterView : UIView<UITableViewDelegate,UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *ingredientFilterTableView;
@property (nonatomic, weak) id<CLIngredientsFilterViewDelegate>delegate;
@property (nonatomic, strong)NSMutableArray *ingredientsFilterArray;
@property (nonatomic, strong) NSMutableArray *ingredientsArray;
@property (weak, nonatomic) IBOutlet UITextField *searchTextField;

-(void)configureViewWithArray :(NSMutableArray *)array;
@end
