//
//  CLCalenderView.h
//  Clover
//
//  Created by Ajith Kumar on 29/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol  CLCalenderViewDelegate <NSObject>

@optional

-(void)doneButtonTappedWithSelectedDate:(NSString *)dateStr andDate:(NSDate *)date;
@end

@interface CLCalenderView : UIView<UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

@property (nonatomic, strong) NSDate *minDate;
@property (nonatomic, strong) NSDate *maxDate;

@property (nonatomic, weak) id<CLCalenderViewDelegate>delegate;

-(void)configureSelectedDate:(NSDate *)selectedDate;
@end
