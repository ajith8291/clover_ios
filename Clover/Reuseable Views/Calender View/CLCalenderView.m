//
//  CLCalenderView.m
//  Clover
//
//  Created by Ajith Kumar on 29/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLCalenderView.h"

@implementation CLCalenderView

-(void)awakeFromNib
{
    self.layer.cornerRadius = 3.0;
    self.clipsToBounds = YES;
//    self.layer.borderWidth = 1.0;
//    self.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    [self getFirstDayOfTheWeekFromDate];
 
    self.datePicker.minimumDate = self.minDate;
    self.datePicker.maximumDate = self.maxDate;
}

- (IBAction)doneButtonAction:(id)sender
{
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd-MMM-yyyy"];
    NSString *selectedDate =[df stringFromDate:self.datePicker.date];
    [self.delegate doneButtonTappedWithSelectedDate:selectedDate andDate:self.datePicker.date];
}

-(void)configureSelectedDate:(NSDate *)selectedDate;
{
    self.datePicker.date = selectedDate;
}

- (void )getFirstDayOfTheWeekFromDate
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    // Edge case where beginning of week starts in the prior month
    NSDateComponents *edgeCase = [[NSDateComponents alloc] init];
    [edgeCase setMonth:2];
    [edgeCase setDay:1];
    NSDateComponents *yearComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]];
    
    [edgeCase setYear:[yearComponents year]];
    NSDate *edgeCaseDate = [calendar dateFromComponents:edgeCase];
    
    NSDateComponents *components = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSWeekCalendarUnit|NSWeekdayCalendarUnit fromDate:edgeCaseDate];
    [components setWeekday:2]; // 1 == Sunday, 7 == Saturday
    [components weekOfMonth];
    
    NSLog(@"Edge case date is %@ and beginning of that week is %@", edgeCaseDate , [calendar dateFromComponents:components]);
    
    NSCalendar *cal = [NSCalendar currentCalendar];
    NSDate *now = [NSDate date];
    NSDate *startOfTheWeek;
    NSDate *endOfWeek;
    NSTimeInterval interval;
    [cal rangeOfUnit:NSWeekCalendarUnit
           startDate:&startOfTheWeek
            interval:&interval
             forDate:now];
    //startOfWeek holds now the first day of the week, according to locale (monday vs. sunday)
    
    endOfWeek = [startOfTheWeek dateByAddingTimeInterval:interval];
    NSDate *minDate = [startOfTheWeek dateByAddingTimeInterval:60*60*24*1];
    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    [df setDateFormat:@"dd-MMM-yyyy"];
    self.minDate= minDate;
    NSDate *maxDate = [startOfTheWeek dateByAddingTimeInterval:60*60*24*56];
    self.maxDate= maxDate;

}
/*
  
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
