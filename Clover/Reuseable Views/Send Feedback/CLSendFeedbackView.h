//
//  CLSendFeedbackView.h
//  Clover
//
//  Created by Ajith Kumar on 04/03/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>


@protocol   CLSendFeedbackViewDelegate<NSObject>

@optional

-(void)uploadUserFeedbackEmail:(NSString *)emailId andMessage:(NSString *)message;
-(void)feedBackCloseButtonTapped;

@end

@interface CLSendFeedbackView : UIView<UITextFieldDelegate,UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextView *messageTextField;

@property (nonatomic, weak) id<CLSendFeedbackViewDelegate>delegate;

-(void)configureYAxisOfView;

@property (nonatomic,assign) CGFloat yAxis;
@end
