//
//  CLSendFeedbackView.m
//  Clover
//
//  Created by Ajith Kumar on 04/03/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLSendFeedbackView.h"
#import "CLConstants.h"
#import "CLUserManager.h"
#import "NSString+DataValidator.h"

@implementation CLSendFeedbackView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
    self.emailTextField.delegate = self;
    self.messageTextField.delegate = self;
    self.emailTextField.layer.cornerRadius = 3.0;
    self.emailTextField.layer.borderWidth = 1.0;
    self.emailTextField.layer.borderColor = [CL_BLUE_COLOR CGColor];
    
    self.emailTextField.leftViewMode = UITextFieldViewModeAlways;
    self.emailTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];
    
    self.messageTextField.layer.borderWidth = 1.0;
    self.messageTextField.layer.borderColor = [CL_BLUE_COLOR CGColor];
    
    CLUserManager *userManager = [CLUserManager sharedManager];
    if (![userManager.user.userEmail isEmptyString]) {
        self.emailTextField.text = userManager.user.userEmail;
    }
    self.emailTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.messageTextField.autocorrectionType = UITextAutocorrectionTypeNo;
}

- (IBAction)sendButtonAction:(id)sender
{
    if ([self.emailTextField.text isEmptyString]||[self.messageTextField.text isEmptyString]) {
        UIAlertView *alertView =[[UIAlertView alloc] initWithTitle:nil message:@"All fields are mandatory" delegate:self cancelButtonTitle:@"ok" otherButtonTitles:nil, nil];
        [alertView show];
    }
    else
    {
        [self.delegate uploadUserFeedbackEmail:self.emailTextField.text andMessage:self.messageTextField.text];
    }
}

-(void)configureYAxisOfView
{
    self.yAxis = self.frame.origin.y;
}
- (IBAction)closeButtonTapped:(id)sender
{
    [self.delegate feedBackCloseButtonTapped];
}

#pragma mark - UITextfiledDelegate methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [self.messageTextField becomeFirstResponder];
    return YES;
}

#pragma mark - UITextViewDelegate methods

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    self.frame = CGRectMake(self.frame.origin.x, self.yAxis-50, self.frame.size.width, self.frame.size.height);
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked)];
    //    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                      [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                      doneButton,
                                      nil]];
    keyboardDoneButtonView.tintColor = [UIColor blackColor];
    textView.inputAccessoryView = keyboardDoneButtonView;
    return YES;
}

-(void)doneClicked
{
    self.frame = CGRectMake(self.frame.origin.x, self.yAxis, self.frame.size.width, self.frame.size.height);
    [self sendButtonAction:self];
    [self endEditing:YES];
}

-(BOOL)textViewShouldEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    return YES;
}

@end
