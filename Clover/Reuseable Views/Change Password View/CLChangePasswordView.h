//
//  CLForgotPasswordView.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 09/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CLChangePasswordViewDelegate<NSObject>

@optional

-(void)changePasswordButtonTapped;
-(void)changePasswordCloseButtonTapped;

@end

@interface CLChangePasswordView : UIView<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UIButton *changePasswordButton;

@property (nonatomic, weak) id<CLChangePasswordViewDelegate>delegate;
@end
