//
//  CLForgotPasswordView.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 09/12/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLChangePasswordView.h"
#import "NSString+DataValidator.h"
#import "CLConstants.h"

@implementation CLChangePasswordView

-(void)awakeFromNib
{
    self.oldPasswordTextField.layer.cornerRadius = 2.0;
    self.oldPasswordTextField.layer.borderWidth = 1.0;
    self.oldPasswordTextField.layer.borderColor = [CL_BLUE_COLOR CGColor];
    self.oldPasswordTextField.leftViewMode = UITextFieldViewModeAlways;
    self.oldPasswordTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];
    self.oldPasswordTextField.delegate = self;
    
    self.passwordTextField.layer.cornerRadius = 2.0;
    self.passwordTextField.layer.borderWidth = 1.0;
    self.passwordTextField.layer.borderColor = [CL_BLUE_COLOR CGColor];
    self.passwordTextField.leftViewMode = UITextFieldViewModeAlways;
    self.passwordTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];
    self.passwordTextField.delegate = self;

    self.confirmPasswordTextField.layer.cornerRadius = 2.0;
    self.confirmPasswordTextField.layer.borderWidth = 1.0;
    self.confirmPasswordTextField.layer.borderColor = [CL_BLUE_COLOR CGColor];
    self.confirmPasswordTextField.leftViewMode = UITextFieldViewModeAlways;
    self.confirmPasswordTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];
    self.confirmPasswordTextField.delegate = self;

    self.layer.cornerRadius = 2.0;
    self.layer.borderWidth = 1.0;
    self.layer.borderColor = [[UIColor grayColor] CGColor];
    self.layer.cornerRadius = 2.0;
}

#pragma mark UIButton Action Methods

- (IBAction)closeButtonAction:(id)sender
{
    [self.delegate changePasswordCloseButtonTapped];
}

- (IBAction)changePasswordButtonAction:(id)sender
{
    if ([self.oldPasswordTextField.text isEmptyString] || [self.passwordTextField.text isEmptyString] || [self.confirmPasswordTextField.text isEmptyString])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"All fields are mandatory" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else if (![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Passwords do not match" delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        [self.delegate changePasswordButtonTapped];
    }
}


#pragma UITextFieldDelegateMethods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.confirmPasswordTextField)
    {
        self.translatesAutoresizingMaskIntoConstraints = YES;
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y-60, self.frame.size.width, self.frame.size.height);
    }
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.oldPasswordTextField) {
        [self.passwordTextField becomeFirstResponder];
    }
    else if(textField == self.passwordTextField)
    {
        [self.confirmPasswordTextField becomeFirstResponder];
    }
    else if (textField == self.confirmPasswordTextField) {
        self.translatesAutoresizingMaskIntoConstraints = YES;
        self.frame = CGRectMake(self.frame.origin.x, self.frame.origin.y+60, self.frame.size.width, self.frame.size.height);
        [self changePasswordButtonAction:self];
    }
    [textField resignFirstResponder];
    return YES;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
