//
//  CLCustomNavigationView.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 27/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import "CLCustomNavigationView.h"

@implementation CLCustomNavigationView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    self.navSearchTextFiled.frame = CGRectMake(self.navSearchTextFiled.frame.origin.x, self.navSearchTextFiled.frame.origin.y, self.navSearchButton.frame.origin.x+self.navSearchButton.frame.size.width, self.navSearchTextFiled.frame.size.height);
    self.navSearchTextFiled.text = @"";
    self.navTitleTextField.center = self.center;
    self.navTitleTextLabel.center = self.center;
    self.navNameSeperatorView.center = self.center;
    
    self.navTitleTextLabel.userInteractionEnabled = YES;
    UITapGestureRecognizer *titleTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(backAction)];
    [self.navTitleTextLabel addGestureRecognizer:titleTap];
}

#pragma mark UIButtonTap Action

- (IBAction)menuButtonActionMethod:(id)sender
{
    [self.delegate menuButtonTapped];
}

- (IBAction)shoppingCartButtonActionMethod:(id)sender
{
    [self.delegate shoppingCartButtonTapped];
}

- (IBAction)searchButtonActionMethod:(id)sender
{
    [self.delegate searchButtonTapped];
}

- (IBAction)backButtonActionMethod:(id)sender
{
    [self.delegate backButtonTapped];
}

- (IBAction)userDetailsEditButtonActionMethod:(id)sender
{
    [self.delegate userDetailsEditButtonTapped];
}

- (IBAction)calenderButtonActionMethod:(id)sender
{
    [self.delegate calenderButtonTapped];
}

-(void)backAction
{
    [self.delegate backButtonTapped];
}

@end
