//
//  CLCustomNavigationView.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 27/11/14.
//  Copyright (c) 2014 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CLCustomNavigationViewDelegate <NSObject>

@optional
-(void)menuButtonTapped;
-(void)searchButtonTapped;
-(void)shoppingCartButtonTapped;
-(void)backButtonTapped;
-(void)userDetailsEditButtonTapped;
-(void)calenderButtonTapped;
@end

@interface CLCustomNavigationView : UIView

@property (nonatomic, weak) id<CLCustomNavigationViewDelegate>delegate;

@property (weak, nonatomic) IBOutlet UIButton *navMenuButton;
@property (weak, nonatomic) IBOutlet UIButton *navCartButton;
@property (weak, nonatomic) IBOutlet UIButton *navSearchButton;
@property (weak, nonatomic) IBOutlet UIImageView *navLogoImageView;
@property (weak, nonatomic) IBOutlet UILabel *navTitleTextLabel;
@property (weak, nonatomic) IBOutlet UITextField *navTitleTextField;

@property (weak, nonatomic) IBOutlet UIButton *navBackButton;
@property (weak, nonatomic) IBOutlet UIButton *navEditButton;
@property (weak, nonatomic) IBOutlet UITextField *navSearchTextFiled;
@property (weak, nonatomic) IBOutlet UIView *navNameSeperatorView;
@property (weak, nonatomic) IBOutlet UIButton *navCalenderButton;

@end
