//
//  CLWriteReviewAndUploadPictureView.h
//  Clover_iOS
//
//  Created by Ajith Kumar on 08/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CLWriteReviewAndUploadPictureViewDelegate <NSObject>

@optional

-(void)submitReviewButtonTappedWithReview:(NSString *)review;
-(void)writeReviewCancelButtonTapped;
-(void)uploadPictureButtonTappedWith:(UIImage *)recipeImage andCaption:(NSString *)caption;

-(void)loadImageFromCameraRoll;
-(void)captureAPictureFromCamera;
@end

@interface CLWriteReviewAndUploadPictureView : UIView<UITextViewDelegate,UITextFieldDelegate,UIActionSheetDelegate>



@property (weak, nonatomic) IBOutlet UILabel *viewTitleLabel;
@property (weak, nonatomic) IBOutlet UITextView *reviewTextView;
@property (weak, nonatomic) IBOutlet UIButton *sendButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIImageView *cameraImageView;
@property (weak, nonatomic) IBOutlet UITextField *captionTextField;
@property (weak, nonatomic) IBOutlet UIView *seperator;
@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (nonatomic, weak) id<CLWriteReviewAndUploadPictureViewDelegate>delegate;

@end
