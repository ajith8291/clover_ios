//
//  CLWriteReviewAndUploadPictureView.m
//  Clover_iOS
//
//  Created by Ajith Kumar on 08/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLWriteReviewAndUploadPictureView.h"
#import "CLConstants.h"

@implementation CLWriteReviewAndUploadPictureView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    self.layer.cornerRadius = 5.0;
    self.clipsToBounds = YES;
    self.reviewTextView.layer.borderWidth = 1.0;
    self.reviewTextView.layer.borderColor = [[UIColor colorWithRed:212.0/255.0 green:212.0/255.0 blue:212.0/255.0 alpha:1.0] CGColor];
    
    self.reviewTextView.delegate = self;
    self.reviewTextView.autocorrectionType = UITextAutocorrectionTypeNo;
    self.captionTextField.delegate = self;
    self.captionTextField.leftViewMode = UITextFieldViewModeAlways;
    self.captionTextField.leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 5, 0)];
    self.captionTextField.layer.borderWidth = 1.0;
    self.captionTextField.autocorrectionType = UITextAutocorrectionTypeNo;
    self.captionTextField.layer.borderColor = [[UIColor colorWithRed:212.0/255.0 green:212.0/255.0 blue:212.0/255.0 alpha:1.0] CGColor];
    self.cameraImageView.userInteractionEnabled = YES;
    UITapGestureRecognizer *cameraTap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(cameraImageViewTapped)];
    [self.cameraImageView addGestureRecognizer:cameraTap];
    
//    self.sendButton.titleLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:15.0];
//    self.cancelButton.titleLabel.font = [UIFont fontWithName:ROBOTO_REGULAR size:15.0];
//    int width = self.frame.size.width/2;
//    
//    self.sendButton.translatesAutoresizingMaskIntoConstraints = YES;
//    self.cancelButton.translatesAutoresizingMaskIntoConstraints = YES;
//    
//    self.sendButton.frame = CGRectMake(0, 0, width, 30);
//    self.cancelButton.frame = CGRectMake(width, 0, width, 30);
//    self.seperator.center= self.bottomView.center;
}

#pragma mark UIButton/Tap Actions

- (IBAction)submitButtonTapped:(id)sender
{
    [self endEditing:YES];
    if ([self.reviewTextView.text isEqualToString:@"Write your Review..."])
    {
        self.reviewTextView.text = @"";
    }
    [self.delegate uploadPictureButtonTappedWith:self.cameraImageView.image andCaption:self.reviewTextView.text];
}

- (IBAction)cancelButtonTapped:(id)sender
{
    [self endEditing:YES];
    [self.delegate writeReviewCancelButtonTapped];
}

-(void)cameraImageViewTapped
{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Select Image" delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:
                                  @"Camera",
                                  @"Gallery",
                                  nil];
    [actionSheet showInView:self];

}

#pragma mark UIACtionSheetDelegate Marthods

- (void)actionSheet:(UIActionSheet *)popup clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    switch (buttonIndex) {
        case 0:
            [self loadImageFromCamera];
            break;
        case 1:
            [self loadImageFromGallery];
            break;
        default:
            break;
    }
}

-(void)loadImageFromCamera
{
    [self.delegate captureAPictureFromCamera];
}

-(void)loadImageFromGallery
{
    [self.delegate loadImageFromCameraRoll];
    
}


#pragma mark UITextViewDelegate

-(BOOL)textViewShouldBeginEditing:(UITextView *)textView
{
    if ([textView.text isEqualToString:@"Write your Review..."])
    {
        self.reviewTextView.text = @"";
    }
    
    textView.textColor = [UIColor blackColor];
    textView.font = [UIFont fontWithName:ROBOTO_REGULAR size:14.0];
    
    UIToolbar* keyboardDoneButtonView = [[UIToolbar alloc] init];
    [keyboardDoneButtonView sizeToFit];
    UIBarButtonItem* doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                   style:UIBarButtonItemStyleBordered target:self
                                                                  action:@selector(doneClicked)];
    //    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:doneButton, nil]];
    
    [keyboardDoneButtonView setItems:[NSArray arrayWithObjects:[[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                      [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil],
                                      doneButton,
                                      nil]];
    keyboardDoneButtonView.tintColor = [UIColor blackColor];
    textView.inputAccessoryView = keyboardDoneButtonView;
    self.frame = CGRectMake(self.center.x-self.frame.size.width/2, self.center.y-self.frame.size.height+30, self.frame.size.width, self.frame.size.height);
    return YES;
}

-(void)doneClicked
{
    self.frame = CGRectMake(self.center.x-self.frame.size.width/2, self.center.y-30, self.frame.size.width, self.frame.size.height);
    [self endEditing:YES];
}

-(BOOL) textViewShouldEndEditing:(UITextView *)textView
{
    [textView resignFirstResponder];
    return YES;
}

#pragma mark UITextFieldDelegate Methods

-(BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    textField.textColor = [UIColor blackColor];
    textField.font = [UIFont fontWithName:ROBOTO_REGULAR size:14.0];
    textField.placeholder = @"";
    return YES;
}

-(BOOL)textFieldShouldEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

@end
