//
//  CLMealPlanerGridView.m
//  Clover
//
//  Created by Ajith Kumar on 16/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import "CLMealPlanerGridView.h"
#import <UIImageView+AFNetworking.h>
#import "NSString+DataValidator.h"
#import "UIImage+colors.h"

@implementation CLMealPlanerGridView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)awakeFromNib
{
    [super awakeFromNib];
}

-(void)configureView:(NSString *)nameString andImageUrl:(NSString *)imageURL
{
    self.recipeNameLabel.text = nameString;
    if ([imageURL isEmptyString])
    {
        [self.recipeImageView setImage:[UIImage imageNamed:@"no-image"]];
    }
    else
    {
        
        NSString *imgUrl = [imageURL stringByReplacingOccurrencesOfString:@"[w]" withString:[NSString stringWithFormat:@"%d",200]];
        imgUrl = [imgUrl stringByReplacingOccurrencesOfString:@"[h]" withString:[NSString stringWithFormat:@"%d",200]];
        
        UIImage *placeholderImg = [UIImage imageWithColor:[UIColor lightGrayColor]];
        
        NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:imgUrl]];
        
        __weak UIImageView *weakImg = self.recipeImageView;
        
        [self.recipeImageView setImageWithURLRequest:request placeholderImage:placeholderImg success:^(NSURLRequest *request, NSHTTPURLResponse *response, UIImage *image)
         {
             
             dispatch_async(dispatch_get_main_queue(), ^{
                 weakImg.image = image;
                 weakImg.alpha = 0.0;
                 
                 [UIView animateWithDuration:1.0 animations:^{
                     weakImg.alpha = 1.0;
                 }];
             });
             
         } failure:^(NSURLRequest *request, NSHTTPURLResponse *response, NSError *error) {
             
             
         }];
    }

}

- (IBAction)deleteButtonAction:(id)sender
{
    UIAlertView *deleteAlert = [[UIAlertView alloc] initWithTitle:nil message:@"Would you like to remove the recipe" delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    [deleteAlert show];
}

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    switch (buttonIndex) {
        case 0:
            
            break;
            
        case 1:
            [self.delegate deleteRecipeFromMealPlanWith:self.mealPlanId];
            break;

        default:
            break;
    }
    self.mealPlanDeleteButton.hidden = YES;
    [self.layer removeAnimationForKey:@"iconShake"];

}

@end
