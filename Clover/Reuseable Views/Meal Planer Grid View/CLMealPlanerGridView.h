//
//  CLMealPlanerGridView.h
//  Clover
//
//  Created by Ajith Kumar on 16/01/15.
//  Copyright (c) 2015 Inkoniq. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol CLMealPlanerGridViewDelegate <NSObject>

@optional

-(void)deleteRecipeFromMealPlanWith:(NSString *)mealPlanId;

@end

@interface CLMealPlanerGridView : UIView<UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *recipeImageView;
@property (weak, nonatomic) IBOutlet UILabel *recipeNameLabel;
@property (weak, nonatomic) IBOutlet UIButton *mealPlanDeleteButton;

-(void)configureView:(NSString *)nameString andImageUrl:(NSString *)imageURL;

@property(nonatomic, strong) NSString *mealPlanId;
@property(nonatomic, strong) NSString *recipeId;

@property (nonatomic, weak) id<CLMealPlanerGridViewDelegate>delegate;
@end
